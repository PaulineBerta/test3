/****************************************************************************
** Meta object code from reading C++ file 'ChartQuick2TR.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.11)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../atomic/client/AudioPlot/ChartQuick2TR.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ChartQuick2TR.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.11. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ChartQuick2TR_t {
    QByteArrayData data[29];
    char stringdata0[319];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ChartQuick2TR_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ChartQuick2TR_t qt_meta_stringdata_ChartQuick2TR = {
    {
QT_MOC_LITERAL(0, 0, 13), // "ChartQuick2TR"
QT_MOC_LITERAL(1, 14, 17), // "chartWidthChanged"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 11), // "timeChanged"
QT_MOC_LITERAL(4, 45, 16), // "lAeqValueChanged"
QT_MOC_LITERAL(5, 62, 16), // "lCeqValueChanged"
QT_MOC_LITERAL(6, 79, 18), // "lCpeakValueChanged"
QT_MOC_LITERAL(7, 98, 8), // "passData"
QT_MOC_LITERAL(8, 107, 15), // "QVector<double>"
QT_MOC_LITERAL(9, 123, 10), // "chartWidth"
QT_MOC_LITERAL(10, 134, 13), // "setChartWidth"
QT_MOC_LITERAL(11, 148, 5), // "width"
QT_MOC_LITERAL(12, 154, 9), // "setMarker"
QT_MOC_LITERAL(13, 164, 10), // "Initialize"
QT_MOC_LITERAL(14, 175, 4), // "time"
QT_MOC_LITERAL(15, 180, 7), // "setTime"
QT_MOC_LITERAL(16, 188, 5), // "cTime"
QT_MOC_LITERAL(17, 194, 9), // "lAeqValue"
QT_MOC_LITERAL(18, 204, 12), // "setLAeqValue"
QT_MOC_LITERAL(19, 217, 5), // "cLAeq"
QT_MOC_LITERAL(20, 223, 9), // "lCeqValue"
QT_MOC_LITERAL(21, 233, 12), // "setLCeqValue"
QT_MOC_LITERAL(22, 246, 11), // "lCpeakValue"
QT_MOC_LITERAL(23, 258, 14), // "setLCpeakValue"
QT_MOC_LITERAL(24, 273, 7), // "cLCpeak"
QT_MOC_LITERAL(25, 281, 14), // "updatePlotSize"
QT_MOC_LITERAL(26, 296, 11), // "initQwtPlot"
QT_MOC_LITERAL(27, 308, 4), // "step"
QT_MOC_LITERAL(28, 313, 5) // "iStep"

    },
    "ChartQuick2TR\0chartWidthChanged\0\0"
    "timeChanged\0lAeqValueChanged\0"
    "lCeqValueChanged\0lCpeakValueChanged\0"
    "passData\0QVector<double>\0chartWidth\0"
    "setChartWidth\0width\0setMarker\0Initialize\0"
    "time\0setTime\0cTime\0lAeqValue\0setLAeqValue\0"
    "cLAeq\0lCeqValue\0setLCeqValue\0lCpeakValue\0"
    "setLCpeakValue\0cLCpeak\0updatePlotSize\0"
    "initQwtPlot\0step\0iStep"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ChartQuick2TR[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       5,  154, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  119,    2, 0x06 /* Public */,
       3,    0,  120,    2, 0x06 /* Public */,
       4,    0,  121,    2, 0x06 /* Public */,
       5,    0,  122,    2, 0x06 /* Public */,
       6,    0,  123,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    1,  124,    2, 0x0a /* Public */,
       9,    0,  127,    2, 0x0a /* Public */,
      10,    1,  128,    2, 0x0a /* Public */,
      12,    0,  131,    2, 0x0a /* Public */,
      13,    0,  132,    2, 0x0a /* Public */,
      14,    0,  133,    2, 0x0a /* Public */,
      15,    1,  134,    2, 0x0a /* Public */,
      17,    0,  137,    2, 0x0a /* Public */,
      18,    1,  138,    2, 0x0a /* Public */,
      20,    0,  141,    2, 0x0a /* Public */,
      21,    1,  142,    2, 0x0a /* Public */,
      22,    0,  145,    2, 0x0a /* Public */,
      23,    1,  146,    2, 0x0a /* Public */,
      25,    0,  149,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      26,    0,  150,    2, 0x02 /* Public */,
      27,    1,  151,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 8,    2,
    QMetaType::Int,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   16,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   19,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   19,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString,   24,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   28,

 // properties: name, type, flags
       9, QMetaType::Int, 0x00495103,
      14, QMetaType::QString, 0x00495103,
      17, QMetaType::QString, 0x00495103,
      20, QMetaType::QString, 0x00495103,
      22, QMetaType::QString, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,

       0        // eod
};

void ChartQuick2TR::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ChartQuick2TR *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->chartWidthChanged(); break;
        case 1: _t->timeChanged(); break;
        case 2: _t->lAeqValueChanged(); break;
        case 3: _t->lCeqValueChanged(); break;
        case 4: _t->lCpeakValueChanged(); break;
        case 5: _t->passData((*reinterpret_cast< QVector<double>(*)>(_a[1]))); break;
        case 6: { int _r = _t->chartWidth();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 7: _t->setChartWidth((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 8: _t->setMarker(); break;
        case 9: _t->Initialize(); break;
        case 10: { QString _r = _t->time();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 11: _t->setTime((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: { QString _r = _t->lAeqValue();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 13: _t->setLAeqValue((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 14: { QString _r = _t->lCeqValue();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 15: _t->setLCeqValue((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 16: { QString _r = _t->lCpeakValue();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 17: _t->setLCpeakValue((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 18: _t->updatePlotSize(); break;
        case 19: _t->initQwtPlot(); break;
        case 20: _t->step((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ChartQuick2TR::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ChartQuick2TR::chartWidthChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ChartQuick2TR::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ChartQuick2TR::timeChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (ChartQuick2TR::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ChartQuick2TR::lAeqValueChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (ChartQuick2TR::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ChartQuick2TR::lCeqValueChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (ChartQuick2TR::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ChartQuick2TR::lCpeakValueChanged)) {
                *result = 4;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<ChartQuick2TR *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->chartWidth(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->time(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->lAeqValue(); break;
        case 3: *reinterpret_cast< QString*>(_v) = _t->lCeqValue(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->lCpeakValue(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<ChartQuick2TR *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setChartWidth(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setTime(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setLAeqValue(*reinterpret_cast< QString*>(_v)); break;
        case 3: _t->setLCeqValue(*reinterpret_cast< QString*>(_v)); break;
        case 4: _t->setLCpeakValue(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject ChartQuick2TR::staticMetaObject = { {
    &QQuickPaintedItem::staticMetaObject,
    qt_meta_stringdata_ChartQuick2TR.data,
    qt_meta_data_ChartQuick2TR,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ChartQuick2TR::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ChartQuick2TR::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ChartQuick2TR.stringdata0))
        return static_cast<void*>(this);
    return QQuickPaintedItem::qt_metacast(_clname);
}

int ChartQuick2TR::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickPaintedItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void ChartQuick2TR::chartWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void ChartQuick2TR::timeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void ChartQuick2TR::lAeqValueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void ChartQuick2TR::lCeqValueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void ChartQuick2TR::lCpeakValueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
