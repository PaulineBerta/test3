package com.AllianTech.ATomicSound;
import org.qtproject.qt5.android.bindings.QtApplication;
import org.qtproject.qt5.android.bindings.QtActivity;
import android.accessibilityservice.AccessibilityService;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import java.util.Collection;
import java.util.HashMap;

public class ATomicUsbManager {
        private static final String ACTION_USB_PERMISSION = "com.AllianTech.ATomicSound.USB_PERMISSION";
        private PendingIntent permissionIntent;
        private static Context context;

        public ATomicUsbManager(Context c){

            context=c;
            permissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
            BroadcastReceiver usbReceiver = new BroadcastReceiver() {
                @Override

                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (ACTION_USB_PERMISSION.equals(action)) {
                        synchronized (this) {
                            UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                            if (device != null) {
                                UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
                                if (manager != null) {
                                    if (manager.hasPermission(device)) addUSBDevice(device); // Open if we have permission.
                                    else manager.requestPermission(device, permissionIntent); // Request permission otherwise.
                                }
                            }
                        }
                    }
                    else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) { // USB device attached.
                        UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                        if (device != null) {
                            UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
                            if (manager != null) {
                                if (manager.hasPermission(device)) addUSBDevice(device);
                                else manager.requestPermission(device, permissionIntent); // Request permission otherwise.
                            }
                        }
                    }
                }
            };
            IntentFilter filter = new IntentFilter();
            filter.addAction(ACTION_USB_PERMISSION);
            filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
            context.registerReceiver(usbReceiver, filter);

        }
        private static void addUSBDevice(UsbDevice device) {
            int fd;
            UsbManager manager = (UsbManager)context.getSystemService(Context.USB_SERVICE);
            if (manager == null) return;
            UsbDeviceConnection connection = manager.openDevice(device);
        }

 String getATomicProductName() {
     StringBuilder usblist = new StringBuilder("");
     UsbManager manager = (UsbManager)context.getSystemService(Context.USB_SERVICE);
     HashMap<String, UsbDevice> devices = manager.getDeviceList();
     Collection<UsbDevice> ite = devices.values();
     UsbDevice[] usbs = ite.toArray(new UsbDevice[]{});
     for (UsbDevice usb : usbs){
         usblist.append(usb.getProductName() );
     }
     return usblist.toString();
  }
 String getATomicSerialNumber() {
    StringBuilder usblist = new StringBuilder("");
    UsbManager manager = (UsbManager)context.getSystemService(Context.USB_SERVICE);
    HashMap<String, UsbDevice> devices = manager.getDeviceList();
    Collection<UsbDevice> ite = devices.values();
    UsbDevice[] usbs = ite.toArray(new UsbDevice[]{});
    for (UsbDevice usb : usbs){
        usblist.append(usb.getSerialNumber());

    }
    return usblist.toString();
 }
}
