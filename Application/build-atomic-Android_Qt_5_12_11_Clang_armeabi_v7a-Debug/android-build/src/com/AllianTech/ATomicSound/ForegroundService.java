/**
*******************************************************************************
** A     ForegroundService class:
**           -	creates a foreground service
**           -	display notifications
** @author  Wevioo SE Team
**
*******************************************************************************
**/
package com.AllianTech.ATomicSound;
import org.qtproject.qt5.android.bindings.QtApplication;
import org.qtproject.qt5.android.bindings.QtActivity;
import android.R.mipmap;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.support.v4.app.NotificationCompat;
import android.support.annotation.Nullable;
import org.qtproject.qt5.android.bindings.QtService;
import android.os.Environment;
//import java.io.IOException;
import android.os.Build;
import android.os.PowerManager;

// to get low latency and audio pro flags
import android.content.pm.PackageManager;

import android.widget.Toast; //kuba
import android.util.Log;

public class ForegroundService extends QtService {
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    protected PowerManager.WakeLock mWakeLock;
    private static Context m_Context;
    public static final String TAG = "QtJavaDebug";

    /**
    * constructor of the ForegroundService class
    * @param void
    * @return
    */
    public ForegroundService() {
    }
    /**
    * require WakeLock on creation
    * @param void
    * @return void
    */
    @Override
    public void onCreate() {
        super.onCreate();
        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        this.mWakeLock.acquire();

        String sToastText = "LOW LANTECY : ";
        boolean hasLowLatencyFeature = getPackageManager().hasSystemFeature(PackageManager.FEATURE_AUDIO_LOW_LATENCY);
        if(hasLowLatencyFeature){
            sToastText += "YES ";
        }else{
            sToastText += "NO ";
        }

        sToastText += ", AUDIO PRO : ";
        boolean hasProFeature = getPackageManager().hasSystemFeature(PackageManager.FEATURE_AUDIO_PRO);
        if(hasProFeature){
            sToastText += "YES ";
        }else{
            sToastText += "NO ";
        }

        Log.d(TAG, ">>> " + sToastText);

        Toast.makeText(this, sToastText ,Toast.LENGTH_SHORT).show(); //kuba

    }

    /**
    * allow the service to run in the background and display the notification
    * @param intent Intent
    * @param flags integer
    * @param startId integer
    * @return integer: START_NOT_STICKY
    */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String input = intent.getStringExtra("inputExtra");
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, ForegroundService.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("ATomic Sound")
                .setContentText(input)
                .setSmallIcon(com.AllianTech.ATomicSound.R.drawable.icon_foreground)
                .setContentIntent(pendingIntent)
                .build();

        //Log.i(TAG, " Before Service");
        startForeground(1, notification);

        return START_NOT_STICKY;
    }


    /**
    * detect when the application is removed to stop service safely
    * @param rootIntent Intent
    * @return void
    */
    @Override
    public void onTaskRemoved(Intent rootIntent) {
          super.onTaskRemoved(rootIntent);
    }

    /**
    * detect when the application is destroyed
    * @param void
    * @return void
    */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
    * disable binding components to the service
    * @param intent Intent
    * @return IBinder : null : do not allow binding
    */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
    * creates the foreground notification channel
    * @param void
    * @return void
    */
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_MAX
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    /**
    * start the foreground service
    * @param context Context
    * @return void
    */
    public static void startMyService(Context context) {
        m_Context= context;
        Intent serviceIntent = new Intent(context, ForegroundService.class);
        serviceIntent.putExtra("inputExtra", "ATomic Sound is running");

        context.startForegroundService(serviceIntent);
    }
    /**
    * stop the foreground service
    * @param context Context
    * @return void
    */
    public static void stopMyService(Context context) {
        Intent serviceIntent = new Intent(context, ForegroundService.class);
        context.stopService(serviceIntent);
    }

}
