/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.AllianTech.ATomicSound;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.AllianTech.ATomicSound";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 0;
  public static final String VERSION_NAME = "v1.0.0.0 [DEMO INTERNE 0.0]";
}
