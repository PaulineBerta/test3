/**
*******************************************************************************
** @file    LogFile.h
** @version
** @author  Wevioo SE Team
** @brief   creates and updates all required files depending on the application
**          state
**
*******************************************************************************
**/

#ifndef LOGFILE_H
#define LOGFILE_H

#include <QtCore/QObject>
#include <QByteArray>
#include <QFile>
#include <QDateTime>
#include "android/libs/AcousticMeasSo.h"
#include "android/libs/AcousticMeasTypes.h"


/**
*******************************************************************************
** @enum  TeMeasFileStatus
** @brief represents the status of the csv files: opened or closed
**
*******************************************************************************
**/
typedef  enum {
  CSV_OPEN,                     /*!< a .csv file is opened */
  CSV_CLOSE                     /*!< a .csv file is closed */
}TeMeasFileStatus;

/**
*******************************************************************************
** @enum  TeLogList
** @brief different events can be written to the LogFile.xml
**
*******************************************************************************
**/
typedef enum{
  /* EVENTS */
  //	CSV – Data/Log
  EVENT_OPEN_LOG_FILE=0,
  EVENT_CLOSE_LOG_FILE,

  EVENT_START_CSV_RECORDING,
  EVENT_STOP_CSV_RECORDING,
  // WAV
  EVENT_START_WAV_RECORDING,
  EVENT_STOP_WAV_RECORDING,
  // ACQUISITION
  EVENT_START_ACQUISITION,
  EVENT_STOP_ACQUISITION,
  // CALIBRATION
  EVENT_START_CALIBRATION,
  EVENT_STOP_CALIBRATION,
  /* ERRORS */
  // Erreurs ATomic :
  ERROR_ATOMIC_NOT_FOUND,
  ERROR_BLOCK_LOST,
  // Erreurs AcousticMeas
  ERROR_CONFIGURATION,
  // Erreurs liées aux fichiers utilisés / générés :
  ERROR_INSUFFICIENT_STORAGE_SPACE,
  ERROR_OPENING_WAV,
  ERROR_WRITING_WAV,
  ERROR_CLOSING_WAV,
  //	CSV – Data/Log
  ERROR_OPENING_CSV,
  ERROR_WRITING_CSV,
  ERROR_CLOSING_CSV,
  //	XML
  ERROR_OPENING_XML,
  ERROR_WRITING_XML,
  ERROR_READING_XML,
  ERROR_CLOSING_XML,
  //   CALIBRATION
  ERROR_CALIBRATION
}TeLogList;

/**
*******************************************************************************
** @struct  TrAtomicLogDesc
** @brief holds all the events could be faced during use of the
**          ATomic Sound application
**
*******************************************************************************
**/
typedef struct{
  TeLogList eLogList;                   /*!< event to log */
  QString cType;                        /*!< event type: EVENT or ERROR */
  QString cMessage;                     /*!< event message to log */
}TrAtomicLog;

static const TrAtomicLog arAtmicLog[] = {
  /* EVENTS */
  //	CSV – Data/Log
  {EVENT_OPEN_LOG_FILE, "EVENT", "Open LogFile"},
  {EVENT_CLOSE_LOG_FILE,"EVENT","Close LogFile"},

  {EVENT_START_CSV_RECORDING, "EVENT", "Start CSV Recording"},
  {EVENT_STOP_CSV_RECORDING,"EVENT","Stop CSV Recording"},

  {EVENT_START_WAV_RECORDING, "EVENT", "Start WAV Recording"},
  {EVENT_STOP_WAV_RECORDING,"EVENT","Stop WAV Recording"},

  {EVENT_START_ACQUISITION,"EVENT","Start Acquisition"},
  {EVENT_STOP_ACQUISITION,"EVENT","Stop Acquisition"},

  {EVENT_START_CALIBRATION, "EVENT", "Start Calibration"},
  {EVENT_STOP_CALIBRATION, "EVENT", "Stop Calibration"},

  /* ERRORS */
  // Error ATomic :
  {ERROR_ATOMIC_NOT_FOUND, "ERROR", "ATomic not found"},
  {ERROR_BLOCK_LOST, "ERROR", "Block Lost"},

  // Error AcousticMeas
  {ERROR_CONFIGURATION, "ERROR", "Error Configuring AcousticMeas"},

  // errors related to the used / generated files :
  // WAV
  {ERROR_INSUFFICIENT_STORAGE_SPACE, "ERROR", "Insufficient storage space"},
  {ERROR_OPENING_WAV, "ERROR", "Error Opening WAV"},
  {ERROR_WRITING_WAV, "ERROR", "Error Writing WAV"},
  {ERROR_CLOSING_WAV, "ERROR", "Error Closing WAV"},

  //	CSV – Data/Log
  {ERROR_OPENING_CSV, "ERROR", "Error Opening CSV"},
  {ERROR_WRITING_CSV, "ERROR", "Error Writing CSV"},
  {ERROR_CLOSING_CSV, "ERROR", "Error Closing CSV"},
  //	XML
  {ERROR_OPENING_XML, "ERROR", "Error Opening XML"},
  {ERROR_WRITING_XML, "ERROR", "Error Writing XML "},
  {ERROR_READING_XML, "ERROR", "Error Reading XML"},
  {ERROR_CLOSING_XML, "ERROR", "Error Closing XML"},
  //   CALIBRATION
  {ERROR_CALIBRATION, "ERROR", "Error Calibration"}
};
/**
*******************************************************************************
** @class  LogFile
** @brief definition of the logfile class: that enables creating and writing
**         to csv files
**
*******************************************************************************
**/
class LogFile : public QObject
{
  Q_OBJECT

public:
  /**
  *******************************************************************************
  ** @fn  LogFile(QObject *parent) : QObject(parent)
  ** @param[in] QObject *parent
  ** @param[out]
  ** @brief Constructor of the LogFile
  ** @return
  **
  *******************************************************************************
  **/
  explicit LogFile(QObject *parent = nullptr);  /*!< Constructor of the LogFile class */

  /**
  *******************************************************************************
  ** @fn  void OpenCsv()
  ** @param[in] void
  ** @param[out] bool
  ** @brief Opens the CSV file
  ** @return void
  **
  *******************************************************************************
  **/
  void OpenCsv(TrAcousticMeasConfig, QString, int, QString, QString, QString);

  /**
  *******************************************************************************
  ** @fn  void CloseCsv()
  ** @param[in] void
  ** @param[out] void
  ** @brief Closes all Data CSV files
  ** @return void
  **
  *******************************************************************************
  **/
  void CloseCsv();

  /**
  *******************************************************************************
  ** @fn  void DeleteRTCsv()
  ** @param[in] void
  ** @param[out] void
  ** @brief Closes all Data CSV files
  ** @return void
  **
  *******************************************************************************
  **/
  void DeleteRTCsv();

  /**
  *******************************************************************************
  ** @fn  bool OpenLog()
  ** @param[in] void
  ** @param[out] bool
  ** @brief Opens the Log file
  ** @return bool: returns 0 if the file is successfully opened else 1
  **
  *******************************************************************************
  **/
  bool OpenLog();

  /**
  *******************************************************************************
  ** @fn  int64_t WriteCsvHeader(TrAcousticMeasConfig )
  ** @param[in] TrAcousticMeasConfig current configuration
  ** @param[out] int the size of the header, if error returns -1
  ** @brief Writes the csv-file Header
  ** @return int: size of the header if success else returns -1
  **
  *******************************************************************************
  **/
  long WriteCsvHeader(TrAcousticMeasConfig);

  /**
  *******************************************************************************
  ** @fn  int64_t WriteItCsv(TrAcousticResBasis rAcousticRes)
  ** @param[in] TrAcousticResBasis rAcousticRes
  ** @param[out] int64_t
  ** @brief Writes the csv-file data every IT (integration time)
  ** @return int: size of the file if success else returns -1
  **
  *******************************************************************************
  **/
  long WriteItCsv(TrAcousticResBasis);

  /**
  *******************************************************************************
  ** @fn  int64_t WriteItCsv(TrReverberationRes rReverberationRes, int reverbCounter)
  ** @param[in]
  ** @param[out]
  ** @brief
  ** @return
  **
  *******************************************************************************
  **/
  long WriteRTCsv(TrAcousticMeasConfig, TrReverberationRes, int);

  /**
  *******************************************************************************
  ** @fn  int64_t WriteFitCsv(TrAcousticResBasis rAcousticRes, int sonoState)
  ** @param[in] TrAcousticResBasis rAcousticRes
  ** @param[out] int64_t
  ** @brief Writes the csv-file data every FIT, currently is set to 200 ms
  ** @return int: size of the file if success else returns -1
  **
  *******************************************************************************
  **/
  long WriteFitCsv(TrAcousticResBasis, int);

  /**
  *******************************************************************************
  ** @fn  int64_t WriteLogHeader()
  ** @param[in] void
  ** @param[out] void
  ** @brief initializes the header of the LogFile.csv file
  ** @return
  **
  *******************************************************************************
  **/
  long WriteLogHeader();

  /**
  *******************************************************************************
  ** @fn  int64_t WriteLogFile(TeLogList eLogList)
  ** @param[in] TeLogList eLogList
  ** @param[out] int64_t
  ** @brief updates the LogFile.csv file
  ** @return the size of the LogFile.csv in case of error returns -1
  **
  *******************************************************************************
  **/
  long WriteLogFile(TeLogList );

  /**
  *******************************************************************************
  ** @fn  void LogFile::CloseLog()
  ** @param[in] void
  ** @param[out] void
  ** @brief updates then closes the LogFile.csv file
  ** @return void
  **
  ****************************************************************************close***
  **/
  void CloseLog();

signals:

public slots:

public:
  /*QString m_cFreqTab3rdOctave[27] = {"50Hz","63Hz","80Hz","100Hz","125Hz","160Hz","200Hz","250Hz","315Hz","400Hz","500Hz","630Hz",
                                    "800Hz","1000Hz","1250Hz","1600Hz","2000Hz","2500Hz","3150Hz","4000Hz","5000Hz","6300Hz","8000Hz",
                                    "10000Hz","12500Hz","16000Hz","20000Hz"};
  QString m_cFreqTabOctave[9] = {"63Hz","125Hz","250Hz","500Hz","1000Hz","2000Hz","4000Hz","8000Hz","16000Hz"};*/

  QString m_cFreqTab3rdOctave[36] = {"6.3Hz","8Hz","10Hz","12.5Hz","16Hz","20Hz","25Hz","31.5Hz","40Hz","50Hz","63Hz",
                                    "80Hz","100Hz","125Hz","160Hz","200Hz","250Hz","315Hz","400Hz","500Hz","630Hz",
                                    "800Hz","1000Hz","1250Hz","1600Hz","2000Hz","2500Hz","3150Hz","4000Hz","5000Hz",
                                    "6300Hz","8000Hz","10000Hz","12500Hz","16000Hz","20000Hz"};
  QString m_cFreqTabOctave[12] = {"8Hz","16Hz","31.5Hz","63Hz","125Hz","250Hz","500Hz","1000Hz","2000Hz","4000Hz",
                                 "8000Hz","16000Hz"};

  QString *m_cFreqTab;
  int m_iFreqIndex;

  QFile m_LogFile;                          /*!< current logFile.csv file currently in use */
  QFile m_CsvItFile;                        /*!< current *_IT.csv file currently in use */
  QFile m_CsvFitFile;                       /*!< current *_FIT.csv file currently in use */

  QFile m_CsvRTFile;                        /*!<  */
  QFile m_CsvFitRTFile;                     /*!<  */

  QString m_cProjectName;
  QString m_cRoomType;
  QString m_cRoomNumber;

  int m_iIndexFitRecord;
  int m_iFreqCountOffset;
  QString m_cSpectrum;
  QString m_cCalculMode;

  TeMeasFileStatus m_eMeasFileStatus;       /*!< holds the states of files: opened or closed */
  TeSpectrum eSpectrum;                     /*!< holds the Spectrum Type set by user in GUI */
  QString m_cCodified;                      /*!< holds the codified field"s value */

  int counterTimestamp = 0;
};

#endif // LOGFILE_H
