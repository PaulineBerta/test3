/**
*******************************************************************************
** @file    LogFile.cpp
** @version
** @author  Wevioo SE Team
** @brief   creates and updates all required files (.csv files) depending
**          on the application state
**
*******************************************************************************
**/
#include "LogFile.h"
#include <QtAndroid>
#include <QtAndroidExtras/QAndroidJniEnvironment>
#include <QDir>
#include <QDebug>

// ****************************************************************************
// CONSTANTS
// ****************************************************************************

#define HEADER_TOP_I_OCTAVE "IT;"
#define HEADER_TOP_I_3RD_OCTAVE "IT;"

#define HEADER_TOP_FIT_OCTAVE "FIT;"
#define HEADER_TOP_FIT_3RD_OCTAVE "FIT;"

#define HEADER_TOP_G_OCTAVE "G;"
#define HEADER_TOP_G_3RD_OCTAVE "G;"

#define HEADER_TIMESTAMP  "TimeStamp;"
#define HEADER_CALCULMODE  "CalculMode;"
#define HEADER_SPECTRUM  "Spectrum;"
#define HEADER_ITTIME  "ItTime;"
#define HEADER_TIMEWEIGH  "TimeWeighting;"

///HEADERS SONOMETER / REVERBERATION TIME
#define HEADER_BOTTOM_SLOW "LAS;LCS;LZS;LASmax;LCSmax;LZSmax;LCpeak;LZpeak;"
#define HEADER_BOTTOM_FAST "LAF;LCF;LZF;LAFmax;LCFmax;LZFmax;LCpeak;LZpeak;"
#define HEADER_BOTTOM_IMPULSE "LAI;LCI;LZI;LAImax;LCImax;LZImax;LCpeak;LZpeak;"
#define HEADER_BOTTOM_NONE "LAx;LCx;LZx;LAxmax;LCxmax;LZxmax;LCpeak;LZpeak;"

#define HEADER_BOTTOM_LEQ_FIT "LAeq;LCeq;LZeq;"
#define HEADER_BOTTOM_OCTAVE "8Hz;16Hz;31,5Hz;63Hz;125Hz;250Hz;500Hz;1000Hz;2000Hz;4000Hz;8000Hz;16000Hz;" // Also used for Reverberation Time CSV
#define HEADER_BOTTOM_3RD_OCTAVE  "6,3Hz;8Hz;10Hz;12,5Hz;16Hz;20Hz;25Hz;31,5Hz;40Hz;50Hz;63Hz;80Hz;100Hz;125Hz;160Hz;200Hz;250Hz;315Hz;400Hz;500Hz;630Hz;800Hz;1000Hz;1250Hz;1600Hz;2000Hz;2500Hz;3150Hz;4000Hz;5000Hz;6300Hz;8000Hz;10000Hz;12500Hz;16000Hz;20000Hz;" // Also used for Reverberation Time CSV
#define HEADER_BOTTOM_ADDITIONAL "Overload; Codified; UserComment; Screenshot"

#define HEADER_TOP_TR "Frequencies; AvgT20(s); AvgT30(s); AvgT60(s); AvgT(s); T20(s); StatusT20; T30(s); StatusT30; T60(s); StatusT60; Ki20(%0); Ki30(%0); Ki60(%0); Curvature(%); Leq Noise(dB); Leq Source(dB); Hex Status; LevelStartFitTr20; LevelStartFitTr30; LevelStartFitTr60; SlopeTr20; SlopeTr30; SlopeTr60; StartFitTr; EndFitTr20; EndFitTr30; EndFitTr60; TXX(s); StatusTxx; KiXX(%0); CursorStart; CursorEnd; StartFitTrxx; EndFitTrxx; LevelStartFitTrxx; SlopeTrxx; AvgT20(s)*; AvgT30(s)*; AvgT60(s)*; AvgT20/30/60(s)*;"

#define TO_STR(MACRO_ARG)  #MACRO_ARG
#define HEADER_LOG_FILE  "Date;Type;Message"

/**
*******************************************************************************
** @fn  static const QString GetLogType(TeLogList  eLogList)
** @param[in] TeLogList  eLogList
** @param[out] const QString
** @brief returns the type field to be printed in the CSV Log related
**        to the error or event entered as input
** @return QString the type of the log input: Event or Error
**
*******************************************************************************
**/
static const QString GetLogType(TeLogList eLogList) {
  return arAtmicLog[eLogList].cType;
}

/**
*******************************************************************************
** @fn  static const QString GetLogType(TeLogList  eLogList)
** @param[in] TeLogList  eLogList
** @param[out] const QString
** @brief returns the message field to be printed in the CSV Log related
**        to the error or event entered as input
** @return QString the message of the log input
**
*******************************************************************************
**/
static const QString GetLogMessage(TeLogList eLogList) {
  return arAtmicLog[eLogList].cMessage;
}

/**
*******************************************************************************
** @fn  LogFile::LogFile(QObject *parent) : QObject(parent)
** @param[in] QObject *parent
** @param[out]
** @brief Constructor of the LogFile
** @return
**
*******************************************************************************
**/
LogFile::LogFile(QObject *parent) : QObject(parent),
  m_eMeasFileStatus(CSV_CLOSE),eSpectrum(SPECTRUM_3RD_OCTAVE),m_cCodified("false") {

}

/**
*******************************************************************************
** @fn  void LogFile::OpenCsv()
** @param[in] void
** @param[out] bool
** @brief Opens the CSV file
** @return void
**
*******************************************************************************
**/
void LogFile::OpenCsv(TrAcousticMeasConfig rAcousticMeasConfig, QString cFilename, int iReverbCounter,
                      QString projectName, QString roomType, QString roomNumber) {

      QAndroidJniObject path = QAndroidJniObject::callStaticObjectMethod("android.os.Environment",
                                                           "getExternalStorageDirectory",
                                                           "()Ljava/io/File;");

      m_cProjectName = projectName;
      m_cRoomType = roomType;
      m_cRoomNumber = roomNumber;

      if(rAcousticMeasConfig.eCalculMode == MODE_SOUNDLEVELMETER) {
          m_cCalculMode = "MODE_SOUNDLEVELMETER";

          if(!cFilename.isEmpty()){
              m_CsvItFile.setFileName(path.toString()
                                      + QString("/AllianTech/ATomicSound/Projects/ENV/CSV/")
                                      + cFilename + QString("_")
                                      + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss")
                                      + QString("_IT.csv"));

              m_CsvFitFile.setFileName(path.toString()
                                       + QString("/AllianTech/ATomicSound/Projects/ENV/CSV/")
                                       + cFilename + QString("_")
                                       + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss")
                                       + QString("_FIT.csv"));

          } else {
              m_CsvItFile.setFileName(path.toString()
                                      + QString("/AllianTech/ATomicSound/Projects/ENV/CSV/")
                                      + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss")
                                      + QString("_IT.csv"));

              m_CsvFitFile.setFileName(path.toString()
                                       + QString("/AllianTech/ATomicSound/Projects/ENV/CSV/")
                                       + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss")
                                       + QString("_FIT.csv"));
          }

          if(!m_CsvItFile.exists() & !m_CsvFitFile.exists()) {
              if(!m_CsvItFile.isOpen() & !m_CsvFitFile.isOpen()) {
                  m_CsvItFile.open(QIODevice::WriteOnly | QIODevice::Append);
                  m_CsvFitFile.open(QIODevice::WriteOnly | QIODevice::Append);

                  WriteLogFile(EVENT_START_CSV_RECORDING);
                  WriteCsvHeader(rAcousticMeasConfig);
              }

          } else {
              m_CsvItFile.open(QIODevice::WriteOnly | QIODevice::Append);
              m_CsvFitFile.open(QIODevice::WriteOnly | QIODevice::Append);

              WriteLogFile(EVENT_START_CSV_RECORDING);
          }

      } else if(rAcousticMeasConfig.eCalculMode == MODE_REVERBERATION) {
          m_cCalculMode = "MODE_REVERBERATION";

          //Initialize Index Fit Record
          m_iIndexFitRecord = 25;

          if(!cFilename.isEmpty()){
              m_CsvRTFile.setFileName(path.toString()
                                      + QString("/AllianTech/ATomicSound/Projects/"+m_cProjectName+"/"+m_cRoomType+"_"+m_cRoomNumber+"/BA/CSV/")
                                      + cFilename + QString("_")
                                      + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss")
                                      + QString("_RT") + QString::number(iReverbCounter+1)
                                      + QString(".csv"));

              m_CsvFitRTFile.setFileName(path.toString()
                                       + QString("/AllianTech/ATomicSound/Projects/"+m_cProjectName+"/"+m_cRoomType+"_"+m_cRoomNumber+"/BA/CSV/")
                                       + cFilename + QString("_")
                                       + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss")
                                       + QString("_FIT_RT") + QString::number(iReverbCounter+1)
                                       + QString(".csv"));

          } else {
              m_CsvRTFile.setFileName(path.toString()
                                      + QString("/AllianTech/ATomicSound/Projects/"+m_cProjectName+"/"+m_cRoomType+"_"+m_cRoomNumber+"/BA/CSV/")
                                      + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss")
                                      + QString("_RT") + QString::number(iReverbCounter+1)
                                      + QString(".csv"));

              m_CsvFitRTFile.setFileName(path.toString()
                                       + QString("/AllianTech/ATomicSound/Projects/"+m_cProjectName+"/"+m_cRoomType+"_"+m_cRoomNumber+"/BA/CSV/")
                                       + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss")
                                       + QString("_FIT_RT") + QString::number(iReverbCounter+1)
                                       + QString(".csv"));
          }

          if(!m_CsvRTFile.exists() & !m_CsvFitRTFile.exists()) {
              if(!m_CsvRTFile.isOpen() & !m_CsvFitRTFile.isOpen()) {
                  m_CsvRTFile.open(QIODevice::WriteOnly | QIODevice::Append);
                  m_CsvFitRTFile.open(QIODevice::WriteOnly | QIODevice::Append);

                  WriteLogFile(EVENT_START_CSV_RECORDING);
                  WriteCsvHeader(rAcousticMeasConfig);
              }

          } else {
              m_CsvRTFile.open(QIODevice::WriteOnly | QIODevice::Append);
              m_CsvFitRTFile.open(QIODevice::WriteOnly | QIODevice::Append);

              WriteLogFile(EVENT_START_CSV_RECORDING);
          }
      }
}

/**
*******************************************************************************
** @fn  void LogFile::CloseCsv()
** @param[in] void
** @param[out] void
** @brief Closes all Data CSV files
** @return void
**
*******************************************************************************
**/
void LogFile::CloseCsv() {
    if(m_cCalculMode == "MODE_SOUNDLEVELMETER") {
        m_CsvItFile.close();
        m_CsvFitFile.close();

    } else if(m_cCalculMode == "MODE_REVERBERATION") {
        m_CsvRTFile.close();
        m_CsvFitRTFile.close();
    }
}

/**
*******************************************************************************
** @fn  void LogFile::DeleteRTCsv()
** @param[in] void
** @param[out] void
** @brief
** @return void
**
*******************************************************************************
**/
void LogFile::DeleteRTCsv() {
    QAndroidJniObject path = QAndroidJniObject::callStaticObjectMethod("android.os.Environment",
                                                         "getExternalStorageDirectory",
                                                         "()Ljava/io/File;");

    QString pathDirName = path.toString() + QString("/AllianTech/ATomicSound/Projects/"+m_cProjectName+"/");
    QString pathDirRoom = pathDirName + QString(m_cRoomType+"_"+m_cRoomNumber+"/");
    //QString pathDirENV = pathDirRoom + QString("ENV/");
    QString pathDirBA = pathDirRoom + QString("BA/");
    QString pathDirBACSV = pathDirBA + QString("CSV/");

    QDir dirName(pathDirName);
    QDir dirRoom(pathDirRoom);
    //QDir dirENV(pathDirENV);
    QDir dirBA(pathDirBA);
    QDir dirBACSV(pathDirBACSV);

    if(m_cCalculMode == "MODE_REVERBERATION") {
            m_CsvRTFile.remove();
            m_CsvFitRTFile.remove();

            /*if(dirBA.isEmpty() && dirENV.isEmpty()) {
                dirENV.rmdir(pathDirENV);*/
            if(dirBACSV.isEmpty()) {
                dirBACSV.rmdir(pathDirBACSV);

                if(dirBA.isEmpty()) {
                    dirBA.rmdir(pathDirBA);

                    if(dirRoom.isEmpty()) {
                        dirRoom.rmdir(pathDirRoom);

                        if(dirName.isEmpty()) {
                            dirName.rmdir(pathDirName);
                        }
                    }
                }
            }
    }
}

/**
*******************************************************************************
** @fn  bool LogFile::OpenLog()
** @param[in] void
** @param[out] bool
** @brief Opens the Log file
** @return bool: returns 0 if the file is successfully opened else 1
**
*******************************************************************************
**/
bool LogFile::OpenLog() {
    QAndroidJniObject path = QAndroidJniObject::callStaticObjectMethod("android.os.Environment",
                                                           "getExternalStorageDirectory",
                                                           "()Ljava/io/File;");

    QDir().mkdir(path.toString()+QString("/AllianTech/ATomicSound/Log"));
    m_LogFile.setFileName(path.toString()
                          + QString("/AllianTech/ATomicSound/Log/")
                          + QString("LogFile.csv"));

    if(m_LogFile.exists()) {
        m_LogFile.open(QIODevice::WriteOnly | QIODevice:: Append);
        WriteLogFile(EVENT_OPEN_LOG_FILE);

    } else {
        m_LogFile.open(QIODevice::WriteOnly | QIODevice:: Append);
        WriteLogHeader();
        WriteLogFile(EVENT_OPEN_LOG_FILE);
    }

  return !m_LogFile.isOpen();
}

/**
*******************************************************************************
** @fn  int64_t LogFile::WriteCsvHeader(TrAcousticMeasConfig )
** @param[in] TrAcousticMeasConfig current configuration
** @param[out] int the size of the header, if error returns -1
** @brief Writes the csv-file Header
** @return int: size of the header if success else returns -1
**
*******************************************************************************
**/
long LogFile::WriteCsvHeader(TrAcousticMeasConfig rAcousticMeasConfig) {
  int itSeconds = rAcousticMeasConfig.iIntegrationTimeSec;
  QString HEADER_BOTTOM_LEQ_IT = QString("LAeq,%1s; LCeq,%1s; LZeq,%1s;").arg(itSeconds); // ToDo variable integration time

  QString HEADER_BOTTOM_LP = "";

  QString HEADER_BOTTOM_MODE = "";
  QString HEADER_BOTTOM_SPECTRUM = "";
  QString HEADER_BOTTOM_WEIGH = "";

  switch (rAcousticMeasConfig.eCalculMode) {
    case MODE_SOUNDLEVELMETER:
      HEADER_BOTTOM_MODE = "MODE_SOUNDLEVELMETER";
      break;
    case MODE_CALIBRATION:
      HEADER_BOTTOM_MODE = "MODE_CALIBRATION";
      break;
    case MODE_REVERBERATION:
      HEADER_BOTTOM_MODE = "MODE_REVERBERATION";
      break;
    case MODE_MULTISENSOR:
      HEADER_BOTTOM_MODE = "MODE_MULTISENSOR";
      break;
  }

  switch (rAcousticMeasConfig.rSpectrumConfig.eSpectrum) {
    case SPECTRUM_NO:
      HEADER_BOTTOM_SPECTRUM = "SPECTRUM_NO";
      break;
    case SPECTRUM_OCTAVE:
      HEADER_BOTTOM_SPECTRUM = "SPECTRUM_OCTAVE";
      break;
    case SPECTRUM_3RD_OCTAVE:
      HEADER_BOTTOM_SPECTRUM = "SPECTRUM_3RD_OCTAVE";
      break;
    case SPECTRUM_FFT:
      HEADER_BOTTOM_SPECTRUM = "SPECTRUM_FFT";
      break;
    case SPECTRUM_ZOOM_FFT:
      HEADER_BOTTOM_SPECTRUM = "SPECTRUM_ZOOM_FFT";
      break;
    case SPECTRUM_ENVELOPPE:
      HEADER_BOTTOM_SPECTRUM = "SPECTRUM_ENVELOPPE";
      break;
  }

  switch (rAcousticMeasConfig.rACZFilterConfig.eTimeWeightingLp) {
    case LP_NONE:
      HEADER_BOTTOM_WEIGH = "LP_NONE";
      HEADER_BOTTOM_LP = HEADER_BOTTOM_NONE;
      break;
    case LP_SLOW:
      HEADER_BOTTOM_WEIGH = "LP_SLOW";
      HEADER_BOTTOM_LP = HEADER_BOTTOM_SLOW;
      break;
    case LP_FAST:
      HEADER_BOTTOM_WEIGH = "LP_FAST";
      HEADER_BOTTOM_LP = HEADER_BOTTOM_FAST;
      break;
    case LP_IMPULSE:
      HEADER_BOTTOM_WEIGH = "LP_IMPULSE";
      HEADER_BOTTOM_LP = HEADER_BOTTOM_IMPULSE;
      break;
  }

  if(rAcousticMeasConfig.eCalculMode == MODE_SOUNDLEVELMETER) {

      qDebug()<<"Je rentre dans M O D E   S O U N D L E V E L M E T E R";
      QTextStream outIt(&m_CsvItFile);
      QTextStream outFit(&m_CsvFitFile);

      eSpectrum=rAcousticMeasConfig.rSpectrumConfig.eSpectrum;

      switch (rAcousticMeasConfig.rSpectrumConfig.eSpectrum) {
        case SPECTRUM_NO:
          break;

        case SPECTRUM_OCTAVE:
          outIt <</*Empty*/";" << QString(HEADER_TOP_I_OCTAVE).repeated(23) << "\n";
          outIt << HEADER_TIMESTAMP << HEADER_BOTTOM_LEQ_IT << HEADER_BOTTOM_LP
                << HEADER_BOTTOM_OCTAVE << HEADER_BOTTOM_ADDITIONAL << "\n";
          outIt.flush();

          outFit <</*Empty*/";" << QString(HEADER_TOP_FIT_OCTAVE).repeated(23)
                 << QString(HEADER_TOP_G_OCTAVE).repeated(23) << "\n";
          outFit << HEADER_TIMESTAMP << HEADER_BOTTOM_LEQ_FIT << HEADER_BOTTOM_LP
                 << HEADER_BOTTOM_OCTAVE << HEADER_BOTTOM_LEQ_FIT << HEADER_BOTTOM_LP
                 << HEADER_BOTTOM_OCTAVE << HEADER_BOTTOM_ADDITIONAL << "\n";
          outFit.flush();
          break;

        case SPECTRUM_3RD_OCTAVE:
          outIt <</*Empty*/";" << QString(HEADER_TOP_I_3RD_OCTAVE).repeated(47)<< "\n";
          outIt << HEADER_TIMESTAMP << HEADER_BOTTOM_LEQ_IT << HEADER_BOTTOM_LP
                << HEADER_BOTTOM_3RD_OCTAVE << HEADER_BOTTOM_ADDITIONAL <<"\n" << "\n";
          outIt.flush();

          outFit <</*Empty*/";" << QString(HEADER_TOP_FIT_3RD_OCTAVE).repeated(47)
                 << QString(HEADER_TOP_G_3RD_OCTAVE).repeated(47) << "\n";
          outFit << HEADER_TIMESTAMP << HEADER_BOTTOM_LEQ_FIT << HEADER_BOTTOM_LP
                 << HEADER_BOTTOM_3RD_OCTAVE<< HEADER_BOTTOM_LEQ_FIT << HEADER_BOTTOM_LP
                 << HEADER_BOTTOM_3RD_OCTAVE << HEADER_BOTTOM_ADDITIONAL << "\n" << "\n";;
          outFit.flush();
          break;

        case SPECTRUM_FFT:
          break;

        case SPECTRUM_ZOOM_FFT:
          break;

        case SPECTRUM_ENVELOPPE:
          break;
      }

      m_eMeasFileStatus = CSV_OPEN;

      if(!m_CsvItFile.size() || !m_CsvFitFile.size()) return -1;

  } else if(rAcousticMeasConfig.eCalculMode == MODE_REVERBERATION) {
      qDebug()<<"Je rentre dans M O D E   R E V E R B E R A T I O N";

      QString HEADER_BOTTOM_ADDITIONAL_REVERB = "Overload; Codified; UserComment; Screenshot; Fit Counter";

      QTextStream outFitRT(&m_CsvFitRTFile);
      outFitRT.setCodec("UTF-8");

      eSpectrum=rAcousticMeasConfig.rSpectrumConfig.eSpectrum;

      switch (rAcousticMeasConfig.rSpectrumConfig.eSpectrum) {
        case SPECTRUM_NO:
          break;

        case SPECTRUM_OCTAVE:
          outFitRT <</*Empty*/";" << QString(HEADER_TOP_FIT_OCTAVE).repeated(23)
                << QString(HEADER_TOP_G_OCTAVE).repeated(23) << "\n";
          outFitRT << HEADER_TIMESTAMP << HEADER_BOTTOM_LEQ_FIT << HEADER_BOTTOM_LP
                 << HEADER_BOTTOM_OCTAVE << HEADER_BOTTOM_LEQ_FIT << HEADER_BOTTOM_LP
                 << HEADER_BOTTOM_OCTAVE << HEADER_BOTTOM_ADDITIONAL_REVERB << "\n";
          outFitRT.flush();
          break;

        case SPECTRUM_3RD_OCTAVE:
          outFitRT << HEADER_CALCULMODE << HEADER_BOTTOM_MODE << ";"
                << HEADER_SPECTRUM << HEADER_BOTTOM_SPECTRUM << ";"
                << HEADER_ITTIME << rAcousticMeasConfig.iIntegrationTimeSec << ";"
                << HEADER_TIMEWEIGH << HEADER_BOTTOM_WEIGH << ";"
                << "\n";
          //outFitRT << QString(rAcousticMeasConfig.eCalculMode) << ";" << "\n";
          outFitRT <</*Empty*/";" << QString(HEADER_TOP_FIT_3RD_OCTAVE).repeated(47)
                << QString(HEADER_TOP_G_3RD_OCTAVE).repeated(47) << "\n";
          outFitRT << HEADER_TIMESTAMP << HEADER_BOTTOM_LEQ_FIT << HEADER_BOTTOM_LP
                 << HEADER_BOTTOM_3RD_OCTAVE<< HEADER_BOTTOM_LEQ_FIT << HEADER_BOTTOM_LP
                 << HEADER_BOTTOM_3RD_OCTAVE << HEADER_BOTTOM_ADDITIONAL_REVERB << "\n" << "\n";;
          outFitRT.flush();
          break;

        case SPECTRUM_FFT:
          break;

        case SPECTRUM_ZOOM_FFT:
          break;

        case SPECTRUM_ENVELOPPE:
          break;
      }

      m_eMeasFileStatus = CSV_OPEN;

      if(!m_CsvRTFile.size() || !m_CsvFitRTFile.size()) return -1;
  }

  return 0;
}

/**
*******************************************************************************
** @fn  int64_t LogFile::checkUnderloadAndPutToFormat(float fAcousticResValue, char cFormat, int iPrecision)
** @param[in] float fAcousticResValue, char cFormat, int iPrecision
** @param[out] sFinalString
** @brief If the value is possitive just adjust the format, else returns underload char symbol
** @return QStrind: Formated value or Underload Symbol
**
*******************************************************************************
**/
QString checkUnderloadAndPutToFormat(float fAcousticResValue, char cFormat, int iPrecision) {
   QLocale locale=QLocale("fr_FR");
   QString sFinalString = "";

   if (fAcousticResValue >= 0) {
       sFinalString = locale.toString(qAbs(static_cast<double>(fAcousticResValue)), cFormat, iPrecision);

   } else {
       sFinalString = "v";
   }

   return sFinalString;
}

/**
*******************************************************************************
** @fn  int64_t LogFile::WriteItCsv(TrAcousticRes rAcousticRes)
** @param[in] TrAcousticRes rAcousticRes
** @param[out] int64_t
** @brief Writes the csv-file data every IT (integration time)
** @return int: size of the file if success else returns -1
**
*******************************************************************************
**/
long LogFile::WriteItCsv(TrAcousticResBasis rAcousticRes) {
  QTextStream out(&m_CsvItFile);
  out.setCodec("UTF-8");

  int iMax=36;
  if(eSpectrum==SPECTRUM_OCTAVE) {
      iMax=12;
  }

  QDateTime dateTime=QDateTime::currentDateTime();
  int iDateCsv;
  iDateCsv = QDateTime::currentDateTime().toString("zzz").toInt()/100 +1;

  if(iDateCsv== 10){
      iDateCsv=0;
      dateTime=dateTime.addSecs(1);
  }
  out<<dateTime.toString("yyyy/MM/dd hh:mm:ss.")<<iDateCsv<<"00;";

  ///IT
  for (int index = 0; index < 3; index++) { //LeqA; LeqC; LeqZ;
      out<<checkUnderloadAndPutToFormat(rAcousticRes.rIt.rACZRes.afLeq[index],'f',1)<<";";
  }

  for (int index = 0; index < 3; index++) { //LpA; LpC; LpZ;
      out<<checkUnderloadAndPutToFormat(rAcousticRes.rIt.rACZRes.afLpACZ[index],'f',1)<<";";
  }

  for (int index = 0; index < 3; index++) { //LpAmax; LpCmax; LpZmax;
      out<<checkUnderloadAndPutToFormat(rAcousticRes.rIt.rACZRes.afLpMaxACZ[index],'f',1)<<";";
  }
  out<<checkUnderloadAndPutToFormat(rAcousticRes.rIt.rACZRes.afPeakCZ[0],'f',1)<<";";
  out<<checkUnderloadAndPutToFormat(rAcousticRes.rIt.rACZRes.afPeakCZ[1],'f',1)<<";";

  for (int index = 0; index < iMax; index++) {
      //"8Hz; 16Hz; 31.5Hz; 63Hz; 125Hz; 250Hz; 500Hz; 1KHz; 2KHz; 4KHz; 8KHz; 16KHz";
      out<<checkUnderloadAndPutToFormat(rAcousticRes.rIt.rOctaveRes.afLeq[index],'f',1) << ";" ;
  }

  //Overload Combined
  if(rAcousticRes.rIt.bOverload) out << "true"; else out<< "false";

  //Codified
  out<<";"<<m_cCodified;
  out<< "\n";
  out.flush();

  /** ********************************************************* **/
  if(out.status()==QTextStream::WriteFailed) return -1;

  return m_CsvItFile.size();
}

/**
*******************************************************************************
** @fn  int64_t LogFile::WriteRTCsv(TrAcousticMeasConfig rAcousticMeasConfig, TrAcousticRes rAcousticRes, int reverbCounter)
** @param[in] TrAcousticRes rAcousticRes
** @param[out] int64_t
** @brief Writes the csv-file data every IT (integration time)
** @return int: size of the file if success else returns -1
**
*******************************************************************************
**/
long LogFile::WriteRTCsv(TrAcousticMeasConfig rAcousticMeasConfig, TrReverberationRes rReverberationRes, int reverbCounter) {
  QTextStream outRT(&m_CsvRTFile);
  outRT.setCodec("UTF-8");

  int iMax=36;
  if(eSpectrum==SPECTRUM_3RD_OCTAVE) {
      iMax=36;
      m_iFreqCountOffset = 9;
      //m_iFreqIndex = 27;
      m_iFreqIndex = 36;
      m_cFreqTab = m_cFreqTab3rdOctave;

  } else if(eSpectrum==SPECTRUM_OCTAVE) {
      iMax=12;
      m_iFreqCountOffset = 3;
      //m_iFreqIndex = 9;
      m_iFreqIndex = 12;
      m_cFreqTab = m_cFreqTabOctave;
  }

  /** Information related to the Project **/
  outRT<<"Information related to the Project:"<<"\n";

  outRT<<"Project Name"<<";"<<m_cProjectName<<"\n";
  outRT<<"Room Type"<<";"<<m_cRoomType<<"\n";
  outRT<<"Room Number"<<";"<<m_cRoomNumber<<"\n";
  outRT<<"RT Count"<<";"<<reverbCounter<<"\n"<<"\n";

  /** Configuration used during the TR Measurement **/
  outRT<<"Configuration used during the TR Measurement:"<<"\n";

  outRT<<"Source"<<";"<<rAcousticMeasConfig.rReverberationConfig.eTypeSource<<"\n";  //"Impulse"
  outRT<<"Type of Spectrum"<<";"<<rAcousticMeasConfig.rSpectrumConfig.eSpectrum<<"\n"; //"3rd Octave"
  outRT<<"Trigger Threshold(dBA)"<<";"<<rAcousticMeasConfig.rReverberationConfig.fThresholdSourcedB<<"\n"; //"90"
  outRT<<"Decrease Time(s)"<<";"<<rAcousticMeasConfig.rReverberationConfig.fTimeRecordDecrease<<"\n"; //"3"
  outRT<<"Max. Noise Level(dB)"<<";"<<rAcousticMeasConfig.rReverberationConfig.fMaxNoiseLeveldB<<"\n"; //"50"
  outRT<<"Wav Generation"<<";"<<"False"<<"\n"<<"\n";

  outRT<<"CountSecNoise"<<";"<<QString::number(rReverberationRes.fCountSecNoise)<<"\n"; //"5"
  outRT<<"CountSecSource"<<";"<<QString::number(rReverberationRes.fCountSecSource)<<"\n"; //"0"
  outRT<<"DeltaMaxMinNoise"<<";"<<QString::number(rReverberationRes.fDeltaMaxMinNoise)<<"\n"; //"7,8"
  outRT<<"IndexFirstFitRecordDecrease"<<";"<<rReverberationRes.iIndexFirstFitRecordDecrease<<"\n";
  outRT<<"RecalcRtDate"<<";"<<"-"<<"\n";
  /*qDebug()<<"Count min : "<<rAcousticMeasConfig.rReverberationConfig.iIndexFreqStart;
  qDebug()<<"Freq Value min : "<<m_cFreqTab[rAcousticMeasConfig.rReverberationConfig.iIndexFreqStart];*/
  outRT<<"FreqMin"<<";"<<m_cFreqTab[rAcousticMeasConfig.rReverberationConfig.iIndexFreqStart]<<"\n"; //"100Hz"
  /*qDebug()<<"Count max : "<<rAcousticMeasConfig.rReverberationConfig.iIndexFreqEnd;
  qDebug()<<"Freq Value max : "<<m_cFreqTab[rAcousticMeasConfig.rReverberationConfig.iIndexFreqEnd];*/
  outRT<<"FreqMax"<<";"<<m_cFreqTab[rAcousticMeasConfig.rReverberationConfig.iIndexFreqEnd]<<"\n"; //"10000Hz"

  outRT<<HEADER_TOP_TR<<";"<<";"<<";"<<"\n";

  float fAvgTr20 = 0;
  float fAvgTr30 = 0;
  float fAvgTr60= 0;
  float fAvgTr = 0;
  float counterAvgTr20 = 0;
  float counterAvgTr30= 0;
  float counterAvgTr60= 0;
  float counterAvgTr = 0;

  for (int freqIndex = m_iFreqCountOffset; freqIndex < m_iFreqIndex; freqIndex++) { //Frequencies from 50Hz to 20kHz //27
      outRT<<m_cFreqTab[freqIndex]<<";";

      ///AvgT20(s); AvgT30(s); AvgT60(s); AvgT(s)
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afAvgTr20Sec[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afAvgTr30Sec[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afAvgTr60Sec[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afAvgTrSec[freqIndex],'f',2)<<";";

      ///Calculate the average values Average
      if(rReverberationRes.afAvgTr20Sec[freqIndex] != 0) {
          fAvgTr20 += rReverberationRes.afAvgTr20Sec[freqIndex];
          counterAvgTr20 += 1;
      }

      if(rReverberationRes.afAvgTr30Sec[freqIndex] != 0) {
          fAvgTr30 += rReverberationRes.afAvgTr30Sec[freqIndex];
          counterAvgTr30 += 1;
      }

      if(rReverberationRes.afAvgTr60Sec[freqIndex] != 0) {
          fAvgTr60 += rReverberationRes.afAvgTr60Sec[freqIndex];
          counterAvgTr60 += 1;
      }

      if(rReverberationRes.afAvgTrSec[freqIndex] != 0) {
          fAvgTr += rReverberationRes.afAvgTrSec[freqIndex];
          counterAvgTr += 1;
      }

      ///T20(s); StatusT20; T30(s); StatusT30; T60(s); StatusT60
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afTr20Sec[freqIndex],'f',2)<<";";

      QString sStatusT20 = "";
      if ((((int)rReverberationRes.auiFlagTr[freqIndex]) & D_T20_INSUFFICIENT_DYNAMIC) > 0) {
          sStatusT20 += "D";
      }

      if ((((int)rReverberationRes.auiFlagTr[freqIndex]) & I_T20_TOO_LOW) > 0) {
          sStatusT20 += "<";
      }

      if ((((int)rReverberationRes.auiFlagTr[freqIndex]) & KI_T20_NO_LINEARITY) > 0) {
          sStatusT20 += "E"; // "ε";
      }
      outRT<<sStatusT20<<";";

      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afTr30Sec[freqIndex],'f',2)<<";";

      QString sStatusT30 = "";
      if ((((int)rReverberationRes.auiFlagTr[freqIndex]) & D_T30_INSUFFICIENT_DYNAMIC) > 0) {
          sStatusT30 += "D";
      }

      if ((((int)rReverberationRes.auiFlagTr[freqIndex]) & I_T30_TOO_LOW) > 0) {
          sStatusT30 += "<";
      }

      if ((((int)rReverberationRes.auiFlagTr[freqIndex]) & KI_T30_NO_LINEARITY) > 0) {
          sStatusT30 += "E"; // "ε";
      }

      if ((((int)rReverberationRes.auiFlagTr[freqIndex]) & CURVATURE_TOO_LOW) > 0 ||
          (((int)rReverberationRes.auiFlagTr[freqIndex]) & CURVATURE_TOO_HIGH) > 0) {
          sStatusT30 += "C";
      }
      outRT<<sStatusT30<<";";

      QString sStatusT60 = "";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afTr60Sec[freqIndex],'f',2)<<";";
      if ((((int)rReverberationRes.auiFlagTr[freqIndex]) & D_T60_INSUFFICIENT_DYNAMIC) > 0) {
          sStatusT60 += "D";
      }

      if ((((int)rReverberationRes.auiFlagTr[freqIndex]) & I_T60_TOO_LOW) > 0) {
          sStatusT60 += "<";
      }

      if ((((int)rReverberationRes.auiFlagTr[freqIndex]) & KI_T60_NO_LINEARITY) > 0) {
          sStatusT60 += "E"; // "ε";
      }
      outRT<<sStatusT60<<";";

      ///Ki20(%0); Ki30(%0); Ki60(%0)
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afKi20[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afKi30[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afKi60[freqIndex],'f',2)<<";";

      ///Curvature(%)
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afCurvaturePercent[freqIndex],'f',2)<<";";

      ///Leq Noise(dB); Leq Source(dB)
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afLeqNoisedB[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afLeqSourcedB[freqIndex],'f',2)<<";";

      ///Hex Status
      outRT<<QString("0x%1").arg(rReverberationRes.auiFlagTr[freqIndex], 4, 16, QLatin1Char('0'));
      outRT<<m_cFreqTab[freqIndex]<<";";

      ///LevelStartFitTr20; LevelStartFitTr30; LevelStartFitTr60
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afLevelStartFitTr20[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afLevelStartFitTr30[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afLevelStartFitTr60[freqIndex],'f',2)<<";";

      ///SlopeTr20; SlopeTr30; SlopeTr60; StartFitTr; EndFitTr20; EndFitTr30; EndFitTr60
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afSlopeTr20[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afSlopeTr30[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afSlopeTr60[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.aiStartFitTr[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.aiEndFitTr20[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.aiEndFitTr30[freqIndex],'f',2)<<";";
      outRT<<checkUnderloadAndPutToFormat(rReverberationRes.aiEndFitTr60[freqIndex],'f',2)<<";";

      ///TXX(s); StatusTxx; KiXX(%0); CursorStart; CursorEnd; StartFitTrxx; EndFitTrxx;
      ///LevelStartFitTrxx; SlopeTrxx; AvgT20(s)*; AvgT30(s)*; AvgT60(s)*; AvgT20/30/60(s)*
      for (int index=0; index<13; index++) {
          outRT<<"-"<<";";
      }

      outRT<<"\n";
  }

  // Last Line, Avg**
  outRT<<"Avg**"<<";";

  ///AvgT20(s); AvgT30(s); AvgT60(s); AvgT(s)
  if(counterAvgTr20 != 0) {
      fAvgTr20 = fAvgTr20/counterAvgTr20;
  }
  outRT<<checkUnderloadAndPutToFormat(fAvgTr20,'f',2)<<";";
  if(counterAvgTr30 != 0) {
      fAvgTr30 = fAvgTr30/counterAvgTr30;
  }
  outRT<<checkUnderloadAndPutToFormat(fAvgTr30,'f',2)<<";";
  if(counterAvgTr60 != 0) {
      fAvgTr60 = fAvgTr60/counterAvgTr60;
  }
  outRT<<checkUnderloadAndPutToFormat(fAvgTr60,'f',2)<<";";
  if(counterAvgTr != 0) {
      fAvgTr = fAvgTr/counterAvgTr;
  }
  outRT<<checkUnderloadAndPutToFormat(fAvgTr,'f',2)<<";";

  ///T20(s); StatusT20; T30(s); StatusT30; T60(s); StatusT60
  outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afTr20Sec[NB_MAX_FREQ_TR-1],'f',2)<<";"<<";";
  outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afTr30Sec[NB_MAX_FREQ_TR-1],'f',2)<<";"<<";";
  outRT<<checkUnderloadAndPutToFormat(rReverberationRes.afTr60Sec[NB_MAX_FREQ_TR-1],'f',2)<<";"<<";";

  outRT<<"\n";

  // Empty Line
  outRT<<";"<<"\n";

  // Comments
  outRT<<"*Different result from the original by including one or more recalculated Txx values"<<"\n";
  outRT<<"**Calculated average value of the columns but only between Fmin and Fmax (included)"<<"\n";

  outRT.flush();

  if(outRT.status()==QTextStream::WriteFailed) return -1;

  return m_CsvRTFile.size();
}

/**
*******************************************************************************
** @fn  int64_t LogFile::WriteFitCsv(TrAcousticResBasis rAcousticRes)
** @param[in] TrAcousticRes rAcousticRes
** @param[out] int64_t
** @brief Writes the csv-file data every FIT, currently is set to 200 ms
** @return int: size of the file if success else returns -1
**
*******************************************************************************
**/
long LogFile::WriteFitCsv(TrAcousticResBasis rAcousticRes, int iSonoState) {
    if(iSonoState == 2 || iSonoState == 3) {
        QTextStream out(&m_CsvFitFile);
        out.setCodec("UTF-8");

        QLocale locale=QLocale("fr_FR");
        /** **/
        if (rAcousticRes.iCountFit != 0) { //Fit//
            int iMax = 36;
            bool bOverload = false;

            if(eSpectrum==SPECTRUM_OCTAVE) {
                iMax=12;
            }

            for (int iIndexFit = 0; iIndexFit < rAcousticRes.iCountFit; iIndexFit++) {
                //Check if any iIndexFit is overload state to change the csv entry in the additional column
                bOverload = rAcousticRes.rFit[iIndexFit].bOverload || bOverload;

                QDateTime dateTime=QDateTime::currentDateTime();
                int iDateCsv;
                iDateCsv = QDateTime::currentDateTime().toString("zzz").toInt()/100 +1;

                if(iDateCsv == 10) {
                    iDateCsv=0;
                    dateTime=dateTime.addSecs(1);
                }
                out<<dateTime.toString("yyyy/MM/dd hh:mm:ss.")<<iDateCsv<<"00;";

                //IT
                for (int index = 0; index < 3; index++) { //LeqA; LeqC; LeqZ;
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rFit[iIndexFit].rACZRes.afLeq[index],'f',1)<<";";
                }

                for (int index = 0; index < 3; index++) { //LpA; LpC; LpZ;
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rFit[iIndexFit].rACZRes.afLpACZ[index],'f',1)<<";";
                }

                for (int index = 0; index < 3; index++) { //LpAmax; LpCmax; LpZmax;
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rFit[iIndexFit].rACZRes.afLpMaxACZ[index],'f',1)<<";";
                }
                out<<checkUnderloadAndPutToFormat(rAcousticRes.rFit[iIndexFit].rACZRes.afPeakCZ[0],'f',1)<<";";
                out<<checkUnderloadAndPutToFormat(rAcousticRes.rFit[iIndexFit].rACZRes.afPeakCZ[1],'f',1)<<";";

                for (int index = 0; index < iMax; index++) {
                    //"8Hz; 16Hz; 31.5Hz; 63Hz; 125Hz; 250Hz; 500Hz; 1KHz; 2KHz; 4KHz; 8KHz; 16KHz";
                    out<<locale.toString(rAcousticRes.rFit[iIndexFit].rOctaveRes.afLeq[index],'f',1) << ";" ;
                }

                //G
                for (int index = 0; index < 3; index++) { //LeqA; LeqC; LeqZ;
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rGlobal[iIndexFit].rACZRes.afLeq[index],'f',1)<<";";
                }

                for (int index = 0; index < 3; index++) { //LpA; LpC; LpZ;
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rGlobal[iIndexFit].rACZRes.afLpACZ[index],'f',1)<<";";
                }

                for (int index = 0; index < 3; index++) { //LpAmax; LpCmax; LpZmax;
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rGlobal[iIndexFit].rACZRes.afLpMaxACZ[index],'f',1)<<";";
                }
                out<<checkUnderloadAndPutToFormat(rAcousticRes.rGlobal[iIndexFit].rACZRes.afPeakCZ[0],'f',1)<<";";
                out<<checkUnderloadAndPutToFormat(rAcousticRes.rGlobal[iIndexFit].rACZRes.afPeakCZ[1],'f',1)<<";";

                for (int index = 0; index < iMax; index++) {
                    //"8Hz; 16Hz; 31.5Hz; 63Hz; 125Hz; 250Hz; 500Hz; 1KHz; 2KHz; 4KHz; 8KHz; 16KHz";
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rGlobal[iIndexFit].rOctaveRes.afLeq[index],'f',1)<<";";
                }

                //Overload Combined
                if(bOverload) out << "true"; else out<< "false";

                //Codified
                out<<";"<<m_cCodified;
                out<<"\n";
                out.flush();
              }
          }

        if(out.status() == QTextStream::WriteFailed) {
            return -1;

        } else {
            return m_CsvFitFile.size();
        }

    } else if(iSonoState == 6) {
        QTextStream out(&m_CsvFitRTFile);
        out.setCodec("UTF-8");

        QLocale locale=QLocale("fr_FR");
        /** **/
        if (rAcousticRes.iCountFit != 0) { //Fit//
            int iMax = 36;
            bool bOverload = false;

            if(eSpectrum==SPECTRUM_OCTAVE) {
                iMax=12;
            }

            for (int iIndexFit = 0; iIndexFit < rAcousticRes.iCountFit; iIndexFit++) {
                //Check if any iIndexFit is overload state to change the csv entry in the additional column
                bOverload = rAcousticRes.rFit[iIndexFit].bOverload || bOverload;

                QDateTime dateTime=QDateTime::currentDateTime();
                int iDateCsv;
                iDateCsv = QDateTime::currentDateTime().toString("zzz").toInt()/100 +1;

                if(iDateCsv == 10) {
                    iDateCsv=0;
                    dateTime=dateTime.addSecs(1);
                }
                out<<dateTime.toString("yyyy/MM/dd hh:mm:ss.")<<iDateCsv<<"00;";

                //IT
                for (int index = 0; index < 3; index++) { //LeqA; LeqC; LeqZ;
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rFit[iIndexFit].rACZRes.afLeq[index],'f',1)<<";";
                }

                for (int index = 0; index < 3; index++) { //LpA; LpC; LpZ;
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rFit[iIndexFit].rACZRes.afLpACZ[index],'f',1)<<";";
                }

                for (int index = 0; index < 3; index++) { //LpAmax; LpCmax; LpZmax;
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rFit[iIndexFit].rACZRes.afLpMaxACZ[index],'f',1)<<";";
                }
                out<<checkUnderloadAndPutToFormat(rAcousticRes.rFit[iIndexFit].rACZRes.afPeakCZ[0],'f',1)<<";";
                out<<checkUnderloadAndPutToFormat(rAcousticRes.rFit[iIndexFit].rACZRes.afPeakCZ[1],'f',1)<<";";

                for (int index = 0; index < iMax; index++) {
                    //"8Hz; 16Hz; 31.5Hz; 63Hz; 125Hz; 250Hz; 500Hz; 1KHz; 2KHz; 4KHz; 8KHz; 16KHz";
                    out<<locale.toString(rAcousticRes.rFit[iIndexFit].rOctaveRes.afLeq[index],'f',1) << ";" ;
                }

                //G
                for (int index = 0; index < 3; index++) { //LeqA; LeqC; LeqZ;
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rGlobal[iIndexFit].rACZRes.afLeq[index],'f',1)<<";";
                }

                for (int index = 0; index < 3; index++) { //LpA; LpC; LpZ;
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rGlobal[iIndexFit].rACZRes.afLpACZ[index],'f',1)<<";";
                }

                for (int index = 0; index < 3; index++) { //LpAmax; LpCmax; LpZmax;
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rGlobal[iIndexFit].rACZRes.afLpMaxACZ[index],'f',1)<<";";
                }
                out<<checkUnderloadAndPutToFormat(rAcousticRes.rGlobal[iIndexFit].rACZRes.afPeakCZ[0],'f',1)<<";";
                out<<checkUnderloadAndPutToFormat(rAcousticRes.rGlobal[iIndexFit].rACZRes.afPeakCZ[1],'f',1)<<";";

                for (int index = 0; index < iMax; index++) {
                    //"8Hz; 16Hz; 31.5Hz; 63Hz; 125Hz; 250Hz; 500Hz; 1KHz; 2KHz; 4KHz; 8KHz; 16KHz";
                    out<<checkUnderloadAndPutToFormat(rAcousticRes.rGlobal[iIndexFit].rOctaveRes.afLeq[index],'f',1)<<";";
                }

                //Overload Combined
                if(bOverload) out << "true"; else out<< "false";

                //Codified
                out<<";"<<m_cCodified;

                //User Comment / Screenshot
                out<<";"<<";";

                //Index Fit Record
                out<<";"<<m_iIndexFitRecord;
                m_iIndexFitRecord++;

                out<<"\n";
                out.flush();
              }
          }

        if(out.status() == QTextStream::WriteFailed) {
            return -1;

        } else {
            return m_CsvFitRTFile.size();
        }
    }
}

/**
*******************************************************************************
** @fn  int64_t LogFile::WriteLogHeader()
** @param[in] void
** @param[out] void
** @brief initializes the header of the LogFile.csv file
** @return
**
*******************************************************************************
**/
long LogFile::WriteLogHeader() {
  QTextStream out(&m_LogFile);

  out.setCodec("UTF-8");
  out<< HEADER_LOG_FILE << "\n";

  return m_LogFile.size();
}

/**
*******************************************************************************
** @fn  int64_t LogFile::WriteLogFile(TeLogList eLogList)
** @param[in] TeLogList eLogList
** @param[out] int64_t
** @brief updates the LogFile.csv file
** @return the size of the LogFile.csv in case of error returns -1
**
*******************************************************************************
**/
long LogFile::WriteLogFile(TeLogList eLogList) {
  if(!m_LogFile.isOpen()){
      OpenLog();
  }

  QTextStream out(&m_LogFile);

  out.setCodec("UTF-8");
  out<<QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss.zzz")
    << ";" << GetLogType(eLogList)
    << ";" << GetLogMessage(eLogList)
    << "\n";
  out.flush();

  return m_LogFile.size();
}

/**
*******************************************************************************
** @fn  void LogFile::CloseLog()
** @param[in] void
** @param[out] void
** @brief updates then closes the LogFile.csv file
** @return void
**
****************************************************************************close***
**/
void LogFile::CloseLog() {
  qDebug() << "Closing log file";
  WriteLogFile(EVENT_CLOSE_LOG_FILE);
  m_LogFile.close();
}
