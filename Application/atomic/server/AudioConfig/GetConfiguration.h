/**
*******************************************************************************
** @file    GetConfiguration.h
** @version V0.0.1
** @author  Wevioo SE Team
** @brief   Configure the AcousticMeas and Calibrate the ATomic USB Card
**
*******************************************************************************
**/

#ifndef CONFIGURATIONGET_H
#define CONFIGURATIONGET_H

#include <QObject>
#include <QtAndroid>
#include <QtAndroidExtras/QAndroidJniObject>
#include <QFile>
#include <QtCore>
#include <QtXml>
#include "AudioGet/AudioGet.h"
#include "rep_configuration_source.h"

/**
*******************************************************************************
** @enum  TeStateCfg
** @brief enumeratos all the states of the configuration
**
*******************************************************************************
**/
typedef enum{
    CFG_ENABLED=0,                  /*!< configuration is enabled: can be saved or reseted */
    CFG_DISABLED                    /*!< configuration is disabled */
}TeStateCfg;

/**
*******************************************************************************
** @enum  TeCalibrationQmlStates
** @brief enumeratos all the states of the calibration
**
*******************************************************************************
**/
typedef enum{
    CALIB_READY=0,               /*!< calibration is ready to be launched */
    CALIB_BEFORE_CORRECTION,     /*!< calibration is launched and waiting for correction */
    CALIB_AFTER_CORRECTION,      /*!< calibration correction is obtained */
    CALIB_DISABLED               /*!< calibration is disabled */
}TeCalibrationQmlStates;

/**
*******************************************************************************
** @class  GetConfiguration
** @brief definition of the configuration class
**          this class holdes all the methods related to both the configuration
**          and calibration
**
*******************************************************************************
**/
class GetConfiguration : public configurationSimpleSource
{

    Q_OBJECT
    Q_PROPERTY(quint8 audioCfgState READ audioCfgState WRITE setAudioCfgState NOTIFY audioCfgStateChanged)
    Q_PROPERTY(QString audiolabSensitivity READ getAudioLabSensitivity WRITE setAudioLabSensitivity NOTIFY audiolabSensitivityChanged)
    Q_PROPERTY(QString calibrationDate READ getCalibrationDate WRITE setCalibrationDate NOTIFY calibrationDateChanged)

    Q_PROPERTY(quint8 libAudioCalibStatus READ libAudioCalibStatus WRITE setLibAudioCalibStatus NOTIFY libAudioCalibStatusChanged)
    Q_PROPERTY(QString lastCalibrationDate  READ getLastCalibrationDate WRITE setLastCalibrationDate NOTIFY lastCalibrationDateChanged)

public:

    /**
  *******************************************************************************
  ** @fn  explicit GetConfiguration(QObject *parent,  QPointer<AudioGet> )
  ** @param[in] QObject *parent     pointer to the parent object of this class
  ** @param[in] QPointer<AudioGet>  holds a reference of the AudioGet instance
  ** @param[out]
  ** @brief constructor of the GetConfiguration's class
  ** @return
  **
  *******************************************************************************
  **/
    explicit GetConfiguration(QObject *parent,  QPointer<AudioGet> );

    /**
  *******************************************************************************
  ** @fn virtual ~GetConfiguration()
  ** @param[in] void
  ** @param[out]
  ** @brief destructor of the GetConfiguration's class: stops current running service
  ** @return
  **
  *******************************************************************************
  **/
    virtual ~GetConfiguration() override{
        QtAndroid::androidService().callMethod<void>("stopSelf");
    }

    /**
  *******************************************************************************
  ** @fn  cfgState()
  ** @param[in] void
  ** @param[out] quint8 current Configuration's state
  ** @brief gets the current configuration status
  ** @return
  **
  *******************************************************************************
  **/
    quint8 audioCfgState();

    /**
  *******************************************************************************
  ** @fn  setCfgState(quint8 uiState)
  ** @param[in] quint8 uiState  current state
  ** @param[out] void
  ** @brief sets the current configuration status
  ** @return
  **
  *******************************************************************************
  **/
    void setAudioCfgState(quint8 uiState);

    /**
  *******************************************************************************
  ** @fn  quint8 libCalibStatus()
  ** @param[in] void
  ** @param[out] quint8
  ** @brief gets the calibration's engine status
  **         [depending upon the library]
  ** @return
  **
  *******************************************************************************
  **/
    quint8 libAudioCalibStatus();

    /**
  *******************************************************************************
  ** @fn  void setLibCalibStatus(quint8 uiState)
  ** @param[in] quint8 uiState  current calibration's state
  ** @param[out] void
  ** @brief sets the current calibration status
  ** @return
  **
  *******************************************************************************
  **/
    void setLibAudioCalibStatus(quint8 uiState);

    /**
  *******************************************************************************
  ** @fn  QString getAudioLabSensitivity()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the laboratory sensitivity of the ATomic card
  ** @return QString
  **
  *******************************************************************************
  **/
    QString getAudioLabSensitivity();

    /**
  *******************************************************************************
  ** @fn  void GetConfiguration::setAudioLabSensitivity(QString )
  ** @param[in] QString  laboratory sensitivity entered in the configuration tab
  ** @param[out] void
  ** @brief sets the laboratory sensitivity
  ** @return void
  **
  *******************************************************************************
  **/
    void setAudioLabSensitivity(QString);

    /**
  *******************************************************************************
  ** @fn  QString getCalibrationDate()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the last date of calibration
  ** @return QString
  **
  *******************************************************************************
  **/
    QString getCalibrationDate();

    /**
  *******************************************************************************
  ** @fn  void setCalibrationDate(QString )
  ** @param[in] QString  date of calibration
  ** @param[out] void
  ** @brief sets the full date of the calibration
  ** @return void
  **
  *******************************************************************************
  **/
    void setCalibrationDate(QString);

    /**
  *******************************************************************************
  ** @fn  QString getFilesName()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the filename
  ** @return QString
  **
  *******************************************************************************
  **/
    QString getFilesName();


    /**
  *******************************************************************************
  ** @fn  QString getLastCalibrationDate()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the last calibration full date
  ** @return QString
  **
  *******************************************************************************
  **/
    QString getLastCalibrationDate();

    /**
  *******************************************************************************
  ** @fn  void setLastCalibrationDate(const QString )
  ** @param[in]QString last calibration date
  ** @param[out] void
  ** @brief sets the last calibration date
  ** @return void
  **
  *******************************************************************************
  **/
    void setLastCalibrationDate(const QString);


    /**
  *******************************************************************************
  ** @fn  void writeCurrentFile()
  ** @param[in] void
  ** @param[out] void
  ** @brief writes the "current.xml" file into device after on every configuration
**             save
  ** @return void
  **
  *******************************************************************************
  **/
    void writeCurrentFile();

    /**
  *******************************************************************************
  ** @fn  void writeDefaultFile(QString)
  ** @param[in] void
  ** @param[out] void
  ** @brief writes the "default.xml" file into device:
  **          the defaultfile is written whenever this file is not found in the
  **          specified directory
  ** @return void
  **
  *******************************************************************************
  **/
    void writeDefaultFile(QString);

    /**
  *******************************************************************************
  ** @fn  void writeSaveFile(QString)
  ** @param[in] void
  ** @param[out] void
  ** @brief
  ** @return void
  **
  *******************************************************************************
  **/
    void writeSaveFile(QString, QString);

    /**
  *******************************************************************************
  ** @fn  void writeSaveFile(QString configFilePath)
  ** @param[in] void
  ** @param[out] void
  ** @brief
  ** @return void
  **
  *******************************************************************************
  **/
    void updateConfigFile();

    /**
  *******************************************************************************
  ** @fn  void updateCurrentFile(QString configFilePath)
  ** @param[in] void
  ** @param[out] void
  ** @brief
  ** @return void
  **
  *******************************************************************************
  **/
    //void updateCurrentFile();

    /**
  *******************************************************************************
  ** @fn  QString traverseCurrentFile(QString fElement, QString sElement)
  ** @param[in] QString fElement        node name
  ** @param[in] QString sElement        element name
  ** @param[out] QString
  ** @brief  gets tha parameter from "current.xml" file according to the node
  **           and element names as printed in the xml file
  ** @return  parameter value as a string from the current.xml file
  **
  *******************************************************************************
  **/
    QString traverseCurrentFile(QString, QString, QString, QString);

    /**
  *******************************************************************************
  ** @fn  QString traverseDefaultFile(QString fElement, QString sElement)
  ** @param[in] QString fElement        node name
  ** @param[in] QString sElement        element name
  ** @param[out] QString
  ** @brief  gets tha parameter from "default.xml" file according to the node
  **           and element names as printed in the xml file
  ** @return  parameter value as a string from the current.xml file
  **
  *******************************************************************************
  **/
    QString traverseDefaultFile(QString, QString, QString, QString);

    /**
  *******************************************************************************
  ** @fn  QString traverseSaveFile(QString, QString)
  ** @param[in] QString fElement        node's name
  ** @param[in] QString sElement        element's name
  ** @param[out] QString
  ** @brief  gets tha parameter from "default.xml" file according to the node
  **           and element names as printed in the xml file
  ** @return  parameter value as a string from the current.xml file
  **
  *******************************************************************************
  **/
    QString traverseSaveFile(QString);

    /**
  *******************************************************************************
  ** @fn  QString traverseSelectedFile(QString, QString, QString, QString)
  ** @param[in] QString fElement        node's name
  ** @param[in] QString sElement        element's name
  ** @param[out] QString
  ** @brief  gets tha parameter from "default.xml" file according to the node
  **           and element names as printed in the xml file
  ** @return  parameter value as a string from the current.xml file
  **
  *******************************************************************************
  **/
    QString traverseSelectedFile(QString, QString, QString, QString);

    /**
  *******************************************************************************
  ** @fn  void setProjectParam()
  ** @param[in] void
  ** @param[out] void
  ** @brief sets the both configuration and calibration tabs according to the
  **         current.xml file if existed else according to the default.xml file
  ** @return void
  **
  *******************************************************************************
  **/
    void setProjectParam();

    /**
    *******************************************************************************
    ** @fn  quint8 GetConfiguration::cfgState()
    ** @param[in] void
    ** @param[out] quint8 current Configuration's state
    ** @brief returns the current Configuration status
    ** @return
    **
    *******************************************************************************
    **/
    void deleteOldFiles();

public slots:

    /**
  *******************************************************************************
  ** @fn  void setconfiguration()
  ** @param[in] void
  ** @param[out] void
  ** @brief sets the configuration by writing the currentfile and configuring
  **            the AcousticMeas Engine
  ** @return void
  **
  *******************************************************************************
  **/
    void setconfiguration() override;

    /**
  *******************************************************************************
  ** @fn  void setReset()
  ** @param[in] void
  ** @param[out] void
  ** @brief sets the both configuration and calibration tabs according to the
  **         default values (from defaultfile.xml)
  ** @return void
  **
  *******************************************************************************
  **/
    virtual void setReset() override;

    /**
  *******************************************************************************
  ** @fn  void setDefault()
  ** @param[in] void
  ** @param[out] void
  ** @brief sets the both configuration and calibration tabs according to the
  **         current.xml file if existed else according to the default.xml file
  ** @return void
  **
  *******************************************************************************
  **/
    virtual void setDefault() override;

    /**
  *******************************************************************************
  ** @fn  void configAcousticMeas(bool, bool)
  ** @param[in] bool : if true: reset the engine
  **                    else configure from current.xml
  ** @param[out] void
  ** @brief sets the rAcousticMeasConfig structure from the default.xml file if
  **         the entered parameter is true, else if false, sets it from the
  **         current.xml file and configures the AcousticMeasSo engine accordingly
  ** @return void
  **
  *******************************************************************************
  **/
    virtual void configAcousticMeas(bool, bool) override;

    /**
  *******************************************************************************
  ** @fn  void setConfigurationAtStart()
  ** @param[in] void
  ** @param[out] void
  ** @brief sets the rAcousticMeasConfig structure from the current.xml file if
  **         existed else from the default.xml
  ** @return void
  **
  *******************************************************************************
  **/
    virtual void setConfigurationAtStart() override;

    /**
  *******************************************************************************
  ** @fn  void GetConfiguration::setCalibration(bool isDefault)
  ** @param[in] bool isDefault: set default calibration's parameters if true
  **                            else set last-saved-calibation parameters
  ** @param[out] void
  ** @brief sets the calibration's parameters (updates the calibraion's screen
  **        values)
  ** @return void
  **
  *******************************************************************************
  **/
    virtual void setCalibration(bool isDefault) override;

    /**
  *******************************************************************************
  ** @fn  void GetConfiguration::uponSonoStateChange()
  ** @param[in] void
  ** @param[out] void
  ** @brief sets the state of the configuration and calibration upon the changes
  **          of the sonometer state
  ** @return void
  **
  *******************************************************************************
  **/
    void uponSonoStateChange();

    /**
  *******************************************************************************
  ** @fn  void  startCalibration()
  ** @param[in] void
  ** @param[out] void
  ** @brief start the calibration process of the AcousticMeas
  ** @return void
  **
  *******************************************************************************
  **/
    virtual void startCalibration() override;

    /**
  *******************************************************************************
  ** @fn  void  startReverberation()
  ** @param[in] void
  ** @param[out] void
  ** @brief start the reverberation process of the AcousticMeas
  ** @return void
  **
  *******************************************************************************
  **/
    //virtual void configNewMode() override;

    /**
  *******************************************************************************
  ** @fn  void stopCalibration(bool bStopCalib)
  ** @param[in] bool bStopCalib
  ** @param[out] void
  ** @brief stopes the current calibration
  ** @return
  **
  *******************************************************************************
  **/
    virtual void stopCalibration(bool) override;

    /**
  *******************************************************************************
  ** @fn  void validateCalibration()
  ** @param[in] void
  ** @param[out] void
  ** @brief validate and store the achieved calibration
  ** @return
  **
  *******************************************************************************
  **/
    virtual void validateCalibration() override;

    /**
  *******************************************************************************
  ** @fn  void storeCalibration()
  ** @param[in] void
  ** @param[out] void
  ** @brief stores the calibration-resulted values in the current.xml file
  **        and sets it in the AcousticMeas structure
  ** @return
  **
  *******************************************************************************
  **/
    virtual void storeCalibration() override;

    /**
  *******************************************************************************
  ** @fn  void onStatusOfCalibrationChanged()
  ** @param[in] void
  ** @param[out] void
  ** @brief updates the GUI depending on the calibration progress
  ** @return
  **
  *******************************************************************************
  **/
    void onStatusOfCalibrationChanged();

    /**
  *******************************************************************************
  ** @fn  bool configureAndStartSono(TrAcousticMeasConfig rAcousticMeasConfig)
  ** @param[in] TrAcousticMeasConfig rAcousticMeasConfig
  ** @param[out] bool: returns true upon success
  ** @brief configure the engine at launching time of the application
  ** @return bool  1: success
  **               0: failure
  **
  *******************************************************************************
  **/
    bool configureAndStartSono(TrAcousticMeasConfig);

    /**
  *******************************************************************************
  ** @fn  void GetConfiguration::stopAfterCorrection()
  ** @param[in] void
  ** @param[out] void
  ** @brief stop the calibration without storing it
  ** @return
  **
  *******************************************************************************
  **/
    virtual void stopAfterCorrection() override;

    /**
  *******************************************************************************
  ** @fn  void GetConfiguration::setFilesName(QString cFilesName)
  ** @param[in]QString cFilesName  filename
  ** @param[out] void
  ** @brief sets the filename (for Data-CSV and WAV files)
  ** @return void
  **:
  *******************************************************************************
  **/
    void onFilesNameChanged(QString);

    /**
  *******************************************************************************
  ** @fn  void initializeCfg()
  ** @param[in] void
  ** @param[out] void
  ** @brief Creates configuration directory and needed files (default.xml/ current.xml)
  **         and sets different parameters accordingly
  ** @return
  **
  *******************************************************************************
  **/
    virtual void initializeCfg() override;

    /**
  *******************************************************************************
  ** @fn  void pathConfigFileName(QString)
  ** @param[in] void
  ** @param[out] void
  ** @brief Creates configuration directory and needed files (default.xml/ current.xml)
  **         and sets different parameters accordingly
  ** @return
  **
  *******************************************************************************
  **/
    virtual void pathConfigFileName(QString) override;

signals:
    void        audioCfgStateChanged();                       /*!< emitted when configuration status has changed: is either enabled or disabled */
    void        audiolabSensitivityChanged();                 /*!< emitted when the laboratory-sensitivity value is updated */
    void        calibrationDateChanged();                     /*!< emitted when the Laboratory Accreditation Date has changed */

    /*****Calibration****/
    void        lastCalibrationDateChanged();                 /*!< emitted when a new calibration is stored, to update the last calibration field in the GUI */
    //  void        errorConfiguration();                         /*!< emitted when an error has occured during configuration */

    void        checkState();                                 /*!< emitted periodically to check the calibration progress
                                                                 this signal is emitted every m_CalibTimer is triggered */
    void        calibStateChanged();                          /*!< emitted when the calibration status has changed to update the GUI */
    void        libAudioCalibStatusChanged();                 /*!< emitted when calibration is either enabled or disabled */
    //  void        calibrationSuccess();                         /*!< emitted upon calibration success */

    /*!< be emitted to audioGet */
    void        fileNameChanged(QString);                      /*!< AudioGet:: to be emitted to WavFile class */
    void        configurationEnded(TrAcousticMeasConfig);      /*!< AudioGet:: to be emitted AudioGet */

private:
    TeStateCfg  m_eStateCfg=CFG_DISABLED;                        /*!< state of configuration: enabled or disabled */
    QString     m_cLabSensitivity="";                         /*!< holds the value of the laboratory sensitivity */
    QString     m_cConversionFactor; //PatchBetaDynamMetadata
    QString     m_cCalibrationDate;                           /*!< holds the Laboratory Accreditation Date */
    QString     m_cFilesName;                                 /*!< holds the value of the fileName introduced in the GUI */
    bool       m_bIsStart= true;                              /*!< this parameter is introduced to prevent the pop-up from appearing
                                                                 at the start of the application (due to the first configuration) */

    /**   CALIBRATION RELATED **/
    TeStatusOfCalibration m_eStatusOfCalibration=CAL_NOT_RUNNING;              /*!< represents the status of calibration, depending with the AcousticMeas engine
                                                                                at start it is set at CAL_NOT_RUNNING */
    TeStatusOfCalibration m_ePreviousStatusOfCalibration=CAL_NOT_RUNNING;      /*!< represents the previous status of calibration, depending with the AcousticMeas engine
                                                                                and m_eStatusOfCalibration, at start it is set at CAL_NOT_RUNNING */
    TeCalibrationQmlStates m_eCalibrationQmlStates=CALIB_READY;                /*!< At start the calibration is enabled to users */

    QString     m_cLastCalibrationDate;                                        /*!< holds the last calibration Date */
    QTimer*     m_CalibTimer;                                                  /*!< this timer is introduced to spy on the status of calibration periodically
                                                                                  and accordingly updates the GUI */
    bool        m_bIsCalibration= false;                                       /*!< this parameter is introduced to differenciate between configuration and calibration */

    bool        m_bIsReverberation= false;

    //Patch_FullScaleMethod
    int const FULLSCALE_MAX_CHAR_COUNT = 5;  //Bigger length could return a false positive as could enter into the Version digits!s
    int const FULLSCALE_MIN_CHAR_COUNT = 2;

    float const MAX_GAIN_INTERCHANNEL = 30.0f;
    float const DEFAULT_GAIN_INTERCHANNEL = 20.0f;

protected:
    QFile m_currentFile;                                     /*!< file containing the current AcousticMeas configuration */
    QFile m_saveFile;                                     /*!< file containing the current AcousticMeas configuration */
    QFile m_defaultOctaveFile;                               /*!< file containing the default AcousticMeas configuration */
    QFile m_defaultThirdOctaveFile;                          /*!< file containing the default AcousticMeas configuration */
    QFile m_selectedFile;                          /*!< file containing the default AcousticMeas configuration */

    /* DELETE DEFAULT AND CURRENT FILES FROM ATOMIC SOUND v0.x.x.x VERSIONS */
    QFile m_oldDefaultFile;
    QFile m_oldCurrentFile;

    QDir m_oldCSVDir;

public:
    QPointer<AudioGet>  m_oAudioGet=nullptr;   /*!< holds a reference to the AudioGet class */

    //Indep CH Count
    int iIndepChCount = 3; // ATomic1 : 0 ; ATomic2 : 1,2

    float *m_afFullScaledB = new float[iIndepChCount];
    float *m_afFullScaleLin = new float[iIndepChCount];
    float *m_afFftRealOverlapPercent = new float[iIndepChCount];

    QString m_oPathConfigFile = "/path/initialisation/config/file";
    QFile *m_oConfigFile;
};

#endif // CONFIGURATIONGET_H
