/**
*******************************************************************************
** @file    GetConfiguration.cpp
** @version V0.0.1
** @author  Wevioo SE Team
** @brief   Configure the AcousticMeas and Calibrate the ATomic USB Card
**
*******************************************************************************
**/

#include "GetConfiguration.h"
#include <QtXml>
#include <QDebug>
#include <QAndroidJniEnvironment>
#include <QMessageBox>
#include "rep_configuration_source.h"

#define NUMBER_OF_CHANNELS 1

#define INDEX_FREQ_100HZ_3RD_OCTAVE 12
#define INDEX_FREQ_10KHZ_3RD_OCTAVE 32
#define INDEX_FREQ_125HZ_OCTAVE 4
#define INDEX_FREQ_8KHZ_OCTAVE 10
#define INDEX_FREQ_50HZ_3RD_OCTAVE 9
#define INDEX_FREQ_20KHZ_3RD_OCTAVE 35
#define INDEX_FREQ_63HZ_OCTAVE 3
#define INDEX_FREQ_16KHZ_OCTAVE 11

/**
*******************************************************************************
** @fn inline qreal MIN_THRESHOLD(double NSTolerance,double NS)
** @param[in] double NSTolerance: Nominal Sensitivity tolerance
** @param[in] double NS : Nominal Sensitivity
** @param[out] qreal
** @brief measures the minimum threshold accepted
**
** @return qreal
**
*******************************************************************************
**/
inline qreal MIN_THRESHOLD(double NSTolerance, double NS){
    return qPow(10,(-1*NSTolerance/20))*NS;
}

/**
*******************************************************************************
** @fn inline qreal MAX_THRESHOLD(double NSTolerance,double NS)
** @param[in] double NSTolerance: Nominal Sensitivity tolerance
** @param[in] double NS : Nominal Sensitivity
** @param[out] qreal
** @brief measures the maximum threshold accepted
**
** @return qreal
**
*******************************************************************************
**/
inline qreal MAX_THRESHOLD(double NSTolerance,double NS) {
    return qPow(10,(NSTolerance/20))*NS;
}

/**
*******************************************************************************
** @fn inline qreal REAL_SENSITIVITY(double dLabSensitivity, double dCalibCorr)
** @param[in] double dLabSensitivity: Laboratory Sensitivity
** @param[in] double dCalibCorr : Calibration correction
** @param[out] qreal
** @brief measures the real sensitivity value
**
** @return qreal
**
*******************************************************************************
**/
inline qreal REAL_SENSITIVITY(double dLabSensitivity, double dCalibCorr) {
    return dLabSensitivity*qPow(10,dCalibCorr/20.00);        //should be stored with 2 decimals at least for more accurate result
}

/**
*******************************************************************************
** @fn  GetConfiguration::GetConfiguration(QObject *parent,
**                                QPointer<AudioGet> audioGet) : QObject(parent)
** @param[inout] QObject *parent  parent Object
** @param[in] QPointer<AudioGet>  AudioGet Object
** @brief Constructor of GetConfiguration class
** @return
**
*******************************************************************************
**/
GetConfiguration::GetConfiguration(QObject *parent, QPointer<AudioGet> audioGet) : configurationSimpleSource(parent),
    m_oAudioGet(audioGet) {
    QAndroidJniObject path;
    QString pathString;
    QUrl pathConfigFile;

    qDebug()<<"pathConfigFile : "<<pathConfigFile;
    path = QAndroidJniObject::callStaticObjectMethod("android.os.Environment",
                                                     "getExternalStorageDirectory",
                                                     "()Ljava/io/File;");
    pathString = QString("file://")+path.toString();
    pathConfigFile = QUrl(pathString);

    setRootPath(pathConfigFile);
    qDebug()<<"rootPath dans GET CONFIG: "<<rootPath();

    if(m_oAudioGet) {
        QObject::connect(this,SIGNAL(configurationEnded(TrAcousticMeasConfig)),m_oAudioGet,SLOT(InitConfigurationFiles(TrAcousticMeasConfig)));
        QObject::connect(this,SIGNAL(errorConfiguration()),m_oAudioGet,SLOT(InitConfigurationFiles(TrAcousticMeasConfig)));
        QObject::connect(m_oAudioGet,SIGNAL(sonoStateChanged(int)),this,SLOT(uponSonoStateChange()));

    } else {
        delete m_oAudioGet;
        qDebug()<< "error in pointer assignement";
    }

    for(int i = 0; i < iIndepChCount; i++) {
        m_afFullScaledB[i] = 0;
        m_afFullScaleLin[i] = 0;
        m_afFftRealOverlapPercent[i] = 0;
    }
}

/**
*******************************************************************************
** @fn  GetConfiguration::initializeCfg()
** @param[in] void
** @param[out] void
** @brief Creates configuration directory and needed files (default.xml/ current.xml)
**         and sets different parameters accordingly
** @return
**
*******************************************************************************
**/
void GetConfiguration::initializeCfg() {
    QAndroidJniObject path;
    path = QAndroidJniObject::callStaticObjectMethod("android.os.Environment",
                                                     "getExternalStorageDirectory",
                                                     "()Ljava/io/File;");

    QDir().mkdir(path.toString()+QString("/AllianTech/ATomicSound/Configuration"));
    m_defaultThirdOctaveFile.setFileName(path.toString()+ QString("/AllianTech/ATomicSound/Configuration/")
                                         + QLatin1String("/default_third_octave_config.xml"));
    m_defaultOctaveFile.setFileName(path.toString()+ QString("/AllianTech/ATomicSound/Configuration/")
                                    + QLatin1String("/default_octave_config.xml"));

    m_selectedFile.setFileName(path.toString()+ QString("/AllianTech/ATomicSound/Configuration/")
                               + QLatin1String("/default_third_octave_config.xml"));

    QDir().mkdir(path.toString()+QString("/AllianTech/ATomicSound/Save"));
    m_saveFile.setFileName(path.toString()
                           + QString("/AllianTech/ATomicSound/Save/")
                           + QLatin1String("/save.xml"));
    m_currentFile.setFileName(path.toString()
                              + QString("/AllianTech/ATomicSound/Save/")
                              + QLatin1String("/current.xml"));

    QObject::connect(this, SIGNAL(filesNameChanged(QString)), this, SLOT(onFilesNameChanged(QString)));

    if (m_defaultThirdOctaveFile.exists()==false) {
        writeDefaultFile("ThirdOctave");
    }

    if (m_defaultOctaveFile.exists()==false) {
        writeDefaultFile("Octave");                 // writes the default.xml file if not existed
    }

    if (m_currentFile.exists()) {
        // set parameters according the current.xml file
        setCalibration(false);
        setDefault();

    } else {
        // set parameters according the default.xml file
        setCalibration(true);
        setReset();
    }

    if (m_saveFile.exists()==false) {
        writeSaveFile("/storage/emulated/0/AllianTech/ATomicSound/Configuration/default_third_octave_config.xml","default_third_octave_config.xml");

    } else {
        updateConfigFile();
    }

    /** DELETE DEFAULT AND CURRENT FILES FROM ATOMIC SOUND v0.x.x.x VERSIONS **/
    deleteOldFiles();

    /** CALIBRATION RELATED CONNECTIONS **/
    //  QObject::connect(this,SIGNAL(calibrationSuccess()),this,SLOT(validateCalibration()));
    QObject::connect(this,SIGNAL(checkState()),this, SLOT(onStatusOfCalibrationChanged()));

    setCalibState(CALIB_READY);
    m_ePreviousStatusOfCalibration = TIMEOUT_CALIB;
    m_eStatusOfCalibration=CAL_NOT_RUNNING;

    // timer to check the state of the calibration each 500 ms
    m_CalibTimer= new QTimer();
    m_CalibTimer->setInterval(500);
    QObject::connect(m_CalibTimer, SIGNAL(timeout()), this, SLOT(onStatusOfCalibrationChanged()));
}

/**
*******************************************************************************
** @fn  quint8 GetConfiguration::cfgState()
** @param[in] void
** @param[out] quint8 current Configuration's state
** @brief returns the current Configuration status
** @return
**
*******************************************************************************
**/
void GetConfiguration::deleteOldFiles() {
    QAndroidJniObject path;
    path = QAndroidJniObject::callStaticObjectMethod("android.os.Environment",
                                                     "getExternalStorageDirectory",
                                                     "()Ljava/io/File;");

    m_oldDefaultFile.setFileName(path.toString()+ QString("/AllianTech/ATomicSound/Configuration/")
                                  + QLatin1String("/default.xml"));
    m_oldCurrentFile.setFileName(path.toString()
                                  + QString("/AllianTech/ATomicSound/Configuration/")
                                  + QLatin1String("/current.xml"));
    m_oldCSVDir.setPath(path.toString()+ QString("/AllianTech/ATomicSound/CSV"));

    QString errorStr;
    int errorLine;
    int errorColumn;
    QDomDocument doc;

    /* DELETE DEFAULT OLD FILE */
    if(m_oldDefaultFile.isOpen()) {
        m_oldDefaultFile.close();
    }

    m_oldDefaultFile.open(QIODevice::ReadOnly | QIODevice ::Text);

    if(!m_oldDefaultFile.isOpen()) {
        qDebug()<<"failed to open old default file";
        m_oAudioGet->getFileError(m_oldDefaultFile.error(),XML);

    } else {
        if(!doc.setContent(&m_oldDefaultFile, false, &errorStr, &errorLine, &errorColumn)) {
            qDebug() << "Error: Parse error at line " << errorLine << ", "
                     << "column " << errorColumn << ": "
                     << qPrintable(errorStr) ;
            qDebug()<<"failed to load current file";
        }
        m_oldDefaultFile.close();
    }

    QDomElement root = doc.firstChildElement();

    if(m_oldDefaultFile.exists()) {
        if(root.namedItem("Session").hasChildNodes() == false) {
            if(m_oldDefaultFile.isOpen()) {
                m_oldDefaultFile.close();
            }

            qDebug()<<" DEFAULT REMOVE ";
            m_oldDefaultFile.remove();
        }
    }

    /* DELETE CURRENT OLD FILE */
    if(m_oldCurrentFile.exists()) {
        if(m_oldCurrentFile.isOpen()) {
            m_oldCurrentFile.close();
        }

        m_oldCurrentFile.remove();
    }

    /* DELETE CSV OLD DIRECTORY */ // Je ne sais pas si c'est très judicieux, si l'utilisateur à déjà des fichiers à l'intérieur!
    /*if(m_oldCSVDir.exists()) {
        QDir().rmdir(path.toString()+ QString("/AllianTech/ATomicSound/CSV"));
    }*/
}

/**
*******************************************************************************
** @fn  quint8 GetConfiguration::cfgState()
** @param[in] void
** @param[out] quint8 current Configuration's state
** @brief returns the current Configuration status
** @return
**
*******************************************************************************
**/
quint8 GetConfiguration::audioCfgState() {
    return static_cast<quint8>(m_eStateCfg);
}

/**
*******************************************************************************
** @fn  void GetConfiguration::setCfgState(quint8 uiState)
** @param[in] quint8 uiState  current state
** @param[out] void
** @brief sets the current configuration status
** @return
**
*******************************************************************************
**/
void GetConfiguration::setAudioCfgState(quint8 uiState) {
    if(m_oAudioGet->sonoState()==SM_SONOMETER_READY) {
        // The configuration state can be accessed and configuration enabled only when the application is in
        // the SM_SONOMETER_READY state
        if(uiState != m_eStateCfg) {
            m_eStateCfg = static_cast<TeStateCfg>(uiState);
            setCfgState(m_eStateCfg);
            emit audioCfgStateChanged();
        }

    } else {
        // in state other than SM_SONOMETER_READY, the configuration of the ATomic card is disabled
        m_eStateCfg = static_cast<TeStateCfg>(CFG_DISABLED);
        setCfgState(m_eStateCfg);
        emit audioCfgStateChanged();
    }
}

/**
*******************************************************************************
** @fn  quint8 GetConfiguration::libCalibStatus()
** @param[in] void
** @param[out] quint8
** @brief gets the calibration's engine status
**         [depending upon the library]
** @return
**
*******************************************************************************
**/
quint8 GetConfiguration::libAudioCalibStatus() {
    return static_cast<quint8>(m_eStatusOfCalibration);
}

/**
*******************************************************************************
** @fn  void GetConfiguration::setLibCalibStatus(quint8 uiState)
** @param[in] quint8 uiState  current calibration's state
** @param[out] void
** @brief sets the current calibration status
** @return
**
*******************************************************************************
**/
void GetConfiguration::setLibAudioCalibStatus(quint8 uiState) {
    // the library-calibration state mirrors the calibration progress
    // this state is set depending on the AcousticMeasSo status of calibration
    if(m_ePreviousStatusOfCalibration != uiState && m_eStatusOfCalibration != uiState) {
        m_ePreviousStatusOfCalibration = m_eStatusOfCalibration;
        m_eStatusOfCalibration = static_cast<TeStatusOfCalibration>(uiState);

    }

    setLibCalibStatus(m_eStatusOfCalibration);
    emit libAudioCalibStatusChanged();
}

/**
*******************************************************************************
** @fn  QString GetConfiguration::getLabSensitivity()
** @param[in] void
** @param[out] QString
** @brief gets the laboratory sensitivity of the ATomic card
** @return QString
**
*******************************************************************************
**/
QString GetConfiguration::getAudioLabSensitivity() {
    return m_cLabSensitivity;
}

/**
*******************************************************************************
** @fn  void GetConfiguration::setLabSensitivity(QString cLabSensitivity)
** @param[in] QString cLabSensitivity  laboratory sensitivity entered
** @param[out] void
** @brief sets the laboratory sensitivity
** @return void
**
*******************************************************************************
**/
void GetConfiguration::setAudioLabSensitivity(QString cLabSensitivity) {
    if(m_cLabSensitivity != cLabSensitivity) {
        m_cLabSensitivity = QString::number(cLabSensitivity.toDouble(),'f',2);
        setLabSensitivity(m_cLabSensitivity);
        emit audiolabSensitivityChanged();
        setRealSensivity(QString::number(m_cLabSensitivity.toDouble(),'f',2));
        setCalibLabSensitivity(QString::number(m_cLabSensitivity.toDouble(),'f',2));
    }
}

/**
*******************************************************************************
** @fn  QString GetConfiguration::getCalibrationDate()
** @param[in] void
** @param[out] QString
** @brief gets the last date of calibration
** @return QString
**
*******************************************************************************
**/
QString GetConfiguration::getCalibrationDate() {
    return m_cCalibrationDate;
}

/**
*******************************************************************************
** @fn  void GetConfiguration::setCalibrationDate(QString cCalibrationDate)
** @param[in] QString cCalibrationDate  date of calibration
** @param[out] void
** @brief sets the full date of the calibration
** @return void
**
*******************************************************************************
**/
void GetConfiguration::setCalibrationDate(QString cCalibrationDate) {
    m_cCalibrationDate = cCalibrationDate;

    QString cDaysection=m_cCalibrationDate.section("/",-1);
    QString cDay=cDaysection.section(" ",0,0);
    setCalibrationDay(cDay);

    QString cMonth=m_cCalibrationDate.section("/",1,1);
    setCalibrationYear(cMonth);

    QString cYear=m_cCalibrationDate.section("/",0,0);
    setCalibrationMonth(cYear);

    emit calibrationDateChanged();
}

/**
*******************************************************************************
** @fn  void GetConfiguration::setFilesName(QString cFilesName)
** @param[in]QString cFilesName  filename
** @param[out] void
** @brief sets the filename (for Data-CSV and WAV files)
** @return void
**:
*******************************************************************************
**/
void GetConfiguration::onFilesNameChanged(QString cFilesName) {
    if(m_cFilesName != cFilesName) {
        if(!cFilesName.contains("\\") && !cFilesName.contains("/") &&
                !cFilesName.contains("\"") && !cFilesName.contains(":") &&
                !cFilesName.contains("*") && !cFilesName.contains("?") &&
                !cFilesName.contains("<") && !cFilesName.contains(">") && !cFilesName.contains("|")) {

            m_cFilesName = cFilesName;
            m_oAudioGet->setFilename(m_cFilesName);

        } else {
            setFilesName(m_cFilesName);
        }
    }
}

/**
*******************************************************************************
** @fn  QString GetConfiguration::getLastCalibrationDate()
** @param[in] void
** @param[out] QString
** @brief gets the last calibration full date
** @return QString
**
*******************************************************************************
**/
QString GetConfiguration::getLastCalibrationDate() {
    return m_cLastCalibrationDate;
}

/**
*******************************************************************************
** @fn  void GetConfiguration::setLastCalibrationDate
**                                          (const QString cLastCalibrationDate)
** @param[in]QString cLastCalibrationDate last calibration date
** @param[out] void
** @brief sets the last calibration date
** @return void
**
*******************************************************************************
**/
void GetConfiguration::setLastCalibrationDate(const QString cLastCalibrationDate) {
    m_cLastCalibrationDate = cLastCalibrationDate;
    QString cDaySection=m_cLastCalibrationDate.section("/",-1);

    setLastCalibrationDateDay(cDaySection.section(" ",0,0));
    setLastcalibrationDateYear(m_cLastCalibrationDate.section("/",0,0));
    setLastcalibrationDateMonth(m_cLastCalibrationDate.section("/",1,1));

    emit lastCalibrationDateChanged();
}

/**
*******************************************************************************
** @fn  void updateCurrentFile(QString configFilePath)
** @param[in] void
** @param[out] void
** @brief
** @return void
**
*******************************************************************************
**/
void GetConfiguration::writeCurrentFile() {
    bool bInternalMicro = true;

    if(MIN_THRESHOLD(tolerance().toDouble(), nominalSensitivity().toDouble()) <= labSensitivity().toDouble() &&
            labSensitivity().toDouble() <= MAX_THRESHOLD(tolerance().toDouble(), nominalSensitivity().toDouble())) {

        // Update Current XML
        QDomDocument document;

        //QDomText setup = document.createTextNode("<?xml version='1.0' encoding='utf-8'?>\n");
        //document.appendChild(setup);

        QDomElement config = document.createElement("Configuration");
        document.appendChild(config);

        QDomElement version = document.createElement("SoftwareVersion");
        config.appendChild(version);

        QDomElement hard = document.createElement("HardwareVersion");
        config.appendChild(hard);

        QDomElement num = document.createElement("SensorNumber");
        config.appendChild(num);

        QDomElement root = document.createElement("Sensor");
        config.appendChild(root);

        QDomElement calibration = document.createElement("Calibration");
        config.appendChild(calibration);

        QDomElement check = document.createElement("Check");
        config.appendChild(check);

        QDomElement measure = document.createElement("Measure");
        config.appendChild(measure);

        QDomElement vib = document.createElement("MeasureVib");
        config.appendChild(vib);

        QDomElement building = document.createElement("Buildings");
        config.appendChild(building);

        QDomElement session = document.createElement("Session");
        config.appendChild(session);

        if(m_oAudioGet->m_eDeviceModel != 4) {
            bInternalMicro = false;
        } else {
            bInternalMicro = true;
        }

        /// SOFTWARE
        /*** SoftwareVersion : Product ***/
        auto product = document.createElement("Product");
        product.appendChild(document.createTextNode(traverseSelectedFile("SoftwareVersion","Product","","Second Level")));
        version.appendChild(product);

        /*** SoftwareVersion : AppliVersion ***/
        auto appliversion = document.createElement("AppliVersion");
        appliversion.appendChild(document.createTextNode(traverseSelectedFile("SoftwareVersion","AppliVersion","","Second Level")));
        version.appendChild(appliversion);

        /*** SoftwareVersion : XMLVersion ***/
        auto xmlversion = document.createElement("XMLVersion");
        xmlversion.appendChild(document.createTextNode(traverseSelectedFile("SoftwareVersion","XMLVersion","","Second Level")));
        version.appendChild(xmlversion);

        if(!bInternalMicro) {
            m_oAudioGet->getATomicInfo();
        } else {
            qDebug()<<"We Use INTERNAL MICROPHONE !";
        }

        /// HARDWARE
        /*** HardwareVersion : Name ***/
        auto hardname = document.createElement("Name");
        hardname.appendChild(document.createTextNode(traverseSelectedFile("HardwareVersion","Name","","Second Level")));
        hard.appendChild(hardname);

        /*** HardwareVersion : SN ***/
        auto hardsn = document.createElement("SN");
        if(!bInternalMicro) {
            hardsn.appendChild(document.createTextNode(m_oAudioGet->serialNumber().mid(m_oAudioGet->serialNumber().indexOf('#',0,Qt::CaseInsensitive),6)));
        } else {
            hardsn.appendChild(document.createTextNode(traverseSelectedFile("HardwareVersion","SN","","Second Level")));
        }
        hard.appendChild(hardsn);

        /*** HardwareVersion : UsbProductId ***/
        auto usbid = document.createElement("UsbProductId");
        if(!bInternalMicro) {
            usbid.appendChild(document.createTextNode(m_oAudioGet->productId()));
        } else {
            usbid.appendChild(document.createTextNode(traverseSelectedFile("HardwareVersion","UsbProductId","","Second Level")));
        }
        hard.appendChild(usbid);

        /*** HardwareVersion : UsbSerialNumber ***/
        auto usbserialnum = document.createElement("UsbSerialNumber");
        if(!bInternalMicro) {
            usbserialnum.appendChild(document.createTextNode(m_oAudioGet->serialNumber()));
        } else {
            usbserialnum.appendChild(document.createTextNode(traverseSelectedFile("HardwareVersion","UsbSerialNumber","","Second Level")));
        }
        hard.appendChild(usbserialnum);

        /*** HardwareVersion : NumberOfSensors ***/
        auto numOfSensor = document.createElement("NumberOfSensors");
        numOfSensor.appendChild(document.createTextNode(traverseSelectedFile("HardwareVersion","NumberOfSensors","","Second Level")));
        hard.appendChild(numOfSensor);

        /*** HardwareVersion : NumberOfChannels ***/
        auto numberOfChannels = document.createElement("NumberOfChannels");
        numberOfChannels.appendChild(document.createTextNode(traverseSelectedFile("HardwareVersion","NumberOfChannels","","Second Level")));
        hard.appendChild(numberOfChannels);

        /*** HardwareVersion : Chan1FullScale_mV ***/
        auto chan1FullScale_mV = document.createElement("Chan1FullScale_mV");

        if(!bInternalMicro) {
            QString serialNum= m_oAudioGet->serialNumber();
            QString sFs1Tag = "F1/";  //Patch_FullScaleMethod

            int iFirstF1 =  serialNum.indexOf(sFs1Tag) + sFs1Tag.length();
            QStringRef subStringFS1(&serialNum, iFirstF1, FULLSCALE_MAX_CHAR_COUNT); //alliantech

            //Retrieve first Fullscale
            for(int i = FULLSCALE_MAX_CHAR_COUNT; i > FULLSCALE_MIN_CHAR_COUNT; i--) {
                QChar cLastChar = subStringFS1.back();
                //check if last character is NOT digit or a valid decimal separator
                if ((cLastChar.digitValue()== -1) && (cLastChar != '.') && (cLastChar != ',')) {
                    //Remove last chart if is not a digit!
                    subStringFS1.chop(1);

                } else {
                    //If a valid char if found we consider that we have the whole
                    break;
                }
            }

            chan1FullScale_mV.appendChild(document.createTextNode(subStringFS1.toString()));
        } else {
            chan1FullScale_mV.appendChild(document.createTextNode(traverseSelectedFile("HardwareVersion","Chan1FullScale_mV","","Second Level")));
        }
        hard.appendChild(chan1FullScale_mV);

        /*** HardwareVersion : Chan2FullScale_mV ***/
        auto chan2FullScale_mV = document.createElement("Chan2FullScale_mV");
        if(!bInternalMicro) {
            QString serialNum= m_oAudioGet->serialNumber();
            QString sFs2Tag = "F2/";  //Patch_FullScaleMethod

            int iFirstF2 =  serialNum.indexOf(sFs2Tag) + sFs2Tag.length();
            QStringRef subStringFS2(&serialNum, iFirstF2, FULLSCALE_MAX_CHAR_COUNT); //alliantech

            //Retrieve first Fullscale
            for(int i = FULLSCALE_MAX_CHAR_COUNT; i > FULLSCALE_MIN_CHAR_COUNT; i--) {
                QChar cLastChar = subStringFS2.back();
                //check if last character is NOT digit or a valid decimal separator
                if ((cLastChar.digitValue() == -1) && (cLastChar != '.') && (cLastChar != ',')) {
                    //Remove last chart if is not a digit!
                    subStringFS2.chop(1);

                } else {
                    //If a valid char if found we consider that we have the whole
                    break;
                }
            }

            chan2FullScale_mV.appendChild(document.createTextNode(subStringFS2.toString()));
        } else {
            chan2FullScale_mV.appendChild(document.createTextNode(traverseSelectedFile("HardwareVersion","Chan2FullScale_mV","","Second Level")));
        }
        hard.appendChild(chan2FullScale_mV);

        /*** HardwareVersion : UserDefinedFullScale ***/
        auto userDefinedFullScale = document.createElement("UserDefinedFullScale");
        userDefinedFullScale.appendChild(document.createTextNode(traverseSelectedFile("HardwareVersion","UserDefinedFullScale","","Second Level")));
        hard.appendChild(userDefinedFullScale);

        /*** HardwareVersion : CIC ***/
        auto cIC = document.createElement("CIC");
        cIC.appendChild(document.createTextNode(traverseSelectedFile("HardwareVersion","CIC","","Second Level")));
        hard.appendChild(cIC);

        /*** HardwareVersion : TEDS ***/
        auto tEDS = document.createElement("TEDS");
        tEDS.appendChild(document.createTextNode(traverseSelectedFile("HardwareVersion","TEDS","","Second Level")));
        hard.appendChild(tEDS);

        /// SENSOR NUMBER
        /*** SENSOR : Number ***/
        auto sensorNumber = document.createElement("SensorNumber");
        sensorNumber.appendChild(document.createTextNode(traverseSelectedFile("SensorNumber","","","First Level")));
        config.appendChild(sensorNumber);

        /// SENSOR
        /*** SENSOR : Type Microphone ***/
        auto typeMicro = document.createElement("Type");
        typeMicro.appendChild(document.createTextNode(traverseSelectedFile("Sensor","Type","","Second Level")));
        root.appendChild(typeMicro);

        /*** SENSOR : Name Microphone ***/
        auto typeMicrophone = document.createElement("Name");
        /*if(!bInternalMicro) {
            m_oAudioGet->getATomicInfo();
        } else {
            qDebug()<<"We Use INTERNAL MICROPHONE !";
        }*/

        if(microphone()=="") {
            QString val = traverseSelectedFile("Sensor","Name","","Second Level");
            typeMicrophone.appendChild(document.createTextNode(val));

        } else {
            typeMicrophone.appendChild(document.createTextNode(microphone()));
        }
        root.appendChild(typeMicrophone);

        /*** SENSOR : Serial Number ***/
        auto serialNumbertype = document.createElement("SensorSN");
        /*if(!bInternalMicro) {
            m_oAudioGet->getATomicInfo();
        } else {
            qDebug()<<"We Use INTERNAL MICROPHONE !";
        }*/

        if(serialNumber()=="") {
            QString val = traverseSelectedFile("Sensor","SensorSN","","Second Level");
            serialNumbertype.appendChild(document.createTextNode(val));

        } else {
            serialNumbertype.appendChild(document.createTextNode(serialNumber()));
        }
        root.appendChild(serialNumbertype);

        /*** SENSOR : Nominal Senstivity SI ***/
        auto sensivity = document.createElement("NominalSenstivitySI");
        /*if(!bInternalMicro) {
            m_oAudioGet->getATomicInfo();
        } else {
            qDebug()<<"We Use INTERNAL MICROPHONE !";
        }*/

        if(nominalSensitivity().isEmpty()) {
            QString val = traverseSelectedFile("Sensor","NominalSenstivitySI","","Second Level");
            sensivity.appendChild(document.createTextNode(val));

        } else {
            sensivity.appendChild(document.createTextNode(nominalSensitivity()));
        }
        root.appendChild(sensivity);

        /*** SENSOR : Tolerance Nominal Sensitivity dB ***/
        auto tolerancetype = document.createElement("ToleranceNominalSensitivitydB");
        /*if(!bInternalMicro) {
            m_oAudioGet->getATomicInfo();
        } else {
            qDebug()<<"We Use INTERNAL MICROPHONE !";
        }*/

        if(tolerance().isEmpty()) {
            QString val=traverseSelectedFile("Sensor","ToleranceNominalSensitivitydB","","Second Level");
            tolerancetype.appendChild(document.createTextNode(val));

        } else {
            tolerancetype.appendChild(document.createTextNode(tolerance()));
        }
        root.appendChild(tolerancetype);

        /*** SENSOR : ConversionFactorChan ***/
        auto conversionFactorChan = document.createElement("ConversionFactorChan");
        if(!bInternalMicro) {
            QString serialNum= m_oAudioGet->serialNumber();
            QString sFs1Tag = "F1/";  //Patch_FullScaleMethod

            int iFirstF1 =  serialNum.indexOf(sFs1Tag) + sFs1Tag.length();
            QStringRef subStringFS1(&serialNum, iFirstF1, FULLSCALE_MAX_CHAR_COUNT); //alliantech

            //PatchMetaDynam - prepare params for the function
            int iFullScaleDriver = 1; // 1 for float PCM
            float fLabSensitivity;//
            if(labSensitivity().isEmpty()) {
                QString val=traverseSelectedFile("Sensor","LaboratorySensitivitySI","","Second Level");
                fLabSensitivity = val.toFloat();//PatchMetaDynam

            } else {
                fLabSensitivity = labSensitivity().toFloat();//PatchMetaDynam
            }

            float fCalibrationCorrection;
            if(calibrationCorrection1()=="") {
                fCalibrationCorrection = 0.0f;

            } else {
                fCalibrationCorrection = calibrationCorrection1().toFloat();
            }

            float fFullScaleCh1_mV = subStringFS1.toString().toFloat();
            //QString sAux = "5500";
            //float fFullScaleCh1_mV = sAux.toFloat();

            // Calculate Real Sensitivity (Real Sensitivity = Nominal Sensitivity * 10^(Calibration Correcton/20))
            float fFixedSensitivity = fLabSensitivity * static_cast<float>(qPow(10.0f, (fCalibrationCorrection / 20.0f)));

            // Calculate Conversion factor (Conversion Factor = Full Scale Voltage / (Full Scale Driver * Real Sensivity))
            float fConversionFactorChan = fFullScaleCh1_mV / (iFullScaleDriver * fFixedSensitivity);

            conversionFactorChan.appendChild(document.createTextNode(QString::number(fConversionFactorChan)));
        } else {
            conversionFactorChan.appendChild(document.createTextNode(traverseSelectedFile("Sensor","ConversionFactorChan","","Second Level")));
        }
        root.appendChild(conversionFactorChan);

        /*** SENSOR : Accreditation Date ***/
        auto dateAcc = document.createElement("LaboratoryAccreditationDate");
        if(m_currentFile.exists()) {
            QString valu = traverseCurrentFile("Sensor","LaboratorySensitivitySI","","Second Level");
            QString valud = traverseSelectedFile("Sensor","LaboratorySensitivitySI","","Second Level");
            if(valu != labSensitivity()) {
                QString dat = QDateTime().currentDateTime().toString("yyyy/MM/dd hh:mm");
                setCalibrationDate(dat);
                dateAcc.appendChild(document.createTextNode(dat));

            } else {
                QString dat = traverseCurrentFile("Sensor","LaboratoryAccreditationDate","","Second Level");
                setCalibrationDate(dat);
                dateAcc.appendChild(document.createTextNode(dat));
            }

        } else {
            dateAcc.appendChild(document.createTextNode(traverseSelectedFile("Sensor","AccreditationDate","","Second Level")));
        }
        root.appendChild(dateAcc);

        /*** SENSOR : Laboratory Accreditation Date ***/
        auto date = document.createElement("LaboratoryAccreditationDate");
        date.appendChild(document.createTextNode(traverseSelectedFile("Sensor","LaboratoryAccreditationDate","","Second Level")));
        root.appendChild(date);

        /*** SENSOR : Laboratory Sensitivity SI ***/
        auto labsensivity = document.createElement("LaboratorySensitivitySI");
        if(labSensitivity().isEmpty()) {
            QString val = traverseSelectedFile("Sensor","LaboratorySensitivitySI","","Second Level");
            labsensivity.appendChild(document.createTextNode(val));

        } else {
            labsensivity.appendChild(document.createTextNode(labSensitivity()));
        }
        root.appendChild(labsensivity);

        /*** SENSOR : Wind Screen ***/
        auto windScreen = document.createElement("WindScreen");
        windScreen.appendChild(document.createTextNode(traverseSelectedFile("Sensor","WindScreen","","Second Level")));
        root.appendChild(windScreen);

        /*** SENSOR : FreeField Correction dB ***/
        auto freeFieldCorrectiondB = document.createElement("FreeFieldCorrectiondB");
        freeFieldCorrectiondB.appendChild(document.createTextNode(traverseSelectedFile("Sensor","FreeFieldCorrectiondB","","Second Level"))); //"-0.4" ToDo: implement WindScreen Field to enable or disable FreeFieldCorrenction
        root.appendChild(freeFieldCorrectiondB);

        /*** SENSOR : Class ***/
        auto sensorclass = document.createElement("Class");
        if(classConfig().isEmpty()) {
            QString val=traverseSelectedFile("Sensor","Class","","Second Level");
            sensorclass.appendChild(document.createTextNode(val));

        } else {
            sensorclass.appendChild(document.createTextNode(classConfig()));
        }
        root.appendChild(sensorclass);

        /*** SENSOR : TEDS ***/
        auto teds = document.createElement("TEDS");
        teds.appendChild(document.createTextNode(traverseSelectedFile("Sensor","TEDS","","Second Level")));
        root.appendChild(teds);

        /*** SENSOR : TEDSAddress ***/
        auto tEDSAddress = document.createElement("TEDSAddress");
        tEDSAddress.appendChild(document.createTextNode(traverseSelectedFile("Sensor","TEDSAddress","","Second Level")));
        root.appendChild(tEDSAddress);

        /*** SENSOR : User Units ***/
        auto userUnits = document.createElement("UserUnits");
        userUnits.appendChild(document.createTextNode(traverseSelectedFile("Sensor","UserUnits","","Second Level")));
        root.appendChild(userUnits);

        /// CALIBRATION
        /*** Calibration : Level ***/
        auto caliblevel = document.createElement("Level");
        if(QString::number(calibrationLevel())=="") {
            QString val=traverseSelectedFile("Calibration","Level","","Second Level");
            caliblevel.appendChild(document.createTextNode(val));

        } else {
            caliblevel.appendChild(document.createTextNode(QString::number(qCeil(calibrationLevel()))));
        }
        calibration.appendChild(caliblevel);

        /*** Calibration : Date ***/
        auto calibdate = document.createElement("Date");
        if(m_currentFile.exists()) {
            if(m_bIsCalibration) { //AllianTech - Patch Calibration Date
                //If save config after calibration
                QString newdate =QDateTime().currentDateTime().toString("yyyy/MM/dd hh:mm");
                setLastCalibrationDate(newdate);
                calibdate.appendChild(document.createTextNode(newdate));

            } else {
                //If save config from config screen
                QString lastdate = traverseCurrentFile("Calibration","Date","","Second Level");
                //QString lastdate=QFileInfo(m_currentFile).lastModified().toString("yyyy/MM/dd hh:mm");
                setLastCalibrationDate(lastdate);
                calibdate.appendChild(document.createTextNode(lastdate));
            }

        } else {
            calibdate.appendChild(document.createTextNode(traverseSelectedFile("Calibration","Date","","Second Level")));
        }
        calibration.appendChild(calibdate);

        /*** Calibration : Calibration Correction dB ***/
        auto calibrationCorrectiondB = document.createElement("CalibrationCorrectiondB");
        if(calibrationCorrection1()=="") {
            calibrationCorrectiondB.appendChild(document.createTextNode("0"));

        } else {
            calibrationCorrectiondB.appendChild(document.createTextNode(calibrationCorrection1()));
        }
        calibration.appendChild(calibrationCorrectiondB);

        /*** Calibration : Stability Level dB ***/
        auto calibrationStabilityLeveldB = document.createElement("StabilityLeveldB");
        calibrationStabilityLeveldB.appendChild(document.createTextNode(traverseSelectedFile("Calibration","StabilityLeveldB","","Second Level")));
        calibration.appendChild(calibrationStabilityLeveldB);

        /*** Calibration : Selected Ch ***/
        auto selectedCh = document.createElement("SelectedCh");
        selectedCh.appendChild(document.createTextNode(traverseSelectedFile("Calibration","SelectedCh","","Second Level")));
        calibration.appendChild(selectedCh);

        /// CHECK
        /*** Check : CIC ***/
        auto checkCIC = document.createElement("CIC");
        checkCIC.appendChild(document.createTextNode(traverseSelectedFile("Check","CIC","","Second Level")));
        check.appendChild(checkCIC);

        /*** Check : SignalType1 ***/
        auto checkSignalType1 = document.createElement("SignalType1");
        checkSignalType1.appendChild(document.createTextNode(traverseSelectedFile("Check","SignalType1","","Second Level")));
        check.appendChild(checkSignalType1);

        /*** Check : Signal Level 1 Percent ***/
        auto checkSignalLevel1Percent = document.createElement("SignalLevel1Percent");
        checkSignalLevel1Percent.appendChild(document.createTextNode(traverseSelectedFile("Check","SignalLevel1Percent","","Second Level")));
        check.appendChild(checkSignalLevel1Percent);

        /*** Check : Signal Frequency 1 ***/
        auto checkSignalFrequency1 = document.createElement("SignalFrequency1");
        checkSignalFrequency1.appendChild(document.createTextNode(traverseSelectedFile("Check","SignalFrequency1","","Second Level")));
        check.appendChild(checkSignalFrequency1);

        /*** Check : Signal Type 2 ***/
        auto checkSignalType2 = document.createElement("SignalType2");
        checkSignalType2.appendChild(document.createTextNode(traverseSelectedFile("Check","SignalType2","","Second Level")));
        check.appendChild(checkSignalType2);

        /*** Check : Signal Level 2 Percent ***/
        auto checkSignalLevel2Percent = document.createElement("SignalLevel2Percent");
        checkSignalLevel2Percent.appendChild(document.createTextNode(traverseSelectedFile("Check","SignalLevel2Percent","","Second Level")));
        check.appendChild(checkSignalLevel2Percent);

        /*** Check : Signal Frequency 2 ***/
        auto checkSignalFrequency2 = document.createElement("SignalFrequency2");
        checkSignalFrequency2.appendChild(document.createTextNode(traverseSelectedFile("Check","SignalFrequency2","","Second Level")));
        check.appendChild(checkSignalFrequency2);

        /*** Check : Signal Type 3 ***/
        auto checkSignalType3 = document.createElement("SignalType3");
        checkSignalType3.appendChild(document.createTextNode(traverseSelectedFile("Check","SignalType3","","Second Level")));
        check.appendChild(checkSignalType3);

        /*** Check : Signal Level 3 Percent ***/
        auto checkSignalLevel3Percent = document.createElement("SignalLevel3Percent");
        checkSignalLevel3Percent.appendChild(document.createTextNode(traverseSelectedFile("Check","SignalLevel3Percent","","Second Level")));
        check.appendChild(checkSignalLevel3Percent);

        /*** Check : Signal Frequency 3 ***/
        auto checkSignalFrequency3 = document.createElement("SignalFrequency3");
        checkSignalFrequency3.appendChild(document.createTextNode(traverseSelectedFile("Check","SignalFrequency3","","Second Level")));
        check.appendChild(checkSignalFrequency3);

        /*** Check : Max Deviation dB ***/
        auto checkMaxDeviationdB= document.createElement("MaxDeviationdB");
        checkMaxDeviationdB.appendChild(document.createTextNode(traverseSelectedFile("Check","MaxDeviationdB","","Second Level")));
        check.appendChild(checkMaxDeviationdB);

        /// MEASURE
        /*** Measure : Configuration Type ***/
        auto measureconfigurationType = document.createElement("ConfigurationType");
        measureconfigurationType.appendChild(document.createTextNode(traverseSelectedFile("Measure","ConfigurationType","","Second Level")));
        measure.appendChild(measureconfigurationType);

        /*** Measure : Mode ***/
        auto mode = document.createElement("Mode");
        mode.appendChild(document.createTextNode(traverseSelectedFile("Measure","Mode","","Second Level")));
        measure.appendChild(mode);

        /*** Measure : Input Channel ***/
        auto measureInputChannel = document.createElement("InputChannel");
        measureInputChannel.appendChild(document.createTextNode(traverseSelectedFile("Measure","InputChannel","","Second Level")));
        measure.appendChild(measureInputChannel);

        /*** Measure : ACZ Time Weighting ***/
        auto measureACZTimeWeighting = document.createElement("ACZTimeWeighting");
        if(timeWeighting()=="") {
            QString val=traverseSelectedFile("Measure","ACZTimeWeighting","","Second Level");
            measureACZTimeWeighting.appendChild(document.createTextNode(val));

        } else {
            measureACZTimeWeighting.appendChild(document.createTextNode(timeWeighting()));
        }
        measure.appendChild(measureACZTimeWeighting);

        /*** Measure : Spectrum Type ***/
        auto measureSpectrumType = document.createElement("SpectrumType");
        if(spectrumConfiguration()=="") {
            QString val=traverseSelectedFile("Measure","SpectrumType","","Second Level");
            measureSpectrumType.appendChild(document.createTextNode(val));

        } else {
            measureSpectrumType.appendChild(document.createTextNode(spectrumConfiguration()));
        }
        measure.appendChild(measureSpectrumType);

        QDomElement spectrumConfig = document.createElement("SpectrumConfig");
        measure.appendChild(spectrumConfig);

        /// SPECTRUM CONFIG
        /*** Spectrum Config : Spectrum ***/
        auto eSpectrum = document.createElement("eSpectrum");
        eSpectrum.appendChild(document.createTextNode(traverseSelectedFile("Measure","SpectrumConfig","eSpectrum","Third Level")));
        spectrumConfig.appendChild(eSpectrum);

        /*** Spectrum Config : Window ***/
        auto eWindow = document.createElement("eWindow");
        eWindow.appendChild(document.createTextNode(traverseSelectedFile("Measure","SpectrumConfig","eWindow","Third Level")));
        spectrumConfig.appendChild(eWindow);

        /*** Spectrum Config : FFT Size ***/
        auto eFFTSize = document.createElement("eFFTSize");
        eFFTSize.appendChild(document.createTextNode(traverseSelectedFile("Measure","SpectrumConfig","eFFTSize","Third Level")));
        spectrumConfig.appendChild(eFFTSize);

        /*** Spectrum Config : FFT Averaging ***/
        auto eFFTAveraging = document.createElement("eFFTAveraging");
        eFFTAveraging.appendChild(document.createTextNode(traverseSelectedFile("Measure","SpectrumConfig","eFFTAveraging","Third Level")));
        spectrumConfig.appendChild(eFFTAveraging);

        /*** Spectrum Config : FFT Overlap Percent ***/
        auto eFFTOverlapPercent = document.createElement("eFFTOverlapPercent");
        eFFTOverlapPercent.appendChild(document.createTextNode(traverseSelectedFile("Measure","SpectrumConfig","eFFTOverlapPercent","Third Level")));
        spectrumConfig.appendChild(eFFTOverlapPercent);

        /*** Spectrum Config : FFT Output Unit ***/
        auto eFFTOutputUnit = document.createElement("eFFTOutputUnit");
        eFFTOutputUnit.appendChild(document.createTextNode(traverseSelectedFile("Measure","SpectrumConfig","eFFTOutputUnit","Third Level")));
        spectrumConfig.appendChild(eFFTOutputUnit);

        /*** Spectrum Config : Zoom Frequency Hz ***/
        auto fZoomFrequencyHz = document.createElement("fZoomFrequencyHz");
        fZoomFrequencyHz.appendChild(document.createTextNode(traverseSelectedFile("Measure","SpectrumConfig","fZoomFrequencyHz","Third Level")));
        spectrumConfig.appendChild(fZoomFrequencyHz);

        /*** Spectrum Config : Zoom Factor ***/
        auto eZoomFactor = document.createElement("eZoomFactor");
        eZoomFactor.appendChild(document.createTextNode(traverseSelectedFile("Measure","SpectrumConfig","eZoomFactor","Third Level")));
        spectrumConfig.appendChild(eZoomFactor);

        /*** Spectrum Config : Max Band width Hz ***/
        auto fMaxBandwidthHz = document.createElement("fMaxBandwidthHz");
        fMaxBandwidthHz.appendChild(document.createTextNode(traverseSelectedFile("Measure","SpectrumConfig","fMaxBandwidthHz","Third Level")));
        spectrumConfig.appendChild(fMaxBandwidthHz);

        /// MEASURE
        /*** Measure : Spectrum Time Weighting ***/
        auto measureSpectrumTimeWeighting = document.createElement("SpectrumTimeWeighting");
        measureSpectrumTimeWeighting.appendChild(document.createTextNode(traverseSelectedFile("Measure","SpectrumTimeWeighting","","Second Level")));
        measure.appendChild(measureSpectrumTimeWeighting);

        /*** Measure : Integration Time Sec ***/
        auto measureIntegrationTimeSec= document.createElement("IntegrationTimeSec");
        if(m_currentFile.exists()) {
            if(QString::number(integrationTime())=="") {
                QString val = traverseCurrentFile("Measure","IntegrationTimeSec","","Second Level");
                measureIntegrationTimeSec.appendChild(document.createTextNode(val));

            } else {
                measureIntegrationTimeSec.appendChild(document.createTextNode(QString::number(integrationTime())));
            }

        } else {
            measureIntegrationTimeSec.appendChild(document.createTextNode(traverseSelectedFile("Measure","IntegrationTimeSec","","Second Level")));
        }
        measure.appendChild(measureIntegrationTimeSec);

        /*** Measure : Fast Integration Time Milli Sec ***/
        auto measureFastIntegrationTimeMilliSec= document.createElement("FastIntegrationTimeMilliSec");
        measureFastIntegrationTimeMilliSec.appendChild(document.createTextNode(traverseSelectedFile("Measure","FastIntegrationTimeMilliSec","","Second Level")));
        measure.appendChild(measureFastIntegrationTimeMilliSec);

        /*** Measure : Max Wav Size In Minutes ***/
        auto maxWavSizeInMinutes = document.createElement("MaxWavSizeInMinutes");
        maxWavSizeInMinutes.appendChild(document.createTextNode(traverseSelectedFile("Measure","MaxWavSizeInMinutes","","Second Level")));
        measure.appendChild(maxWavSizeInMinutes);

        /*** Measure : Start Slm With Wav ***/
        auto startSlmWithWav = document.createElement("StartSlmWithWav");
        startSlmWithWav.appendChild(document.createTextNode(traverseSelectedFile("Measure","StartSlmWithWav","","Second Level")));
        measure.appendChild(startSlmWithWav);

        /*** Measure : Enable Spectrogram ***/
        auto enableSpec = document.createElement("EnableSpectrogram");
        enableSpec.appendChild(document.createTextNode(traverseSelectedFile("Measure","EnableSpectrogram","","Second Level")));
        measure.appendChild(enableSpec);

        /// MEASURE VIB
        /*** Measure Vib : Configuration Type ***/
        auto vibConfigurationType = document.createElement("ConfigurationType");
        vibConfigurationType.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","ConfigurationType","","Second Level")));
        vib.appendChild(vibConfigurationType);

        /*** Measure Vib : Mode ***/
        auto modeVib = document.createElement("Mode");
        modeVib.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","Mode","","Second Level")));
        vib.appendChild(modeVib);

        /*** Measure Vib : Input Channel ***/
        auto vibInputChannel = document.createElement("InputChannel");
        vibInputChannel.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","InputChannel","","Second Level")));
        vib.appendChild(vibInputChannel);

        /*** Measure Vib : ACZ Time Weighting ***/
        auto vibACZTimeWeighting = document.createElement("ACZTimeWeighting");
        vibACZTimeWeighting.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","ACZTimeWeighting","","Second Level")));
        vib.appendChild(vibACZTimeWeighting);

        QDomElement vibSpectrumConfig = document.createElement("SpectrumConfig");
        vib.appendChild(vibSpectrumConfig);

        /// SPECTRUM CONFIG
        /*** Spectrum Config : Spectrum ***/
        auto vibSpectrum = document.createElement("eSpectrum");
        vibSpectrum.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","SpectrumConfig","eSpectrum","Third Level")));
        vibSpectrumConfig.appendChild(vibSpectrum);

        /*** Spectrum Config : Window ***/
        auto vibWindow = document.createElement("eWindow");
        vibWindow.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","SpectrumConfig","eWindow","Third Level")));
        vibSpectrumConfig.appendChild(vibWindow);

        /*** Spectrum Config : FFT Size ***/
        auto vibFFTSize = document.createElement("eFFTSize");
        vibFFTSize.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","SpectrumConfig","eFFTSize","Third Level")));
        vibSpectrumConfig.appendChild(vibFFTSize);

        /*** Spectrum Config : FFT Averaging ***/
        auto vibFFTAveraging = document.createElement("eFFTAveraging");
        vibFFTAveraging.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","SpectrumConfig","eFFTAveraging","Third Level")));
        vibSpectrumConfig.appendChild(vibFFTAveraging);

        /*** Spectrum Config : FFT Overlap Percent ***/
        auto vibFFTOverlapPercent = document.createElement("eFFTOverlapPercent");
        vibFFTOverlapPercent.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","SpectrumConfig","eFFTOverlapPercent","Third Level")));
        vibSpectrumConfig.appendChild(vibFFTOverlapPercent);

        /*** Spectrum Config : FFT Output Unit ***/
        auto vibFFTOutputUnit = document.createElement("eFFTOutputUnit");
        vibFFTOutputUnit.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","SpectrumConfig","eFFTOutputUnit","Third Level")));
        vibSpectrumConfig.appendChild(vibFFTOutputUnit);

        /*** Spectrum Config : Zoom Frequency Hz ***/
        auto vibZoomFrequencyHz = document.createElement("fZoomFrequencyHz");
        vibZoomFrequencyHz.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","SpectrumConfig","fZoomFrequencyHz","Third Level")));
        vibSpectrumConfig.appendChild(vibZoomFrequencyHz);

        /*** Spectrum Config : Zoom Factor ***/
        auto vibZoomFactor = document.createElement("eZoomFactor");
        vibZoomFactor.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","SpectrumConfig","eZoomFactor","Third Level")));
        vibSpectrumConfig.appendChild(vibZoomFactor);

        /*** Spectrum Config : Max Band width Hz ***/
        auto vibMaxBandwidthHz = document.createElement("fMaxBandwidthHz");
        vibMaxBandwidthHz.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","SpectrumConfig","fMaxBandwidthHz","Third Level")));
        vibSpectrumConfig.appendChild(vibMaxBandwidthHz);

        /// MEASURE VIB
        /*** Measure Vib : Spectrum Time Weighting ***/
        auto vibSpectrumTimeWeighting = document.createElement("SpectrumTimeWeighting");
        vibSpectrumTimeWeighting.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","SpectrumTimeWeighting","","Second Level")));
        vib.appendChild(vibSpectrumTimeWeighting);

        /*** Measure Vib : Integration Time Sec ***/
        auto vibIntegrationTimeSec = document.createElement("IntegrationTimeSec");
        vibIntegrationTimeSec.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","IntegrationTimeSec","","Second Level")));
        vib.appendChild(vibIntegrationTimeSec);

        /*** Measure Vib : Fast Integration Time Milli Sec ***/
        auto vibFastIntegrationTimeMilliSec = document.createElement("FastIntegrationTimeMilliSec");
        vibFastIntegrationTimeMilliSec.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","FastIntegrationTimeMilliSec","","Second Level")));
        vib.appendChild(vibFastIntegrationTimeMilliSec);

        /*** Measure Vib : Max Wav Size In Minutes ***/
        auto vibMaxWavSizeInMinutes = document.createElement("MaxWavSizeInMinutes");
        vibMaxWavSizeInMinutes.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","MaxWavSizeInMinutes","","Second Level")));
        vib.appendChild(vibMaxWavSizeInMinutes);

        /*** Measure Vib : Start Slm With Wav ***/
        auto vibStartSlmWithWav = document.createElement("StartSlmWithWav");
        vibStartSlmWithWav.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","StartSlmWithWav","","Second Level")));
        vib.appendChild(vibStartSlmWithWav);

        /*** Measure Vib : Enable Spectrogram ***/
        auto vibEnableSpec = document.createElement("EnableSpectrogram");
        vibEnableSpec.appendChild(document.createTextNode(traverseSelectedFile("MeasureVib","EnableSpectrogram","","Second Level")));
        vib.appendChild(vibEnableSpec);

        /// BUILDINGS
        /*** Buildings : Configuration Type ***/
        auto buildingConfigurationType= document.createElement("ConfigurationType");
        buildingConfigurationType.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ConfigurationType","","Second Level")));
        building.appendChild(buildingConfigurationType);

        QDomElement reverberationTime = document.createElement("ReverberationTime");
        building.appendChild(reverberationTime);

        /// REVERBERATION TIME
        /*** Reverberation Time : Type Source ***/
        auto reverberationTimeTypeSource = document.createElement("TypeSource");
        reverberationTimeTypeSource.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","TypeSource","Third Level")));
        reverberationTime.appendChild(reverberationTimeTypeSource);

        /*** Reverberation Time : Type Spectrum ***/
        auto reverberationTimeSpectrumType = document.createElement("SpectrumType");
        reverberationTimeSpectrumType.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","SpectrumType","Third Level")));
        reverberationTime.appendChild(reverberationTimeSpectrumType);

        /*** Reverberation Time : Threshold dB ***/
        auto reverberationTimeThresholddB = document.createElement("ThresholddB");
        reverberationTimeThresholddB.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","ThresholddB","Third Level")));
        reverberationTime.appendChild(reverberationTimeThresholddB);

        /*** Reverberation Time : Decrease Time Sec ***/
        auto decreaseTimeSec = document.createElement("DecreaseTimeSec");
        decreaseTimeSec.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","DecreaseTimeSec","Third Level")));
        reverberationTime.appendChild(decreaseTimeSec);

        /*** Reverberation Time : Extended Record ***/
        auto extendedRecord = document.createElement("ExtendedRecord");
        extendedRecord.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","ExtendedRecord","Third Level")));
        reverberationTime.appendChild(extendedRecord);

        /*** Reverberation Time : Max Noise Level dB ***/
        auto maxNoiseLeveldB = document.createElement("MaxNoiseLeveldB");
        maxNoiseLeveldB.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","MaxNoiseLeveldB","Third Level")));
        reverberationTime.appendChild(maxNoiseLeveldB);

        /*** Reverberation Time : Mode ***/
        auto reverbMode = document.createElement("Mode");
        reverbMode.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","Mode","Third Level")));
        reverberationTime.appendChild(reverbMode);

        /*** ReverberationTime : Activate Wav Generation ***/
        auto activateWavGeneration = document.createElement("ActivateWavGeneration");
        activateWavGeneration.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","ActivateWavGeneration","Third Level")));
        reverberationTime.appendChild(activateWavGeneration);

        /*** ReverberationTime : Nb Sec Measure Noise ***/
        auto nbSecMeasureNoise = document.createElement("NbSecMeasureNoise");
        nbSecMeasureNoise.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","NbSecMeasureNoise","Third Level")));
        reverberationTime.appendChild(nbSecMeasureNoise);

        /*** ReverberationTime : Nb Sec Stabilization Source ***/
        auto nbSecStabilizationSource = document.createElement("NbSecStabilizationSource");
        nbSecStabilizationSource.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","NbSecStabilizationSource","Third Level")));
        reverberationTime.appendChild(nbSecStabilizationSource);

        /*** ReverberationTime : Nb Sec Measure Source ***/
        auto nbSecMeasureSource = document.createElement("NbSecMeasureSource");
        nbSecMeasureSource.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","NbSecMeasureSource","Third Level")));
        reverberationTime.appendChild(nbSecMeasureSource);

        /*** ReverberationTime : Time Out Sec ***/
        auto reverberationTimeTimeOutSec = document.createElement("TimeOutSec");
        reverberationTimeTimeOutSec.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","TimeOutSec","Third Level")));
        reverberationTime.appendChild(reverberationTimeTimeOutSec);

        /*** ReverberationTime : Display Freq Max Index ***/
        auto displayFreqMaxIndex = document.createElement("DisplayFreqMaxIndex");
        displayFreqMaxIndex.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","DisplayFreqMaxIndex","Third Level")));
        reverberationTime.appendChild(displayFreqMaxIndex);

        /*** ReverberationTime : Display Freq Min Index ***/
        auto displayFreqMinIndex = document.createElement("DisplayFreqMinIndex");
        displayFreqMinIndex.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","DisplayFreqMinIndex","Third Level")));
        reverberationTime.appendChild(displayFreqMinIndex);

        /*** ReverberationTime : Min Tr Sec ***/
        auto minTrSec = document.createElement("MinTrSec");
        minTrSec.appendChild(document.createTextNode(traverseSelectedFile("Buildings","ReverberationTime","MinTrSec","Third Level")));
        reverberationTime.appendChild(minTrSec);

        /// SESSION
        /*** Session : Configuration File Name ***/
        auto configFileName = document.createElement("ConfigFileName");
        configFileName.appendChild(document.createTextNode(traverseSelectedFile("Session","ConfigFileName","","Second Level")));
        session.appendChild(configFileName);

        /*** Session : Creation Date Time ***/
        auto creaDate = document.createElement("CreationDateTime");
        creaDate.appendChild(document.createTextNode(traverseSelectedFile("Session","CreationDateTime","","Second Level")));
        session.appendChild(creaDate);

        /*** Session : Default Boot Mode ***/
        auto defaultBoot = document.createElement("DefaultBootMode");
        defaultBoot.appendChild(document.createTextNode(traverseSelectedFile("Session","DefaultBootMode","","Second Level")));
        session.appendChild(defaultBoot);

        /*** Session : Record From Start ***/
        auto recStart = document.createElement("RecordFromStart");
        recStart.appendChild(document.createTextNode(traverseSelectedFile("Session","RecordFromStart","","Second Level")));
        session.appendChild(recStart);

        /*** Session : Project Name ***/
        auto projectName = document.createElement("ProjectName");
        projectName.appendChild(document.createTextNode(traverseSelectedFile("Session","ProjectName","","Second Level")));
        session.appendChild(projectName);

        /*** Session : Location Name ***/
        auto locName = document.createElement("LocationName");
        locName.appendChild(document.createTextNode(traverseSelectedFile("Session","LocationName","","Second Level")));
        session.appendChild(locName);

        /*** Session : Location Number ***/
        auto locNum = document.createElement("LocationNumber");
        locNum.appendChild(document.createTextNode(traverseSelectedFile("Session","LocationNumber","","Second Level")));
        session.appendChild(locNum);

        /*** Session : Last Calcul Mode ***/
        auto calcMode = document.createElement("LastCalculMode");
        calcMode.appendChild(document.createTextNode(traverseSelectedFile("Session","LastCalculMode","","Second Level")));
        session.appendChild(calcMode);

        QDomElement slmDisplayConfig = document.createElement("SlmDisplayConfiguration");
        session.appendChild(slmDisplayConfig);

        /// DISPLAY CONFIGURATION
        /*** Display Configuration : Serie 1 Indicator Index ***/
        auto serie1 = document.createElement("iSerie1IndicatorIndex");
        serie1.appendChild(document.createTextNode(traverseSelectedFile("Session","SlmDisplayConfiguration","iSerie1IndicatorIndex","Third Level")));
        slmDisplayConfig.appendChild(serie1);

        /*** Display Configuration : Serie 2 Indicator Index ***/
        auto serie2 = document.createElement("iSerie2IndicatorIndex");
        serie2.appendChild(document.createTextNode(traverseSelectedFile("Session","SlmDisplayConfiguration","iSerie2IndicatorIndex","Third Level")));
        slmDisplayConfig.appendChild(serie2);

        /*** Display Configuration : Serie 3 Indicator Index ***/
        auto serie3 = document.createElement("iSerie3IndicatorIndex");
        serie3.appendChild(document.createTextNode(traverseSelectedFile("Session","SlmDisplayConfiguration","iSerie3IndicatorIndex","Third Level")));
        slmDisplayConfig.appendChild(serie3);

        /*** Session : Initial Comment ***/
        auto comment = document.createElement("InitialComment");
        comment.appendChild(document.createTextNode(traverseSelectedFile("Session","InitialComment","","Second Level")));
        session.appendChild(comment);

        // Write to file
        if(!QDir(m_oAudioGet->path()+"/AllianTech/ATomicSound/Save").exists()) {
            QDir directory;
            directory.mkpath(m_oAudioGet->path()+"/AllianTech/ATomicSound/Save");
        }

        if(m_currentFile.isOpen()) {
            m_currentFile.close();
        }

        m_currentFile.open(QIODevice::WriteOnly | QIODevice::Text);

        if(!m_currentFile.isOpen()) {
            qDebug()<<"Failed to open old current file";
            m_oAudioGet->getFileError(m_currentFile.error(),XML);

        } else {
            QTextStream stream(&m_currentFile);
            stream<<document.toString();
            m_currentFile.close();
            setDefault();
            configAcousticMeas(false, false);
        }

    } else {
        emit outOfRange();
    }
}

/**
*******************************************************************************
** @fn  void GetConfiguration::writeDefaultFile(QString configFile)
** @param[in] void
** @param[out] void
** @brief writes the "default.xml" file into device:
**          the defaultfile is written whenever this file is not found in the
**          specified directory
** @return void
**
*******************************************************************************
**/
void GetConfiguration::writeDefaultFile(QString configFile) {
    // Write Default XML
    QDomDocument document;

    /*QDomText setup = document.createTextNode("<?xml version='1.0' encoding='utf-8'?>\n");
    document.appendChild(setup);*/

    QDomElement config = document.createElement("Configuration");
    document.appendChild(config);

    QDomElement version = document.createElement("SoftwareVersion");
    config.appendChild(version);

    QDomElement hard = document.createElement("HardwareVersion");
    config.appendChild(hard);

    QDomElement num = document.createElement("SensorNumber");
    config.appendChild(num);

    QDomElement root = document.createElement("Sensor");
    config.appendChild(root);

    QDomElement calibration = document.createElement("Calibration");
    config.appendChild(calibration);

    QDomElement check = document.createElement("Check");
    config.appendChild(check);

    QDomElement measure = document.createElement("Measure");
    config.appendChild(measure);

    QDomElement vib = document.createElement("MeasureVib");
    config.appendChild(vib);

    QDomElement building = document.createElement("Buildings");
    config.appendChild(building);

    QDomElement session = document.createElement("Session");
    config.appendChild(session);

    /// SOFTWARE
    /*** SoftwareVersion : Product ***/
    auto product = document.createElement("Product");
    product.appendChild(document.createTextNode("ATomicSound"));
    version.appendChild(product);

    /*** SoftwareVersion : AppliVersion ***/
    auto appliversion = document.createElement("AppliVersion");
    appliversion.appendChild(document.createTextNode("v1.0.0.0[DEMO_INTERNE_0.0]"));
    version.appendChild(appliversion);

    /*** SoftwareVersion : XMLVersion ***/
    auto xmlversion = document.createElement("XMLVersion");
    xmlversion.appendChild(document.createTextNode("v1.9.3"));
    version.appendChild(xmlversion);

    /// HARDWARE
    /*** HardwareVersion : Name ***/
    auto hardname = document.createElement("Name");
    hardname.appendChild(document.createTextNode("ATomic-1"));
    hard.appendChild(hardname);

    /*** HardwareVersion : SN ***/
    auto hardsn = document.createElement("SN");
    hardsn.appendChild(document.createTextNode("#0000"));
    hard.appendChild(hardsn);

    /*** HardwareVersion : UsbProductId ***/
    auto usbid = document.createElement("UsbProductId");
    usbid.appendChild(document.createTextNode("AllianTech ATomic-1 #0000"));
    hard.appendChild(usbid);

    /*** HardwareVersion : UsbSerialNumber ***/
    auto usbserialnum = document.createElement("UsbSerialNumber");
    usbserialnum.appendChild(document.createTextNode("F1/5000;F2/500;V1.01;#0000")); //PatchBetaDynamMetadata
    hard.appendChild(usbserialnum);

    /*** HardwareVersion : NumberOfSensors ***/
    auto numOfSensor = document.createElement("NumberOfSensors");
    numOfSensor.appendChild(document.createTextNode("1"));
    hard.appendChild(numOfSensor);

    /*** HardwareVersion : NumberOfChannels ***/
    auto numberOfChannels = document.createElement("NumberOfChannels");
    numberOfChannels.appendChild(document.createTextNode("1"));
    hard.appendChild(numberOfChannels);

    /*** HardwareVersion : Chan1FullScale_mV ***/
    auto chan1FullScale_mV = document.createElement("Chan1FullScale_mV");
    chan1FullScale_mV.appendChild(document.createTextNode("5000"));
    hard.appendChild(chan1FullScale_mV);

    /*** HardwareVersion : Chan2FullScale_mV ***/
    auto chan2FullScale_mV = document.createElement("Chan2FullScale_mV");
    chan2FullScale_mV.appendChild(document.createTextNode("500"));
    hard.appendChild(chan2FullScale_mV);

    /*** HardwareVersion : UserDefinedFullScale ***/
    auto userDefinedFullScale = document.createElement("UserDefinedFullScale");
    userDefinedFullScale.appendChild(document.createTextNode("NO"));
    hard.appendChild(userDefinedFullScale);

    /*** HardwareVersion : CIC ***/
    auto cIC = document.createElement("CIC");
    cIC.appendChild(document.createTextNode("NO"));
    hard.appendChild(cIC);

    /*** HardwareVersion : TEDS ***/
    auto tEDS = document.createElement("TEDS");
    tEDS.appendChild(document.createTextNode("NO"));
    hard.appendChild(tEDS);

    /// SENSOR NUMBER
    /*** SENSOR : Number ***/
    auto sensorNumber = document.createElement("SensorNumber");
    sensorNumber.appendChild(document.createTextNode("1"));
    config.appendChild(sensorNumber);

    /// SENSOR
    /*** SENSOR : Type Microphone ***/
    auto typeMicro = document.createElement("Type");
    typeMicro.appendChild(document.createTextNode("MICROPHONE"));
    root.appendChild(typeMicro);

    /*** SENSOR : Name Microphone ***/
    auto nameMicro = document.createElement("Name");
    nameMicro.appendChild(document.createTextNode("to be defined by the user"));
    root.appendChild(nameMicro);

    /*** SENSOR : Serial Number ***/
    auto serialNumber = document.createElement("SensorSN");
    serialNumber.appendChild(document.createTextNode("to be defined by the user"));
    root.appendChild(serialNumber);

    /*** SENSOR : Nominal Senstivity SI ***/
    auto sensivity = document.createElement("NominalSenstivitySI");
    sensivity.appendChild(document.createTextNode("50"));
    root.appendChild(sensivity);

    /*** SENSOR : Tolerance Nominal Sensitivity dB ***/
    auto tolerance = document.createElement("ToleranceNominalSensitivitydB");
    tolerance.appendChild(document.createTextNode("2.00"));
    root.appendChild(tolerance);

    /*** SENSOR : ConversionFactorChan ***/
    auto conversionFactorChan = document.createElement("ConversionFactorChan");
    conversionFactorChan.appendChild(document.createTextNode("100"));
    root.appendChild(conversionFactorChan);

    /*** SENSOR : Accreditation Date ***/
    auto accreditationDate = document.createElement("AccreditationDate");
    accreditationDate.appendChild(document.createTextNode("2000/01/01"));
    root.appendChild(accreditationDate);

    /*** SENSOR : Laboratory Accreditation Date ***/
    auto date = document.createElement("LaboratoryAccreditationDate");
    date.appendChild(document.createTextNode("2000/01/01"));
    root.appendChild(date);

    /*** SENSOR : Laboratory Sensitivity SI ***/
    auto labsensivity = document.createElement("LaboratorySensitivitySI");
    labsensivity.appendChild(document.createTextNode("50"));
    root.appendChild(labsensivity);

    /*** SENSOR : Wind Screen ***/
    auto windScreen = document.createElement("WindScreen");
    windScreen.appendChild(document.createTextNode("NO"));
    root.appendChild(windScreen);

    /*** SENSOR : FreeField Correction dB ***/
    auto freeFieldCorrectiondB = document.createElement("FreeFieldCorrectiondB");
    freeFieldCorrectiondB.appendChild(document.createTextNode("-0.4")); // origin -0.4
    root.appendChild(freeFieldCorrectiondB);

    /*** SENSOR : Class ***/
    auto sensorclass = document.createElement("Class");
    sensorclass.appendChild(document.createTextNode("CLASS_1"));
    root.appendChild(sensorclass);

    /*** SENSOR : TEDS ***/
    auto teds = document.createElement("TEDS");
    teds.appendChild(document.createTextNode("NO"));
    root.appendChild(teds);

    /*** SENSOR : TEDSAddress ***/
    auto tedsAddress = document.createElement("TEDSAddress");
    tedsAddress.appendChild(document.createTextNode("000000"));
    root.appendChild(tedsAddress);

    /*** SENSOR : User Units ***/
    auto userUnits = document.createElement("UserUnits");
    userUnits.appendChild(document.createTextNode(""));
    root.appendChild(userUnits);

    /// CALIBRATION
    /*** Calibration : Level ***/
    auto caliblevel = document.createElement("Level");
    caliblevel.appendChild(document.createTextNode("94"));
    calibration.appendChild(caliblevel);

    /*** Calibration : Date ***/
    auto calibdate = document.createElement("Date");
    calibdate.appendChild(document.createTextNode("2000/01/01 00:00"));
    calibration.appendChild(calibdate);

    /*** Calibration : Calibration Correction dB ***/
    auto calibrationCorrectiondB = document.createElement("CalibrationCorrectiondB");
    calibrationCorrectiondB.appendChild(document.createTextNode("0"));
    calibration.appendChild(calibrationCorrectiondB);

    /*** Calibration : Stability Level dB ***/
    auto calibrationStabilityLeveldB = document.createElement("StabilityLeveldB");
    calibrationStabilityLeveldB.appendChild(document.createTextNode("0.5"));
    calibration.appendChild(calibrationStabilityLeveldB);

    /*** Calibration : Selected Ch ***/
    auto selectedCh = document.createElement("SelectedCh");
    selectedCh.appendChild(document.createTextNode("0"));
    calibration.appendChild(selectedCh);

    /// CHECK
    /*** Check : CIC ***/
    auto checkCIC = document.createElement("CIC");
    checkCIC.appendChild(document.createTextNode("NO"));
    check.appendChild(checkCIC);

    /*** Check : SignalType1 ***/
    auto checkSignalType1 = document.createElement("SignalType1");
    checkSignalType1.appendChild(document.createTextNode("SINUS"));
    check.appendChild(checkSignalType1);

    /*** Check : Signal Level 1 Percent ***/
    auto checkSignalLevel1Percent = document.createElement("SignalLevel1Percent");
    checkSignalLevel1Percent.appendChild(document.createTextNode("0.1"));
    check.appendChild(checkSignalLevel1Percent);

    /*** Check : Signal Frequency 1 ***/
    auto checkSignalFrequency1 = document.createElement("SignalFrequency1");
    checkSignalFrequency1.appendChild(document.createTextNode("1000"));
    check.appendChild(checkSignalFrequency1);

    /*** Check : Signal Type 2 ***/
    auto checkSignalType2 = document.createElement("SignalType2");
    checkSignalType2.appendChild(document.createTextNode("WHITENOISE"));
    check.appendChild(checkSignalType2);

    /*** Check : Signal Level 2 Percent ***/
    auto checkSignalLevel2Percent = document.createElement("SignalLevel2Percent");
    checkSignalLevel2Percent.appendChild(document.createTextNode("100"));
    check.appendChild(checkSignalLevel2Percent);

    /*** Check : Signal Frequency 2 ***/
    auto checkSignalFrequency2 = document.createElement("SignalFrequency2");
    checkSignalFrequency2.appendChild(document.createTextNode("0"));
    check.appendChild(checkSignalFrequency2);

    /*** Check : Signal Type 3 ***/
    auto checkSignalType3 = document.createElement("SignalType3");
    checkSignalType3.appendChild(document.createTextNode("PINKNOISE"));
    check.appendChild(checkSignalType3);

    /*** Check : Signal Level 3 Percent ***/
    auto checkSignalLevel3Percent = document.createElement("SignalLevel3Percent");
    checkSignalLevel3Percent.appendChild(document.createTextNode("500"));
    check.appendChild(checkSignalLevel3Percent);

    /*** Check : Signal Frequency 3 ***/
    auto checkSignalFrequency3 = document.createElement("SignalFrequency3");
    checkSignalFrequency3.appendChild(document.createTextNode("0"));
    check.appendChild(checkSignalFrequency3);

    /*** Check : Max Deviation dB ***/
    auto checkMaxDeviationdB= document.createElement("MaxDeviationdB");
    checkMaxDeviationdB.appendChild(document.createTextNode("0.5"));
    check.appendChild(checkMaxDeviationdB);

    /// MEASURE
    /*** Measure : Configuration Type ***/
    auto measureconfigurationType = document.createElement("ConfigurationType");
    measureconfigurationType.appendChild(document.createTextNode("MODE_SOUNDLEVELMETER"));
    measure.appendChild(measureconfigurationType);

    /*** Measure : Mode ***/
    auto mode = document.createElement("Mode");
    mode.appendChild(document.createTextNode("RealTime"));
    measure.appendChild(mode);

    /*** Measure : Input Channel ***/
    auto measureInputChannel = document.createElement("InputChannel");
    measureInputChannel.appendChild(document.createTextNode("1"));
    measure.appendChild(measureInputChannel);

    /*** Measure : ACZ Time Weighting ***/
    auto measureACZTimeWeighting = document.createElement("ACZTimeWeighting");
    measureACZTimeWeighting.appendChild(document.createTextNode("LP_SLOW"));
    measure.appendChild(measureACZTimeWeighting);

    QDomElement spectrumConfig = document.createElement("SpectrumConfig");
    measure.appendChild(spectrumConfig);

    /// SPECTRUM CONFIG
    /*** Spectrum Config : Spectrum ***/
    if(configFile == "ThirdOctave") {
        auto eSpectrum = document.createElement("eSpectrum");
        eSpectrum.appendChild(document.createTextNode("SPECTRUM_3RD_OCTAVE"));
        spectrumConfig.appendChild(eSpectrum);

    } else if(configFile == "Octave") {
        auto eSpectrum = document.createElement("eSpectrum");
        eSpectrum.appendChild(document.createTextNode("SPECTRUM_OCTAVE"));
        spectrumConfig.appendChild(eSpectrum);
    }

    /*** Spectrum Config : Window ***/
    auto eWindow = document.createElement("eWindow");
    eWindow.appendChild(document.createTextNode("RECTANGULAR"));
    spectrumConfig.appendChild(eWindow);

    /*** Spectrum Config : FFT Size ***/
    auto eFFTSize = document.createElement("eFFTSize");
    eFFTSize.appendChild(document.createTextNode("FFT_256"));
    spectrumConfig.appendChild(eFFTSize);

    /*** Spectrum Config : FFT Averaging ***/
    auto eFFTAveraging = document.createElement("eFFTAveraging");
    eFFTAveraging.appendChild(document.createTextNode("LINEAR"));
    spectrumConfig.appendChild(eFFTAveraging);

    /*** Spectrum Config : FFT Overlap Percent ***/
    auto eFFTOverlapPercent = document.createElement("eFFTOverlapPercent");
    eFFTOverlapPercent.appendChild(document.createTextNode("OVERLAP_0"));
    spectrumConfig.appendChild(eFFTOverlapPercent);

    /*** Spectrum Config : FFT Output Unit ***/
    auto eFFTOutputUnit = document.createElement("eFFTOutputUnit");
    eFFTOutputUnit.appendChild(document.createTextNode("UNIT_DB"));
    spectrumConfig.appendChild(eFFTOutputUnit);

    /*** Spectrum Config : Zoom Frequency Hz ***/
    auto fZoomFrequencyHz = document.createElement("fZoomFrequencyHz");
    fZoomFrequencyHz.appendChild(document.createTextNode("12000"));
    spectrumConfig.appendChild(fZoomFrequencyHz);

    /*** Spectrum Config : Zoom Factor ***/
    auto eZoomFactor = document.createElement("eZoomFactor");
    eZoomFactor.appendChild(document.createTextNode("ZOOM_0"));
    spectrumConfig.appendChild(eZoomFactor);

    /*** Spectrum Config : Max Band width Hz ***/
    auto fMaxBandwidthHz = document.createElement("fMaxBandwidthHz");
    fMaxBandwidthHz.appendChild(document.createTextNode("20000"));
    spectrumConfig.appendChild(fMaxBandwidthHz);

    /// MEASURE
    /*** Measure : Spectrum Time Weighting ***/
    auto measureSpectrumTimeWeighting = document.createElement("SpectrumTimeWeighting");
    measureSpectrumTimeWeighting.appendChild(document.createTextNode("LP_NONE"));
    measure.appendChild(measureSpectrumTimeWeighting);

    /*** Measure : Integration Time Sec ***/
    auto measureIntegrationTimeSec = document.createElement("IntegrationTimeSec");
    measureIntegrationTimeSec.appendChild(document.createTextNode("1"));
    measure.appendChild(measureIntegrationTimeSec);

    /*** Measure : Fast Integration Time Milli Sec ***/
    auto measureFastIntegrationTimeMilliSec = document.createElement("FastIntegrationTimeMilliSec");
    measureFastIntegrationTimeMilliSec.appendChild(document.createTextNode("200"));
    measure.appendChild(measureFastIntegrationTimeMilliSec);

    /*** Measure : Max Wav Size In Minutes ***/
    auto maxWavSizeInMinutes = document.createElement("MaxWavSizeInMinutes");
    maxWavSizeInMinutes.appendChild(document.createTextNode("60"));
    measure.appendChild(maxWavSizeInMinutes);

    /*** Measure : Start Slm With Wav ***/
    auto startSlmWithWav = document.createElement("StartSlmWithWav");
    startSlmWithWav.appendChild(document.createTextNode("NO"));
    measure.appendChild(startSlmWithWav);

    /*** Measure : Enable Spectrogram ***/
    auto enableSpec = document.createElement("EnableSpectrogram");
    enableSpec.appendChild(document.createTextNode("NO"));
    measure.appendChild(enableSpec);

    /// MEASURE VIB
    /*** Measure Vib : Configuration Type ***/
    auto vibConfigurationType = document.createElement("ConfigurationType");
    vibConfigurationType.appendChild(document.createTextNode("MODE_MULTISENSOR"));
    vib.appendChild(vibConfigurationType);

    /*** Measure Vib : Mode ***/
    auto modeVib = document.createElement("Mode");
    modeVib.appendChild(document.createTextNode("RealTime"));
    vib.appendChild(modeVib);

    /*** Measure Vib : Input Channel ***/
    auto vibInputChannel = document.createElement("InputChannel");
    vibInputChannel.appendChild(document.createTextNode("1"));
    vib.appendChild(vibInputChannel);

    /*** Measure Vib : ACZ Time Weighting ***/
    auto vibACZTimeWeighting = document.createElement("ACZTimeWeighting");
    vibACZTimeWeighting.appendChild(document.createTextNode("LP_SLOW"));
    vib.appendChild(vibACZTimeWeighting);

    QDomElement vibSpectrumConfig = document.createElement("SpectrumConfig");
    vib.appendChild(vibSpectrumConfig);

    /// SPECTRUM CONFIG
    /*** Spectrum Config : Spectrum ***/
    if(configFile == "ThirdOctave") {
        auto eSpectrum = document.createElement("eSpectrum");
        eSpectrum.appendChild(document.createTextNode("SPECTRUM_3RD_OCTAVE"));
        vibSpectrumConfig.appendChild(eSpectrum);

    } else if(configFile == "Octave") {
        auto eSpectrum = document.createElement("eSpectrum");
        eSpectrum.appendChild(document.createTextNode("SPECTRUM_OCTAVE"));
        vibSpectrumConfig.appendChild(eSpectrum);
    }

    /*** Spectrum Config : Window ***/
    auto vibWindow = document.createElement("eWindow");
    vibWindow.appendChild(document.createTextNode("RECTANGULAR"));
    vibSpectrumConfig.appendChild(vibWindow);

    /*** Spectrum Config : FFT Size ***/
    auto vibFFTSize = document.createElement("eFFTSize");
    vibFFTSize.appendChild(document.createTextNode("FFT_256"));
    vibSpectrumConfig.appendChild(vibFFTSize);

    /*** Spectrum Config : FFT Averaging ***/
    auto vibFFTAveraging = document.createElement("eFFTAveraging");
    vibFFTAveraging.appendChild(document.createTextNode("LINEAR"));
    vibSpectrumConfig.appendChild(vibFFTAveraging);

    /*** Spectrum Config : FFT Overlap Percent ***/
    auto vibFFTOverlapPercent = document.createElement("eFFTOverlapPercent");
    vibFFTOverlapPercent.appendChild(document.createTextNode("OVERLAP_0"));
    vibSpectrumConfig.appendChild(vibFFTOverlapPercent);

    /*** Spectrum Config : FFT Output Unit ***/
    auto vibFFTOutputUnit = document.createElement("eFFTOutputUnit");
    vibFFTOutputUnit.appendChild(document.createTextNode("UNIT_DB"));
    vibSpectrumConfig.appendChild(vibFFTOutputUnit);

    /*** Spectrum Config : Zoom Frequency Hz ***/
    auto vibZoomFrequencyHz = document.createElement("fZoomFrequencyHz");
    vibZoomFrequencyHz.appendChild(document.createTextNode("12000"));
    vibSpectrumConfig.appendChild(vibZoomFrequencyHz);

    /*** Spectrum Config : Zoom Factor ***/
    auto vibZoomFactor = document.createElement("eZoomFactor");
    vibZoomFactor.appendChild(document.createTextNode("ZOOM_0"));
    vibSpectrumConfig.appendChild(vibZoomFactor);

    /*** Spectrum Config : Max Band width Hz ***/
    auto vibMaxBandwidthHz = document.createElement("fMaxBandwidthHz");
    vibMaxBandwidthHz.appendChild(document.createTextNode("20000"));
    vibSpectrumConfig.appendChild(vibMaxBandwidthHz);

    /// MEASURE VIB
    /*** Measure Vib : Spectrum Time Weighting ***/
    auto vibSpectrumTimeWeighting = document.createElement("SpectrumTimeWeighting");
    vibSpectrumTimeWeighting.appendChild(document.createTextNode("LP_NONE"));
    vib.appendChild(vibSpectrumTimeWeighting);

    /*** Measure Vib : Integration Time Sec ***/
    auto vibIntegrationTimeSec = document.createElement("IntegrationTimeSec");
    vibIntegrationTimeSec.appendChild(document.createTextNode("1"));
    vib.appendChild(vibIntegrationTimeSec);

    /*** Measure Vib : Fast Integration Time Milli Sec ***/
    auto vibFastIntegrationTimeMilliSec = document.createElement("FastIntegrationTimeMilliSec");
    vibFastIntegrationTimeMilliSec.appendChild(document.createTextNode("200"));
    vib.appendChild(vibFastIntegrationTimeMilliSec);

    /*** Measure Vib : Max Wav Size In Minutes ***/
    auto vibMaxWavSizeInMinutes = document.createElement("MaxWavSizeInMinutes");
    vibMaxWavSizeInMinutes.appendChild(document.createTextNode("60"));
    vib.appendChild(vibMaxWavSizeInMinutes);

    /*** Measure Vib : Start Slm With Wav ***/
    auto vibStartSlmWithWav = document.createElement("StartSlmWithWav");
    vibStartSlmWithWav.appendChild(document.createTextNode("NO"));
    vib.appendChild(vibStartSlmWithWav);

    /*** Measure Vib : Enable Spectrogram ***/
    auto vibEnableSpec = document.createElement("EnableSpectrogram");
    vibEnableSpec.appendChild(document.createTextNode("NO"));
    vib.appendChild(vibEnableSpec);

    /// BUILDINGS
    /*** Buildings : Configuration Type ***/
    auto buildingConfigurationType= document.createElement("ConfigurationType");
    buildingConfigurationType.appendChild(document.createTextNode("BUILDINGS"));
    building.appendChild(buildingConfigurationType);

    QDomElement reverberationTime = document.createElement("ReverberationTime");
    building.appendChild(reverberationTime);

    /// REVERBERATION TIME
    /*** Reverberation Time : Type Source ***/
    //if(configFile == "ThirdOctave") {
        auto reverberationTimeTypeSource= document.createElement("TypeSource");
        reverberationTimeTypeSource.appendChild(document.createTextNode("IMPULSE"));
        reverberationTime.appendChild(reverberationTimeTypeSource);
    /*} else if(configFile == "Octave") {
        auto reverberationTimeTypeSource= document.createElement("TypeSource");
        reverberationTimeTypeSource.appendChild(document.createTextNode("INTERRUPTED"));
        reverberationTime.appendChild(reverberationTimeTypeSource);
    }*/

    /*** Reverberation Time : Spectrum Type ***/
    if(configFile == "ThirdOctave") {
        auto spectrumType = document.createElement("SpectrumType");
        spectrumType.appendChild(document.createTextNode("SPECTRUM_3RD_OCTAVE"));
        reverberationTime.appendChild(spectrumType);

    } else if(configFile == "Octave") {
        auto spectrumType = document.createElement("SpectrumType");
        spectrumType.appendChild(document.createTextNode("SPECTRUM_OCTAVE"));
        reverberationTime.appendChild(spectrumType);
    }

    /*** Reverberation Time : Threshold dB ***/
    auto reverberationTimeThresholddB= document.createElement("ThresholddB");
    reverberationTimeThresholddB.appendChild(document.createTextNode("85"));
    reverberationTime.appendChild(reverberationTimeThresholddB);

    /*** Reverberation Time : Decrease Time Sec ***/
    auto decreaseTimeSec = document.createElement("DecreaseTimeSec");
    decreaseTimeSec.appendChild(document.createTextNode("3"));
    reverberationTime.appendChild(decreaseTimeSec);

    /*** Reverberation Time : Extended Record ***/
    auto extendedRecord = document.createElement("ExtendedRecord");
    extendedRecord.appendChild(document.createTextNode("NO"));
    reverberationTime.appendChild(extendedRecord);

    /*** Reverberation Time : Max Noise Level dB ***/
    auto maxNoiseLeveldB = document.createElement("MaxNoiseLeveldB");
    maxNoiseLeveldB.appendChild(document.createTextNode("65"));
    reverberationTime.appendChild(maxNoiseLeveldB);

    /*** Reverberation Time : Mode ***/
    auto reverbMode = document.createElement("Mode");
    reverbMode.appendChild(document.createTextNode("RealTime"));
    reverberationTime.appendChild(reverbMode);

    /*** ReverberationTime : Activate Wav Generation ***/
    auto activateWavGeneration = document.createElement("ActivateWavGeneration");
    activateWavGeneration.appendChild(document.createTextNode("NO"));
    reverberationTime.appendChild(activateWavGeneration);

    /*** ReverberationTime : Nb Sec Measure Noise ***/
    auto nbSecMeasureNoise = document.createElement("NbSecMeasureNoise");
    nbSecMeasureNoise.appendChild(document.createTextNode("5"));
    reverberationTime.appendChild(nbSecMeasureNoise);

    /*** ReverberationTime : Nb Sec Stabilization Source ***/
    auto nbSecStabilizationSource = document.createElement("NbSecStabilizationSource");
    nbSecStabilizationSource.appendChild(document.createTextNode("3"));
    reverberationTime.appendChild(nbSecStabilizationSource);

    /*** ReverberationTime : Nb Sec Measure Source ***/
    auto nbSecMeasureSource = document.createElement("NbSecMeasureSource");
    nbSecMeasureSource.appendChild(document.createTextNode("2"));
    reverberationTime.appendChild(nbSecMeasureSource);

    /*** ReverberationTime : Time Out Sec ***/
    auto reverberationTimeTimeOutSec = document.createElement("TimeOutSec");
    reverberationTimeTimeOutSec.appendChild(document.createTextNode("3"));
    reverberationTime.appendChild(reverberationTimeTimeOutSec);

    /*** ReverberationTime : Display Freq Max/Min Index ***/
    if(configFile == "ThirdOctave") {
        auto displayFreqMaxIndex = document.createElement("DisplayFreqMaxIndex");
        displayFreqMaxIndex.appendChild(document.createTextNode("35"));
        reverberationTime.appendChild(displayFreqMaxIndex);

        auto displayFreqMinIndex = document.createElement("DisplayFreqMinIndex");
        displayFreqMinIndex.appendChild(document.createTextNode("9"));
        reverberationTime.appendChild(displayFreqMinIndex);

    } else if(configFile == "Octave") {
        auto displayFreqMaxIndex = document.createElement("DisplayFreqMaxIndex");
        displayFreqMaxIndex.appendChild(document.createTextNode("11"));
        reverberationTime.appendChild(displayFreqMaxIndex);

        auto displayFreqMinIndex = document.createElement("DisplayFreqMinIndex");
        displayFreqMinIndex.appendChild(document.createTextNode("3"));
        reverberationTime.appendChild(displayFreqMinIndex);
    }

    /*** ReverberationTime : Min Tr Sec ***/
    auto minTrSec = document.createElement("MinTrSec");
    minTrSec.appendChild(document.createTextNode("0.4"));
    reverberationTime.appendChild(minTrSec);

    /// SESSION
    /*** Session : Configuration File Name ***/
    auto configFileName = document.createElement("ConfigFileName");
    configFileName.appendChild(document.createTextNode("Default"));
    session.appendChild(configFileName);

    /*** Session : Creation Date Time ***/
    auto creaDate = document.createElement("CreationDateTime");
    creaDate.appendChild(document.createTextNode("2000/01/01T00:00:00.00000+00:00"));
    session.appendChild(creaDate);

    /*** Session : Default Boot Mode ***/
    auto defaultBoot = document.createElement("DefaultBootMode");
    defaultBoot.appendChild(document.createTextNode("ENV"));
    session.appendChild(defaultBoot);

    /*** Session : Record From Start ***/
    auto recStart = document.createElement("RecordFromStart");
    recStart.appendChild(document.createTextNode("NO"));
    session.appendChild(recStart);

    /*** Session : Project Name ***/
    auto projectName = document.createElement("ProjectName");
    projectName.appendChild(document.createTextNode("Default"));
    session.appendChild(projectName);

    /*** Session : Location Name ***/
    //if(configFile == "ThirdOctave") {
        auto locName = document.createElement("LocationName");
        locName.appendChild(document.createTextNode("Workshop"));
        session.appendChild(locName);
    /*} else if(configFile == "Octave") {
        auto locName = document.createElement("LocationName");
        locName.appendChild(document.createTextNode("Workshop"));
        session.appendChild(locName);
    }*/

    /*** Session : Location Number ***/
    auto locNum = document.createElement("LocationNumber");
    locNum.appendChild(document.createTextNode("1"));
    session.appendChild(locNum);

    /*** Session : Last Calcul Mode ***/
    auto calcMode = document.createElement("LastCalculMode");
    calcMode.appendChild(document.createTextNode("MODE_SOUDLEVELMETER"));
    session.appendChild(calcMode);

    QDomElement slmDisplayConfig = document.createElement("SlmDisplayConfiguration");
    session.appendChild(slmDisplayConfig);

    /// DISPLAY CONFIGURATION
    /*** Display Configuration : Serie 1 Indicator Index ***/
    auto serie1 = document.createElement("iSerie1IndicatorIndex");
    serie1.appendChild(document.createTextNode("0"));
    slmDisplayConfig.appendChild(serie1);

    /*** Display Configuration : Serie 2 Indicator Index ***/
    auto serie2 = document.createElement("iSerie2IndicatorIndex");
    serie2.appendChild(document.createTextNode("3"));
    slmDisplayConfig.appendChild(serie2);

    /*** Display Configuration : Serie 3 Indicator Index ***/
    auto serie3 = document.createElement("iSerie3IndicatorIndex");
    serie3.appendChild(document.createTextNode("4"));
    slmDisplayConfig.appendChild(serie3);

    /*** Session : Initial Comment ***/
    auto comment = document.createElement("InitialComment");
    comment.appendChild(document.createTextNode(""));
    session.appendChild(comment);

    //Write to file
    if(!QDir(m_oAudioGet->path()+"/AllianTech/ATomicSound/Configuration").exists()) {
        QDir directory;
        directory.mkpath(m_oAudioGet->path()+"/AllianTech/ATomicSound/Configuration");
    }

    if(configFile == "ThirdOctave") {
        if(m_defaultThirdOctaveFile.isOpen()) {
            m_defaultThirdOctaveFile.close();
        }

        m_defaultThirdOctaveFile.open(QIODevice::WriteOnly | QIODevice ::Text);

        if(!m_defaultThirdOctaveFile.isOpen()) {
            m_oAudioGet->getFileError(m_defaultThirdOctaveFile.error(),XML);

        } else {
            QTextStream streamSono(&m_defaultThirdOctaveFile);
            streamSono<< document.toString();
            m_defaultThirdOctaveFile.close();
            setReset(); // ????????????????????
        }

    } else if(configFile == "Octave") {
        if(m_defaultOctaveFile.isOpen()) {
            m_defaultOctaveFile.close();
        }

        m_defaultOctaveFile.open(QIODevice::WriteOnly | QIODevice ::Text);

        if(!m_defaultOctaveFile.isOpen()) {
            m_oAudioGet->getFileError(m_defaultOctaveFile.error(),XML);

        } else {
            QTextStream streamSono(&m_defaultOctaveFile);
            streamSono<< document.toString();
            m_defaultOctaveFile.close();
            setReset(); // ????????????????????
        }
    }
}

/**
*******************************************************************************
** @fn  void writeSaveFile(QString configFilePath)
** @param[in] void
** @param[out] void
** @brief
** @return void
**
*******************************************************************************
**/
void GetConfiguration::writeSaveFile(QString filePath, QString fileName) {
    // Write Save XML
    QDomDocument document;
    QDomElement save = document.createElement("Save");
    document.appendChild(save);

    /// SAVE
    /*** SAVE : ConfigFileName ***/
    auto path = document.createElement("ConfigFilePath");
    path.appendChild(document.createTextNode(filePath));
    save.appendChild(path);

    /*** SAVE : ConfigFileName ***/
    auto name = document.createElement("ConfigFileName");
    name.appendChild(document.createTextNode(fileName));
    save.appendChild(name);

    // Write to file
    if(!QDir(m_oAudioGet->path()+"/AllianTech/ATomicSound/Save").exists()) {
        QDir directory;
        directory.mkpath(m_oAudioGet->path()+"/AllianTech/ATomicSound/Save");
    }

    if(m_saveFile.isOpen()) {
        m_saveFile.close();
    }

    m_saveFile.open(QIODevice::WriteOnly | QIODevice ::Text);

    if(!m_saveFile.isOpen()) {
        m_oAudioGet->getFileError(m_saveFile.error(),XML);

    } else {
        QTextStream streamSono(&m_saveFile);
        streamSono<< document.toString();
        m_saveFile.close();
    }

    // Update Config XML Text
    updateConfigFile();
}

/**
*******************************************************************************
** @fn  void writeSaveFile(QString configFilePath)
** @param[in] void
** @param[out] void
** @brief
** @return void
**
*******************************************************************************
**/
void GetConfiguration::updateConfigFile() {
    m_selectedFile.setFileName(traverseSaveFile("ConfigFilePath"));
    setConfigFileName(traverseSaveFile("ConfigFileName"));

    // Save selected file parameters into CurrentFile
    writeCurrentFile();
    // Update Reverb Project Parameters connected to the IHM
    setProjectParam();
}

/**
*******************************************************************************
** @fn  void pathConfigFileName(QString)
** @param[in] void
** @param[out] void
** @brief Creates configuration directory and needed files (default.xml/ current.xml)
**         and sets different parameters accordingly
** @return
**
*******************************************************************************
**/
void GetConfiguration::pathConfigFileName(QString configFilePath) {
    QString headerPath = "file://";
    int headerLength = headerPath.length();

    // Enlever file:// en début de l'Url provenant de la partie QML
    m_oPathConfigFile = configFilePath.remove(0,headerLength);

    qDebug()<<"m_oPathConfigFile : "<<m_oPathConfigFile;

    // FileName
    m_oConfigFile = new QFile(m_oPathConfigFile);

    QFileInfo fileInfo(m_oConfigFile->fileName());
    QString configFileName(fileInfo.fileName());

    qDebug()<<"ConfigFileName : "<<configFileName;

    writeSaveFile(configFilePath, configFileName);
}

/**
*******************************************************************************
** @fn  QString GetConfiguration::traverseCurrentFile(QString fElement,
**                                                    QString sElement)
** @param[in] QString fElement        node name
** @param[in] QString sElement        element name
** @param[out] QString
** @brief  gets tha parameter from "current.xml" file according to the node
**           and element names as printed in the xml file
** @return  parameter value as a string from the current.xml file
**
*******************************************************************************
**/
QString GetConfiguration::traverseCurrentFile(QString fElement, QString sElement, QString tElement, QString numElement) {
    QString errorStr;
    int errorLine;
    int errorColumn;
    QDomDocument doc;

    if(m_currentFile.isOpen()) {
        m_currentFile.close();
    }

    m_currentFile.open(QIODevice::ReadOnly | QIODevice ::Text);

    if(!m_currentFile.isOpen()) {
        qDebug()<<"failed to open current file";
        m_oAudioGet->getFileError(m_currentFile.error(),XML);

    } else {
        if(!doc.setContent(&m_currentFile, false, &errorStr, &errorLine, &errorColumn)) {
            qDebug() << "Error: Parse error at line " << errorLine << ", "
                     << "column " << errorColumn << ": "
                     << qPrintable(errorStr) ;
            qDebug()<<"failed to load current file";
        }
        m_currentFile.close();
    }

    // Get the root element
    QDomElement root = doc.firstChildElement();
    QDomElement firstelement = root.namedItem(fElement).toElement();
    QDomElement secondelement = firstelement.namedItem(sElement).toElement();
    QDomElement thirdelement = secondelement.namedItem(tElement).toElement();

    if (numElement == "Third Level") {
        if(!firstelement.isNull() && !secondelement.isNull()) {
            // We have a <name>..</name> element in the set
            /*qDebug()<<"firstelement selected"<<firstelement.text();
            qDebug()<<"secondelement selected"<<secondelement.text();
            qDebug()<<"thirdelement selected"<<thirdelement.text();*/
        }

        return thirdelement.text();

    } else if (numElement == "Second Level") {
        if(!firstelement.isNull()) {
            // We have a <name>..</name> element in the set
            /*qDebug()<<"firstelement selected"<<firstelement.text();
            qDebug()<<"secondelement selected"<<secondelement.text();*/
        }

        return secondelement.text();

    } else {
        // We have a <name>..</name> element in the set
        //qDebug()<<"firstelement selected"<<firstelement.text();

        return firstelement.text();
    }
}

/**
*******************************************************************************
** @fn  QString GetConfiguration::traverseDefaultFile(QString fElement,
**                                                    QString sElement)
** @param[in] QString fElement        node's name
** @param[in] QString sElement        element's name
** @param[out] QString
** @brief  gets tha parameter from "default.xml" file according to the node
**           and element names as printed in the xml file
** @return  parameter value as a string from the current.xml file
**
*******************************************************************************
**/
QString GetConfiguration::traverseDefaultFile(QString fElement, QString sElement, QString tElement, QString numElement) {
    QDomDocument doc;

    if(m_defaultThirdOctaveFile.isOpen()) {
        m_defaultThirdOctaveFile.close();
    }

    m_defaultThirdOctaveFile.open(QIODevice::ReadOnly|QIODevice::Text);

    if(!m_defaultThirdOctaveFile.isOpen()) {
        qDebug()<<"failed to open default file";
        m_oAudioGet->getFileError(m_defaultThirdOctaveFile.error(),XML);
        return "error xml";

    } else {
        QString errorStr;
        int errorLine;
        int errorColumn;

        if(!doc.setContent(&m_defaultThirdOctaveFile, false, &errorStr, &errorLine, &errorColumn)) {
            qDebug() << "Error: Parse error at line " << errorLine << ", "
                     << "column " << errorColumn << ": "
                     << qPrintable(errorStr) ;
            qDebug()<<"failed to load default file";
        }

        m_defaultThirdOctaveFile.close();
    }

    // Get the root element
    QDomElement root = doc.firstChildElement();
    QDomElement firstelement = root.namedItem(fElement).toElement();
    QDomElement secondelement = firstelement.namedItem(sElement).toElement();
    QDomElement thirdelement = secondelement.namedItem(tElement).toElement();

    if (numElement == "Third Level") {
        if(!firstelement.isNull() && !secondelement.isNull()) {
            // We have a <name>..</name> element in the set
            /*qDebug()<<"firstelement selected"<<firstelement.text();
            qDebug()<<"secondelement selected"<<secondelement.text();
            qDebug()<<"thirdelement selected"<<thirdelement.text();*/
        }

        return thirdelement.text();

    } else if (numElement == "Second Level") {
        if(!firstelement.isNull()) {
            // We have a <name>..</name> element in the set
            /*qDebug()<<"firstelement selected"<<firstelement.text();
            qDebug()<<"secondelement selected"<<secondelement.text();*/
        }

        return secondelement.text();

    } else {
        // We have a <name>..</name> element in the set
        //qDebug()<<"firstelement selected"<<firstelement.text();

        return firstelement.text();
    }
}

/**
*******************************************************************************
** @fn  QString GetConfiguration::traverseDefaultFile(QString fElement,
**                                                    QString sElement)
** @param[in] QString fElement        node's name
** @param[in] QString sElement        element's name
** @param[out] QString
** @brief  gets tha parameter from "default.xml" file according to the node
**           and element names as printed in the xml file
** @return  parameter value as a string from the current.xml file
**
*******************************************************************************
**/
QString GetConfiguration::traverseSaveFile(QString fElement) {
    QDomDocument doc;

    if(m_saveFile.isOpen()) {
        m_saveFile.close();
    }

    m_saveFile.open(QIODevice::ReadOnly|QIODevice ::Text);

    if(!m_saveFile.isOpen()) {
        qDebug()<<"failed to open save file";
        m_oAudioGet->getFileError(m_saveFile.error(),XML);
        return "error xml";

    } else {
        QString errorStr;
        int errorLine;
        int errorColumn;

        if(!doc.setContent(&m_saveFile, false, &errorStr, &errorLine, &errorColumn)) {
            qDebug() << "Error: Parse error at line " << errorLine << ", "
                     << "column " << errorColumn << ": "
                     << qPrintable(errorStr) ;
            qDebug()<<"failed to load save file";
        }

        m_saveFile.close();
    }

    // Get the root element
    QDomElement root = doc.firstChildElement();
    QDomElement firstelement = root.namedItem(fElement).toElement();

    if(!firstelement.isNull()) {
        // We have a <name>..</name> element in the set
        //qDebug()<<"firstelement save"<<firstelement.text();
    }

    return firstelement.text();
}

/**
*******************************************************************************
** @fn  QString traverseSelectedFile(QString, QString, QString, QString)
** @param[in] QString fElement        node's name
** @param[in] QString sElement        element's name
** @param[out] QString
** @brief  gets tha parameter from "default.xml" file according to the node
**           and element names as printed in the xml file
** @return  parameter value as a string from the current.xml file
**
*******************************************************************************
**/
QString GetConfiguration::traverseSelectedFile(QString fElement, QString sElement, QString tElement, QString numElement) {
    QDomDocument doc;

    if(m_selectedFile.isOpen()) {
        m_selectedFile.close();
    }

    m_selectedFile.open(QIODevice::ReadOnly|QIODevice::Text);

    if(!m_selectedFile.isOpen()) {
        qDebug()<<"failed to open selected file";
        m_oAudioGet->getFileError(m_selectedFile.error(),XML);
        return "error xml";

    } else {
        QString errorStr;
        int errorLine;
        int errorColumn;

        if(!doc.setContent(&m_selectedFile, false, &errorStr, &errorLine, &errorColumn)) {
            qDebug() << "Error: Parse error at line " << errorLine << ", "
                     << "column " << errorColumn << ": "
                     << qPrintable(errorStr) ;
            qDebug()<<"failed to load selected file";
        }

        m_selectedFile.close();
    }

    // Get the root element
    QDomElement root = doc.firstChildElement();
    QDomElement firstelement = root.namedItem(fElement).toElement();
    QDomElement secondelement = firstelement.namedItem(sElement).toElement();
    QDomElement thirdelement = secondelement.namedItem(tElement).toElement();

    if (numElement == "Third Level") {
        if(!firstelement.isNull() && !secondelement.isNull()) {
            // We have a <name>..</name> element in the set
            /*qDebug()<<"firstelement selected"<<firstelement.text();
            qDebug()<<"secondelement selected"<<secondelement.text();
            qDebug()<<"thirdelement selected"<<thirdelement.text();*/
        }

        return thirdelement.text();

    } else if (numElement == "Second Level") {
        if(!firstelement.isNull()) {
            // We have a <name>..</name> element in the set
            /*qDebug()<<"firstelement selected"<<firstelement.text();
            qDebug()<<"secondelement selected"<<secondelement.text();*/
        }

        return secondelement.text();

    } else {
        // We have a <name>..</name> element in the set
        //qDebug()<<"firstelement selected"<<firstelement.text();

        return firstelement.text();
    }
}

/**
*******************************************************************************
** @fn  void GetConfiguration::setconfiguration()
** @param[in] void
** @param[out] void
** @brief sets the configuration by writing the currentfile and configuring
**            the AcousticMeas Engine
** @return void
**
*******************************************************************************
**/
void GetConfiguration::setconfiguration() {

    if(!m_defaultThirdOctaveFile.exists()) {
        writeDefaultFile("ThirdOctave");
    }

    writeCurrentFile();

    if(!m_currentFile.exists()) {
        setCalibrationDate(traverseDefaultFile("Sensor","LaboratoryAccreditationDate","","Second Level"));

    } else {
        setCalibrationDate(traverseCurrentFile("Sensor","LaboratoryAccreditationDate","","Second Level"));
    }
}

/**
*******************************************************************************
** @fn  void GetConfiguration::setProjectParam()()
** @param[in] void
** @param[out] void
** @brief sets the both configuration and calibration tabs according to the
**         current.xml file if existed else according to the default.xml file
** @return void
**
*******************************************************************************
**/
void GetConfiguration::setProjectParam() {
    m_oAudioGet->setProjectName(traverseCurrentFile("Session","ProjectName","","Second Level"));
    m_oAudioGet->setRoomType(traverseCurrentFile("Session","LocationName","","Second Level"));
    m_oAudioGet->setRoomNumber(traverseCurrentFile("Session","LocationNumber","","Second Level"));
}

/**
*******************************************************************************
** @fn  void GetConfiguration::setReset()
** @param[in] void
** @param[out] void
** @brief sets the both configuration and calibration tabs according to the
**         default values (from defaultfile.xml)
** @return void
**
*******************************************************************************
**/
void GetConfiguration::setReset() {
    setMicrophone(traverseDefaultFile("Sensor","Name","","Second Level"));
    setSerialNumber(traverseDefaultFile("Sensor","SensorSN","","Second Level"));
    setNominalSensitivity(QString::number(traverseDefaultFile("Sensor","NominalSenstivitySI","","Second Level").toDouble(),'f',2));
    setTolerance(QString::number(traverseDefaultFile("Sensor","ToleranceNominalSensitivitydB","","Second Level").toDouble(),'f',2));
    setLabSensitivity(traverseDefaultFile("Sensor","LaboratorySensitivitySI","","Second Level"));

    setChan1FullScale(traverseDefaultFile("HardwareVersion","Chan1FullScale_mV","","Second Level"));
    setChan2FullScale(traverseDefaultFile("HardwareVersion","Chan2FullScale_mV","","Second Level"));

    setCalibrationDate(traverseDefaultFile("Sensor","LaboratoryAccreditationDate","","Second Level"));
    //setFilesName(traverseDefaultFile("Sensor","FilesName","","Second Level"));

    if(traverseDefaultFile("Measure","IntegrationTimeSec","","Second Level").toInt()>0) {
        setIntegrationTime(traverseDefaultFile("Measure","IntegrationTimeSec","","Second Level").toInt());
    }

    setSpectrumConfiguration(traverseDefaultFile("Measure","SpectrumConfig","eSpectrum","Third Level"));
    setTimeWeighting(traverseDefaultFile("Measure","ACZTimeWeighting","","Second Level"));
    setConversionFactor(traverseDefaultFile("Sensor","ConversionFactorChan","","Second Level")); //PatchBetaDynamMetadata

    // Reverberation Default Parameters
    setSource(traverseDefaultFile("Buildings","ReverberationTime","TypeSource","Third Level"));
    setSpectrumType(traverseDefaultFile("Buildings","ReverberationTime","SpectrumType","Third Level"));
    setChartTime(traverseDefaultFile("Buildings","ReverberationTime","DecreaseTimeSec","Third Level"));

    setCalibration(true);
}

/**
*******************************************************************************
** @fn  void GetConfiguration::setDefault()
** @param[in] void
** @param[out] void
** @brief sets the both configuration and calibration tabs according to the
**         current.xml file if existed else according to the default.xml file
** @return void
**
*******************************************************************************
**/
void GetConfiguration::setDefault() {
    if(!m_currentFile.exists()) {
        setReset();

    } else {
        setMicrophone(traverseCurrentFile("Sensor","Name","","Second Level"));
        setSerialNumber(traverseCurrentFile("Sensor","SensorSN","","Second Level"));
        setNominalSensitivity(QString::number(traverseCurrentFile("Sensor","NominalSenstivitySI","","Second Level").toDouble(),'f',2));
        setTolerance(traverseCurrentFile("Sensor","ToleranceNominalSensitivitydB","","Second Level"));
        setLabSensitivity(traverseCurrentFile("Sensor","LaboratorySensitivitySI","","Second Level"));

        setChan1FullScale(traverseCurrentFile("HardwareVersion","Chan1FullScale_mV","","Second Level"));
        setChan2FullScale(traverseCurrentFile("HardwareVersion","Chan2FullScale_mV","","Second Level"));

        setCalibrationDate(traverseCurrentFile("Sensor","LaboratoryAccreditationDate","","Second Level"));
        //setFilesName(traverseCurrentFile("Sensor","FilesName","","Second Level"));

        if(traverseCurrentFile("Measure","IntegrationTimeSec","","Second Level").toInt()>0){
            setIntegrationTime(traverseCurrentFile("Measure","IntegrationTimeSec","","Second Level").toInt());
        }

        setSpectrumConfiguration(traverseCurrentFile("Measure","SpectrumConfig","eSpectrum","Third Level"));
        setTimeWeighting(traverseCurrentFile("Measure","ACZTimeWeighting","","Second Level"));
        setCalibration(false);
        setConversionFactor(traverseCurrentFile("Sensor","ConversionFactorChan","","Second Level")); //PatchBetaDynamMetadata

        // Reverberation Parameters
        setSource(traverseCurrentFile("Buildings","ReverberationTime","TypeSource","Third Level"));
        setSpectrumType(traverseCurrentFile("Buildings","ReverberationTime","SpectrumType","Third Level"));
        setChartTime(traverseCurrentFile("Buildings","ReverberationTime","DecreaseTimeSec","Third Level"));
    }

    //Update WAVs future Metadata
    /*QString sAux;
    m_oAudioGet->m_rListHeader.rIPRD.cData = "M" + serialNumber();                                             //PatchBetaDynamMetadata - IPRD: Sensor Tipe (M - Microphone) + SensorSN
    m_oAudioGet->m_rListHeader.rINAM.cData = sAux.sprintf("%1.3e", calibrationCorrection1().toDouble());       //PatchBetaDynamMetadata - INAM: Calibration correction #.###e+0
    m_oAudioGet->m_rListHeader.rIART.cData = sAux.sprintf("%1.3e", labSensitivity().toDouble()/1000);          //PatchBetaDynamMetadata - IART: Lab Sensitivity #.###e+0
    QString serialNum= m_oAudioGet->serialNumber();
    QStringRef sFullScaleCh1(&serialNum, serialNum.indexOf("F1/") + 3, 4);
    m_oAudioGet->m_rListHeader.rIGNR.cData = sAux.sprintf("%1.3e", sFullScaleCh1.toDouble()/1000);             //PatchBetaDynamMetadata - IGNR: Chan1FullScale #.###e+0
    m_oAudioGet->m_rListHeader.rICMT.cData = sAux.sprintf("%1.3e", conversionFactor().toDouble());*/             //PatchBetaDynamMetadata - ICMT: Calibration correction #.###e+0
}

/**
*******************************************************************************
** @fn  void GetConfiguration::configAcousticMeas(bool isDefault)
** @param[in] bool isDefault  if true, reset the engine
**                            else configure from current.xml
** @param[out] void
** @brief sets the rAcousticMeasConfig structure from the default.xml file if
**         the entered parameter is true, else if false, sets it from the
**         current.xml file and configures the AcousticMeasSo engine accordingly
** @return void
**
*******************************************************************************
**/
void GetConfiguration::configAcousticMeas(bool isDefault, bool isPopup) {
    TrAcousticMeasConfig rAcousticMeasConfig = m_oAudioGet->getAcousticMeasConfig();
    AcousticMeasSo acousticMeasSo = m_oAudioGet->getAcousticMeasSo();

    if(m_oAudioGet->modeState() == 0 || m_oAudioGet->modeState() == 1) { // à voir pour le démarrage avec state 0, peut etre interessant de regrouper avec state 1
        rAcousticMeasConfig.eCalculMode = MODE_SOUNDLEVELMETER;
    } else if(m_oAudioGet->modeState() == 2) {
        rAcousticMeasConfig.eCalculMode = MODE_REVERBERATION;
    }

    if(isDefault) {
        // Configuration is set according the default.xml file
        setReset();          // set different parameters,in GUI, from default.xml file
        m_bIsCalibration=true;

    } else {
        // Configuration is set according the current.xml file
        setDefault();       // set different parameters, in GUI, from current.xml file
    }

    // Dépend du type de capteur sélectionné, ATomic1, ATomic 2.1, Accelero,...
    qDebug() << "### CONFIG Model : " << m_oAudioGet->m_eDeviceModel;
    switch (m_oAudioGet->m_eDeviceModel) {
      case ATOMIC_1:
        rAcousticMeasConfig.eChanTreated = TeChanTreated::CHAN_STEREO_FULL;
        break;

      case ATOMIC_2:
        rAcousticMeasConfig.eChanTreated = TeChanTreated::CHAN_LEFT; //Only use CH1 if AT2 Detected!!
        break;

      default:
        rAcousticMeasConfig.eChanTreated = TeChanTreated::CHAN_STEREO_FULL;
        break;
    }

    if(rAcousticMeasConfig.eCalculMode == MODE_SOUNDLEVELMETER) {

        /*!< Set Fast Integration Time MilliSec */
        rAcousticMeasConfig.eFastIntegrationTime = TeFastIntegrationTime::FIT_200MS;

        /*!< Set IT value */
        rAcousticMeasConfig.iIntegrationTimeSec = integrationTime();

        /*!< Set Time Weightning */
        if(timeWeighting().contains("LP_NONE",Qt::CaseInsensitive)) {
            rAcousticMeasConfig.rACZFilterConfig.eTimeWeightingLp = LP_NONE;
        } else if(timeWeighting().contains("LP_SLOW",Qt::CaseInsensitive)) {
            rAcousticMeasConfig.rACZFilterConfig.eTimeWeightingLp = LP_SLOW;
        } else if(timeWeighting().contains("LP_FAST",Qt::CaseInsensitive)) {
            rAcousticMeasConfig.rACZFilterConfig.eTimeWeightingLp = LP_FAST;
        } else if(timeWeighting().contains("LP_IMPULSE",Qt::CaseInsensitive)) {
            rAcousticMeasConfig.rACZFilterConfig.eTimeWeightingLp = LP_IMPULSE;
        } else {
            // Default State
            rAcousticMeasConfig.rACZFilterConfig.eTimeWeightingLp = LP_NONE;
        }

        /*!< Set Spectrum */
        if(spectrumConfiguration().contains("SPECTRUM_NO",Qt::CaseInsensitive)) {
            rAcousticMeasConfig.rSpectrumConfig.eSpectrum = SPECTRUM_NO;
        } else if(spectrumConfiguration().contains("SPECTRUM_3RD_OCTAVE",Qt::CaseSensitive)) {
            rAcousticMeasConfig.rSpectrumConfig.eSpectrum = SPECTRUM_3RD_OCTAVE;
        } else if(spectrumConfiguration().contains("SPECTRUM_OCTAVE",Qt::CaseSensitive)) {
            rAcousticMeasConfig.rSpectrumConfig.eSpectrum = SPECTRUM_OCTAVE;
        } else if(spectrumConfiguration().contains("SPECTRUM_FFT",Qt::CaseSensitive)) {
            rAcousticMeasConfig.rSpectrumConfig.eSpectrum = SPECTRUM_FFT;
        } else {
            // Default State
            rAcousticMeasConfig.rSpectrumConfig.eSpectrum = SPECTRUM_3RD_OCTAVE;
        }

        /*!< Set Nominal Sensitivity */
        rAcousticMeasConfig.rSensorParams.fSensitivitySI = static_cast<float>(nominalSensitivity().toDouble()/1000.00);//PatchNewSo
        qDebug()<<" Nominal Sensitivity : "<<rAcousticMeasConfig.rSensorParams.fSensitivitySI;

        /*!< Set Calibration Correction */
        rAcousticMeasConfig.rSensorParams.fCalibrationCorrectiondB = static_cast<float>(calibrationCorrection1().toDouble());
        qDebug()<<" Calibration Correction : "<<rAcousticMeasConfig.rSensorParams.fCalibrationCorrectiondB;

        /*!< Set Free Field Correction */
        if(traverseSelectedFile("Sensor","WindScreen","","Second Level") == "YES") {
            rAcousticMeasConfig.rSensorParams.fFreeFielCorrectiondB = traverseSelectedFile("Sensor","FreeFieldCorrectiondB","","Second Level").toFloat();
            qDebug()<<" FreeFieldCorrectiondB in default file : "<<rAcousticMeasConfig.rSensorParams.fFreeFielCorrectiondB;
        } else {
            rAcousticMeasConfig.rSensorParams.fFreeFielCorrectiondB = 0.0;
        }
        qDebug()<<" FreeFieldCorrectiondB : "<<rAcousticMeasConfig.rSensorParams.fFreeFielCorrectiondB;

        /*!< Set Calibration Level */
        if(calibrationLevel() > 0){
            rAcousticMeasConfig.rCalibrationConfig.fLeveldB = static_cast<float>(calibrationLevel());
            qDebug()<<" Calibration Level : "<<rAcousticMeasConfig.rCalibrationConfig.fLeveldB;
        } else {
            // Default State
            rAcousticMeasConfig.rCalibrationConfig.fLeveldB = 94;
            setCalibrationLevel(qCeil(94));
            qDebug()<<" Calibration Level par défault : "<<rAcousticMeasConfig.rCalibrationConfig.fLeveldB;
        }


        rAcousticMeasConfig.rSensorParams.fFullScaleVolt = chan1FullScale().toFloat() / 1000.0f;

        if ((chan1FullScale().toFloat() != 0) && (chan2FullScale().toFloat() != 0)) {
            rAcousticMeasConfig.rSensorParams.fGainInterChanneldB = 20.0f * (float)log10(chan1FullScale().toFloat() / chan2FullScale().toFloat());

            if (rAcousticMeasConfig.rSensorParams.fGainInterChanneldB > MAX_GAIN_INTERCHANNEL) {
                rAcousticMeasConfig.rSensorParams.fGainInterChanneldB = DEFAULT_GAIN_INTERCHANNEL;
            }

        } else {
            rAcousticMeasConfig.rSensorParams.fGainInterChanneldB = DEFAULT_GAIN_INTERCHANNEL;
        }

        qDebug()<<" Chan1 Full Scale : "<<chan1FullScale();
        qDebug()<<" Chan2 Full Scale : "<<chan2FullScale();

        qDebug()<<" fFullScaleVolt : "<<rAcousticMeasConfig.rSensorParams.fFullScaleVolt;

        //rAcousticMeasConfig.rSensorParams.fGainInterChanneldB = 12;
        qDebug()<<" fGainInterChanneldB : "<<rAcousticMeasConfig.rSensorParams.fGainInterChanneldB;

        /*!< Set Class Config */
        if(classConfig()=="CLASS_0") {
            rAcousticMeasConfig.rCalibrationConfig.eClassMicro = CLASS_0;
        } else if(classConfig()=="CLASS_1") {
            rAcousticMeasConfig.rCalibrationConfig.eClassMicro = CLASS_1;
        } else if(classConfig()=="CLASS_2") {
            rAcousticMeasConfig.rCalibrationConfig.eClassMicro = CLASS_2;
        } else {
            // Default State
            rAcousticMeasConfig.rCalibrationConfig.eClassMicro = CLASS_1;
        }

        qDebug()<<"AcousticMeas configure Mode SONOMETER";

    //} else if(m_oAudioGet->modeState() == 2) {
    } else if(rAcousticMeasConfig.eCalculMode == MODE_REVERBERATION) {

        /*!< Set Fast Integration Time MilliSec */
        rAcousticMeasConfig.eFastIntegrationTime = TeFastIntegrationTime::FIT_8MS;

        /*!< Set IT value */
        //rAcousticMeasConfig.iIntegrationTimeSec = integrationTime(); // A DEMANDER! Je vais chercher celui de Measure, et pas celui de Buildings?
                                                                       // C'est le même obligatoirement? Il ne faudrait pas mettre tous ses paramètres aussi dans Buildings?

        /*!< Set Time Weightning */
        //rAcousticMeasConfig.rACZFilterConfig.eTimeWeightingLp = LP_NONE; // Pareil je le récupère où lui?

        /*!< Set Spectrum */
        if(spectrumType().contains("SPECTRUM_NO",Qt::CaseInsensitive)) {
            rAcousticMeasConfig.rSpectrumConfig.eSpectrum = SPECTRUM_NO;
        } else if(spectrumType().contains("SPECTRUM_3RD_OCTAVE",Qt::CaseSensitive)) {
            rAcousticMeasConfig.rSpectrumConfig.eSpectrum = SPECTRUM_3RD_OCTAVE;
        } else if(spectrumType().contains("SPECTRUM_OCTAVE",Qt::CaseSensitive)) {
            rAcousticMeasConfig.rSpectrumConfig.eSpectrum = SPECTRUM_OCTAVE;
        } else if(spectrumType().contains("SPECTRUM_FFT",Qt::CaseSensitive)) {
            rAcousticMeasConfig.rSpectrumConfig.eSpectrum = SPECTRUM_FFT;
        } else {
            // Default State
            rAcousticMeasConfig.rSpectrumConfig.eSpectrum = SPECTRUM_3RD_OCTAVE;
        }

        /*!< Set Nominal Sensitivity */
        rAcousticMeasConfig.rSensorParams.fSensitivitySI = static_cast<float>(nominalSensitivity().toDouble()/1000.00);//PatchNewSo

        /*!< Set Calibration Correction */
        rAcousticMeasConfig.rSensorParams.fCalibrationCorrectiondB = static_cast<float>(calibrationCorrection1().toDouble());

        /*!< Set Free Field Correction */ // Pas Utile ici, on fait la calibration en Mode Somomètre
        /*if(traverseSelectedFile("Sensor","WindScreen","","Second Level") == "YES") {
            rAcousticMeasConfig.rSensorParams.fFreeFielCorrectiondB = traverseSelectedFile("Sensor","FreeFieldCorrectiondB","","Second Level").toFloat();
            qDebug()<<" FreeFieldCorrectiondB in default file : "<<rAcousticMeasConfig.rSensorParams.fFreeFielCorrectiondB;
        } else {
            rAcousticMeasConfig.rSensorParams.fFreeFielCorrectiondB = 0.0;
        }
        qDebug()<<" FreeFieldCorrectiondB : "<<rAcousticMeasConfig.rSensorParams.fFreeFielCorrectiondB;*/

        /*!< Set Index Frequency Start & End */
        rAcousticMeasConfig.rReverberationConfig.iIndexFreqStart = traverseSelectedFile("Buildings","ReverberationTime","DisplayFreqMinIndex","Third Level").toFloat();
        rAcousticMeasConfig.rReverberationConfig.iIndexFreqEnd = traverseSelectedFile("Buildings","ReverberationTime","DisplayFreqMaxIndex","Third Level").toFloat();

        /*!< Set Threshold Source */
        rAcousticMeasConfig.rReverberationConfig.fThresholdSourcedB = traverseSelectedFile("Buildings","ReverberationTime","ThresholddB","Third Level").toFloat();

        /*!< Set Time Record Decrease */
        rAcousticMeasConfig.rReverberationConfig.fTimeRecordDecrease = traverseSelectedFile("Buildings","ReverberationTime","DecreaseTimeSec","Third Level").toFloat();

        /*!< Set Min TR Sec */
        if(source().contains("IMPULSE",Qt::CaseInsensitive)) {
            rAcousticMeasConfig.rReverberationConfig.eTypeSource = SOURCE_IMPULSE;
        } else if(source().contains("INTERRUPTED",Qt::CaseInsensitive)) {
            rAcousticMeasConfig.rReverberationConfig.eTypeSource = SOURCE_INTERRUPTED;
        } else {
            // Default State
            rAcousticMeasConfig.rReverberationConfig.eTypeSource = SOURCE_IMPULSE;
        }

        /*!< Set Sec Measure Source */
        rAcousticMeasConfig.rReverberationConfig.iNbSecMeasureSource = traverseSelectedFile("Buildings","ReverberationTime","NbSecMeasureSource","Third Level").toFloat();

        /*!< Set Min TR Sec */
        rAcousticMeasConfig.rReverberationConfig.fMinTrSec = traverseSelectedFile("Buildings","ReverberationTime","MinTrSec","Third Level").toFloat();

        /*!< Set Max Noise Level */
        //rAcousticMeasConfig.rReverberationConfig.fMaxNoiseLeveldB = traverseSelectedFile("Buildings","ReverberationTime","MaxNoiseLeveldB","Third Level").toFloat();
        rAcousticMeasConfig.rReverberationConfig.fMaxNoiseLeveldB = 75.0;

        /*!< Set Sec Measure Noise */ // Not editable by the User
        rAcousticMeasConfig.rReverberationConfig.iNbSecMeasureNoise = traverseSelectedFile("Buildings","ReverberationTime","NbSecMeasureNoise","Third Level").toFloat();

        /*!< Set Sec Stabilization Source */ // Not editable by the User
        rAcousticMeasConfig.rReverberationConfig.iNbSecStabilizationSource = traverseSelectedFile("Buildings","ReverberationTime","NbSecStabilizationSource","Third Level").toFloat();

        /*!< Set Calibration Level */
        if(calibrationLevel() > 0){
            rAcousticMeasConfig.rCalibrationConfig.fLeveldB = static_cast<float>(calibrationLevel());
        } else {
            // Default State
            rAcousticMeasConfig.rCalibrationConfig.fLeveldB = 94;
            setCalibrationLevel(qCeil(94));
        }

        /*!< Set Class Config */
        if(classConfig()=="CLASS_0") {
            rAcousticMeasConfig.rCalibrationConfig.eClassMicro = CLASS_0;
        } else if(classConfig()=="CLASS_1") {
            rAcousticMeasConfig.rCalibrationConfig.eClassMicro = CLASS_1;
        } else if(classConfig()=="CLASS_2") {
            rAcousticMeasConfig.rCalibrationConfig.eClassMicro = CLASS_2;
        } else {
            // Default State
            rAcousticMeasConfig.rCalibrationConfig.eClassMicro = CLASS_1;
        }

        qDebug()<<"AcousticMeas configure Mode TR";
    }

    acousticMeasSo.AcousticMeasSo_Escape();
    acousticMeasSo.AcousticMeasSo_Configure(rAcousticMeasConfig, &m_afFullScaledB[0], &m_afFullScaleLin[0], &m_afFftRealOverlapPercent[0]);

    // The AcousticMeasSo is successfuly configured
    emit configurationEnded(rAcousticMeasConfig);

    /** Pop up view */
    if(isPopup && !m_bIsStart && !m_bIsCalibration){
        emit configurationSuccess();
    }

    m_bIsCalibration=false;

    //DEBUG
    /*qDebug()<<"eChanTreated : "<<rAcousticMeasConfig.eChanTreated;
    qDebug()<<"eFastIntegrationTime : "<<rAcousticMeasConfig.eFastIntegrationTime;
    qDebug()<<"iIntegrationTimeSec : "<<rAcousticMeasConfig.iIntegrationTimeSec;
    qDebug()<<"eSpectrum : "<<rAcousticMeasConfig.rSpectrumConfig.eSpectrum;
    qDebug()<<"fSensitivitySI : "<<rAcousticMeasConfig.rSensorParams.fSensitivitySI;
    qDebug()<<"fCalibrationCorrectiondB : "<<rAcousticMeasConfig.rSensorParams.fCalibrationCorrectiondB;
    qDebug()<<"fFreeFielCorrectiondB : "<<rAcousticMeasConfig.rSensorParams.fFreeFielCorrectiondB;
    qDebug()<<"iIndexFreqStart : "<<rAcousticMeasConfig.rReverberationConfig.iIndexFreqStart;
    qDebug()<<"iIndexFreqEnd : "<<rAcousticMeasConfig.rReverberationConfig.iIndexFreqEnd;
    qDebug()<<"fThresholdSourcedB : "<<rAcousticMeasConfig.rReverberationConfig.fThresholdSourcedB;
    qDebug()<<"fTimeRecordDecrease : "<<rAcousticMeasConfig.rReverberationConfig.fTimeRecordDecrease;
    qDebug()<<"eTypeSource : "<<rAcousticMeasConfig.rReverberationConfig.eTypeSource;
    qDebug()<<"iNbSecMeasureSource : "<<rAcousticMeasConfig.rReverberationConfig.iNbSecMeasureSource;
    qDebug()<<"fMinTrSec : "<<rAcousticMeasConfig.rReverberationConfig.fMinTrSec;
    qDebug()<<"fMaxNoiseLeveldB : "<<rAcousticMeasConfig.rReverberationConfig.fMaxNoiseLeveldB;
    qDebug()<<"iNbSecMeasureNoise : "<<rAcousticMeasConfig.rReverberationConfig.iNbSecMeasureNoise;
    qDebug()<<"iNbSecStabilizationSource : "<<rAcousticMeasConfig.rReverberationConfig.iNbSecStabilizationSource;
    qDebug()<<"fLeveldB : "<<rAcousticMeasConfig.rCalibrationConfig.fLeveldB;
    qDebug()<<"eClassMicro : "<<rAcousticMeasConfig.rCalibrationConfig.eClassMicro;*/
}

/**
*******************************************************************************
** @fn  void GetConfiguration::setConfigurationAtStart()( )
** @param[in] void
** @param[out] void
** @brief sets the rAcousticMeasConfig structure from the current.xml file if
**         existed else from the default.xml
** @return void
**
*******************************************************************************
**/
void GetConfiguration::setConfigurationAtStart(){
    initializeCfg();                //creates needed files and initializes GUI parameters
    m_bIsStart=true;

    //configure AcousticMeasSo
    if(m_currentFile.exists()){
        configAcousticMeas(false, false);
    }
    else{
        configAcousticMeas(true, false);
    }
    m_bIsStart=false;
}

/**
*******************************************************************************
** @fn  void GetConfiguration::uponSonoStateChange()
** @param[in] void
** @param[out] void
** @brief sets the state of the configuration and calibration upon the changes
**          of the sonometer state
** @return void
**
*******************************************************************************
**/
void GetConfiguration::uponSonoStateChange(){

    if(m_oAudioGet->sonoState() == SM_SONOMETER_READY){
        // in this state, both the configuration and calibration are enabled
        setCfgState(CFG_ENABLED);
        setCalibState(CALIB_READY);
        setLibAudioCalibStatus(CAL_NOT_RUNNING);
        m_ePreviousStatusOfCalibration=CAL_NOT_RUNNING;
    }
    else if (m_oAudioGet->sonoState() == SM_CALIBRATION) {
        // Only calibration is enabled
        setCfgState(CFG_DISABLED);
        setCalibState(CALIB_BEFORE_CORRECTION);
    }
    else {
        // both the configuration and calibration are disabled
        setCfgState(CFG_DISABLED);
        setCalibState(CALIB_DISABLED);
    }
}

/****** Calibration ***********/

/**
*******************************************************************************
** @fn  void GetConfiguration::setCalibration(bool isDefault)
** @param[in] bool isDefault: set default calibration's parameters if true
**                            else set last-saved-calibation parameters
** @param[out] void
** @brief sets the calibration's parameters (updates the calibraion's screen
**        values)
** @return void
**
*******************************************************************************
**/
void GetConfiguration::setCalibration(bool isDefault) {
    //Sets GUI parameters
    if(isDefault) {
        setAudioLabSensitivity(QString::number(traverseDefaultFile("Sensor","LaboratorySensitivitySI","","Second Level").toDouble(),'f',2)); //setCalibLabSensitivity
        setCalibrationCorrection1(QString::number(traverseDefaultFile("Calibration","CalibrationCorrectiondB","","Second Level").toDouble(),'f',2));
        setLastCalibrationDate(traverseDefaultFile("Calibration","Date","","Second Level"));
        setCalibrationLevel(qCeil(traverseDefaultFile("Calibration","Level","","Second Level").toDouble()));
        setClassConfig(traverseDefaultFile("Sensor","Class","","Second Level"));

    } else {
        setAudioLabSensitivity(QString::number(traverseCurrentFile("Sensor","LaboratorySensitivitySI","","Second Level").toDouble(),'f',2)); //setCalibLabSensitivity
        setCalibrationCorrection1(QString::number(traverseCurrentFile("Calibration","CalibrationCorrectiondB","","Second Level").toDouble(),'f',2));
        setLastCalibrationDate(traverseCurrentFile("Calibration","Date","","Second Level"));
        setCalibrationLevel(qCeil(traverseCurrentFile("Calibration","Level","","Second Level").toDouble()));
        setClassConfig(traverseCurrentFile("Sensor","Class","","Second Level"));

        //AllianTech - Patch recalcule realsesitivity when loading current.xml
        double dLabSensi = traverseCurrentFile("Sensor","LaboratorySensitivitySI","","Second Level").toDouble();
        double dCalibCor = traverseCurrentFile("Calibration","CalibrationCorrectiondB","","Second Level").toDouble();
        double dRealSensi = REAL_SENSITIVITY(dLabSensi, dCalibCor);
        setRealSensivity(QString::number(dRealSensi,'f',2));
    }
}

/**
*******************************************************************************
** @fn  bool GetConfiguration::configureAndStartSono
**                                (TrAcousticMeasConfig rAcousticMeasConfig)
** @param[in] TrAcousticMeasConfig rAcousticMeasConfig
** @param[out] bool: returns true upon success
** @brief configure the engine at launching time of the application
** @return bool  1: success
**               0: failure
**
*******************************************************************************
**/
bool GetConfiguration::configureAndStartSono(TrAcousticMeasConfig rAcousticMeasConfig) {
    //Reinitialize all status of data
    m_oAudioGet->reset();
    //Configure measure
    m_bIsCalibration=true;
    m_oAudioGet->m_acousticMeas.AcousticMeasSo_Escape();
    m_oAudioGet->m_acousticMeas.AcousticMeasSo_Configure(rAcousticMeasConfig, &m_afFullScaledB[0], &m_afFullScaleLin[0], &m_afFftRealOverlapPercent[0]);

    //At start no block lost
    m_oAudioGet->setBlockLost(0);
    m_oAudioGet->setIsDanger(false);

    //Update state machine CALIBRATION
    if(rAcousticMeasConfig.eCalculMode == MODE_CALIBRATION) {
        //Initialize state machine of SonoMaster
        m_oAudioGet->setSonoState(SM_CALIBRATION);
        m_oAudioGet->m_rAcousticMeasConfig = rAcousticMeasConfig;
        return true;
    } else {
        return false;
    }
}

/**
*******************************************************************************
** @fn  void  startCalibration()
** @param[in] void
** @param[out] void
** @brief start the calibration process of the AcousticMeas
** @return void
**
*******************************************************************************
**/
void GetConfiguration::startCalibration() {
    TrAcousticMeasConfig rAcousticMeasConfig = m_oAudioGet->getAcousticMeasConfig();
    AcousticMeasSo acousticMeasSo = m_oAudioGet->getAcousticMeasSo();

    // The calibration can be initiated only when the Sonometer is at SM_SONOMETER_READY state
    if(m_oAudioGet->sonoState() == SM_SONOMETER_READY) {
        m_bIsCalibration=true;
        rAcousticMeasConfig.eCalculMode = MODE_CALIBRATION;
        rAcousticMeasConfig.eChanTreated = CHAN_LEFT;
        rAcousticMeasConfig.eFastIntegrationTime = FIT_200MS;
        rAcousticMeasConfig.iIntegrationTimeSec = 1;

        rAcousticMeasConfig.rSensorParams.fSensitivitySI = static_cast<float>(nominalSensitivity().toDouble()/1000.00); //PatchNewSo

        /*!< Set Calibration Level */
        if(calibrationLevel() > 0){
            rAcousticMeasConfig.rCalibrationConfig.fLeveldB = static_cast<float>(calibrationLevel());
        } else {
            // Default State
            rAcousticMeasConfig.rCalibrationConfig.fLeveldB = 94;
            setCalibrationLevel(qCeil(94));
        }

        /*!< Set Class Config */
        if(classConfig()=="CLASS_0") {
            rAcousticMeasConfig.rCalibrationConfig.eClassMicro = CLASS_0;
        } else if(classConfig()=="CLASS_1") {
            rAcousticMeasConfig.rCalibrationConfig.eClassMicro = CLASS_1;
        } else if(classConfig()=="CLASS_2") {
            rAcousticMeasConfig.rCalibrationConfig.eClassMicro = CLASS_2;
        } else {
            // Default State
            rAcousticMeasConfig.rCalibrationConfig.eClassMicro = CLASS_1;
        }

        acousticMeasSo.AcousticMeasSo_Escape();
        acousticMeasSo.AcousticMeasSo_Configure(rAcousticMeasConfig, &m_afFullScaledB[0], &m_afFullScaleLin[0], &m_afFftRealOverlapPercent[0]);

        m_oAudioGet->m_oLogFile->WriteLogFile(EVENT_START_CALIBRATION);

        //Update state machine CALIBRATION
        m_oAudioGet->setIsDanger(false);
        setCalibState(CALIB_BEFORE_CORRECTION);
        m_oAudioGet->setSonoState(SM_CALIBRATION);
        m_oAudioGet->setBlockLost(0);

        setLibCalibStatus(CAL_NOT_RUNNING);
        setLceq("");
        setCalibrationCorrection2("");
        setCounter("");
        setRealSens("");
        m_CalibTimer->start();
    }
}

/**
*******************************************************************************
** @fn  void GetConfiguration::onStatusOfCalibrationChanged()
** @param[in] void
** @param[out] void
** @brief updates the GUI depending on the calibration progress
** @return
**
*******************************************************************************
**/
void GetConfiguration::onStatusOfCalibrationChanged() {
    setLibCalibStatus(m_oAudioGet->m_rAcousticRes.rCalibrationRes.eStatusOfCalibration);

    switch(libCalibStatus()) {
    case CAL_NOT_RUNNING: {
        setLceq("");
        setCalibrationCorrection2("");
        setCounter("");
        setRealSens("");
        m_CalibTimer->start();
    }
        break;

    case SEARCHING_LEVEL: {
        setLceq(QString::number(static_cast<double>(qAbs(m_oAudioGet->m_rAcousticRes.rCalibrationRes.fLeqCdB)),'f',2));
        setCounter(QString::number(m_oAudioGet->m_rAcousticRes.rCalibrationRes.iCountMeas));
        setRealSens(QString::number(realSensivity().toDouble(),'f',2));
        m_CalibTimer->start();
    }
        break;

    case LOOK_FOR_STABILIZATION: {
        setLceq(QString::number(static_cast<double>(qAbs(m_oAudioGet->m_rAcousticRes.rCalibrationRes.fLeqCdB)),'f',2));
        setRealSens(QString::number(REAL_SENSITIVITY(labSensitivity().toDouble(),static_cast<double>(m_oAudioGet->m_rAcousticRes.rCalibrationRes.fCalibrationCorrectiondB)*1.00),'f',2));
        setCounter(QString::number(m_oAudioGet->m_rAcousticRes.rCalibrationRes.iCountMeas));
        m_CalibTimer->start();
    }
        break;

    case WAIT_FOR_STABILIZED: {
        setLceq(QString::number(static_cast<double>(qAbs(m_oAudioGet->m_rAcousticRes.rCalibrationRes.fLeqCdB)),'f',2));
        setCounter(QString::number(m_oAudioGet->m_rAcousticRes.rCalibrationRes.iCountMeas));
        setRealSens(QString::number(REAL_SENSITIVITY(labSensitivity().toDouble(),static_cast<double>(m_oAudioGet->m_rAcousticRes.rCalibrationRes.fCalibrationCorrectiondB)*1.00),'f',2));
        m_CalibTimer->start();
    }
        break;

    case CALIBRATION_DONE: {
        setLceq(QString::number(static_cast<double>(qAbs(m_oAudioGet->m_rAcousticRes.rCalibrationRes.fLeqCdB)),'f',2));
        setCounter(QString::number(m_oAudioGet->m_rAcousticRes.rCalibrationRes.iCountMeas));
        setCalibrationCorrection2(QString::number(static_cast<double>(m_oAudioGet->m_rAcousticRes.rCalibrationRes.fCalibrationCorrectiondB),'f',2));
        setRealSens(QString::number(REAL_SENSITIVITY(labSensitivity().toDouble(),calibrationCorrection2().toDouble()),'f',2));
        setCalibState(CALIB_AFTER_CORRECTION);
        m_CalibTimer->start();
    }
        break;

    case TIMEOUT_CALIB: {
        if(m_ePreviousStatusOfCalibration != TIMEOUT_CALIB) {
            m_ePreviousStatusOfCalibration = TIMEOUT_CALIB;

            if(calibrationCorrection2()==""){
                //If no calibration value is reached after 60 seconds, calibration is automatically canceled
                emit calibrationTimeOut();
                m_CalibTimer->stop();

            } else {
                //If the calibration value has been found but has not been validated after 60 seconds
                m_CalibTimer->stop();
                emit calibrationTimeOutSuccess();
            }
        }
    }
        break;
    }
}

/**
*******************************************************************************
** @fn  void GetConfiguration::validateCalibration()
** @param[in] void
** @param[out] void
** @brief validate and store the achieved calibration
** @return
**
*******************************************************************************
**/
void GetConfiguration::validateCalibration() {

    setRealSensivity(QString::number(realSens().toDouble(),'f',2));
    m_oAudioGet->m_rAcousticMeasConfig.rSensorParams.fCalibrationCorrectiondB=static_cast<float>(calibrationCorrection2().toDouble());
    m_oAudioGet->m_rAcousticMeasConfig.rSensorParams.fSensitivitySI = static_cast<float>(nominalSensitivity().toDouble()/1000.00); //PatchNewSo

    storeCalibration();        // Save the calibration: updates the current.xml file and set AcousticMeas related parameters
    m_oAudioGet->m_oLogFile->WriteLogFile(EVENT_STOP_CALIBRATION);
    m_oAudioGet->setIsDanger(false);
    m_oAudioGet->m_acousticMeas.AcousticMeasSo_Escape();
    m_oAudioGet->m_rAcousticMeasConfig.eCalculMode=MODE_SOUNDLEVELMETER;
    m_oAudioGet->m_acousticMeas.AcousticMeasSo_Configure(m_oAudioGet->m_rAcousticMeasConfig, &m_afFullScaledB[0], &m_afFullScaleLin[0], &m_afFftRealOverlapPercent[0]);
}

/**
*******************************************************************************
** @fn  void GetConfiguration::stopCalibration(bool bStopCalib)
** @param[in] bool bStopCalib
** @param[out] void
** @brief stopes the current calibration
** @return
**
*******************************************************************************
**/
void GetConfiguration::stopCalibration(bool bStopCalib) {
    if(bStopCalib) {
        m_oAudioGet->m_oLogFile->WriteLogFile(EVENT_STOP_CALIBRATION);
        m_oAudioGet->setIsDanger(false);
        setDefault();
        m_CalibTimer->stop();
        m_oAudioGet->m_rAcousticMeasConfig.eCalculMode=MODE_SOUNDLEVELMETER;

        //Update sonometer states
        setLibCalibStatus(CAL_NOT_RUNNING);
        m_oAudioGet->setSonoState(SM_SONOMETER_READY);
        setCalibState(CALIB_READY);
        m_oAudioGet->m_acousticMeas.AcousticMeasSo_Escape();
        m_oAudioGet->m_acousticMeas.AcousticMeasSo_Configure(m_oAudioGet->m_rAcousticMeasConfig, &m_afFullScaledB[0], &m_afFullScaleLin[0], &m_afFftRealOverlapPercent[0]);

    } else {
        emit checkState();         // signal emitted to check the calibration progress
    }
}

/**
*******************************************************************************
** @fn  void GetConfiguration::stopAfterCorrection()
** @param[in] void
** @param[out] void
** @brief stop the calibration without storing it
** @return
**
*******************************************************************************
**/

void GetConfiguration::stopAfterCorrection() {
    m_oAudioGet->m_oLogFile->WriteLogFile(EVENT_STOP_CALIBRATION);
    m_oAudioGet->setIsDanger(false);
    m_CalibTimer->stop();
}

/**
*******************************************************************************
** @fn  void GetConfiguration::storeCalibration()
** @param[in] void
** @param[out] void
** @brief stores the calibration-resulted values in the current.xml file
**        and sets it in the AcousticMeas structure
** @return
**
*******************************************************************************
**/
void GetConfiguration::storeCalibration() {
    setCalibrationCorrection1(QString::number(static_cast<double>(m_oAudioGet->m_rAcousticRes.rCalibrationRes.fCalibrationCorrectiondB),'f',2));
    setCalibrationLevel(static_cast<double>(m_oAudioGet->m_rAcousticMeasConfig.rCalibrationConfig.fLeveldB));
    m_bIsCalibration=true;
    writeCurrentFile();
    m_CalibTimer->stop();
    setLibCalibStatus(CAL_NOT_RUNNING);
    m_oAudioGet->setSonoState(SM_SONOMETER_READY);
    setCalibState(CALIB_READY);
}

