
/**
*******************************************************************************
** @file    audioget.h
** @version
** @author  Wevioo SE Team
** @brief   Collects audio data and implements a recover process from error
**
*******************************************************************************
**/

#ifndef AUDIOGET_H
#define AUDIOGET_H

#include <QObject>
#include <QtAndroid>
#include <QtAndroidExtras/QAndroidJniObject>
#include <QFile>
#include <QtCore>
#include <math.h>
#include "android/libs/AcousticMeasSo.h"
#include "android/libs/AcousticMeasTypes.h"
#include "AudioInterface/AudioInterface.h"
#include "AudioLog/LogFile.h"
#include "WavFile/WavFile.h"

#include <QAndroidService>
#include "rep_atomic_source.h"
#pragma once
//#include <stdint.h> //kuba - to use int64_t

//Define
#define SIZE_BLOCK 9600

/**
*******************************************************************************
** @enum  TeStateSonoMaster
** @brief represents the status of the overall sonometer application
**
*******************************************************************************
**/
typedef enum{
  SM_STANDBY=0,
  SM_SONOMETER_READY, //1
  SM_SONOMETER_LOGGING, //2
  SM_SONOMETER_WAV_RECORDING, //3
  SM_CALIBRATION, //4
  SM_REVERBERATION_TIME_READY, //5
  SM_REVERBERATION_TIME_RUNNING, //6
  SM_FACTORY_SETTINGS, //Not Implemented
  SM_ERROR, //8
  SM_IDLE //9
}TeStateSonoMaster;   /*!< different states of the ATomic Sound application */


/**
*******************************************************************************
** @enum  TeStateMode
** @brief
**
*******************************************************************************
**/
typedef enum{
  MODE_ENV=0,
  MODE_BA //1
}TeStateMode;   /*!< different states of the ATomic Sound application */

/**
*******************************************************************************
** @enum  TeFileType
** @brief represents the type of the file: .csv, .wav or .xml file
**
*******************************************************************************
**/
typedef enum{
  CSV=0,
  WAV,
  XML
}TeFileType;

class AudioGet : public atomicSimpleSource
{
  Q_OBJECT
  Q_PROPERTY(quint8 audioSonoState READ audioSonoState WRITE setAudioSonoState NOTIFY audioSonoStateChanged)
  Q_PROPERTY(int audioBlockLost READ audioBlockLost WRITE setAudioBlockLost NOTIFY audioBlockLostChanged)
  Q_PROPERTY(QString chronometer READ chronometer WRITE setChronometer NOTIFY chronometerChanged)
  Q_ENUM(TeStateSonoMaster)

public:
  /**
  *******************************************************************************
  ** @fn  AudioGet(QObject *parent) : QObject(parent)
  ** @param[inout] QObject
  ** @brief Constructor of AudioGet class
  ** @return
  **
  *******************************************************************************
  **/
  explicit AudioGet(QObject* parent=nullptr);     /*!< AudioGet class constructor */

  /**
  *******************************************************************************
  ** @fn  ~AudioGet()
  ** @param[in]
  ** @param[out]
  ** @brief AudioGet's class Destructor
  ** @return deletes measurements
  **
  *******************************************************************************
  **/
  virtual ~AudioGet() override;                   /*!< AudioGet destructor */

  /**
  *******************************************************************************
  ** @fn  void setAudioSonoState(quint8 uiState)
  ** @param[in] quint8 sets the state of Sonometer Master (SM)
  ** @param[out] void
  ** @brief sets the SM state
  ** @return void
  **
  *******************************************************************************
  **/
  void setAudioSonoState(int uiState);            /*!< this function allows the application's state to be set from GUI */

  /**
  *******************************************************************************
  ** @fn  quint8 audioSonoState()
  ** @param[in] void
  ** @param[out] quint8 : SONOMETER current state
  ** @brief gets the SM state
  ** @return void
  **
  *******************************************************************************
  **/
  quint8 audioSonoState(){                        /*!< this function allows the application's state to be read from GUI */
    return m_eStateSonoMaster;
  }

  /**
  *******************************************************************************
  ** @fn  int audioCodified()
  ** @param[in] void
  ** @param[out] int
  ** @brief gets the codified current value
  ** @return void
  **
  *******************************************************************************
  **/
  int audioCodified(){return m_iCodified;}

  /**
  *******************************************************************************
  ** @fn  void setAudioBlockLost(int iError)
  ** @param[in] int iError: if iError != 0 than a block lost has occured
  **                         else reset the block lost counter
  ** @param[out] void
  ** @brief updates the block lost counter, the sonometer state and GUI.
  **        if iError != 0 than a block lost has occured
  **        else no block lost detected
  ** @return void
  **
  *******************************************************************************
  **/
  void setAudioBlockLost(int);

  /**
  *******************************************************************************
  ** @fn  void AudioGet::
  ** @param[in]
  ** @param[out] void
  ** @brief
  ** @return void
  **
  *******************************************************************************
  **/
  void sendBufferFITBA();

  /**
  *******************************************************************************
  ** @fn  int audioBlockLost()
  ** @param[in] void
  ** @param[out] int : blockLost counter
  ** @brief gets the blocklost counter
  ** @return void
  **
  *******************************************************************************
  **/
  int audioBlockLost(){return m_iBlockLostCount;}

  /**
  *******************************************************************************
  ** @fn  void CreateDirectory()
  ** @param[in] void
  ** @param[out] void
  ** @brief Creates the Alliantech directory to where all generated files will
  **        be stored
  ** @return
  **
  *******************************************************************************
  **/
  void CreateDirectory();

  /**
  *******************************************************************************
  ** @fn  void getFileError(QFileDevice::FileError fileError, TeFileType eFileType)
  ** @param[in] QFileDevice::FileError fileError
  ** @param[in] TeFileType eFileType
  ** @param[out] void
  ** @brief returns if a file error has occured and behaves accordingly: updates
  **        the GUI by notifying the user.
  ** @return void
  **
  *******************************************************************************
  **/
  void getFileError(QFileDevice::FileError ,TeFileType);

  /**
  *******************************************************************************
  ** @fn  void WriteWav(float*)
  ** @param[in] float* : combined data issued from AcousticMeasSo_NewBlock
  ** @param[out] void
  ** @brief appends new data into a wave file (*.wav) currently in use
  **        this function is accessed only when the sonometer is at
  **        SONOMETER_WAV_RECORDING state
  ** @return
  **
  *******************************************************************************
  **/
  void WriteWav(float*);

  /**
  *******************************************************************************
  ** @fn  void getATomicInfo()
  ** @param[in] void
  ** @param[out] void
  ** @brief sets the ATomic card related informations: - serial number
  **                                                   - product identification
  ** @return void
  **
  *******************************************************************************
  **/
  void getATomicInfo();

  /*! Creating Chronometer */
  /**
  *******************************************************************************
  ** @fn  QString chronometer()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the current chronometer's value in the specified format
  **               hh:mm:ss
  ** @return
  **
  *******************************************************************************
  **/
  QString chronometer();

  /**
  *******************************************************************************
  ** @fn  QString AudioGet::setChronometer()
  ** @param[in] QString
  ** @param[out] void
  ** @brief sets the current chronometer's value
  ** @return
  **
  *******************************************************************************
  **/
  void setChronometer(QString);

  /**
  *******************************************************************************
  ** @fn  TrAcousticMeasConfig getAcousticMeasConfig()
  ** @param[in] void
  ** @param[out] TrAcousticMeasConfig
  ** @brief gets the TrAcousticMeasConfig instance
  ** @return
  **
  *******************************************************************************
  **/
  TrAcousticMeasConfig getAcousticMeasConfig(){ return m_rAcousticMeasConfig;}

  /**
  *******************************************************************************
  ** @fn  AcousticMeasSo   getAcousticMeasSo()
  ** @param[in] void
  ** @param[out] AcousticMeasSo
  ** @brief gets the AcousticMeasSo object
  ** @return
  **
  *******************************************************************************
  **/
  AcousticMeasSo   getAcousticMeasSo(){  return   m_acousticMeas;}

  /**
  *******************************************************************************
  ** @fn  void setFilename(QString cFileName)
  ** @param[in] QString cFileName: introduced file name in GUI by user
  ** @param[out] void
  ** @brief modify the file name of .wav and .csv files to the introduced argument
  ** @return
  **
  *******************************************************************************
  **/
  void setFilename(QString cFileName){
    m_cFileName=cFileName;
  }

signals:
  void audioCodifiedChanged();                     /*!< emitted upon click on the codified button, at the buttom-left of the screen */
  void audioSonoStateChanged();                    /*!< emitted upon change of the sonometer state */
  void audioIsATomicFoundChanged();                /*!< emitted upon physical connection or disconnection of the ATomic Card */
  void audioBlockLostChanged();                    /*!< emitted upon block lost detection */
  void handleError();                              /*!< emitted whenever an error has occured in order to restore previous state */
  void chronometerChanged();                       /*!< emitted every ellapsed second or reset */

public slots:

  /**
  *******************************************************************************
  ** @fn  void InitConfigurationFiles(TrAcousticMeasConfig rAcousticMeasCfg)
  ** @param[in] TrAcousticMeasConfig rAcousticMeasCfg
  ** @param[out] void
  ** @brief (Slot) called everytime the configuration has been changed:
  **                      upon reset of the configuration
  **                   or upon application of new configuration
  ** @return void
  **
  *******************************************************************************
  **/
  void InitConfigurationFiles(TrAcousticMeasConfig);

  /** ********************************************************** **/
  /**                      SERVICE                               **/
  /** ********************************************************** **/

  /**
  *******************************************************************************
  ** @fn  void setAudioCodified(int iCodified)
  ** @param[in] int iCodified sets the codified parameter
  ** @param[out] void
  ** @brief updates the codified parameter: in the GUI and CSV files
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void setAudioCodified(int state) override;

  /**
  *******************************************************************************
  ** @fn  void
  ** @param[in] int
  ** @param[out] void
  ** @brief
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void setModeApp(int state) override;

  /**
  *******************************************************************************
  ** @fn  void stopAcquisition()
  ** @param[in] void
  ** @param[out] void
  ** @brief stop acquisition and closes all opened files
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void stopAcquisition() override;

  /**
  *******************************************************************************
  ** @fn  void AudioGet::stopWAVAcquisition()
  ** @param[in] void
  ** @param[out] void
  ** @brief stop acquisition and closes all opened files
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void stopWAVAcquisition() override;

  /**
  *******************************************************************************
  ** @fn  void AudioGet::stopReverbCounter()
  ** @param[in] void
  ** @param[out] void
  ** @brief stop acquisition and closes all opened files
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void stopReverbCounter() override;

  /**
  *******************************************************************************
  ** @fn  void AudioGet::resetLastTrMeasure()
  ** @param[in] void
  ** @param[out] void
  ** @brief stop acquisition and closes all opened files
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void resetLastTrMeasure() override;

  /**
  *******************************************************************************
  ** @fn  void AudioGet::reinitReverb()
  ** @param[in] void
  ** @param[out] void
  ** @brief
  ** @return void
  **
  *******************************************************************************
  **/
  //virtual void reinitReverb() override;

  /**
  *******************************************************************************
  ** @fn  void AudioGet::reinitPlot()
  ** @param[in] void
  ** @param[out] void
  ** @brief
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void reinitPlot() override;

  /**
  *******************************************************************************
  ** @fn  void checkATomic()
  ** @param[in] void
  ** @param[out] void
  ** @brief checks weither the ATomic Card is connected via USB or not.
  **        in case the ATomic Card is disconnected, a pop-up is shown and this
  **        slot keeps spying on USB until the card is either connected or the user
  **        cancels the operation than the application will quit
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void checkATomic() override;

  /**
  *******************************************************************************
  ** @fn  void AudioGet::launchNewMode()
  ** @param[in] void
  ** @param[out] void
  ** @brief
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void launchNewMode() override;

  /**
  *******************************************************************************
  ** @fn  void start()
  ** @param[in] void
  ** @param[out] void
  ** @brief (Slot)Detects, initializes ATomic Card and outputs its caracteristics
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void start() override;                                   /*!< slot called when start button is clicked */

  /**
  *******************************************************************************
  ** @fn  void startReverbMode()
  ** @param[in] void
  ** @param[out] void
  ** @brief
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void startReverbMode() override;

  /**
  *******************************************************************************
  ** @fn  void stop()
  ** @param[in] void
  ** @param[out] void
  ** @brief stop recording and writes final wave file
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void stop() override;                                    /*!< slot called when stop button is clicked */

  /**
  *******************************************************************************
  ** @fn  void AudioGet::startReverbMode()
  ** @param[in] void
  ** @param[out] void
  ** @brief (Slot)Detects, initializes ATomic Card and outputs its caracteristics
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void stopReverbMode() override;

  /**
  *******************************************************************************
  ** @fn  void openCsv()
  ** @param[in] void
  ** @param[out] void
  ** @brief creates, opens CSV files (IT and FIT) and updates the state machine
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void openCsv() override;                                 /*!< creates CSV files to store measured data */

  /**
  *******************************************************************************
  ** @fn  void AudioGet::deleteCsv()
  ** @param[in] void
  ** @param[out] void
  ** @brief
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void deleteCsv() override;

  /**
  *******************************************************************************
  ** @fn  void record()
  ** @param[in] void
  ** @param[out] void
  ** @brief this slot is called to start recording of WAV files
  **        and updates the sonometer state to SONOMETER_WAV_RECORDING state
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void record() override;                                  /*!< due to its call, record will begin and thus the generation of wave files */

  /**
  *******************************************************************************
  ** @fn  void onSusspended()
  ** @param[in] void
  ** @param[out] void
  ** @brief this slot depends on the current state of the sonometer
  **        if the sonometer is at the SONOMETER_WAV_RECORDING state, then finishes
  **        all recording and closes opened wav files than transits
  **        to SONOMETER_LOGGING state
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void onSusspended() override;                            /*!< slot called whenever the application is going to be susspended */

  /**
  *******************************************************************************
  ** @fn  void reset()
  ** @param[in] void
  ** @param[out] void
  ** @brief resets the AcousticMeas, the sonometer state and GUI
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void reset() override;                                   /*!< this methods resets the AcousticMeas engine */

  /**
  *******************************************************************************
  ** @fn  void onDisconnected()
  ** @param[in] void
  ** @param[out] void
  ** @brief (Slot) called whenever the ATomic card is disconnected.
  **          all recording is stopped and returns to STANDBY state
  **          in case the application is at:
  **          SONOMETER_WAV_RECORDING or SONOMETER_LOGGING state, all opened files
  **          ( .wav and/or .csv files)
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void onDisconnected() override;                          /*!< slot called whenever the card is disconnected */

  /**
  *******************************************************************************
  ** @fn  updateChronometer()
  ** @param[in] void
  ** @param[out] void
  ** @brief this slot, updates the chronometer display in QML part each 1 second
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void updateChronometer() override;                       /*!< slot called to update the chronometer at the above pannel of thr GUI */

  /**
  *******************************************************************************
  ** @fn  void resetChrono()
  ** @param[in] void
  ** @param[out] void
  ** @brief resets the current chronometer, restart from 00:00:00
  ** @return
  **
  *******************************************************************************
  **/
  virtual void resetChrono() override;

  /**
  *******************************************************************************
  ** @fn  void recover()
  ** @param[in] void
  ** @param[out] void
  ** @brief this slot is called whenever an error has occured and assures the
  **          recovering of the state before the error appearence.
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void recover() override;                                 /*!< called due a bloc lost, assure the recovering from the state of SM_ERROR) */

  /**
  *******************************************************************************
  ** @fn  void changeProjectName()
  ** @param[in] void
  ** @param[out] void
  ** @brief this slot is called whenever an error has occured and assures the
  **          recovering of the state before the error appearence.
  ** @return void
  **
  *******************************************************************************
  **/
  //virtual void changeProjectName(QString) override;

  /**
  *******************************************************************************
  ** @fn  void changeRoomType()
  ** @param[in] void
  ** @param[out] void
  ** @brief this slot is called whenever an error has occured and assures the
  **          recovering of the state before the error appearence.
  ** @return void
  **
  *******************************************************************************
  **/
  //virtual void changeRoomType(QString) override;

  /**
  *******************************************************************************
  ** @fn  void changeRoomNumber()
  ** @param[in] void
  ** @param[out] void
  ** @brief this slot is called whenever an error has occured and assures the
  **          recovering of the state before the error appearence.
  ** @return void
  **
  *******************************************************************************
  **/
  //virtual void changeRoomNumber(int) override;

private slots:
  /**
  *******************************************************************************
  ** @fn  void readData(QList<double> data, int error)
  ** @param[in] QList<double> data
  ** @param[in] int error
  ** @param[out] void
  ** @brief Reads data every period (200ms) and the result error
  ** @return void
  **
  *******************************************************************************
  **/
  void readData(QList<double>,int);               /*!< slot called when there is data to process, every 200ms by default */

public:
  TeATomicModel         m_eDeviceModel = NO_ATOMIC;

  bool                  m_bPermissionIsGranted;                    /*!< state of the application-required permissions */
  quint8                m_uiPaintCount;                            /*!< used to detect the elapsing of IT */
  QPointer<LogFile>     m_oLogFile= nullptr;                       /*!< current logFile object in use */
  QDir                  m_directory;                               /*!< current used directory to store all generated files */
  TrAcousticMeasConfig  m_rAcousticMeasConfig = TrAcousticMeasConfig();                     /*!< TrAcousticMeasConfig structure instance */
  TrAcousticResBasis    m_rAcousticRes;                            /*!< TrAcousticRes structure instance */
  TrReverberationRes    m_rReverberationRes;
  AcousticMeasSo        m_acousticMeas = AcousticMeasSo(0); //PatchNewSo
  quint8                m_uiOctave=36;                              /*!< this parameter holds the spectrum configuration in use
                                                                     if set to 0, the spectrum configuration is set to none
                                                                     if set to 12, the spectrum configuration is set to Octave
                                                                     if set to 36, the spectrum configuration is set to 3rd Octave */
  AcousticRes           m_acousticSrc;                             /*!< AcousticRes instance */
  ReverberationRes      m_reverbSrc;                               /*!< ReverberationRes instance */
  ReverbAverageRes      m_averageSrc;
  RegressionLines       m_regLinesSrc;                             /*!< RegressionLines instance */
  TrListHeader          m_rListHeader; //PatchBetaDynamMetadata

  bool                  m_bThreadWorkerRunning;
  bool                  m_bLastBuffer=false;
  int                   m_iReverbCount=0;

  //Indep CH Count
  int iIndepChCount = 3; // ATomic1 : 0 ; ATomic2 : 1,2

  int m_iCounterSinceStartTR = 0;

  float *m_afFullScaledB = new float[iIndepChCount];
  float *m_afFullScaleLin = new float[iIndepChCount];
  float *m_afFftRealOverlapPercent = new float[iIndepChCount];

  //Save the last two 200ms Buffer to display the Reverberation Time Result
  QVector<QVector<double>> afLeqOctaveTChartStockage;
  QVector<QVector<double>> afLeqOctave;

  int m_iDataCounter=0;
  bool m_bSendFITBA=false;

private:
  QPointer<audioInterface>        m_oAudioInterface=nullptr;         /*!< instance of the AudioInterface class */
  TeStateSonoMaster     m_eStateSonoMaster;                          /*!< current sonometer state */
  TeStateSonoMaster     m_ePreviousState;                            /*!< previous sonometer state */
  int                   m_iBlockLostCount=0;                         /*!< block lost counter, increments everytime a block lost is detected */

  int                   m_iCodified;                                 /*!< this parameter indicates the content of the codified field in the .csv file
                                                                          if set to 0, then is deactivated
                                                                          if set to 1, then activated false, and 'false' is written in the corresponding field
                                                                          if set to 2, then activated true, and 'true" is written in the corresponding field
                                                                     */

  QPointer<WavFile>     m_oWavFile=nullptr;                          /*!< instance of the WavFile class */
  long                  m_llWavSize;

  bool                  m_bIsChronoRunning= false;                   /*!< chronometer state: running (incrementing every second) = true else false */
  QString               m_cChronoString="00:00:00";                  /*!< chronometer structure : hh:mm:ss */

  QTime                 m_StartTime;                                 /*!< when triggered increments the chronometer by 1 second */
  QTimer*               m_Timer;                                     /*!< triggers the m_StartTime every 1 second */
  QTimer*               m_disconnectTimer;                           /*!< on card disconnection, this timer is started to check periodically if the card has been connected */
  int                   m_iIntegrationTime=1000;                     /*!< holds the integration-time value IT, configured from GUI */
  QString               m_cFileName="";                              /*!< holds the file name of generated files, modified from GUI */
  bool                  m_bFirstRun = true; //Patch_betapopup

  bool                  m_bCompleteTR = true; //Realize a Complete TR
};

#endif // AUDIOGET_H
