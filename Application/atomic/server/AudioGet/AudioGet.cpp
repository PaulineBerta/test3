/**
*******************************************************************************
** @file    audioget.cpp
** @version V0.0.1
** @author  Wevioo SE Team
** @brief   Collects audio data and implements a recover process from error
**
*******************************************************************************
**/

#include "AudioGet.h"
#include "AudioConfig/GetConfiguration.h"
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QStorageInfo>

#define UNDERLOAD_CHAR  "\u25BC"             // underload character
//#define MINIMUM_BYTES_REQUIRED 64*1024       // minimum required-free-memory size
#define MINIMUM_BYTES_REQUIRED 100*1024*1024       // minimum required-free-memory size  //Patch_required_space 100MB
/**
*******************************************************************************
** @fn inline QString SetUnderload(double dArg)
** @param[in] double dArg
** @param[out] QString
** @brief this function always returns the absolute value of its input argument
**
** @return QString
**
*******************************************************************************
**/
inline QString SetUnderload(double dArg){
  return QString::number(qAbs(dArg),'f', 1);
}

/**
*******************************************************************************
** @fn inline QString setUNDERLOADCHAR(double dArg)
** @param[in] double dArg
** @param[out] QString
** @brief In case of underload (negative values) addes the underload character
**        on the right side of the value
**
** @return QString
**
*******************************************************************************
**/
inline QString setUNDERLOADCHAR(double dArg)
{
  if(dArg<=0)
    {
      return(UNDERLOAD_CHAR);
    }
  else {
      return("");
    }
}
/**
*******************************************************************************
** @fn  AudioGet::AudioGet(QObject *parent) : QObject(parent)
** @param[inout] QObject
** @brief Constructor of AudioGet class
** @return
**
*******************************************************************************
**/
AudioGet::AudioGet(QObject *parent) : atomicSimpleSource(parent),
  m_bPermissionIsGranted(true),m_uiPaintCount(0),m_eStateSonoMaster(SM_STANDBY),m_ePreviousState(SM_STANDBY),
  m_llWavSize(0) {

  setAppVersion(QCoreApplication::applicationVersion());
  qDebug()<<"                  VERSION : "<<QCoreApplication::applicationVersion();

  m_oAudioInterface= new audioInterface(this);
  m_oAudioInterface->Initialize();
  connect(m_oAudioInterface, SIGNAL(readData(QList<double>,int)), this, SLOT(readData(QList<double>,int)));
  connect(m_oAudioInterface, SIGNAL(deviceDisconnected()), this, SLOT(onDisconnected()));

  connect(this, SIGNAL(handleError()), this, SLOT(recover()));

  m_Timer = new QTimer(this);
  m_Timer->setTimerType(Qt::PreciseTimer);
  connect(m_Timer, SIGNAL(timeout()), this, SLOT(updateChronometer()));

  m_disconnectTimer=new QTimer(this);
  connect(m_disconnectTimer, SIGNAL(timeout()),this,SLOT(checkATomic()));
  m_disconnectTimer->setInterval(200);

  CreateDirectory();

  m_oWavFile= new WavFile(this);
  m_oWavFile->Initialize();

  for (int i = 0; i < iIndepChCount; i++)
  {
      m_afFullScaledB[i] = 0;
      m_afFullScaleLin[i] = 0;
      m_afFftRealOverlapPercent[i] = 0;
  }

  // Initialize acousticMeas class
  m_acousticMeas.AcousticMeasSo_Create();
  /*m_rAcousticMeasConfig.eCalculMode = TeCalculMode::MODE_SOUNDLEVELMETER;
  m_rAcousticMeasConfig.rACZFilterConfig.eTimeWeightingLp = TeTimeWeightingLp::LP_FAST;
  m_rAcousticMeasConfig.rSpectrumConfig.eSpectrum = TeSpectrum::SPECTRUM_3RD_OCTAVE;
  m_rAcousticMeasConfig.eChanTreated = TeChanTreated::CHAN_STEREO_FULL;
  m_rAcousticMeasConfig.eFastIntegrationTime = TeFastIntegrationTime::FIT_200MS;
  m_rAcousticMeasConfig.eDurationBlock = TeDurationBlock::BLOCK_200MS;*/

  //Detec ATomic Model
  getATomicInfo();

  qDebug() << "### CONFIG Model : " << m_oAudioInterface->m_eATomicModel;
  m_eDeviceModel = m_oAudioInterface->m_eATomicModel;

  /*switch (m_oAudioInterface->m_eATomicModel) {
    default:
    case ATOMIC_1:
      m_rAcousticMeasConfig.eChanTreated = TeChanTreated::CHAN_STEREO_FULL;
      break;

    case ATOMIC_2:
      m_rAcousticMeasConfig.eChanTreated = TeChanTreated::CHAN_LEFT; //Only use CH1 if AT2 Detected!!
      break;
  }*/

  m_acousticMeas.AcousticMeasSo_Configure(m_rAcousticMeasConfig, &m_afFullScaledB[0], &m_afFullScaleLin[0], &m_afFftRealOverlapPercent[0]);
  reset();

  m_oLogFile= new LogFile(this);
  m_oLogFile->OpenLog();

  start();
}


/**
*******************************************************************************
** @fn  void AudioGet::CreateDirectory()
** @param[in] void
** @param[out] void
** @brief Creates the Alliantech directory to where all generated files will
**        be stored
** @return
**
*******************************************************************************
**/
void AudioGet::CreateDirectory(){
  /** Create Directory to store files */
  QDir directory;
  QAndroidJniObject path;
  path = QAndroidJniObject::callStaticObjectMethod("android.os.Environment",
                                                   "getExternalStorageDirectory",
                                                   "()Ljava/io/File;");

  QDir().mkdir(path.toString()+QString("/AllianTech/ATomicSound/Configuration"));
  directory.mkpath(path.toString()+QString("/AllianTech/ATomicSound/Log"));

  setPath(path.toString());
}

/**
*******************************************************************************
** @fn  void AudioGet::getATomicInfo()
** @param[in] void
** @param[out] void
** @brief sets the ATomic card related informations: - serial number
**                                                   - product identification
** @return void
**
*******************************************************************************
**/
void AudioGet::getATomicInfo(){
  setSerialNumber(m_oAudioInterface->GetSerialNumber());
  setProductId(m_oAudioInterface->GetProductName());

  //Chech if AT1 or AT2
  m_oAudioInterface->checkATomicModel();
}

/**
*******************************************************************************
** @fn  AudioGet::updateChronometer()
** @param[in] void
** @param[out] void
** @brief this slot, updates the chronometer display in QML part each 1 second
** @return void
**
*******************************************************************************
**/
void AudioGet::updateChronometer(){
  if(m_bIsChronoRunning){
      m_StartTime=m_StartTime.addSecs(1);
      setChronometer(m_StartTime.toString("hh:mm:ss"));
    }
  else {
      qDebug()<<"***********Not Running***********";
      m_StartTime.start();
      m_StartTime.setHMS(0,0,0);
      m_Timer->start(1000);
      m_bIsChronoRunning=true;
      emit chronometerChanged();
    }
}

/**
*******************************************************************************
** @fn  QString AudioGet::chronometer()
** @param[in] void
** @param[out] QString
** @brief gets the current chronometer's value in the specified format
**               hh:mm:ss
** @return
**
*******************************************************************************
**/
QString AudioGet::chronometer(){
  return m_cChronoString;
}

/**
*******************************************************************************
** @fn  QString AudioGet::setChronometer()
** @param[in] QString
** @param[out] void
** @brief sets the current chronometer's value
** @return
**
*******************************************************************************
**/
void AudioGet::setChronometer(QString cTime){
  m_cChronoString=cTime;

  QString hours=cTime.section(":",0,0); // set the hours field of the chronometer
  setChronometerHourso(hours.at(1));
  setChronometerHourst(hours.at(0));

  QString minutes=cTime.section(":",1,1); // set the minutes field of the chronometer
  setChronometerMinuteso(minutes.at(1));
  setChronometerMinutest(minutes.at(0));

  QString second=cTime.section(":",-1); // set the seconds field of the chronometer
  setChronometerSecondo(second.at(1));
  setChronometerSecondt(second.at(0));

  emit chronometerChanged();
}

/**
*******************************************************************************
** @fn  void AudioGet::resetChrono()
** @param[in] void
** @param[out] void
** @brief resets the current chronometer, restart from 00:00:00
** @return
**
*******************************************************************************
**/
void AudioGet::resetChrono(){
  m_StartTime.start();
  m_StartTime.setHMS(0,0,0);
  setChronometer("00:00:00");
  m_Timer->start(1000);
  m_bIsChronoRunning=true;
}
/**
*******************************************************************************
** @fn  void AudioGet::
**        getFileError(QFileDevice::FileError fileError, TeFileType eFileType)
** @param[in] QFileDevice::FileError fileError
** @param[in] TeFileType eFileType
** @param[out] void
** @brief returns if a file error has occured and behaves accordingly: updates
**        the GUI by notifying the user.
** @return void
**
*******************************************************************************
**/
void AudioGet::getFileError(QFileDevice::FileError fileError, TeFileType eFileType){

  switch(fileError){
    case QFileDevice::NoError:          /* No error occurred.*/
      break;
    case QFileDevice::ReadError:	/* An error occurred when reading from the file.*/
      {
        if(eFileType==XML){
            m_oLogFile->WriteLogFile(ERROR_READING_XML);
            setError("Unable to");
            setAcquisition("Read XML");
            setIsDanger(true);
          }
        else return;
      }
      break;
    case QFileDevice::WriteError:	/* An error occurred when writing to the file.*/
      {
        if(eFileType == CSV){
            m_oLogFile->WriteLogFile(ERROR_WRITING_CSV);
            setError("Unable to");
            setAcquisition("Write CSV");
            setIsDanger(true);
          }
        else if (eFileType == WAV) {
            m_oLogFile->WriteLogFile(ERROR_WRITING_WAV);
            setError("Unable to");
            setAcquisition("Write WAV");
            setIsDanger(true);
          }
        else {
            m_oLogFile->WriteLogFile(ERROR_WRITING_XML);
            setError("Unable to");
            setAcquisition("Write XML");
            setIsDanger(true);
          }
      }
      break;
    case QFileDevice::ResourceError:	/* Out of resources (e.g., too many open files, out of memory, etc.)*/
      {
         m_oLogFile->WriteLogFile(ERROR_INSUFFICIENT_STORAGE_SPACE);
         setError("Lack Of");
         setAcquisition("Memory");
         setIsDanger(true);

         if(sonoState()==SM_SONOMETER_WAV_RECORDING){
           m_oWavFile->Close();
           m_oLogFile->WriteLogFile(EVENT_STOP_WAV_RECORDING);
           m_oLogFile->CloseCsv();
           m_oLogFile->WriteLogFile(EVENT_STOP_CSV_RECORDING);
         }
         if(sonoState()==SM_SONOMETER_LOGGING){
           m_oLogFile->CloseCsv();
           m_oLogFile->WriteLogFile(EVENT_STOP_CSV_RECORDING);
         }

         reset();

         /** Pop up view */
         emit insufficientSpaceWarning();
      }
      break;
    case QFileDevice::OpenError:	/* The file could not be opened.*/
      {
        CreateDirectory();
        if(eFileType == CSV){
            if(!m_oLogFile->m_CsvItFile.exists() || !m_oLogFile->m_CsvFitFile.exists()){
                m_oLogFile->WriteLogFile(ERROR_OPENING_CSV);
                setError("Unable to");
                setAcquisition("OPEN CSV");
                setIsDanger(true);
              }
          }
        else if (eFileType == WAV) {
            m_oLogFile->WriteLogFile(ERROR_OPENING_WAV);
            setError("Unable to");
            setAcquisition("OPEN WAV");
            setIsDanger(true);
          }
        else {
            m_oLogFile->WriteLogFile(ERROR_OPENING_XML);
            setError("Unable to");
            setAcquisition("OPEN XML");
            setIsDanger(true);
          }
      }
      break;
    default:
      break;
    }
  if(fileError && fileError != QFileDevice::ResourceError) {
      m_ePreviousState = m_eStateSonoMaster;
      setAudioSonoState(SM_ERROR);
    }
}

/**
*******************************************************************************
** @fn  void AudioGet::launchNewMode()
** @param[in] void
** @param[out] void
** @brief
** @return void
**
*******************************************************************************
**/
void AudioGet::launchNewMode() {
  start();
  resetChrono();
  setIsDanger(false);
}

/**
*******************************************************************************
** @fn  void AudioGet::start()
** @param[in] void
** @param[out] void
** @brief (Slot)Detects, initializes ATomic Card and outputs its caracteristics
** @return void
**
*******************************************************************************
**/
void AudioGet::start() {
  //getATomicInfo();
  if(!QDir(path()+"/AllianTech/ATomicSound/Log").exists())
    {
      QDir directory;
      directory.mkpath(path()+"/AllianTech/ATomicSound/Log");
      m_oLogFile->OpenLog();
    }
  m_oLogFile->WriteLogFile(EVENT_START_ACQUISITION);
  m_oAudioInterface->StartRecording();

  if ((modeState() == 0)) { //Init State
      setAudioSonoState(SM_SONOMETER_READY);
  } else if ((modeState() == 1)) {
      setAudioSonoState(SM_SONOMETER_READY);
  } else if (modeState() == 2) {
      setAudioSonoState(SM_REVERBERATION_TIME_READY);
  }

  resetChrono();
  setIsDanger(false);
}

/**
*******************************************************************************
** @fn  void AudioGet::startReverbMode()
** @param[in] void
** @param[out] void
** @brief (Slot)Detects, initializes ATomic Card and outputs its caracteristics
** @return void
**
*******************************************************************************
**/
void AudioGet::startReverbMode() {
  m_acousticMeas.AcousticMeasSo_StartTr(m_bCompleteTR);

  // To allow the average between RT Measures
  if(m_bCompleteTR) {
      m_bCompleteTR = false;
  }
}

/**
*******************************************************************************
** @fn  void AudioGet::stop()
** @param[in] void
** @param[out] void
** @brief stop CSV recording and closes files
** @return void
**
*******************************************************************************
**/
void AudioGet::stop() {
  qDebug()<<"stopped ...";
  if(sonoState()==SM_SONOMETER_LOGGING) {
      qDebug()<<" Log File Close CSV Sonometer !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
      m_oLogFile->CloseCsv();

      if(m_oLogFile->m_CsvItFile.isOpen() || m_oLogFile->m_CsvFitFile.isOpen()){
          m_oLogFile->WriteLogFile(ERROR_CLOSING_CSV);
          setError("Unable to");
          setAcquisition("Close CSV");
          setIsDanger(true);
          m_ePreviousState = m_eStateSonoMaster;
          setAudioSonoState(SM_ERROR);

      } else {
          m_oLogFile->WriteLogFile(EVENT_STOP_CSV_RECORDING);
          setAudioSonoState(SM_SONOMETER_READY);
          setIsDanger(false);
      }

  } else if(sonoState()==SM_REVERBERATION_TIME_RUNNING) {
      qDebug()<<" Log File Close CSV Reverberation !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
      m_oLogFile->CloseCsv();

      if(m_oLogFile->m_CsvRTFile.isOpen() || m_oLogFile->m_CsvFitRTFile.isOpen()){
          m_oLogFile->WriteLogFile(ERROR_CLOSING_CSV);
          setError("Unable to");
          setAcquisition("Close CSV");
          setIsDanger(true);
          m_ePreviousState = m_eStateSonoMaster;
          setAudioSonoState(SM_ERROR);

      } else {
          stopReverbMode();

          m_oLogFile->WriteLogFile(EVENT_STOP_CSV_RECORDING);
          setAudioSonoState(SM_REVERBERATION_TIME_READY);
          setIsDanger(false);
          qDebug()<<"Je passe dans Stop TR et sonoState : "<<sonoState();
      }
   }
}

/**
*******************************************************************************
** @fn  void AudioGet::startReverbMode()
** @param[in] void
** @param[out] void
** @brief (Slot)Detects, initializes ATomic Card and outputs its caracteristics
** @return void
**
*******************************************************************************
**/
void AudioGet::stopReverbMode() {
  m_acousticMeas.AcousticMeasSo_StopTr();

  if(reverbCounter() == 0) {
      m_bCompleteTR = true;
  }
}

/**
*******************************************************************************
** @fn  void AudioGet::stopAcquisition()
** @param[in] void
** @param[out] void
** @brief stop acquisition and closes all opened files
** @return void
**
*******************************************************************************
**/
void AudioGet::stopAcquisition() {
    stopWAVAcquisition();

    m_oAudioInterface->StopRecording();
    m_oLogFile->WriteLogFile(EVENT_STOP_ACQUISITION);
    m_oLogFile->CloseLog();
    setIsDanger(false);
}

/**
*******************************************************************************
** @fn  void AudioGet::stopWAVAcquisition()
** @param[in] void
** @param[out] void
** @brief stop acquisition and closes all opened files
** @return void
**
*******************************************************************************
**/
void AudioGet::stopWAVAcquisition() {
    if(m_eStateSonoMaster==SM_SONOMETER_WAV_RECORDING) {
        getFileError(m_oWavFile->m_file.error(),WAV);

        if(!m_oWavFile->Close()) {
            m_oLogFile->WriteLogFile(EVENT_STOP_WAV_RECORDING);
            setAudioSonoState(SM_SONOMETER_LOGGING);
            stop();

        } else {
            m_oLogFile->WriteLogFile(ERROR_CLOSING_WAV);
            m_ePreviousState = m_eStateSonoMaster;
            setAudioSonoState(SM_ERROR);
            setError("Unable to");
            setAcquisition("Close WAV");
            setIsDanger(true);
        }
    }
}

/**
*******************************************************************************
** @fn  void AudioGet::stopReverbCounter()
** @param[in] void
** @param[out] void
** @brief stop acquisition and closes all opened files
** @return void
**
*******************************************************************************
**/
void AudioGet::stopReverbCounter() {
    m_iReverbCount = 0;
    setReverbCounter(m_iReverbCount);

    reset();
    m_bCompleteTR = true;

    emit reinitPlotList();
}

/**
*******************************************************************************
** @fn  void AudioGet::resetLastTrMeasure()
** @param[in] void
** @param[out] void
** @brief stop acquisition and closes all opened files
** @return void
**
*******************************************************************************
**/
void AudioGet::resetLastTrMeasure() {
    m_iReverbCount--;
    setReverbCounter(m_iReverbCount);

    deleteCsv();

    m_acousticMeas.AcousticMeasSo_DeleteLastTr(m_rReverberationRes);
}

/**
*******************************************************************************
** @fn  void AudioGet::reinitPlot()
** @param[in] void
** @param[out] void
** @brief stop acquisition and closes all opened files
** @return void
**
*******************************************************************************
**/
void AudioGet::reinitPlot() {
    emit onReset();
}

/**
*******************************************************************************
** @fn  void AudioGet::
** @param[in]
** @param[out] void
** @brief
** @return void
**
*******************************************************************************
**/
void AudioGet::sendBufferFITBA() {
    int bufferNumber = (chartTimeResult()*5)-1;

    if(m_iDataCounter < bufferNumber) {
        m_bLastBuffer=false;
        emit passBufferFITBA(afLeqOctave, m_bLastBuffer, m_iDataCounter);
        m_iDataCounter+=1;

    } else if (m_iDataCounter == bufferNumber) {
        m_bLastBuffer=true;
        emit passBufferFITBA(afLeqOctave, m_bLastBuffer, m_iDataCounter);
        m_iDataCounter+=1;
    }
}

/**
*******************************************************************************
** @fn  void AudioGet::setAudioSonoState(quint8 uiState)
** @param[in] quint8 sets the state of Sonometer Master (SM)
** @param[out] void
** @brief sets the SM state
** @return void
**
*******************************************************************************
**/
void AudioGet::setAudioSonoState(int uiState) {
  if(m_eStateSonoMaster != uiState){
      m_eStateSonoMaster=static_cast<TeStateSonoMaster>(uiState);
      setSonoState(uiState);
      qDebug()<<"SonoState AudioGet :"<<uiState;
      emit audioSonoStateChanged();
      if(m_eStateSonoMaster==SM_ERROR)
        {
          emit handleError();
        }
    }
}

/**
*******************************************************************************
** @fn  void AudioGet::setAudioCodified(int iCodified)
** @param[in] int iCodified sets the codified parameter
** @param[out] void
** @brief updates the codified parameter: in the GUI and CSV files
** @return void
**
*******************************************************************************
**/
void AudioGet::setAudioCodified(int iCodified) {
  m_iCodified=iCodified;
  setCodified(iCodified);
  if(m_iCodified==2)
    m_oLogFile->m_cCodified="true";
  else m_oLogFile->m_cCodified="false";
}

/**
*******************************************************************************
** @fn  void
** @param[in]
** @param[out] void
** @brief
** @return void
**
*******************************************************************************
**/
void AudioGet::setModeApp(int iMode) {
    setModeState(iMode);

    stopWAVAcquisition();
    stop();
    reset();

    setAudioSonoState(SM_IDLE);
}

/**
*******************************************************************************
** @fn  void AudioGet::setAudioBlockLost(int iError)
** @param[in] int iError: if iError != 0 than a block lost has occured
**                         else reset the block lost counter
** @param[out] void
** @brief updates the block lost counter, the sonometer state and GUI.
**        if iError != 0 than a block lost has occured
**        else no block lost detected
** @return void
**
*******************************************************************************
**/
void AudioGet::setAudioBlockLost(int iError) {
  if(m_iBlockLostCount != iError) {
      // a block lost has been dettected
      m_iBlockLostCount=iError;
      setBlockLost(iError);

      if(iError) {
          m_oLogFile->WriteLogFile(ERROR_BLOCK_LOST);
          m_ePreviousState = m_eStateSonoMaster;
          setAudioSonoState(SM_ERROR);
          setError("Block");
          setAcquisition("      Lost");
          setIsDanger(true);

      } else {
          // No block lost
          setIsDanger(false);
      }

      emit audioBlockLostChanged();
    }
}

/**
*******************************************************************************
** @fn  void AudioGet::readData(QList<double> data, int error)
** @param[in] QList<double> data
** @param[in] int error
** @param[out] void
** @brief Reads data every period (200ms) and the result error
** @return void
**
*******************************************************************************
**/
void AudioGet::readData(QList<double> data, int iError) {
  if(m_bFirstRun){
     emit showBetaWarning(); //Patch_betapopup
     m_bFirstRun = false;
  }

  m_uiPaintCount++;

  /** Update the block Lost count **/
  setAudioBlockLost(iError);
  setIsDanger(false);

  /** Calculate **/
  float ch1[SIZE_BLOCK];
  float ch2[SIZE_BLOCK];
  float combined[SIZE_BLOCK];
  for (int i = 0; i < SIZE_BLOCK; i++) {
      ch1[i] = static_cast<float>(data.at(2*i));
      ch2[i] = static_cast<float>(data.at(2*i + 1));
  }


  m_acousticMeas.AcousticMeasSo_NewBlock(ch1, ch2, &m_rAcousticRes, combined);

  QVector<double> adDataTChart;
  int iIndexFit=0;

  QVector<double> adReverbTChart;
  QVector<QVector<double>> afLeqOctaveTChart;
  int iIndexFitReverb=0;

  QVector<double> adReverbNoise;
  QVector<double> adReverbSource;


  if(sonoState() == SM_SONOMETER_READY || sonoState() == SM_SONOMETER_LOGGING || sonoState() == SM_SONOMETER_WAV_RECORDING) {
      adDataTChart.insert(0,qAbs(static_cast<double>(m_rAcousticRes.rFit[iIndexFit].rACZRes.afLeq[0])));
      adDataTChart.insert(1,qAbs(static_cast<double>(m_rAcousticRes.rFit[iIndexFit].rACZRes.afLeq[1])));
      adDataTChart.insert(2,qAbs(static_cast<double>(m_rAcousticRes.rFit[iIndexFit].rACZRes.afPeakCZ[0])));

      emit passDataFIT(adDataTChart);

  } else if(sonoState() == SM_REVERBERATION_TIME_READY || sonoState() == SM_REVERBERATION_TIME_RUNNING) {
      if(m_rAcousticRes.rStatusTrRes.eStatusOfReverberation == STOP_TR) {
          setReverbStatus("Stop TR");
          qDebug()<<"Stop TR";

      } else if(m_rAcousticRes.rStatusTrRes.eStatusOfReverberation == READY) {
          setReverbStatus("Ready");
          //qDebug()<<"Ready";
          m_iCounterSinceStartTR = 0;

      } else if(m_rAcousticRes.rStatusTrRes.eStatusOfReverberation == MEASURING_NOISE_LEVEL) {
          setReverbStatus("Measuring Noise Level");
          //qDebug()<<"Measuring Noise Level";

      } else if(m_rAcousticRes.rStatusTrRes.eStatusOfReverberation == WAITING_SOURCE) {
          setReverbStatus("Waiting Source");
          //qDebug()<<"Waiting Source";

      } else if(m_rAcousticRes.rStatusTrRes.eStatusOfReverberation == WAITING_SOURCE_STABILIZED) {
          setReverbStatus("Waiting Source Stabilized");
          //qDebug()<<"Waiting Source Stabilized";

      } else if(m_rAcousticRes.rStatusTrRes.eStatusOfReverberation == MEASURING_SOURCE_LEVEL) {
          setReverbStatus("Measuring Source Level");
          //qDebug()<<"Measuring Source Level";

      } else if(m_rAcousticRes.rStatusTrRes.eStatusOfReverberation == WAITING_STOP_SOURCE) {
          setReverbStatus("Waiting Stop Source");
          //qDebug()<<"Waiting Stop Source";

      } else if(m_rAcousticRes.rStatusTrRes.eStatusOfReverberation == SOURCE_DECREASE_RECORDING) {
          setReverbStatus("Source Decrease Recording");
          //qDebug()<<"Source Decrease Recording";

            /** Update AudioGet counter to display two data seconds (Decrease Mode = Normal) **/
          if(m_iDataCounter == 0) {
              emit passCounterSinceStartTR(m_iCounterSinceStartTR);
              m_bSendFITBA = true;
          }

            /** Send 200ms FIT Buffer, MODE BA **/
          sendBufferFITBA();

      } else if(m_rAcousticRes.rStatusTrRes.eStatusOfReverberation == TR_MEASURE) {
          setReverbStatus("TR Measure");
          qDebug()<<"TR Measure";

            /** Send the last 200ms FIT Buffer, MODE BA **/
          sendBufferFITBA();

            /** Get TR Result and Update Reverb Counter **/
          m_acousticMeas.AcousticMeasSo_GetTrResult(m_rReverberationRes);
          m_iReverbCount+=1;
          setReverbCounter(m_iReverbCount);

            /** Write CSV RT File **/
          m_oLogFile->WriteRTCsv(m_rAcousticMeasConfig, m_rReverberationRes, m_iReverbCount);
          getFileError(m_oLogFile->m_CsvItFile.error(), CSV);

            /** Stop Signal Acquisition, Close CSV and set SonoState to SM_REVERBERATION_READY **/
          stop();

            /** Pass TR Noise Level to Chart **/
          for (int i=0; i<m_uiOctave+1; i++) { //m_uiOctave+1
              adReverbNoise.insert(i,qAbs(static_cast<double>(m_rReverberationRes.afLeqNoisedB[i])));
          }
          emit passReverbNoise(adReverbNoise);

            /** Pass TR Source Level to Chart **/
          for (int i=0; i<m_uiOctave+1; i++) { //37
              adReverbSource.insert(i,qAbs(static_cast<double>(m_rReverberationRes.afLeqSourcedB[i])));
          }
          emit passReverbSource(adReverbSource);

            /** Pass TR RegressionLines to Chart **/
          QList<int> aiStartTR, aiEndT20, aiEndT30, aiEndT60;
          QList<double> afStartT20, afStartT30, afStartT60, afSlopeT20, afSlopeT30, afSlopeT60;

          int indexFirstDecrease;
          indexFirstDecrease = m_rReverberationRes.iIndexFirstFitRecordDecrease;
          m_regLinesSrc.setIndexFirstFitRecordDecrease(indexFirstDecrease);

          for (int i=0; i<m_uiOctave+1; i++) { //37
              aiStartTR.append(qAbs(static_cast<int>(m_rReverberationRes.aiStartFitTr[i])));
              aiEndT20.append(qAbs(static_cast<int>(m_rReverberationRes.aiEndFitTr20[i])));
              aiEndT30.append(qAbs(static_cast<int>(m_rReverberationRes.aiEndFitTr30[i])));
              aiEndT60.append(qAbs(static_cast<int>(m_rReverberationRes.aiEndFitTr60[i])));

              afStartT20.append(qAbs(static_cast<double>(m_rReverberationRes.afLevelStartFitTr20[i])));
              afStartT30.append(qAbs(static_cast<double>(m_rReverberationRes.afLevelStartFitTr30[i])));
              afStartT60.append(qAbs(static_cast<double>(m_rReverberationRes.afLevelStartFitTr60[i])));
              afSlopeT20.append(qAbs(static_cast<double>(m_rReverberationRes.afSlopeTr20[i])));
              afSlopeT30.append(qAbs(static_cast<double>(m_rReverberationRes.afSlopeTr30[i])));
              afSlopeT60.append(qAbs(static_cast<double>(m_rReverberationRes.afSlopeTr60[i])));
          }

          m_regLinesSrc.setAiStartFitTr(aiStartTR);
          m_regLinesSrc.setAiEndFitTr20(aiEndT20);
          m_regLinesSrc.setAiEndFitTr30(aiEndT30);
          m_regLinesSrc.setAiEndFitTr60(aiEndT60);

          m_regLinesSrc.setAfLevelStartFitTr20(afStartT20);
          m_regLinesSrc.setAfLevelStartFitTr30(afStartT30);
          m_regLinesSrc.setAfLevelStartFitTr60(afStartT60);
          m_regLinesSrc.setAfSlopeTr20(afSlopeT20);
          m_regLinesSrc.setAfSlopeTr30(afSlopeT30);
          m_regLinesSrc.setAfSlopeTr60(afSlopeT60);

          emit passRegressionLines(m_regLinesSrc);

          /** Third Octave Mode Reverb to Table **/
          QList<double> afT20, afT30, afT60;
          QList<bool> afFlagT20, afFlagT30, afFlagT60;
          QList<double> afAvgT20, afAvgT30, afAvgT60;
          for(int i=0; i<m_uiOctave+1; i++) { //37
              afT20.append(static_cast<double>(m_rReverberationRes.afTr20Sec[i]));
              if(((((int)m_rReverberationRes.auiFlagTr[i]) & D_T20_INSUFFICIENT_DYNAMIC) > 0) ||
                 ((((int)m_rReverberationRes.auiFlagTr[i]) & I_T20_TOO_LOW) > 0)) {
                  afFlagT20.append(true);
              } else {
                  afFlagT20.append(false);
              }

              afT30.append(static_cast<double>(m_rReverberationRes.afTr30Sec[i]));
              if(((((int)m_rReverberationRes.auiFlagTr[i]) & D_T30_INSUFFICIENT_DYNAMIC) > 0) ||
                 ((((int)m_rReverberationRes.auiFlagTr[i]) & I_T30_TOO_LOW) > 0) ||
                 ((((int)m_rReverberationRes.auiFlagTr[i]) & CURVATURE_TOO_LOW) > 0) ||
                 ((((int)m_rReverberationRes.auiFlagTr[i]) & CURVATURE_TOO_HIGH) > 0)) {
                  afFlagT30.append(true);
              } else {
                  afFlagT30.append(false);
              }

              afT60.append(static_cast<double>(m_rReverberationRes.afTr60Sec[i]));
              if(((((int)m_rReverberationRes.auiFlagTr[i]) & D_T60_INSUFFICIENT_DYNAMIC) > 0) ||
                 ((((int)m_rReverberationRes.auiFlagTr[i]) & I_T60_TOO_LOW) > 0)) {
                  afFlagT60.append(true);
              } else {
                  afFlagT60.append(false);
              }

              afAvgT20.append(static_cast<double>(m_rReverberationRes.afAvgTr20Sec[i]));
              afAvgT30.append(static_cast<double>(m_rReverberationRes.afAvgTr30Sec[i]));
              afAvgT60.append(static_cast<double>(m_rReverberationRes.afAvgTr60Sec[i]));
          }

          m_reverbSrc.setAfT20(afT20);
          m_reverbSrc.setAfT30(afT30);
          m_reverbSrc.setAfT60(afT60);
          m_reverbSrc.setAfFlagT20(afFlagT20);
          m_reverbSrc.setAfFlagT30(afFlagT30);
          m_reverbSrc.setAfFlagT60(afFlagT60);

          m_averageSrc.setAfAvgT20(afAvgT20);
          m_averageSrc.setAfAvgT30(afAvgT30);
          m_averageSrc.setAfAvgT60(afAvgT60);

          emit passReverberationRes(m_reverbSrc, m_averageSrc);

          /** Reinitialization DataCounter for the next RT Measure **/
          m_iDataCounter=0;

          qDebug()<<"                Passage 00";

          emit saveReverbActiveMeasure();

          qDebug()<<"                Passage 01";

      } else if(m_rAcousticRes.rStatusTrRes.eStatusOfReverberation == NOISE_LEVEL_TOO_HIGH) {
          setReverbStatus("Noise Level Too High");
          stop();
          deleteCsv();

          qDebug()<<"Noise Level Too High";

      } else if(m_rAcousticRes.rStatusTrRes.eStatusOfReverberation == SOURCE_NOT_DETECTED) {
          setReverbStatus("Source Not Detected");
          stop();
          deleteCsv();

          qDebug()<<"Source Not Detected";

      } else if(m_rAcousticRes.rStatusTrRes.eStatusOfReverberation == SOURCE_NEVER_STOPPED) {
          setReverbStatus("Source Never Stopped");
          stop();
          deleteCsv();

          qDebug()<<"Source Never Stopped";
      }

      for (int i=0; i<25; i++) { // 25 x Block 8ms = Block 200ms
          adDataTChart.insert(i,qAbs(static_cast<double>(m_rAcousticRes.rFit[iIndexFit+i].rACZRes.afLeq[0])));
          m_iCounterSinceStartTR++;
      }
      emit passDataFITBA(adDataTChart);

      for (int i=0; i<m_uiOctave+1;i++) {  // 36 Frequency (3rd Octave) //37
          for (int j=0; j<25; j++) { // 25 x Block 8ms = Block 200ms
              adReverbTChart.insert(j,qAbs(static_cast<double>(m_rAcousticRes.rFit[iIndexFitReverb+j].rOctaveRes.afLeq[i])));
          }
          afLeqOctaveTChart.insert(i,adReverbTChart);
      }

      afLeqOctave=afLeqOctaveTChartStockage;
      afLeqOctaveTChartStockage=afLeqOctaveTChart;
  }


  if ((m_eStateSonoMaster == SM_SONOMETER_WAV_RECORDING) || (m_eStateSonoMaster == SM_SONOMETER_LOGGING) || (m_eStateSonoMaster == SM_REVERBERATION_TIME_RUNNING)) {
      m_oLogFile->WriteFitCsv(m_rAcousticRes, sonoState()); // update _FIT.csv file
      getFileError(m_oLogFile->m_CsvFitFile.error(), CSV);
  }


  /*********** Save to WAV ************/
  if (m_eStateSonoMaster==SM_SONOMETER_WAV_RECORDING) {
      QStorageInfo m_StorageInfo(path());
      m_StorageInfo.refresh();

      if(m_StorageInfo.isValid() && m_StorageInfo.bytesAvailable()>= MINIMUM_BYTES_REQUIRED + SIZE_BLOCK) {
        WriteWav(combined);                                //updates wav file

      } else {
        getFileError(QFileDevice::ResourceError,WAV);
      }
  }


  if(m_uiPaintCount==m_iIntegrationTime/200) {

      if(m_eStateSonoMaster==SM_SONOMETER_READY || m_eStateSonoMaster==SM_SONOMETER_LOGGING || m_eStateSonoMaster==SM_SONOMETER_WAV_RECORDING) {
          int iIndexLastGLobal= m_rAcousticRes.iCountGlobal-1;
          QList<QVector<double>> DataToPaint;

          DataToPaint.append({static_cast<double>(m_rAcousticRes.rIt.rACZRes.afLeq[0]),
                              static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rACZRes.afLeq[0])});
          DataToPaint.append({static_cast<double>(m_rAcousticRes.rIt.rACZRes.afLeq[1]),
                              static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rACZRes.afLeq[1])});
          DataToPaint.append({static_cast<double>(m_rAcousticRes.rIt.rACZRes.afLeq[2]),
                              static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rACZRes.afLeq[2])});
          DataToPaint.append({static_cast<double>(m_rAcousticRes.rIt.rACZRes.afPeakCZ[0]),
                              static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rACZRes.afPeakCZ[0])});
          DataToPaint.append({static_cast<double>(m_rAcousticRes.rIt.rACZRes.afPeakCZ[1]),
                              static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rACZRes.afPeakCZ[1])});
          DataToPaint.append({static_cast<double>(m_rAcousticRes.rFit[0].rACZRes.afLpACZ[0]),
                              static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rACZRes.afLpACZ[0])});
          DataToPaint.append({static_cast<double>(m_rAcousticRes.rFit[0].rACZRes.afLpACZ[1]),
                              static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rACZRes.afLpACZ[1])});
          DataToPaint.append({static_cast<double>(m_rAcousticRes.rFit[0].rACZRes.afLpACZ[2]),
                              static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rACZRes.afLpACZ[2])});
          DataToPaint.append({static_cast<double>(m_rAcousticRes.rIt.rACZRes.afLpMaxACZ[0]),
                              static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rACZRes.afLpMaxACZ[0])});
          DataToPaint.append({static_cast<double>(m_rAcousticRes.rIt.rACZRes.afLpMaxACZ[1]),
                              static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rACZRes.afLpMaxACZ[1])});
          DataToPaint.append({static_cast<double>(m_rAcousticRes.rIt.rACZRes.afLpMaxACZ[2]),
                              static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rACZRes.afLpMaxACZ[2])});
          m_acousticSrc.setAdLeq2(DataToPaint);

          int iiOverload=0;
          //patch_overload
          if((m_rAcousticRes.rFit[0].bOverload == true) && (m_rAcousticRes.rGlobal[iIndexLastGLobal].bOverload == false)) {
              iiOverload = 1; // the first column, only, is overloaded

          } else if((m_rAcousticRes.rFit[0].bOverload == false) && (m_rAcousticRes.rGlobal[iIndexLastGLobal].bOverload == true)) {
              iiOverload = 2; // the second column, only, is overloaded

          } else if((m_rAcousticRes.rFit[0].bOverload == true) && (m_rAcousticRes.rGlobal[iIndexLastGLobal].bOverload == true)) {
              iiOverload = 3; // both columns are overloaded

          } else {
               iiOverload = 0; // none of the table' columns are overloaded
          }
          m_acousticSrc.setIOverload(iiOverload);

          QList<double> adLeq, adLeqG;
          if(m_uiOctave>0) {
              for(int i=0; i<m_uiOctave+1; i++) {
                  adLeq.append(static_cast<double>(m_rAcousticRes.rIt.rOctaveRes.afLeq[i]));
                  adLeqG.append(static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rOctaveRes.afLeq[i]));
              }

          } else {
              for(int i=0; i<m_uiOctave+1; i++) { //37
                  adLeq.append(static_cast<double>(m_rAcousticRes.rIt.rOctaveRes.afLeq[i]));
                  adLeqG.append(static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGLobal].rOctaveRes.afLeq[i]));
              }
          }

          m_acousticSrc.setAdLeq(adLeq);
          m_acousticSrc.setAdLeqG(adLeqG);

          emit passData(m_acousticSrc);

          /*!< updates the overload status */
          setOverload(iiOverload);
      }


      if(m_eStateSonoMaster==SM_SONOMETER_WAV_RECORDING || m_eStateSonoMaster==SM_SONOMETER_LOGGING) {
          m_oLogFile->WriteItCsv(m_rAcousticRes);   // update _IT.csv file
          getFileError(m_oLogFile->m_CsvItFile.error(), CSV);
      }


      if(m_eStateSonoMaster!=SM_REVERBERATION_TIME_READY && m_eStateSonoMaster!=SM_REVERBERATION_TIME_RUNNING) {
          /*!< Update the superior panel */
          if(qAbs(static_cast<double>(m_rAcousticRes.rIt.rACZRes.afLeq[0])) < 140) {
              setLAeqT(SetUnderload(static_cast<double>(m_rAcousticRes.rIt.rACZRes.afLeq[0])));
              setLAeqTUNDERLOADCHAR(setUNDERLOADCHAR(static_cast<double>(m_rAcousticRes.rIt.rACZRes.afLeq[0])));
          }

          if(qAbs(static_cast<double>(m_rAcousticRes.rIt.rACZRes.afLeq[1])) < 140) {
              setLCeqT(SetUnderload(static_cast<double>(m_rAcousticRes.rIt.rACZRes.afLeq[1])));
              setLCeqTUNDERLOADCHAR(setUNDERLOADCHAR(static_cast<double>(m_rAcousticRes.rIt.rACZRes.afLeq[1])));
          }

          if(qAbs(static_cast<double>(m_rAcousticRes.rIt.rACZRes.afPeakCZ[0])) < 140) {
              setLCpeakT(SetUnderload(static_cast<double>(m_rAcousticRes.rIt.rACZRes.afPeakCZ[0])));
              setLCpeakTUNDERLOADCHAR(setUNDERLOADCHAR(static_cast<double>(m_rAcousticRes.rIt.rACZRes.afPeakCZ[0])));
          }

          int iIndexLastGlobal = m_rAcousticRes.iCountGlobal - 1;
          if(qAbs(static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGlobal].rACZRes.afLeq[0])) < 140) {
              setLAeqG(SetUnderload(static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGlobal].rACZRes.afLeq[0])));
              setLAeqGUNDERLOADCHAR(setUNDERLOADCHAR(static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGlobal].rACZRes.afLeq[0])));
          }

          if(qAbs(static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGlobal].rACZRes.afLeq[1])) < 140) {
              setLCeqG(SetUnderload(static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGlobal].rACZRes.afLeq[1])));
              setLCeqGUNDERLOADCHAR(setUNDERLOADCHAR(static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGlobal].rACZRes.afLeq[1])));
          }

          if(qAbs(static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGlobal].rACZRes.afPeakCZ[0])) < 140) {
              setLCpeakG(SetUnderload(static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGlobal].rACZRes.afPeakCZ[0])));
              setLCpeakGUNDERLOADCHAR(setUNDERLOADCHAR(static_cast<double>(m_rAcousticRes.rGlobal[iIndexLastGlobal].rACZRes.afPeakCZ[0])));
          }
      }

      m_uiPaintCount=0;
    }
}

/**
*******************************************************************************
** @fn  AudioGet::~AudioGet()
** @param[in]
** @param[out]
** @brief AudioGet's class Destructor
** @return deletes measurements
**
*******************************************************************************
**/
AudioGet::~AudioGet() {
  stopAcquisition();
}

/**
*******************************************************************************
** @fn  void AudioGet::reset()
** @param[in] void
** @param[out] void
** @brief resets the AcousticMeas, the sonometer state and GUI
** @return void
**
*******************************************************************************
**/
void AudioGet::reset() {
  m_acousticMeas.AcousticMeasSo_ResetCalcul();
  qDebug()<<"               RESET CALCUL RESET                 ";
  setAudioBlockLost(0);                         // reset block lost counter
  setIsDanger(false);
  resetChrono();
  if ((modeState() == 1)) {
      setAudioSonoState(SM_SONOMETER_READY);       // reset the sonometer state
  } else if (modeState() == 2) {
      setAudioSonoState(SM_REVERBERATION_TIME_READY);
  }

  reinitPlot();
}

/**
*******************************************************************************
** @fn  void AudioGet::openCsv()
** @param[in] void
** @param[out] void
** @brief creates, opens CSV files (IT and FIT) and updates the state machine
** @return void
**
*******************************************************************************
**/
void AudioGet::openCsv() {
  //emit showBetaWarning(); //Patch_betapopup

  qDebug("********* Open CSV ***********");
  if(sonoState() == SM_REVERBERATION_TIME_READY || sonoState() == SM_SONOMETER_LOGGING || sonoState() == SM_SONOMETER_WAV_RECORDING) {
      m_acousticMeas.AcousticMeasSo_ResetCalcul();
      qDebug()<<"               RESET CALCUL OPEN CSV                 ";
  }

  reinitPlot();

  if(!QDir(path()+"/AllianTech/ATomicSound/Projects").exists()) {
      QDir directory;
      directory.mkpath(path()+"/AllianTech/ATomicSound/Projects");
  }

  if(sonoState() == SM_SONOMETER_READY || sonoState() == SM_SONOMETER_LOGGING || sonoState() == SM_SONOMETER_WAV_RECORDING) {
      if(!QDir(path()+"/AllianTech/ATomicSound/Projects/ENV").exists()) {
          QDir directory;
          directory.mkpath(path()+"/AllianTech/ATomicSound/Projects/ENV");
      }

      if(!QDir(path()+"/AllianTech/ATomicSound/Projects/ENV/CSV").exists()) {
          QDir directory;
          directory.mkpath(path()+"/AllianTech/ATomicSound/Projects/ENV/CSV");
      }

  } else if(sonoState() == SM_REVERBERATION_TIME_READY || sonoState() == SM_REVERBERATION_TIME_RUNNING) {
      if(!QDir(path()+"/AllianTech/ATomicSound/Projects/"+projectName()).exists()) {
          QDir directory;
          directory.mkpath(path()+"/AllianTech/ATomicSound/Projects/"+projectName());
      }

      if(!QDir(path()+"/AllianTech/ATomicSound/Projects/"+projectName()+"/"+roomType()+"_"+roomNumber()).exists()) {
          QDir directory;
          directory.mkpath(path()+"/AllianTech/ATomicSound/Projects/"+projectName()+"/"+roomType()+"_"+roomNumber());
      }

      if(!QDir(path()+"/AllianTech/ATomicSound/Projects/"+projectName()+"/"+roomType()+"_"+roomNumber()+"/BA").exists()) {
          QDir directory;
          directory.mkpath(path()+"/AllianTech/ATomicSound/Projects/"+projectName()+"/"+roomType()+"_"+roomNumber()+"/BA");
      }

      if(!QDir(path()+"/AllianTech/ATomicSound/Projects/"+projectName()+"/"+roomType()+"_"+roomNumber()+"/BA/CSV").exists()) {
          QDir directory;
          directory.mkpath(path()+"/AllianTech/ATomicSound/Projects/"+projectName()+"/"+roomType()+"_"+roomNumber()+"/BA/CSV");
      }

      /*if(!QDir(path()+"/AllianTech/ATomicSound/Projects/"+projectName()+"/"+roomType()+"_"+roomNumber()+"/ENV").exists()) {
          QDir directory;
          directory.mkpath(path()+"/AllianTech/ATomicSound/Projects/"+projectName()+"/"+roomType()+"_"+roomNumber()+"/ENV");
      }*/
  }

  QStorageInfo m_StorageInfo(path());
  m_StorageInfo.refresh();

  if(m_StorageInfo.isValid() && m_StorageInfo.bytesAvailable() >= MINIMUM_BYTES_REQUIRED) {
    m_oLogFile->OpenCsv(m_rAcousticMeasConfig, m_cFileName, reverbCounter(), projectName(), roomType(), roomNumber());

    /** SONOMETER MODE **/
    if(m_oLogFile->m_CsvItFile.isOpen() & m_oLogFile->m_CsvFitFile.isOpen()) {
        // Update the state machine
        if (sonoState() == SM_SONOMETER_READY) {
            setAudioSonoState(SM_SONOMETER_LOGGING);
        }

        setIsDanger(false);

    } else {
        getFileError(m_oLogFile->m_CsvItFile.error(), CSV);
    }

    /** REVERBERATION MODE **/
    if(m_oLogFile->m_CsvRTFile.isOpen() & m_oLogFile->m_CsvFitRTFile.isOpen()) {
        // Update the state machine
        if (sonoState() == SM_REVERBERATION_TIME_READY) {
            setAudioSonoState(SM_REVERBERATION_TIME_RUNNING);
        }

        setIsDanger(false);

    } else {
        getFileError(m_oLogFile->m_CsvItFile.error(), CSV);
    }

  } else {
      getFileError(QFileDevice::ResourceError,CSV);
  }
}

/**
*******************************************************************************
** @fn  void AudioGet::deleteCsv()
** @param[in] void
** @param[out] void
** @brief
** @return void
**
*******************************************************************************
**/
void AudioGet::deleteCsv() {
  qDebug("********* Delete CSV ***********");
  m_oLogFile->DeleteRTCsv();

    /** Reinitialization DataCounter for the next RT Measure **/
  m_iDataCounter=0;
}

/**
*******************************************************************************
** @fn  void AudioGet::recover()
** @param[in] void
** @param[out] void
** @brief this slot is called whenever an error has been acquired
**           enables the recovering of the state before the error has been
**           acquired
** @return void
**
*******************************************************************************
**/
void AudioGet::recover() {
  //on Block Lost detected && Error => recover previous  state
  if(m_eStateSonoMaster==SM_ERROR) {
      switch(m_ePreviousState) {
      case SM_STANDBY: {
            reset();
            start();
      }
          break;

      case SM_SONOMETER_READY: {
            m_oAudioInterface->StopRecording();
            m_oLogFile->WriteLogFile(EVENT_STOP_ACQUISITION);
            m_oAudioInterface->StartRecording();
            m_oLogFile->WriteLogFile(EVENT_START_ACQUISITION);
      }
          break;

      case SM_SONOMETER_LOGGING: {
            m_oLogFile->CloseCsv();
            if(m_oLogFile->m_CsvItFile.isOpen() || m_oLogFile->m_CsvFitFile.isOpen()) {
                m_oLogFile->WriteLogFile(ERROR_CLOSING_CSV);
                setError("Unable to");
                setAcquisition("Close CSV");
                setIsDanger(true);
                m_ePreviousState = m_eStateSonoMaster;
                setAudioSonoState(SM_ERROR);

            } else {
                m_oLogFile->WriteLogFile(EVENT_STOP_CSV_RECORDING);
                m_oAudioInterface->StopRecording();
                m_oLogFile->WriteLogFile(EVENT_STOP_ACQUISITION);

                m_oAudioInterface->StartRecording();
                m_oLogFile->WriteLogFile(EVENT_START_ACQUISITION);
                m_oLogFile->OpenCsv(m_rAcousticMeasConfig, m_cFileName, reverbCounter(), projectName(), roomType(), roomNumber());
            }
      }
          break;

      case SM_SONOMETER_WAV_RECORDING: {
            m_oWavFile->Close();
            m_oLogFile->WriteLogFile(EVENT_STOP_WAV_RECORDING);
            m_oLogFile->CloseCsv();

            if(m_oLogFile->m_CsvItFile.isOpen() || m_oLogFile->m_CsvFitFile.isOpen()) {
                m_oLogFile->WriteLogFile(ERROR_CLOSING_CSV);
                setError("Unable to");
                setAcquisition("Close CSV");
                setIsDanger(true);
                m_ePreviousState = m_eStateSonoMaster;
                setAudioSonoState(SM_ERROR);

            } else {
                m_oLogFile->WriteLogFile(EVENT_STOP_CSV_RECORDING);
                if(!m_oWavFile->Close()) {
                    m_oAudioInterface->StopRecording();
                    m_oLogFile->WriteLogFile(EVENT_STOP_ACQUISITION);

                    m_oAudioInterface->StartRecording();
                    m_oLogFile->WriteLogFile(EVENT_START_ACQUISITION);
                    m_oLogFile->OpenCsv(m_rAcousticMeasConfig, m_cFileName, reverbCounter(), projectName(), roomType(), roomNumber());
                    m_oWavFile->Open(serialNumber(), m_cFileName, m_rListHeader);//PatchBetaDynamMetadata
                    m_oLogFile->WriteLogFile(EVENT_START_WAV_RECORDING);

                } else {
                    m_oLogFile->WriteLogFile(ERROR_CLOSING_WAV);
                    setError("Unable to");
                    setAcquisition("Close WAV");
                    setIsDanger(true);
                    m_ePreviousState = m_eStateSonoMaster;
                    setAudioSonoState(SM_ERROR);
                }
            }
      }
          break;

      case SM_CALIBRATION: {
      }
          break;

      case SM_REVERBERATION_TIME_READY: break; //A compléter
      case SM_REVERBERATION_TIME_RUNNING: break; //A compléter
      case SM_FACTORY_SETTINGS: break; //Not Implemented
      case SM_ERROR: emit handleError();
      case SM_IDLE: break;
      }
  }

  setAudioSonoState(m_ePreviousState);
}

/**
*******************************************************************************
** @fn  void AudioGet::record()
** @param[in] void
** @param[out] void
** @brief this slot is called to start recording of WAV files
**        and updates the sonometer state to SONOMETER_RECORDING state
** @return void
**
*******************************************************************************
**/
void AudioGet::record() {
    if(!QDir(path()+"/AllianTech/ATomicSound/Projects/ENV").exists()) {
        QDir directory;
        directory.mkpath(path()+"/AllianTech/ATomicSound/Projects/ENV");
    }

    if(!QDir(path()+"/AllianTech/ATomicSound/Projects/ENV/WAV").exists()) {
        QDir directory;
        directory.mkpath(path()+"/AllianTech/ATomicSound/Projects/ENV/WAV");
    }

    qDebug("Start Recording in Wav...");
    QStorageInfo m_StorageInfo(path());
  m_StorageInfo.refresh();
  if(m_StorageInfo.isValid() && m_StorageInfo.bytesAvailable() >= MINIMUM_BYTES_REQUIRED) {

    if(!m_oWavFile->Open(serialNumber(), m_cFileName, m_rListHeader)) { //PatchBetaDynamMetadata
        m_oLogFile->WriteLogFile(EVENT_START_WAV_RECORDING);
        setAudioSonoState(SM_SONOMETER_WAV_RECORDING);
        setIsDanger(false);

    } else {
        getFileError(m_oWavFile->m_file.error(),WAV);
    }

  } else {
      getFileError(QFileDevice::ResourceError,WAV);
  }
}

/**
*******************************************************************************
** @fn  void AudioGet::WriteWav(float*)
** @param[in] float* : combined data issued from AcousticMeasSo_NewBlock
** @param[out] void
** @brief appends new data into a wave file (*.wav) currently in use
**        this function is accessed only when the sonometer is at
**        SONOMETER_RECORDING state
** @return
**
*******************************************************************************
**/
void AudioGet::WriteWav(float* combined) {
  if(m_llWavSize<MAX_WAVE_SIZE) {
      QList<double> dCombined;

      for(int iIndex=0;iIndex<SIZE_BLOCK; iIndex++) {
          dCombined.append(static_cast<double>(combined[iIndex]));
      }

      m_llWavSize=m_oWavFile->WriteFloat(dCombined);
      qDebug()<<"wave file size = "<< m_llWavSize;
      getFileError(m_oWavFile->m_file.error(),WAV);

  } else {
      qDebug()<<" (!) MAX Size reached = "<< m_llWavSize;
      if(!m_oWavFile->Close()) {
          m_oLogFile->WriteLogFile(EVENT_STOP_WAV_RECORDING);
          setIsDanger(false);
          m_llWavSize = 0;
          record();
          getFileError(m_oWavFile->m_file.error(),WAV);
      }
   }
}

/**
*******************************************************************************
** @fn  void AudioGet::onSusspended()
** @param[in] void
** @param[out] void
** @brief this slot depends on the current state of the sonometer
**        if the sonometer is at the SONOMETER_WAV_RECORDING state, then finishes
**        all recording and closes opened wav files than transits
**        to SONOMETER_LOGGING state
** @return void
**
*******************************************************************************
**/
void AudioGet::onSusspended() {
  qDebug()<<"Suspended ...";
  if(m_eStateSonoMaster==SM_SONOMETER_WAV_RECORDING) {
      if(!m_oWavFile->Close()) {
          m_oLogFile->WriteLogFile(EVENT_STOP_WAV_RECORDING);
          setAudioSonoState(SM_SONOMETER_LOGGING);
          setIsDanger(false);

      } else {
          m_oLogFile->WriteLogFile(ERROR_CLOSING_WAV);
          m_ePreviousState = m_eStateSonoMaster;
          setAudioSonoState(SM_ERROR);
          setError("Unable to");
          setAcquisition(" Close WAV");
          setIsDanger(true);
      }
   }
}

/**
*******************************************************************************
** @fn  void AudioGet::InitConfigurationFiles
**                                    (TrAcousticMeasConfig rAcousticMeasCfg)
** @param[in] TrAcousticMeasConfig rAcousticMeasCfg
** @param[out] void
** @brief (Slot) called everytime the configuration has been changed:
**           upon reset of the configuration or upon application of
**           new configuration
** @return void
**
*******************************************************************************
**/
void AudioGet::InitConfigurationFiles(TrAcousticMeasConfig rAcousticMeasCfg) {
  m_rAcousticMeasConfig = rAcousticMeasCfg;
  m_iIntegrationTime = m_rAcousticMeasConfig.iIntegrationTimeSec*1000;

  if(m_rAcousticMeasConfig.rSpectrumConfig.eSpectrum==SPECTRUM_OCTAVE) {
      m_uiOctave = 12;
      emit octaveChanged(12);

  } else if(m_rAcousticMeasConfig.rSpectrumConfig.eSpectrum==SPECTRUM_3RD_OCTAVE) {
      m_uiOctave = 36;
      octaveChanged(36);

  } else if(m_rAcousticMeasConfig.rSpectrumConfig.eSpectrum==SPECTRUM_NO) {
      m_uiOctave = 0;
      emit octaveChanged(0);

  } else {
      octaveChanged(36); //other spectrums not yet implimented
  }

  qDebug()<<"                   SUCCESS CONFIGURATION :   OCTAVE : "<<m_uiOctave;

  //m_oAudioInterface->StopRecording();
  //m_oAudioInterface->StartRecording();
  setIsDanger(false);
  reset();
}

/**
*******************************************************************************
** @fn  void AudioGet::onDisconnected()
** @param[in] void
** @param[out] void
** @brief (Slot) called whenever the ATomic card is disconnected.
**          all recording is stopped and returns to STANDBY state
**          in case the application is at:
**          SONOMETER_WAV_RECORDING or SONOMETER_LOGGING state, all opened files
**          ( .wav and/or .csv files)
** @return void
**
*******************************************************************************
**/
void AudioGet::onDisconnected() {
  m_oLogFile->WriteLogFile(ERROR_ATOMIC_NOT_FOUND);

  if(m_eStateSonoMaster==SM_SONOMETER_WAV_RECORDING) {
      if(!m_oWavFile->Close()) {
          m_oLogFile->WriteLogFile(EVENT_STOP_WAV_RECORDING);

      } else {
          m_oLogFile->WriteLogFile(ERROR_CLOSING_WAV);
          setError("Unable to");
          setAcquisition(" Close WAV");
      }
  }

  if(m_eStateSonoMaster==SM_SONOMETER_WAV_RECORDING || m_eStateSonoMaster==SM_SONOMETER_LOGGING) {
      if(!m_oWavFile->Close()) {
          m_oLogFile->WriteLogFile(EVENT_STOP_CSV_RECORDING);

      } else {
          m_oLogFile->WriteLogFile(ERROR_CLOSING_CSV);
          setError("Unable to");
          setAcquisition(" Close CSV");
      }
  }

  if(m_eStateSonoMaster>=SM_SONOMETER_READY) {
      m_oAudioInterface->StopRecording();
      m_oLogFile->WriteLogFile(EVENT_STOP_ACQUISITION);
  }

  setAudioSonoState(SM_STANDBY);
  setAtomicState(0);
  m_disconnectTimer->start();
}

/**
*******************************************************************************
** @fn  void checkATomic()
** @param[in] void
** @param[out] void
** @brief checks weither the ATomic Card is connected via USB or not.
**        in case the ATomic Card is disconnected, a pop-up is shown and this
**        slot keeps spying on USB until the card is either connected or the user
**        cancels the operation than the application will quit
** @return void
**
*******************************************************************************
**/
void AudioGet::checkATomic() {
  getATomicInfo();

#ifdef INTERNAL_MIC

  qDebug()<< "ENABLED INTERNAL MIC!!!";

  //Use internal microphone
  setAtomicState(1);
  m_disconnectTimer->stop();
  reset();

#else

  if(productId().contains("AllianTech", Qt::CaseInsensitive)) {
      setAtomicState(1);
      m_disconnectTimer->stop();
      reset();

  } else {
      setAtomicState(0);
      m_disconnectTimer->start();
  }

#endif
}
