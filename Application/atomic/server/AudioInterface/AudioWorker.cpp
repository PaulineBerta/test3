/**
*******************************************************************************
** @file    audioworker.cpp
** @version
** @author  Wevioo SE Team
** @brief   acquire and read audio data from the USB connected card
**
*******************************************************************************
**/

#include "AudioWorker.h"
#include <QAndroidJniObject> //kuba
#include <QString>
#include <QThread>

//3 Flip Flop Buffer for Acquisition (from Java File)
QAndroidJniObject m_oBlockSamplesPang;
QAndroidJniObject m_oBlockSamplesPeng;
QAndroidJniObject m_oBlockSamplesPing;

bool m_bPangBufferReady = false;
bool m_bPengBufferReady = false;
bool m_bPingBufferReady = false;

QMutex m_Mutex;

/**
*******************************************************************************
** @fn  audioThreadIn::audioThreadIn
** @param[in]
** @param[out] QThread *parent
** @brief IConstructor of the AudioThreadIn class
** @return
**
*******************************************************************************
**/
audioThreadIn::audioThreadIn(QAndroidJniObject *m_oAudioJni)
{
    m_bIsRunningIn = true;

    qDebug() << "####### Init audioThreadIn";

    m_oAudioRecordJni = m_oAudioJni;
}


/**
*******************************************************************************
** @fn  void audioThreadIn::stopThreadIn()
** @param[in] void
** @param[out] void
** @brief stop audioThreadIn
** @return void
**
*******************************************************************************
**/
void audioThreadIn::stopThreadIn() {
    m_bIsRunningIn = false;
    qDebug("-------------- STOP THREAD --------------");
}


/**
*******************************************************************************
** @fn  void audioThreadIn::readRecorderPang()
** @param[in] void
** @param[out] void
** @brief
** @return void
**
*******************************************************************************
**/
void audioThreadIn::readRecorderPang() {
    m_iValueReady = 1;
}


/**
*******************************************************************************
** @fn  void audioThreadIn::readRecorderPeng()
** @param[in] void
** @param[out] void
** @brief
** @return void
**
*******************************************************************************
**/
void audioThreadIn::readRecorderPeng() {
    m_iValueReady = 2;
}


/**
*******************************************************************************
** @fn  void audioThreadIn::readRecorderPing()
** @param[in] void
** @param[out] void
** @brief
** @return void
**
*******************************************************************************
**/
void audioThreadIn::readRecorderPing() {
    m_iValueReady = 3;
}


/**
*******************************************************************************
** @fn  void audioThreadIn::run()
** @param[in] void
** @param[out] void
** @brief audioThreadIn is running
** @return void
**
*******************************************************************************
**/
void audioThreadIn::run() {

    qDebug(">>> JE RENTRE DANS RUN AUDIO THREAD IN");

    m_oAudioRecordJni->callMethod<void>("recordConfig");

    //Init Flags Buffer Ready
    m_bPangBufferReady = false;
    m_bPengBufferReady = false;
    m_bPingBufferReady = false;

    try{
        m_oAudioRecordJni->callMethod<void>("startRecorder");
        m_bIsRunningIn = true;

        //kuba
        //Infinite loop whithin the android thread instead of requesting and blocking from c++
        while(m_bIsRunningIn)
        {
            if (m_iValueReady == 1) {
                //////////////////////////// PANG /////////////////////////////////
                m_Mutex.lock();

                //m_oBlockSamplesPang = m_oAudioRecordJni->callObjectMethod("readRecorder", "(V)[F");
                //m_oAudioRecordJni->callMethod<void>("readRecorder");
                m_oBlockSamplesPang = m_oAudioRecordJni->callObjectMethod("readRecorder", "(I)[F", ACQUISITION_BLOCK_SIZE);

                if (m_bPangBufferReady == true) {
                    qDebug("------------ AUDIORECORD WRITE PANG ERROR !!! ------------");
                } else {
                    m_bPangBufferReady = true;
                    //qDebug("------------ AUDIORECORD WRITE PANG ------------");
                    //qDebug(">>> PANG READY");
                }

                m_iValueReady = 0;

                m_Mutex.unlock();
            }

            if (m_iValueReady == 2) {
                //////////////////////////// PENG /////////////////////////////////
                m_Mutex.lock();

                //m_oBlockSamplesPeng = m_oAudioRecordJni->callObjectMethod("readRecorder", "(V)[F");
                //m_oAudioRecordJni->callMethod<void>("readRecorder");
                m_oBlockSamplesPeng = m_oAudioRecordJni->callObjectMethod("readRecorder", "(I)[F", ACQUISITION_BLOCK_SIZE);

                if (m_bPengBufferReady == true) {
                    qDebug("------------ AUDIORECORD WRITE PENG ERROR !!! ------------");
                } else {
                    m_bPengBufferReady = true;
                    //qDebug("------------ AUDIORECORD WRITE PENG ------------");
                    //qDebug(">>> PENG READY");
                }

                m_iValueReady = 0;

                m_Mutex.unlock();
            }

            if (m_iValueReady == 3) {
                //////////////////////////// PING /////////////////////////////////
                m_Mutex.lock();

                //m_oBlockSamplesPing = m_oAudioRecordJni->callObjectMethod("readRecorder", "(V)[F");
                //m_oAudioRecordJni->callMethod<void>("readRecorder");
                m_oBlockSamplesPing = m_oAudioRecordJni->callObjectMethod("readRecorder", "(I)[F", ACQUISITION_BLOCK_SIZE);

                if (m_bPingBufferReady == true) {
                    qDebug("------------ AUDIORECORD WRITE PING ERROR !!! ------------");
                } else {
                    m_bPingBufferReady = true;
                    //qDebug("------------ AUDIORECORD WRITE PING ------------");
                    //qDebug(">>> PING READY");
                }

                m_iValueReady = 0;

                m_Mutex.unlock();
            }
        }

    } catch (const std::exception&) {
        qDebug(">>>>>>> Exit AudioThreadIn <<<<<<<<<");
    }

}



/**
*******************************************************************************
** @fn  audioThread::audioThreadRender
** @param[in]
** @param[out] QThread *parent
** @brief IConstructor of the AudioThreadRender class
** @return
**
*******************************************************************************
**/
audioThreadRender::audioThreadRender()
{
    m_bIsRunningRender = true;

    qDebug() << "####### Init audioThreadRender";
}


/**
*******************************************************************************
** @fn  void audioThreadRender::stopThreadRender()
** @param[in] void
** @param[out] void
** @brief stop audioThreadRender
** @return void
**
*******************************************************************************
**/
void audioThreadRender::stopThreadRender() {
    m_bIsRunningRender = false;
    qDebug("-------------- STOP THREAD --------------");
}


/**
*******************************************************************************
** @fn  void audioThreadRender::run()
** @param[in] void
** @param[out] void
** @brief audioThreadRender is running
** @return void
**
*******************************************************************************
**/
void audioThreadRender::run() {

    qDebug(">>> JE RENTRE DANS RUN AUDIO THREAD RENDER");

    QAndroidJniEnvironment env;

    try {
        m_bIsRunningRender = true;

        //Infinite loop whithin the android thread instead of requesting and blocking from c++
        while (m_bIsRunningRender) {

            if (m_bPangBufferReady) {
                //////////////////////////// PANG /////////////////////////////////
                m_Mutex.lock(); //lock of ThreadIn

                jfloatArray m_afBlockSamplesPang = m_oBlockSamplesPang.object<jfloatArray>();

                //Convert jFloatArray to QList<double>
                QList<double> dataGeneratedPang;
                if (m_afBlockSamplesPang) {
                    jint bufferSize = env->GetArrayLength(m_afBlockSamplesPang);
                    jfloat *floatElt = env->GetFloatArrayElements(m_afBlockSamplesPang, nullptr);
                    if (bufferSize && floatElt != nullptr) {
                        for (int i = 0; i < bufferSize; i++) {
                            dataGeneratedPang.append(*(floatElt+i));
                          }
                      }
                    env->ReleaseFloatArrayElements(m_afBlockSamplesPang, floatElt, 0);
                  }
                int errorBuffer = 0;

                //Signal PacketGenerated
                emit packetGenerated(dataGeneratedPang, errorBuffer);

                m_bPangBufferReady = false;
                emit pangReady();
                m_Mutex.unlock();
            }

            if (m_bPengBufferReady) {
                //////////////////////////// PENG /////////////////////////////////
                m_Mutex.lock(); //lock of ThreadIn

                jfloatArray m_afBlockSamplesPeng = m_oBlockSamplesPeng.object<jfloatArray>();

                //Convert jFloatArray to QList<double>
                QList<double> dataGeneratedPeng;
                if (m_afBlockSamplesPeng) {
                    jint bufferSize = env->GetArrayLength(m_afBlockSamplesPeng);
                    jfloat *floatElt = env->GetFloatArrayElements(m_afBlockSamplesPeng, nullptr);
                    if (bufferSize && floatElt != nullptr) {
                        for (int i = 0; i < bufferSize; i++) {
                            dataGeneratedPeng.append(*(floatElt+i));
                          }
                      }
                    env->ReleaseFloatArrayElements(m_afBlockSamplesPeng, floatElt, 0);
                  }
                int errorBuffer = 0;

                //Signal PacketGenerated
                emit packetGenerated(dataGeneratedPeng, errorBuffer);

                m_bPengBufferReady = false;
                emit pengReady();
                m_Mutex.unlock();
            }

            if (m_bPingBufferReady) {
                //////////////////////////// PING /////////////////////////////////
                m_Mutex.lock(); //lock of ThreadOut

                jfloatArray m_afBlockSamplesPing = m_oBlockSamplesPing.object<jfloatArray>();

                //Convert jFloatArray to QList<double>
                QList<double> dataGeneratedPing;
                if (m_afBlockSamplesPing) {
                    jint bufferSize = env->GetArrayLength(m_afBlockSamplesPing);
                    jfloat *floatElt = env->GetFloatArrayElements(m_afBlockSamplesPing, nullptr);
                    if (bufferSize && floatElt != nullptr) {
                        for (int i = 0; i < bufferSize; i++) {
                            dataGeneratedPing.append(*(floatElt+i));
                          }
                      }
                    env->ReleaseFloatArrayElements(m_afBlockSamplesPing, floatElt, 0);
                  }
                int errorBuffer = 0;

                //Signal PacketGenerated
                emit packetGenerated(dataGeneratedPing, errorBuffer);

                m_bPingBufferReady = false;
                emit pingReady();
                m_Mutex.unlock();
            }
        }

    } catch (const std::exception&) {
        qDebug(">>>>>>> Exit AudioThreadRender <<<<<<<<<");
    }

    if (m_bIsRunningRender == false) {
        return;
    }

}



/**
*******************************************************************************
** @fn  audioWorker::audioWorker(QObject *parent) : QObject(parent)
** @param[inout] QObject *parent
** @brief IConstructor of the AudioWorker class
** @return
**
*******************************************************************************
**/
audioWorker::audioWorker(QObject *parent) : QObject(parent)
{
  //Internal Mic disabled by default
  int internalMic = 0;

  #ifdef INTERNAL_MIC
  //Enable internal Mic
    internalMic = 1;
  #endif

  m_oAudioJni = new QAndroidJniObject("com/AllianTech/ATomicSound/ATomicAudioIn",
                                         "(Landroid/content/Context;II)V",
                                         QtAndroid::androidContext().object(), SAMPLE_PER_PERIOD * 2, internalMic);
  qDebug() << "####### Init audioWorker";

  m_oThreadIn = new audioThreadIn(m_oAudioJni);
  m_oThreadRender = new audioThreadRender();
}


/**
*******************************************************************************
** @fn  void audioWorker::receivePacketGenerated(QList<double> dataGenerated, int error)
** @param[in] void
** @param[out] void
** @brief receive a QList<double> with the 200ms generated or replayed
** @return void
**
*******************************************************************************
**/
void audioWorker::receivePacketGenerated(QList<double> dataGenerated, int error) {
    emit packetReceived(dataGenerated, error);
}


/**
*******************************************************************************
** @fn  int audioWorker::Initialize()
** @param[in] void
** @param[out] int
** @brief Intializes the audio recorder
** @return int
**
*******************************************************************************
**/
int audioWorker::Initialize() {
    connect(m_oThreadRender, SIGNAL(pangReady()), m_oThreadIn, SLOT(readRecorderPeng()));
    connect(m_oThreadRender, SIGNAL(pengReady()), m_oThreadIn, SLOT(readRecorderPing()));
    connect(m_oThreadRender, SIGNAL(pingReady()), m_oThreadIn, SLOT(readRecorderPang()));

    connect(this, SIGNAL(stopIn()), m_oThreadIn, SLOT(stopThreadIn()));
    connect(this, SIGNAL(stopRender()), m_oThreadRender, SLOT(stopThreadRender()));

    connect(m_oThreadRender, SIGNAL(packetGenerated(QList<double>, int)), this, SLOT(receivePacketGenerated(QList<double>, int)));

    m_bPangBufferReady = false;
    m_bPengBufferReady = false;
    m_bPingBufferReady = false;

    return 0;
}

/**
*******************************************************************************
** @fn  void audioWorker::startRecording()
** @param[in] void
** @param[out] void
** @brief start recording upon the ATomic card from the java part
** @return void
**
*******************************************************************************
**/
void audioWorker::startRecording()
{
  m_oThreadIn->start();
  qDebug(">>> START AUDIO THREAD IN");
  m_oThreadRender->start();
  qDebug(">>> START AUDIO THREAD RENDER");

  emit threadWorkerRunning();
}

/**
*******************************************************************************
** @fn  void audioWorker::stopRecording()
** @param[in] void
** @param[out] void
** @brief stop recording upon the ATomic card from the java part
** @return void
**
*******************************************************************************
**/
void audioWorker::stopRecording()
{   
  emit stopRender();
  while (m_oThreadRender->isRunning()) {
      QThread::msleep(10);
      qDebug("---- THREAD RENDER WAIT -----");
  }

  emit stopIn();
  while (m_oThreadIn->isRunning()) {
      QThread::msleep(10);
      qDebug("---- THREAD IN WAIT -----");
  }

  m_oAudioJni->callMethod<void>("stopRecorder");

  m_bPangBufferReady = false;
  m_bPengBufferReady = false;
  m_bPingBufferReady = false;

  m_oThreadIn->m_iValueReady = 1;

  emit threadWorkerNotRunning();
}
