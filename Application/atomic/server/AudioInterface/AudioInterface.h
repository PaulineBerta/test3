/**
*******************************************************************************
** @file    audiointerface.h
** @version
** @author  Wevioo SE Team
** @brief   manages the acquisition process: start/stop/read data blocks
**
*******************************************************************************
**/

#ifndef AUDIOINTERFACE_H
#define AUDIOINTERFACE_H

#include <QObject>
#include <QThread>
#include "AudioWorker.h"

typedef enum
{
    ATOMIC_1,
    ATOMIC_2,
    ATOMIC_SG,
    ATOMIC_16,
    NO_ATOMIC
}TeATomicModel;

class audioInterface : public QObject
{
  Q_OBJECT

public:
  /**
  *******************************************************************************
  ** @fn  audioInterface(QObject *parent)
  ** @param[inout] QObject
  ** @brief Constructor of the audioInterface class
  ** @return
  **
  *******************************************************************************
  **/
  explicit audioInterface(QObject *parent = nullptr);         /*!< AudioInterface class constructor */

  /**
  *******************************************************************************
  ** @fn  int Initialize()
  ** @param[in] void
  ** @param[out] int
  ** @brief Initializes the thread and connects signal and slots to other classes
  ** @return int
  **
  *******************************************************************************
  **/
  int Initialize();                                           /*!< Initializes the AudioInterface class: connects different signals to slots and
                                                                     starts the thread */
  /**
  *******************************************************************************
  ** @fn  void StartRecording()
  ** @param[in] void
  ** @param[out] void
  ** @brief slot, Start the recording thread
  ** @return void
  **
  *******************************************************************************
  **/
  void StartRecording();                                      /*! emits sig_startRecording signal to the AudioWorker class */

  /**
  *******************************************************************************
  ** @fn  void StopRecording()
  ** @param[in] void
  ** @param[out] void
  ** @brief slot, Stop the recording thread
  ** @return void
  **
  *******************************************************************************
  **/
  void StopRecording();                                       /*! emits sig_stopRecording signal to the AudioWorker class */

  /**
  *******************************************************************************
  ** @fn  QString GetProductName()
  ** @param[in] void
  ** @param[out] QString serial number
  ** @brief returns connected cards serial numbers
  ** @return int
  **
  *******************************************************************************
  **/
  QString GetProductName();                                   /*!< Gets the product name of the ATomic card */

  /**
  *******************************************************************************
  ** @fn  QString GetSerialNumber()
  ** @param[in] void
  ** @param[out] QString product name
  ** @brief returns connected cards product names
  ** @return int
  **
  *******************************************************************************
  **/
  QString GetSerialNumber();

  /**
  *******************************************************************************
  ** @fn  void checkATomicModel()
  ** @param[in] void
  ** @param[out] void
  ** @brief seraches ProductId for ATomic Model information
  ** @return
  **
  *******************************************************************************
  **/
  void checkATomicModel();

signals:
  void readData(QList<double>,int);                           /*!< signal be emitted to pass data and error to the AudioGet class */

  /******** Internal Signals **********/
  //void sig_readNextPacket();                                  /*!< signal be emitted when 200ms (default) elapses and data have been processed */
  void sig_startRecording();                                  /*!< signal be emitted to start the audio recording */
  void sig_stopRecording();                                   /*!< signal be emitted to stop the audio recording */
  void deviceDisconnected();                                  /*!< signal be emitted when ATomic Card is no longer detected */

  void threadWorkerRunning();
  void threadWorkerNotRunning();

private slots:

  /**
  *******************************************************************************
  ** @fn  void processPacket(QList<double> data, int iError)
  ** @param[in] QList<double> data
  ** @param[in] int error
  ** @param[out] void
  ** @brief slot, emits read data signal and read next packet signal
  ** @return void
  **
  *******************************************************************************
  **/
  void processPacket(QList<double>, int);                     /*!< gets the read data from the AudioWorker and emits the sig_readNextPacket signal */

public:
    TeATomicModel m_eATomicModel = NO_ATOMIC;

private:
  QThread m_audioThread;                                      /*!< the audio recorder thread */
  audioWorker m_oAudioWorker;                                 /*!< instance of the AudioWorker class */
  int m_iStartStop;                                           /*!< state of the thread */
  QAndroidJniObject* m_UsbObj;
  bool m_bIsATomicFound= true;

};

#endif // AUDIOINTERFACE_H
