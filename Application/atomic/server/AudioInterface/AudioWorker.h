/**
*******************************************************************************
** @file    audioworker.h
** @version
** @author  Wevioo SE Team
** @brief   acquire and read audio data from the USB connected card
**
*******************************************************************************
**/

#ifndef AUDIOWORKER_H
#define AUDIOWORKER_H

#include <QObject>
#include <QtAndroid>
#include <QtAndroidExtras/QAndroidJniObject>
#include <QtAndroidExtras/QAndroidJniEnvironment>
#include <QtDebug>
#include <QThread>
#include <QMutex>
#include <jni.h>

#define SAMPLE_PER_PERIOD 200 * 48 * 2

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define FECH 48000

class audioThreadIn : public QThread {
    Q_OBJECT

public:
    /**
    *******************************************************************************
    ** @fn  audioThreadIn
    ** @param[in]
    ** @param[out] QThread *parent
    ** @brief IConstructor of the AudioThreadIn class
    ** @return
    **
    *******************************************************************************
    **/
    explicit audioThreadIn(QAndroidJniObject *m_oAudioJni);         /*!< AudioThread constructor */

protected :
    /**
    *******************************************************************************
    ** @fn  void run()
    ** @param[in] void
    ** @param[out] void
    ** @brief audioThreadIn is running
    ** @return void
    **
    *******************************************************************************
    **/
    void run() override;

signals :

public slots:
    /**
    *******************************************************************************
    ** @fn  void :stopThreadIn()
    ** @param[in] void
    ** @param[out] void
    ** @brief stop audioThreadIn
    ** @return void
    **
    *******************************************************************************
    **/
    void stopThreadIn();

    /**
    *******************************************************************************
    ** @fn  void readRecorderPang()
    ** @param[in] void
    ** @param[out] void
    ** @brief
    ** @return void
    **
    *******************************************************************************
    **/
    void readRecorderPang();

    /**
    *******************************************************************************
    ** @fn  void readRecorderPeng()
    ** @param[in] void
    ** @param[out] void
    ** @brief
    ** @return void
    **
    *******************************************************************************
    **/
    void readRecorderPeng();

    /**
    *******************************************************************************
    ** @fn  void readRecorderPing()
    ** @param[in] void
    ** @param[out] void
    ** @brief
    ** @return void
    **
    *******************************************************************************
    **/
    void readRecorderPing();

public :
    int ACQUISITION_BLOCK_DURATION_MS = 200; //Default duration for a block
    int ACQUISITION_BLOCK_SIZE = (int)(ACQUISITION_BLOCK_DURATION_MS * FECH/1000) * 2 ;

    bool m_bIsRunningIn = false;
    int m_iInternalMICInit = 0;
    int m_iBufferSize;

    int m_iValueReady = 1; // Init Start Value

    //AudioRecord mRecorder;
    long t1, t0, tdiff, f1, f0, fdiff;

private :
    //AudioTimestamp outTimestamp;
    int bufferSize;
    QAndroidJniObject *m_oAudioRecordJni;
};


class audioThreadRender : public QThread {
    Q_OBJECT

public:
    /**
    *******************************************************************************
    ** @fn  audioThreadRender
    ** @param[in]
    ** @param[out] QThread *parent
    ** @brief IConstructor of the AudioThreadRender class
    ** @return
    **
    *******************************************************************************
    **/
    explicit audioThreadRender();         /*!< AudioThread constructor */

protected :
    /**
    *******************************************************************************
    ** @fn  void run()
    ** @param[in] void
    ** @param[out] void
    ** @brief audioThreadRender is running
    ** @return void
    **
    *******************************************************************************
    **/
    void run() override;

signals :
    void pangReady();
    void pengReady();
    void pingReady();

    void packetGenerated(QList<double> dataGenerated, int error);

public slots:
    /**
    *******************************************************************************
    ** @fn  void :stopThreadRender()
    ** @param[in] void
    ** @param[out] void
    ** @brief stop audioThreadIn
    ** @return void
    **
    *******************************************************************************
    **/
    void stopThreadRender();

public :
    bool m_bIsRunningRender = false;
    int m_iInternalMICInit = 0;
    int m_iBufferSize;

    //AudioRecord mRecorder;
    long t1, t0, tdiff, f1, f0, fdiff;

private :
    //AudioTimestamp outTimestamp;
    int bufferSize;
    //QAndroidJniObject *m_oAudioRenderJni;
};


class audioWorker : public QObject
{
  Q_OBJECT
public:
  /**
  *******************************************************************************
  ** @fn  audioWorker(QObject *parent) : QObject(parent)
  ** @param[inout] QObject *parent
  ** @brief IConstructor of the AudioWorker class
  ** @return
  **
  *******************************************************************************
  **/
  explicit audioWorker(QObject *parent = nullptr);         /*!< AudioWorker constructor */
  //explicit audioWorker(QObject *parent = 0);        //kuba

  /**
  *******************************************************************************
  ** @fn  int Initialize()
  ** @param[in] void
  ** @param[out] int
  ** @brief Intializes the audio recorder
  ** @return int
  **
  *******************************************************************************
  **/
  int Initialize();                                        /*!< initializes the audio recorder */


signals:
  void packetReceived(QList<double>, int);                 /*!< signal emitted after each 200ms from the start of recording */

  void stopIn();
  void stopRender();

  void threadWorkerRunning();
  void threadWorkerNotRunning();


public slots:
  /**
  *******************************************************************************
  ** @fn  void receivePacketGenerated(QList<double> packetGenerated, int error)
  ** @param[in] void
  ** @param[out] void
  ** @brief receive a QList<double> with the 200ms generated or replayed
  ** @return void
  **
  *******************************************************************************
  **/
  void receivePacketGenerated(QList<double> packetGenerated, int error);

  /**
  *******************************************************************************
  ** @fn  void startRecording()
  ** @param[in] void
  ** @param[out] void
  ** @brief start recording upon the ATomic card from the java part
  ** @return void
  **
  *******************************************************************************
  **/
  void startRecording();                                   /*!< slot called after the reception of sig_startRecording signal from the audioInterface class */

  /**
  *******************************************************************************
  ** @fn  void stopRecording()
  ** @param[in] void
  ** @param[out] void
  ** @brief stop recording upon the ATomic card from the java part
  ** @return void
  **
  *******************************************************************************
  **/
  void stopRecording();                                    /*!< slot executed after the reception of sig_stopRecording signal from the audioInterface class */

public:
  int                m_iPangPengPingCounter = 0; //PANG == 0 / PENG == 1 / PING == 2

private:
  QAndroidJniObject *m_oAudioJni;
  audioThreadIn *m_oThreadIn;
  audioThreadRender *m_oThreadRender;

  bool m_bRecordRunning = false;
};

#endif // AUDIOWORKER_H
