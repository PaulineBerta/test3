/**
*******************************************************************************
** @file    AudioInterface.cpp
** @version
** @author  Wevioo SE Team
** @brief   manages the acquisition process: start/stop/read data blocks
**
*******************************************************************************
**/

#include "AudioInterface.h"
#include <QDebug>

Q_DECLARE_METATYPE(QList<double>)

/**
*******************************************************************************
** @fn  audioInterface::audioInterface(QObject *parent)
** @param[inout] QObject
** @brief Constructor of the audioInterface class
** @return
**
*******************************************************************************
**/
audioInterface::audioInterface(QObject *parent) : QObject(parent) {
  qRegisterMetaType<QList<double> >("QList<double>");
}

/**
*******************************************************************************
** @fn  int audioInterface::Initialize()
** @param[in] void
** @param[out] int
** @brief Initializes the thread and connects signal and slots to other classes
** @return int
**
*******************************************************************************
**/
int audioInterface::Initialize() {
  int ret = m_oAudioWorker.Initialize();
  m_UsbObj =nullptr;

  if (ret == 0) {
      m_iStartStop = 0;

      connect(this, SIGNAL(sig_startRecording()), &m_oAudioWorker, SLOT(startRecording()));
      connect(this, SIGNAL(sig_stopRecording()), &m_oAudioWorker, SLOT(stopRecording()));

      connect(&m_oAudioWorker, SIGNAL(packetReceived(QList<double>,int)), this, SLOT(processPacket(QList<double>,int)));

      m_oAudioWorker.moveToThread(&m_audioThread);
      m_audioThread.start(QThread::TimeCriticalPriority);
  }

  return ret;
}

/**
*******************************************************************************
** @fn  QString audioInterface::GetProductName()
** @param[in] void
** @param[out] QString serial number
** @brief returns connected cards serial numbers
** @return int
**
*******************************************************************************
**/
QString audioInterface::GetProductName(){
  if(!m_UsbObj) {
      m_UsbObj = new QAndroidJniObject("com/AllianTech/ATomicSound/ATomicUsbManager",
                                       "(Landroid/content/Context;)V",
                                       QtAndroid::androidContext().object());
  }

  QString cProductName = m_UsbObj->callObjectMethod("getATomicProductName",
                                                    "()Ljava/lang/String;").toString();
  return cProductName;
}

/**
*******************************************************************************
** @fn  QString audioInterface::GetSerialNumber()
** @param[in] void
** @param[out] QString product name
** @brief returns connected cards product names
** @return int
**
*******************************************************************************
**/
QString audioInterface::GetSerialNumber() {
  if(!m_UsbObj) {
      m_UsbObj = new QAndroidJniObject("com/AllianTech/ATomicSound/ATomicUsbManager",
                                       "(Landroid/content/Context;)V",
                                       QtAndroid::androidContext().object());
  }

  QString cSerialNumber = m_UsbObj->callObjectMethod("getATomicSerialNumber",
                                                     "()Ljava/lang/String;").toString();
  return cSerialNumber;
}

/**
*******************************************************************************
** @fn  void audioInterface::StartRecording()
** @param[in] void
** @param[out] void
** @brief slot, Start the recording thread
** @return void
**
*******************************************************************************
**/
void audioInterface::StartRecording(){
#ifdef INTERNAL_MIC

      m_iStartStop = 1;
      m_bIsATomicFound=true;
      emit sig_startRecording();

      qDebug()<<"////////////////////////////////// JE PASSE DANS INTERNAL MIC ////////////////////////////";

#else

      if(GetProductName().contains("AllianTech", Qt::CaseInsensitive)) {
          qDebug() <<"device detected sucesfully";
          m_iStartStop = 1;
          m_bIsATomicFound=true;
          emit sig_startRecording();

      } else {
          qDebug() <<"device disconnected ";
          emit deviceDisconnected();
          m_bIsATomicFound=false;
      }

#endif
}


/**
*******************************************************************************
** @fn  void audioInterface::StopRecording()
** @param[in] void
** @param[out] void
** @brief slot, Stop the recording thread
** @return void
**
*******************************************************************************
**/
void audioInterface::StopRecording()
{
  m_iStartStop = 0;
  emit sig_stopRecording();
}

/**
*******************************************************************************
** @fn  void audioInterface::processPacket(QList<double> data, int iError)
** @param[in] QList<double> data
** @param[in] int error
** @param[out] void
** @brief slot, emits read data signal and read next packet signal
** @return void
**
*******************************************************************************
**/
void audioInterface::processPacket(QList<double> data, int iError)
{

//kuba
#ifdef INTERNAL_MIC

     emit readData(data, iError);
     m_bIsATomicFound=true;

#else

    if(GetProductName().contains("AllianTech", Qt::CaseInsensitive))
    {
        emit readData(data, iError);
        m_bIsATomicFound=true;
    }
    else if(m_bIsATomicFound){
        qDebug()<<"device disconnected";
        m_bIsATomicFound=false;
        emit deviceDisconnected();
    }

#endif

  /*if (m_iStartStop != 0) { //kuba
      emit sig_readNextPacket();
  }*/
}

/**
*******************************************************************************
** @fn  void audioInterface::checkATomicModel()
** @param[in] void
** @param[out] void
** @brief Checks in ProductID for ATomic Model information
** @return void
**
*******************************************************************************
**/
void audioInterface::checkATomicModel() {
    if(GetProductName().contains("AllianTech", Qt::CaseInsensitive)) {
        qDebug() <<"device detected sucesfully";
        m_iStartStop = 1;
        m_bIsATomicFound=true;

        QString sProductId = GetProductName();

        int iFirstSpacePosition = sProductId.indexOf(" ") + 1; //+1 to not take " "
        int iSecondSpacePosition = sProductId.indexOf(" ", iFirstSpacePosition);
        sProductId.truncate(iSecondSpacePosition);
        QString sRawAtomicModel = sProductId.right(iFirstSpacePosition);

        qDebug() <<"Model : " << sRawAtomicModel;

        //ATOMIC_1
        if((GetProductName().contains("ATomic-1", Qt::CaseInsensitive)) or
           (GetProductName().contains("ATomic-21", Qt::CaseInsensitive))) {
            m_eATomicModel = ATOMIC_1;
            qDebug() << "### DETECTED Model : ATOMIC_1 ";
        }
        //ATOMIC_2
        else if((GetProductName().contains("ATomic-2", Qt::CaseInsensitive))) {
            m_eATomicModel = ATOMIC_2;
            qDebug() << "### DETECTED Model : ATOMIC_2 ";
        }

    }
}

