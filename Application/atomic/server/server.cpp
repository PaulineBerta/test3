/**
*******************************************************************************
** @file    server.cpp
** @version V0.0.1
** @author  Wevioo SE Team
** @brief   Creates a remote host node and enables remoting
**
*******************************************************************************
**/
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>

#include <QFile>
#include <QDataStream>
#include <QtAndroidExtras/QAndroidJniEnvironment>
#include <QAndroidJniObject>
#include <QDateTime>
#include <QAndroidService>
#include <QRemoteObjectHost>
#include <QFile>
#include <QDataStream>
#include <QtAndroidExtras/QAndroidJniEnvironment>
#include <QAndroidJniObject>
#include <QDateTime>
#include <QDebug>
#include "AudioGet/AudioGet.h"
#include "AudioConfig/GetConfiguration.h"

#include "rep_atomic_source.h"

int main(int argc, char *argv[])
{
  QAndroidService app(argc, argv);

  QRemoteObjectHost srcNode(QUrl(QStringLiteral("local:atomic")));
  AudioGet* AudioSrc=new AudioGet(nullptr);
  GetConfiguration* getConfiguration=new GetConfiguration(nullptr, AudioSrc);

  srcNode.enableRemoting(AudioSrc);
  QRemoteObjectHost srcNode2(QUrl(QStringLiteral("local:configuration")));
  srcNode2.enableRemoting(getConfiguration);

  //QObject::connect(qApp, SIGNAL(aboutToQuit()), AudioSrc, SLOT(onServiceTimeOut()));
  return app.exec();
}
