TEMPLATE = lib
TARGET = server
CONFIG += dll
#CONFIG += c++11
QT += core quick
QT += androidextras qml gui quickcontrols2 widgets core printsupport xml
QT += svg
QT += remoteobjects

#debug directives
CONFIG+=debug
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to
# port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# uncomment to enable internal mic
DEFINES += INTERNAL_MIC

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AudioInterface/AudioInterface.cpp \
    AudioInterface/AudioWorker.cpp \
    AudioLog/LogFile.cpp \
    WavFile/WavFile.cpp \
    AudioGet/AudioGet.cpp \
    AudioConfig/GetConfiguration.cpp \
    server.cpp #in QtService this file works as main.cpp


REPC_SOURCE += ../atomic.rep\
               ../configuration.rep

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    AudioInterface/AudioInterface.h \
    AudioInterface/AudioWorker.h \
    AudioLog/LogFile.h \
    WavFile/WavFile.h \
    AudioGet/AudioGet.h \
    android/libs/AcousticMeasSo.h \
    android/libs/AcousticMeasTypes.h \
    AudioConfig/GetConfiguration.h

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/res/xml/accessory_filter.xml \
    android/libs/AcousticMeas_armv7/libAcousticMeas.a

    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android

LIBS += -L$$PWD/android/libs/AcousticMeas_armv7/ -lAcousticMeas

INCLUDEPATH += $$PWD/android/libs/AcousticMeas_armv7
DEPENDPATH += $$PWD/android/libs/AcousticMeas_armv7

PRE_TARGETDEPS += $$PWD/android/libs/AcousticMeas_armv7/libAcousticMeas.a


