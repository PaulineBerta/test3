#ifndef ACOUSTICMEAS_H
#define ACOUSTICMEAS_H

#ifndef ACOUSTICMEAS_GLOBAL_H
#define ACOUSTICMEAS_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ACOUSTICMEAS_LIBRARY)
#  define ACOUSTICMEAS_EXPORT Q_DECL_EXPORT
#else
#  define ACOUSTICMEAS_EXPORT Q_DECL_IMPORT
#endif

#endif // ACOUSTICMEAS_GLOBAL_H

#include "AcousticMeasTypes.h"

//#include <QtCore/qglobal.h>

//Definition of correspondance between C++ entry point and C entry point
//----------------------------------------------------------------------

class ACOUSTICMEAS_EXPORT AcousticMeasSo {

    public: ACOUSTICMEAS_EXPORT AcousticMeasSo(int iNumInstance); //Constructor

    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_Create(void);
    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_Configure(const TrAcousticMeasConfig &rAcousticMeasConfig, float* pfFullScaledB, float* pfFullScaleLin, float* pfFftRealOverlapPercent);
    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_Delete(void);
    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_NewBlock(const float * rChan1BlockSignalNorm1, const float * rChan2BlockSignalNorm1, TrAcousticResBasis *rAcousticRes, float *pfSignalCombined);
    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_Escape(void);
    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_ResetCalcul(void);
    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_GetFftResult(TeTypeFftResult eTypeFftResult, float * pafFftResult);
    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_GetTrResult(TrReverberationRes &rReverberationRes);
    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_RecalculateTr(const TrReverberationRecalculateConfig &rReverberationRecalculateConfig, TrReverberationRecalculateRes &rReverberationRecalcualteRes);
    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_RecalculateOneFreqTr(const TrReverberationRecalculateOneFreqConfig &rReverberationRecalculateOneFreqConfig, TrReverberationRecalculateOneFreqRes &rReverberationRecalcualteOneFreqRes) ;
    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_StartTr(bool bFlagNewCompleteTr);
    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_StopTr(void);
    public: ACOUSTICMEAS_EXPORT void AcousticMeasSo_DeleteLastTr(TrReverberationRes &rPrecedentReverberationRes);

};

#endif // ACOUSTICMEAS_H
