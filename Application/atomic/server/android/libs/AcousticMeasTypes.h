// ***********************************************************************
// HEADER FILE
// ***********************************************************************
// NAME : AcousticMeasTypes.h
// DESCRIPTION : h file of class AcousticMeas for interface with callinfg program
// AUTHOR : AMB
// DATE : 16/08/2018
// ********************************************************************* */

#pragma once

#define NB_MAX_FREQ_SPECTRUM 36

typedef enum
{
    LEQ_A = 0,
    LEQ_C,
    LEQ_Z,
    SIZE_ACZ
}TeLeqType;

typedef enum
{
    PEAK_C = 0,
    PEAK_Z,
    SIZE_PEAK_CZ
}TePeakType;

#ifdef __cplusplus
#include "string.h"
#define MY_WORD int
#ifndef MYTYPE
    #ifdef STM32
        #define MYTYPE float
        #define MYTYPE_FLOAT
    #else
        #define MYTYPE double
    #endif
#endif

// ****************************************************************************
// DEFINE for constant necessary to use AcousticMeas
// ****************************************************************************
#define PI 3.14159265359
#define NB_MAX_FAST_IT_IN_1_BLOCK (200/8)
#define NB_MAX_OTHER_RES_IN_BLOCK (50*8) // To have reverberation time in one block


// ****************************************************************************
// DEFINE for configuration of AcousticMeas
// ****************************************************************************
#define IT_DEFAULT 1
#define SIZE_BLOCK_DEFAULT 9600
#define NB_MAX_FIT_IN_200MS 25 // Maximum number of FIT results in one block
#define BLOCK_DURATION_SEC ((float)SIZE_BLOCK_DEFAULT / (float) SAMPLE_FREQ_48K)  //??AMB A modifier

//Definition of minimum block for Lp calculation
#define SIZE_BLOCK_MIN 384 // 48000* 8ms  //??AMB A vérifier

// Structure for sensor params
//----------------------------
// List of sensors managed
typedef enum
{
    MICROPHONE,
    ACCELEROMETER,
    VELOCIMETER,
    FORCE,
    PRESSURE,
    OTHER
}TeSensorType;

struct TrSensorParams
{
    TrSensorParams()
    {
        eType = MICROPHONE;
        fFullScaleVolt = 5.00f;
        fSensitivitySI = 0.05f;
        fCalibrationCorrectiondB = 0.0f;
        fGainInterChanneldB = 20.0f;
        fFreeFielCorrectiondB = 0.1f;
    };

    TeSensorType eType;
    float fFullScaleVolt;
    float fSensitivitySI;
    float fCalibrationCorrectiondB;
    float fGainInterChanneldB; // Used for full dynamic
    float fFreeFielCorrectiondB; //To take into account the foam which is not there during calibration
};

// Structure for calculation on filter ACZ
//----------------------------------------
typedef enum
{
    LP_NONE,	// No Lp calculation
    LP_SLOW,	// Calculate Lp slow
    LP_FAST,	// Calculate Lp fast
    LP_IMPULSE
}TeTimeWeightingLp;

struct TrACZFilterConfig
{
    TrACZFilterConfig()
    {
        eTimeWeightingLp = LP_NONE;
    };

    // List of calculation on ACZ
    TeTimeWeightingLp 	eTimeWeightingLp;	//Kind of calculation for Lp : 0 -> NONE, 1 -> SLOW, 2-> FAST
};

//Parameters for all the sensors to perform RMS and peak measurement
typedef enum
{
    DECIM_NONE,	// No decimation performed on input signal
    DECIM_BY_2,	// Decimation by 2 so sampling rate = 24000Hz
    DECIM_BY_4,	// Decimation by 4 so sampling rate = 12000Hz
    DECIM_BY_8	// Decimation by 2 so sampling rate = 6000Hz
}TeDecimationFactor;

typedef enum
{
    HP_NONE,	// No high pass filter
    HP_3HZ,		// High pass 3 Hz
    HP_10HZ,	// High pass 10 Hz
    HP_80HZ,	// High pass 80 Hz
}TeHighPassFilter;

typedef enum
{
    LOWPASS_NONE,	// No low pass filter
    LP_125HZ,
    LP_250HZ,
    LP_500HZ,
    LP_1KHZ,	// Low pass 1 KHz
    LP_2KHZ,	// Low pass 2 KHz
    LP_4KHZ,	// Low pass 4 KHz
    LP_8KHZ,	// Low pass 8 KHz
}TeLowPassFilter;

struct TrMultiSensorConfig
{
    TrMultiSensorConfig()
    {
        eDecimationFactor = DECIM_NONE;
        eHighPassFilter = HP_NONE;
        eLowPassFilter = LOWPASS_NONE;
    };

    // List of calculation parameters for calculation of rms and peak value
    TeDecimationFactor eDecimationFactor; // Decimation Factor not used at start perhaps used dor storage of wav or FFT
    TeHighPassFilter eHighPassFilter;	//High pass filter
    TeLowPassFilter eLowPassFilter;		//Low pass filter
};

// Structure for calculation of spectrum
//---------------------------------------------
typedef enum
{
    SPECTRUM_NO,		//No spectrum calculation
    SPECTRUM_OCTAVE,	//Spectre d'octave
    SPECTRUM_3RD_OCTAVE,	//Spectre 1/3 d'octave
    SPECTRUM_FFT,
    SPECTRUM_ZOOM_FFT,
    SPECTRUM_ENVELOPPE
}TeSpectrum;

typedef enum
{
    RECTANGULAR,
    HAMMING,
    HANNING,
    FLAT_TOP,
    BLACKMAN_HARRIS
}TeWindow;

typedef enum
{
    FFT_256 = 256,
    FFT_512 = 512,
    FFT_1024 = 1024,
    FFT_2048 = 2048,
    FFT_4096 = 4096,
    FFT_8192 = 8192,
    FFT_16384 = 16384,
}TeFFTSize;

typedef enum
{
    LINEAR,
    EXPONENTIAL,
    PEAK
}TeFFTAveraging;

typedef enum
{
    OVERLAP_0 = 0,
    OVERLAP_25 = 25,
    OVERLAP_50 = 50,
    OVERLAP_75 = 75
}TeFFTOverlapPercent;

typedef enum
{
    ZOOM_0 = 1,
    ZOOM_2 = 2,
    ZOOM_4 = 4,
    ZOOM_8 = 8,
    ZOOM_16 = 16
}TeZoomFftFactor;

typedef enum
{
    UNIT_LINEAR = 0,
    UNIT_DB = 1
}TeFFTOutputUnit;

#define MIN_OVERLAP_FFT 0.0234375f // Necessary because the sampling rate is not a multiple of a power of 2
//So we will add even to x% overlap, depending of the FFT size : 384 (16384pts), 192 (8192 pts), 96 (4096 pts), 48 (2048 pts), 24 (1024 pts), 12 (512 pts), 6 (256 pts)

struct TrSpectrumConfig
{
    TrSpectrumConfig()
    {
        eSpectrum = SPECTRUM_3RD_OCTAVE;
        eWindow = RECTANGULAR;
        eFFTSize = FFT_1024;
        eFFTAveraging = LINEAR;
        eFFTOverlapPercent = OVERLAP_0;
        eFFTOutputUnit = UNIT_DB;
        fZoomFrequencyHz = 0.0f;
        eZoomFactor = ZOOM_0;
        fMaxBandwidthHz = 20000.0f;
    };

    TeSpectrum eSpectrum;
    TeWindow eWindow;
    TeFFTSize eFFTSize;
    TeFFTAveraging eFFTAveraging;
    TeFFTOverlapPercent eFFTOverlapPercent;
    TeFFTOutputUnit eFFTOutputUnit;
    float fZoomFrequencyHz; // il zoom frequency is 0, it's only a FFT on decimated signal
    TeZoomFftFactor eZoomFactor;
    float fMaxBandwidthHz;
};

// Configuration for calibration
typedef enum
{
    CLASS_0,		//Class 0 error calibration allowed 1dB
    CLASS_1,		//Class 1 error calibration allowed 1.5dB
    CLASS_2			//Class 2 error calibration allowed 2dB
}TeClassMicro;
struct TrCalibrationConfig
{
    TrCalibrationConfig()
    {
        fLeveldB = 94.0f;
        eClassMicro = CLASS_1; //1dB for class 0, 1.5dB for class 1, 2dB for class 2
        fStabilitydB = 0.5f;
    };
    float fLeveldB; // To define level of calibration
    TeClassMicro eClassMicro; // Class of microphone
    float fStabilitydB; // Stability of the Leq C searched
};

// Configuration for reverberation time calculation
typedef enum
{
    SOURCE_IMPULSE,		// Impulse Source
    SOURCE_INTERRUPTED	// Stopped source
}TeTypeSource;

#define TIME_MAX_SECONDS_RECORD_DECREASE 3.0f
#define TIME_MAX_SECONDS_EXTENDED_RECORD_DECREASE 10.0f
#define NB_MAX_FREQ_TR (NB_MAX_FREQ_SPECTRUM + 1)
#define INDEX_FREQ_100HZ_3RD_OCTAVE 12
#define INDEX_FREQ_5KHZ_3RD_OCTAVE 29
#define INDEX_FREQ_125HZ_OCTAVE 4
#define INDEX_FREQ_4KHZ_OCTAVE 9
#define DEFAULT_LEVEL_SOURCE 90.0f
#define DEFAULT_MAX_NOISE_LEVEL_DB 50.0f
#define DEFAULT_NB_SEC_MEASURE_NOISE 5
#define DEFAULT_NB_SEC_STABILIZE_SOURCE 3
#define DEFAULT_NB_SEC_MEASURE_SOURCE 2
#define DEFAULT_MIN_TR_SEC 0.4f

struct TrReverberationConfig
{
    TrReverberationConfig()
    {
        fThresholdSourcedB = DEFAULT_LEVEL_SOURCE;
        fTimeRecordDecrease = TIME_MAX_SECONDS_RECORD_DECREASE; //10.0f of extended record is selected
        eTypeSource = SOURCE_IMPULSE;
        fMaxNoiseLeveldB = DEFAULT_MAX_NOISE_LEVEL_DB;
        iIndexFreqStart = INDEX_FREQ_100HZ_3RD_OCTAVE;
        iIndexFreqEnd = INDEX_FREQ_5KHZ_3RD_OCTAVE;
        iNbSecMeasureNoise = DEFAULT_NB_SEC_MEASURE_NOISE;
        iNbSecStabilizationSource = DEFAULT_NB_SEC_STABILIZE_SOURCE;
        iNbSecMeasureSource = DEFAULT_NB_SEC_MEASURE_SOURCE;
        fMinTrSec = DEFAULT_MIN_TR_SEC;
    };
    float fThresholdSourcedB; // Threshold to start reverberation time calculation
    float fTimeRecordDecrease; //Time to record decrease
    TeTypeSource eTypeSource;
    float fMaxNoiseLeveldB;  // Level maximum of noise measured. If level is too high reverberation time cannot be calculated
    int iIndexFreqStart;
    int iIndexFreqEnd;
    int iNbSecMeasureNoise;
    int iNbSecStabilizationSource; // Number of seconds to stabilize source
    int iNbSecMeasureSource;
    float fMinTrSec; //Minimum values of TR

};


// MOST IMPORTANT PARAMETER which fix size of INPUT block
//-----------------------------------------------------------------
typedef enum
{
    BLOCK_40MS = 40,
    BLOCK_200MS = 200	// SIZE_BLOCK = 9600
}TeDurationBlock;

typedef enum
{
    SAMPLE_FREQ_48K = 48000,
    SAMPLE_FREQ_24K = 24000
}TeSampleFrequency;

typedef enum
{
    FIT_8MS = 8,	// SIZE_BLOCK = 384
    FIT_10MS = 10, // SIZE_BLOCK = 480
    FIT_20MS = 20, // SIZE_BLOCK = 960
    FIT_40MS = 40,	// SIZE_BLOCK = 1920
    FIT_200MS = 200	// SIZE_BLOCK = 9600
}TeFastIntegrationTime;

typedef enum
{
    FLOAT_NORM = 1,			// Float normalized between 0 and 1
    FLOAT_16B = 32768, 		// Float transtyped from int 16
    FLOAT_24B = 8388608, 	// Float transtyped from int 24
    FLOAT_32B = 2147483648 	// Float transtyped from int 32
}TeSampleFormat;

typedef enum
{
    CHAN_LEFT,          // Treatment of left channel
    CHAN_RIGHT,         // Treatment of right channel
    CHAN_STEREO_FULL,   // Treatment of right and left channel for full dynamic
    SIMUL_WHITE_NOISE,	// Simul white noise as an input
    SIMUL_PINK_NOISE,	// Simul pink noise as an input
    CHAN_ALL_3,			// All the 3 channels
    CHAN_MULTIPLEX16_0,  //Channel 0 of multiplex of 16
    CHAN_MULTIPLEX16_1,  //Channel 1 of multiplex of 16
    CHAN_MULTIPLEX16_2,  //Channel 2 of multiplex of 16
    CHAN_MULTIPLEX16_3,  //Channel 3 of multiplex of 16
    CHAN_MULTIPLEX16_4,  //Channel 4 of multiplex of 16
    CHAN_MULTIPLEX16_5,  //Channel 5 of multiplex of 16
    CHAN_MULTIPLEX16_6,  //Channel 6 of multiplex of 16
    CHAN_MULTIPLEX16_7,  //Channel 7 of multiplex of 16
    CHAN_MULTIPLEX16_8,  //Channel 8 of multiplex of 16
    CHAN_MULTIPLEX16_9,  //Channel 9 of multiplex of 16
    CHAN_MULTIPLEX16_10,  //Channel 10 of multiplex of 16
    CHAN_MULTIPLEX16_11,  //Channel 11 of multiplex of 16
    CHAN_MULTIPLEX16_12,  //Channel 12 of multiplex of 16
    CHAN_MULTIPLEX16_13,  //Channel 13 of multiplex of 16
    CHAN_MULTIPLEX16_14,  //Channel 14 of multiplex of 16
    CHAN_MULTIPLEX16_15,  //Channel 15 of multiplex of 16
}TeChanTreated;

typedef enum
{
    MODE_SOUNDLEVELMETER,    // Only calculation of acoustic levels
    MODE_CALIBRATION,        // Perform calibration
    MODE_REVERBERATION,		// Calculation of reverberation time
    MODE_MULTISENSOR		// Calculation of rms and peak values for all sensors

}TeCalculMode;


//Configuration for real time mode acoustic measurement
//-----------------------------------------------------
struct TrAcousticMeasConfig
{
    TrAcousticMeasConfig()
    {
        eSampleFrequency = SAMPLE_FREQ_48K;
        eChanTreated = CHAN_STEREO_FULL;
        eSampleFormat = FLOAT_NORM;
        iIntegrationTimeSec = IT_DEFAULT;
        eFastIntegrationTime = FIT_200MS;
        eDurationBlock = BLOCK_200MS;
        eCalculMode = MODE_SOUNDLEVELMETER;
    };
    TeSampleFrequency eSampleFrequency;
    TrSensorParams rSensorParams;
    TrACZFilterConfig rACZFilterConfig;
    TrMultiSensorConfig rMultiSensorConfig;
    TrSpectrumConfig rSpectrumConfig;
    TrCalibrationConfig rCalibrationConfig;
    TrReverberationConfig rReverberationConfig;
    TeChanTreated eChanTreated;
    TeSampleFormat eSampleFormat;
    int iIntegrationTimeSec; // Integration time : integer  multiple of 1 second
    TeFastIntegrationTime eFastIntegrationTime; // Only 3 values allowed
    TeDurationBlock	eDurationBlock;// Only 1 value for the moment
    TeCalculMode eCalculMode; // calculation mode
};

//Configuration for replay of TR calculation
//---------------------------------------
#define SIZE_MAX_RECORD_DECREASE_FIT (int)((float)TIME_MAX_SECONDS_EXTENDED_RECORD_DECREASE *1000.0f/ (float)FIT_8MS)
struct TrReverberationRecalculateConfig
{
    TrReverberationRecalculateConfig()
    {
        iIndexFreqStart = INDEX_FREQ_100HZ_3RD_OCTAVE;
        iIndexFreqEnd = INDEX_FREQ_5KHZ_3RD_OCTAVE;
        fMinTrSec = 0;
        eFastIntegrationTime = FIT_8MS;
        memset((void*)&iStartFitTr[0], 0, NB_MAX_FREQ_TR * sizeof(int));
        memset((void*)&iEndFitTr[0], 0, NB_MAX_FREQ_TR * sizeof(int));
        memset((void*)&afFitDecrease[0], 0, NB_MAX_FREQ_TR * SIZE_MAX_RECORD_DECREASE_FIT * sizeof(float));
    };
    int iIndexFreqStart;
    int iIndexFreqEnd;
    float fMinTrSec;
    TeFastIntegrationTime eFastIntegrationTime;
    int iStartFitTr[NB_MAX_FREQ_TR]; //Index of first measure in FIT used for calculation
    int iEndFitTr[NB_MAX_FREQ_TR]; //Index of last measure in FIT unit used for calculation
    float afFitDecrease[NB_MAX_FREQ_TR][SIZE_MAX_RECORD_DECREASE_FIT];//Record of decrease of spectrum in FIT unit during 10s

};

//Configuration for replay of TR calculation for one frequency only
//-----------------------------------------------------------------
#define SIZE_MAX_RECORD_DECREASE_FIT (int)((float)TIME_MAX_SECONDS_EXTENDED_RECORD_DECREASE *1000.0f/ (float)FIT_8MS)
struct TrReverberationRecalculateOneFreqConfig
{
    TrReverberationRecalculateOneFreqConfig()
    {
        fMinTrSec = 0;
        iStartFitTr = 0;
        iEndFitTr = 0;
        eFastIntegrationTime = FIT_8MS;
        memset((void*)&afFitDecrease[0], 0, (SIZE_MAX_RECORD_DECREASE_FIT * sizeof(float)));
    };
    float fMinTrSec;
    int iStartFitTr; //Index of first measure in FIT used for calculation
    int iEndFitTr; //Index of last measure in FIT unit used for calculation
    TeFastIntegrationTime eFastIntegrationTime; // FIT if one day it chan,ges betwwen measurement
    float afFitDecrease[SIZE_MAX_RECORD_DECREASE_FIT];//Record of decrease of spectrum in FIT unit during 10s

};

//-----------------------------------------
//Configuration for direction determination
//-----------------------------------------
//!Definition of point (x, y, z)
typedef struct
{
    float         fX;
    float         fY;
    float         fZ;
}TrCoordinates;

#define NB_CHANNELS_ANTENNA_MAX 4
#define LENGTH_ARM 0.18f // Length Paul antenna
struct TrMicrophonePositionMeter
{

    TrMicrophonePositionMeter()
    {
        iNbChannels = NB_CHANNELS_ANTENNA_MAX;
        //Z1 horizontal microphone corresponding to north
        arChannel[0].fX = LENGTH_ARM;
        arChannel[0].fY = 0;
        arChannel[0].fZ = 0;
        //Z2
        arChannel[1].fX = -LENGTH_ARM * 0.5f;
        arChannel[1].fY = LENGTH_ARM * 0.866f;
        arChannel[1].fZ = 0;
        //Z3
        arChannel[2].fX = -LENGTH_ARM * 0.5f;
        arChannel[2].fY = -LENGTH_ARM * 0.866f;
        arChannel[2].fZ = 0;
        //Z4
        arChannel[3].fX = 0;
        arChannel[3].fY = 0;
        arChannel[3].fZ = LENGTH_ARM;
    };
    int			  iNbChannels;
    TrCoordinates arChannel[NB_CHANNELS_ANTENNA_MAX];
};

#define NB_MAX_PASS	3 // Number maximum of localizatio pass

struct TrDirectionConfig
{
    TrDirectionConfig()
    {
        eSampleFrequency = SAMPLE_FREQ_48K;
        iNbDecimationBy2 = 2; // So for 48K, the sampling frequency of the signal used for localization is 12KHz
        eLowPassFilter = LOWPASS_NONE;
        iSizeBlockMax = 9600;	// Size of input block max
        fSoundSpeed = 340.0f;
        fAzimuthStart = (float)-PI;
        fAzimuthEnd = (float)PI;
        fElevationStart = (float)-PI / 2.0f;
        fElevationEnd = (float)PI / 2.0f;
        afGridResolutionSteps[0] = 36; //5� resolution in azimuth and  elevation
        afGridResolutionSteps[1] = 180;//1� resolution in azimuth and  elevation
        afGridResolutionSteps[2] = 512;

        //Test 1------------------------------
        //Test with slice of 200ms and no overlap and one pass of localization only
        // Config used for all Lias tests
        fDurationSliceSec = 0.2f; // Size of input block
        fOverlapPercent = 0; // To have only one correlation of the signal
        iNumberOfSteps = 1;

        //Test 2 To have slice of 50ms and overlap of 25ms------------------------------
        //fDurationSliceSec = 0.05f; // Size of a slice
        //fOverlapPercent = 50.0f; // Overlap of signal for 2 slices
        //iNumberOfSteps = 1;

        /*aiNptsSaut[0] = 300; // Size of  step between 2 slices
        aiNptsSaut[1] = 2400; // To have only one correlation of the signal
        aiNptsSaut[2] = 2400; // To have only one correlation of the signal
        */
        aiFinesse[0] = 1; //Number of points on interpolation A SUPPRIMER
        aiFinesse[1] = 1;
        aiFinesse[2] = 1;
        fThresholdMaxVsMin = 1.5f;
        fThresholdMaxVsAvg = 1.5f;
        iInterpo = 0;
    };
    TrMicrophonePositionMeter rAntenna; //!< Coordinates of microphones of antenna
    TeSampleFrequency  eSampleFrequency;	//!< Sampling of input in Hz
    int			  iNbDecimationBy2; //!< Number of decimation by 2 of the signal
    TeLowPassFilter eLowPassFilter; //!< Filtering of inpout signal
    float		  iSizeBlockMax;	//!< Size max of 1 input block for localization
    float		  fSoundSpeed;
    float         fAzimuthStart;
    float         fElevationStart;
    float         fAzimuthEnd;
    float         fElevationEnd;
    int			  iNumberOfSteps; // Number of steps for the direction calculation
    float         afGridResolutionSteps[NB_MAX_PASS];//!< Grid resolution in steps relative to PI (Elevation) or 2*PI (Azimuth)
    float		  fDurationSliceSec;
    float         fOverlapPercent;
    int           aiFinesse[NB_MAX_PASS];
    float		  fThresholdMaxVsMin;
    float		  fThresholdMaxVsAvg;
#define INTERPOLATION_SHANNON 0
#define INTERPOLATION_POLYNOMIAL 3
    int           iInterpo;	//!< if = 0 : shannon, otherwise degree of polynomial (but only degre 3)

};

// ****************************************************************************
// Results of calculation returned by NewBlock
// ****************************************************************************
struct TrOneRateACZFilterRes
{
    TrOneRateACZFilterRes()
    {
        memset((void*)&afLeq[0], 0, SIZE_ACZ * sizeof(float));
        memset((void*)&afLpMaxACZ[0], 0, SIZE_ACZ * sizeof(float));
        memset((void*)&afLpACZ[0], 0, SIZE_ACZ * sizeof(float));
        memset((void*)&afPeakCZ[0], 0, SIZE_PEAK_CZ * sizeof(float));
    }
    float afLeq[SIZE_ACZ];
    float afPeakCZ[SIZE_PEAK_CZ];
    float afLpACZ[SIZE_ACZ];
    float afLpMaxACZ[SIZE_ACZ];
};

struct TrOneRateOctaveFilterRes
{
    TrOneRateOctaveFilterRes()
    {
        memset((void*)&afLeq[0], 0, NB_MAX_FREQ_SPECTRUM * sizeof(float));
    }
    float afLeq[NB_MAX_FREQ_SPECTRUM];
};

struct TrOneRateAcousticRes
{
    TrOneRateAcousticRes()
    {
        bOverload = false;
        iCountMeas = 0;
    }
    bool bOverload;
    int iCountMeas; //Number of measures sent
    TrOneRateACZFilterRes rACZRes;
    TrOneRateOctaveFilterRes rOctaveRes;
};

#define SIZE_MULTISENSOR_RES 2 // 2 RMS and peak value calculated for each sensor
struct TrOneRateMultiSensorRes
{
    TrOneRateMultiSensorRes()
    {
        bOverload = false;
        iCountMeas = 0;
        memset((void*)&afRmsMeas[0], 0, SIZE_MULTISENSOR_RES * sizeof(float));
        memset((void*)&afPeakMeas[0], 0, SIZE_MULTISENSOR_RES * sizeof(float));
    }
    bool bOverload;
    int iCountMeas; //Number of measures sent
    float afRmsMeas[SIZE_MULTISENSOR_RES];
    float afPeakMeas[SIZE_MULTISENSOR_RES];

};
#define SIZE_MAX_FFT (FFT_16384/2)
struct TrFftRes
{
    TrFftRes()
    {
        bFit = false;
        memset((void*)&afFitFft[0], 0, SIZE_MAX_FFT * sizeof(float));
        bIt = false;
        memset((void*)&afItFft[0], 0, SIZE_MAX_FFT * sizeof(float));
        bGlobal = false;
        memset((void*)&afGlobalFft[0], 0, SIZE_MAX_FFT * sizeof(float));
    }
    bool bFit;// fft calculated at the FIT rate
    float afFitFft[SIZE_MAX_FFT];
    bool bIt;// fft calculated at the IT rate
    float afItFft[SIZE_MAX_FFT];
    bool bGlobal;// fft global calculated at the IT rate available
    float afGlobalFft[SIZE_MAX_FFT];
};

//Definition of enum to Get FFt results
typedef enum {
    FFT_RESULT_FIT,			//!< Flag to get FFT results of last FIT
    FFT_RESULT_IT,          //!< Flag to get FFT results of last IT
    FFT_RESULT_GLB			//!< Flag to get FFT results of last global calculation
} TeTypeFftResult;

//Definition of structure for calibration result
typedef enum {
    CAL_NOT_RUNNING,			//!< Calibration not running
    SEARCHING_LEVEL,        //!< Search for level of calibration
    LOOK_FOR_STABILIZATION,  //!< Search calibration stabilized
    WAIT_FOR_STABILIZED,       //!< Level stabilized, wait for validation
    CALIBRATION_DONE,		//!< Calibration done
    TIMEOUT_CALIB			//!< Level not found or not validated after 1 minute
} TeStatusOfCalibration;

struct TrCalibrationRes
{
    TrCalibrationRes()
    {
        eStatusOfCalibration = CAL_NOT_RUNNING;
        iCountMeas = 0;
        fLeqCdB = 0;
        fCalibrationCorrectiondB = 0;
        fRealSensitivityVoltByPa = 0;
    }
    TeStatusOfCalibration eStatusOfCalibration;
    int iCountMeas; //Number of calibration results sent
    float fLeqCdB;
    float fCalibrationCorrectiondB; //calibration correction
    float fRealSensitivityVoltByPa;  // updated real sensitivity
};

//Definition of structure for reverberation time running result
typedef enum {
    STOP_TR,	//!< Reverberation time not running / 0
    READY,        //!< Reverberation time ready waiting StartTr / 1
    MEASURING_NOISE_LEVEL,        //!< measurement of noise level / 2
    WAITING_SOURCE,        //! < wait on of source / 3
    WAITING_SOURCE_STABILIZED, //! < wait source stabilized / 4
    MEASURING_SOURCE_LEVEL, //!< Measure of source level / 5
    WAITING_STOP_SOURCE, //!< Waiting for stop of source / 6
    SOURCE_DECREASE_RECORDING, //! Recirding decrease / 7
    TR_MEASURE, //!< Reverberation time calculated / 8
    //Error case
    NOISE_LEVEL_TOO_HIGH,  //!< Calculation impossible because level of noise is too high
    SOURCE_NOT_DETECTED,   //!< Source not detected
    SOURCE_NEVER_STOPPED,	//!< Permanent source is never stopped
} TeStatusOfReverberation;

//Define bit field for the different defaults
#define N_NOISE_TOO_HIGH			0x0001  // Noise level too high
#define D_T20_INSUFFICIENT_DYNAMIC	0x0002  // < 31dB
#define D_T30_INSUFFICIENT_DYNAMIC	0x0004	// < 41dB
#define D_T60_INSUFFICIENT_DYNAMIC	0x0008	// < 71dB
#define I_T20_TOO_LOW				0x0010  // T20 < threshold sigle '<'
#define I_T30_TOO_LOW				0x0020	// T30 < threshold sigle '<'
#define I_T60_TOO_LOW				0x0040	// T60 < threshold sigle '<'
#define KI_T20_NO_LINEARITY			0x0080	// Non linearity error fior T20 > 5/1000
#define KI_T30_NO_LINEARITY			0x0100	// Non linearity error fior T30 > 5/1000
#define KI_T60_NO_LINEARITY			0x0200	// Non linearity error fior T60 > 5/1000
#define CURVATURE_TOO_LOW			0x0400  // C < 0 so error of measurement in column T30
#define CURVATURE_TOO_HIGH			0x0800  // C > 10 so TR is not a line in column T30
#define I_TXX_TOO_LOW				0x1000  // Txx recalculated < threshold
#define KI_TXX_NO_LINEARITY			0x2000	// Non linearity error for TXX > 5/1000


/*RANGE_TOO_LOW_T20,       //!< Range too low  < 31dB for T20
RANGE_TOO_LOW_T30,       //!< Range too low < 41dB for T30
REVERBERATION_TIME_TOO_LOW,		//!< < 0.24s
NON_LINEARITY,		//!< khi >1%
CURVATURE_OUT_OF_RANGE,		//!< C < 0 or C >10%
SOURCE_LINEARITY_OUT_OF_RANGE,		//!< DIffrence between 2 neighbouring band > 6dB
*/
//Store in result of TR the whole 3rd octave spectrum + Leq A
#define NB_MAX_FREQ_TR (NB_MAX_FREQ_SPECTRUM + 1)
struct TrReverberationRes
{
    TrReverberationRes()
    {
        fCountSecNoise = 0;
        fCountSecSource = 0;
        fDeltaMaxMinNoise = 0;
        iCountMeasTr = 0;
        iIndexFirstFitRecordDecrease = 0;
        memset((void*)&afLeqNoisedB[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afLeqSourcedB[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afTr20Sec[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afTr30Sec[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afTr60Sec[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afKi20[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afKi30[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afKi60[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afCurvaturePercent[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&aiStartFitTr[0], 0, NB_MAX_FREQ_TR * sizeof(int));
        memset((void*)&afLevelStartFitTr20[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afLevelStartFitTr30[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afLevelStartFitTr60[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&aiEndFitTr20[0], 0, NB_MAX_FREQ_TR * sizeof(int));
        memset((void*)&aiEndFitTr30[0], 0, NB_MAX_FREQ_TR * sizeof(int));
        memset((void*)&aiEndFitTr60[0], 0, NB_MAX_FREQ_TR * sizeof(int));
        memset((void*)&auiFlagTr[0], 0, NB_MAX_FREQ_TR * sizeof(unsigned int));
        memset((void*)&afAvgTr20Sec[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afAvgTr30Sec[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afAvgTr60Sec[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afAvgTrSec[0], 0, NB_MAX_FREQ_TR * sizeof(float));
    }
    //! Counter of number of seconds for noise measurement and for level of source measurement
    float fCountSecNoise;
    float fCountSecSource;
    float fDeltaMaxMinNoise;
    int iIndexFirstFitRecordDecrease; //Index of first FIT used for record decrease in number of blocks since StartTr
    int iCountMeasTr; // number of Tr measured for one room
    float afLeqNoisedB[NB_MAX_FREQ_TR]; //Level of noise
    float afLeqSourcedB[NB_MAX_FREQ_TR]; //Level of the source for start of calculation
    float afTr20Sec[NB_MAX_FREQ_TR]; //Value of Tr20 in seconds (last value = average over frequency)
    float afTr30Sec[NB_MAX_FREQ_TR]; //Value of Tr30 in seconds (last value = average over frequency)
    float afTr60Sec[NB_MAX_FREQ_TR]; //Value of Tr60 in seconds (last value = average over frequency)
    float afKi20[NB_MAX_FREQ_TR]; //Linearity error for T20 for each frequency
    float afKi30[NB_MAX_FREQ_TR]; //Linearity error for T30 for each frequency
    float afKi60[NB_MAX_FREQ_TR]; //Linearity error for T60 for each frequencys
    float afCurvaturePercent[NB_MAX_FREQ_TR]; //Value of curvature in percentage
    int aiStartFitTr[NB_MAX_FREQ_TR]; //Index of first measure in FIT used for calculation in offst / iIndexFirstFitRecordDecrease
    float afLevelStartFitTr20[NB_MAX_FREQ_TR]; //Value of level (or offset) of the first sample used in LMS : used for line drawing
    float afLevelStartFitTr30[NB_MAX_FREQ_TR]; //Value of level (or offset) of the first sample used in LMS : used for line drawing
    float afLevelStartFitTr60[NB_MAX_FREQ_TR]; //Value of level (or offset) of the first sample used in LMS : used for line drawing
    int aiEndFitTr20[NB_MAX_FREQ_TR]; //Index of last measure in FIT unit used for calculation of Tr20 in offst / iIndexFirstFitRecordDecrease
    int aiEndFitTr30[NB_MAX_FREQ_TR]; //Index of last measure in FIT unit used for calculation of Tr30 in offst / iIndexFirstFitRecordDecrease
    int aiEndFitTr60[NB_MAX_FREQ_TR]; //Index of last measure in FIT unit used for calculation of Tr60 in offst / iIndexFirstFitRecordDecrease
    float afSlopeTr20[NB_MAX_FREQ_TR]; //Slope for Tr20 in dB/s
    float afSlopeTr30[NB_MAX_FREQ_TR]; //Slope for Tr30 in dB/s
    float afSlopeTr60[NB_MAX_FREQ_TR]; //Slope for Tr60 in dB/s
    unsigned int auiFlagTr[NB_MAX_FREQ_TR]; //Flag of error on TR depending of frequency
    float afAvgTr20Sec[NB_MAX_FREQ_TR]; //Value of Tr20 in seconds average on the six measurement (last value = average over frequency)
    float afAvgTr30Sec[NB_MAX_FREQ_TR]; //Value of Tr30 in seconds average on the six measurement (last value = average over frequency)
    float afAvgTr60Sec[NB_MAX_FREQ_TR]; //Value of Tr60 in seconds average on the six measurement (last value = average over frequency)
    float afAvgTrSec[NB_MAX_FREQ_TR]; //Average of T20/T30/T60 for each frequency (last value = average over frequency)
};

//Results for replay of Tr betwwen 2 index
struct TrReverberationRecalculateRes
{
    TrReverberationRecalculateRes()
    {
        memset((void*)&afTr20Sec[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afTr30Sec[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afTr60Sec[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afKi20[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afKi30[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&afKi60[0], 0, NB_MAX_FREQ_TR * sizeof(float));
        memset((void*)&auiFlagTr[0], 0, NB_MAX_FREQ_TR * sizeof(unsigned int));
    }
    float afTr20Sec[NB_MAX_FREQ_TR]; //Value of Tr calculated betwwen the 2 indes
    float afTr30Sec[NB_MAX_FREQ_TR]; //Value of Tr calculated betwwen the 2 indes
    float afTr60Sec[NB_MAX_FREQ_TR]; //Value of Tr calculated betwwen the 2 indes
    float afKi20[NB_MAX_FREQ_TR]; //Linearity error for Tr calculated
    float afKi30[NB_MAX_FREQ_TR]; //Linearity error for Tr calculated
    float afKi60[NB_MAX_FREQ_TR]; //Linearity error for Tr calculated
    float afCurvaturePercent[NB_MAX_FREQ_TR]; //Value of curvature in percentage
    unsigned int auiFlagTr[NB_MAX_FREQ_TR]; //Flag of error on TR depending of frequency
};

//Results for replay of Tr between 2 index for 1 frequency only
struct TrReverberationRecalculateOneFreqRes
{
    TrReverberationRecalculateOneFreqRes()
    {
        fTrxxSec = 0;
        fKixx = 0;
        fLevelStartFitTrxx = 0;
        uiFlagTr = 0;
        fSlopeTrxx = 0;
    }
    float fTrxxSec; //Value of Tr20 calculated betwwen the 2 index
    float fKixx; //Linearity error for Tr calculated
    float fLevelStartFitTrxx; // Level of start after LMS
    float fSlopeTrxx; // slope
    unsigned int uiFlagTr; //Flag of error on TR depending of frequency
};


//Definition of structure for short TR rese
struct TrStatusTrRes
{
    TrStatusTrRes()
    {
        eStatusOfReverberation = STOP_TR;
        iCountFitFromStartTr = 0;
    }
    TeStatusOfReverberation eStatusOfReverberation;
    int iCountFitFromStartTr; // Number of FIT treated since start of Tr
};

//Definition of information that there are other results that must be read
typedef enum
{
    NONE,//No other indicators calculated
    REVERBERATION_TIME // Reverberation time resultsFTr

}TeOtherAcousticRes;

//Definition of structure for other AcousticRes
struct TrOtherAcousticRes
{
    TrOtherAcousticRes()
    {
        eOtherAcousticRes = NONE;
    }
    TeOtherAcousticRes eOtherAcousticRes;
};

//Definition of results at different rate returned by NewBlock
//------------------------------------------------------------
// For acoustic measurement anf for multi sensor measurement
/*struct TrAcousticRes
{
    TrAcousticRes()
    {
        iCountGlobal = 0;
        iCountFit = 0;
        bIt = false;
        bCalib = false;
        bOtherAcousticRes = false;
        bFft = false;
    }
    int iCountGlobal; // number of global result available
    int iCountFit;	// Number of FIT results
    bool bIt;	// true if integration time result available
    bool bCalib; //true il calibration result available . For the moment, always at the same time as it results
    bool bOtherAcousticRes;	// true if other acoustic res to read
    bool bFft; // Results of FFT to read
    TrOneRateAcousticRes rGlobal[NB_MAX_FAST_IT_IN_1_BLOCK]; // Structure of results at global rate / one result every FIT
    TrOneRateAcousticRes rFit[NB_MAX_FAST_IT_IN_1_BLOCK]; // Structure of results at FIT rate
    TrOneRateAcousticRes rIt; // Structure of results at IT rate
    TrOneRateMultiSensorRes rGlobalMultiSensor[NB_MAX_FAST_IT_IN_1_BLOCK]; // Structure of results at global rate / one result every FIT
    TrOneRateMultiSensorRes rFitMultiSensor[NB_MAX_FAST_IT_IN_1_BLOCK]; // Structure of results at FIT rate
    TrOneRateMultiSensorRes rItMultiSensor; // Structure of results at IT rate
    TrCalibrationRes rCalibrationRes; // calibration results
    TrStatusTrRes	rStatusTrRes; // short results for status of TR
    TrOtherAcousticRes rOtherAcousticRes; //Other acousticRes that must be read
    TrFftRes	rFFtRes; // Results of FFT calculation every FIT
};*/

// For acoustic measurement anf for multi sensor measurement
struct TrAcousticResBasis
{
    TrAcousticResBasis()
    {
        iCountGlobal = 0;
        iCountFit = 0;
        bIt = false;
        bCalib = false;
        bOtherAcousticRes = false;
        bFft = false;
    }
    int iCountGlobal; // number of global result available
    int iCountFit;	// Number of FIT results
    bool bIt;	// true if integration time result available
    bool bCalib; //true il calibration result available . For the moment, always at the same time as it results
    bool bOtherAcousticRes;	// true if other acoustic res to read
    bool bFft; // Results of FFT to read
    TrOneRateAcousticRes rGlobal[NB_MAX_FAST_IT_IN_1_BLOCK]; // Structure of results at global rate / one result every FIT
    TrOneRateAcousticRes rFit[NB_MAX_FAST_IT_IN_1_BLOCK]; // Structure of results at FIT rate
    TrOneRateAcousticRes rIt; // Structure of results at IT rate
    TrOneRateMultiSensorRes rGlobalMultiSensor[NB_MAX_FAST_IT_IN_1_BLOCK]; // Structure of results at global rate / one result every FIT
    TrOneRateMultiSensorRes rFitMultiSensor[NB_MAX_FAST_IT_IN_1_BLOCK]; // Structure of results at FIT rate
    TrOneRateMultiSensorRes rItMultiSensor; // Structure of results at IT rate
    TrCalibrationRes rCalibrationRes; // calibration results
    TrStatusTrRes	rStatusTrRes; // short results for status of TR
    TrOtherAcousticRes rOtherAcousticRes; //Other acousticRes that must be read
};

//Definition of results for one step of localization
//--------------------------------------------------
typedef struct
{
    //! Elevation and azimuth
    float fElevationDegree;
    float fAzimuthDegree;
    //! Status of valid position
    bool bValidPosition;
    //! Energy max of sum of cross correlation, also named : likelihood
    float fMaxLikelihood;
    //! Energy min of sum of cross correlation
    float fMinLikelihood;
    //! Average energy without the maximum point
    float fAvgLikelihood;
    //! Index of start of localisation
    float fDateStartSec;
    //! Number of pass of localization
    int iIndexPass;
} TrOneDirectionResult;
#define NB_MAX_DETAILED_DIRECTION 16
typedef struct
{
    TrOneDirectionResult BestDirectionResult;
    TrOneDirectionResult DetailedDirectionResult[NB_MAX_DETAILED_DIRECTION];
    int iNbDetailedResult;
} TrDirectionResult;

//Definition of coefficients for LOWPASS
#define NB_CELL_ORDER2_LOWPASS_FILTER 2

//Coefficients must be stored in the following order
//Number of order 2 cells ex:2 for an order 4 filter
//Then b12, b11, b10, -a12, -a11, b22, b21, b20, -a22, -a21 ....
//For a new imput sample x(n) we apply following formula, nb cells order 2 times :
//y(n) = b0*x(n) + b1*x(n-1) + b2*x(n-2) -a1*y(n-1) -a2*y(n-2)

// Filter low pass freq=  125.00 FS=48000 order=4
const float afCoefFilterLP_125_butter[2 * 5 + 1] = { 2
 , 0.000000017542356, 0.000000035084713, 0.000000017542356,-0.970217660023835, 1.969953922572107
, 0.250000402072902, 0.500000788521145, 0.250000398453288,-0.987555194122764, 1.987289135828422
};

// Filter low pass freq=  250.00 FS=48000 order=4
const float afCoefFilterLP_250_butter[2 * 5 + 1] = { 2
 , 0.000000274811984, 0.000000549623986, 0.000000274811988,-0.941316919111412, 1.940277513721573
, 0.250002167469839, 0.500004319314908, 0.250002163850199,-0.975267574111935, 1.974209991114771
};

// Filter low pass freq=  500.00 FS=48000 order=4
const float afCoefFilterLP_500_butter[2 * 5 + 1] = {2
 , 0.000004217162479, 0.000008434325222, 0.000004217162540,-0.886036948316172, 1.881998798434950
, 0.250016302623547, 0.500032589621441, 0.250016299003702,-0.951164891035905, 1.946987297230067
};

//generated coefficients on 07-May-2021
// Filter low pass freq= 1000.00 FS=48000 order=4
const float afCoefFilterLP_1000_butter[2 * 5 + 1] = { 2
 , 0.000062181138096, 0.000124362280079, 0.000062181138997,-0.784773331782916, 1.769504348513282
, 0.250103524268303, 0.500207032905502, 0.250103520647195,-0.904852228768163, 1.888555953888603
};

//generated file third octave coefficients on 07-May-2021
// Filter low pass freq= 2000.00 FS=48000 order=4
const float afCoefFilterLP_2000_butter[2 * 5 + 1] = { 2
 , 0.000850286529360, 0.001700573111862, 0.000850286541671,-0.614051781937975, 1.559054301141843
, 0.250666945336069, 0.501333875005822, 0.250666941706805,-0.819760442927188, 1.757753609482574
};

// Filter low pass freq= 4000.00 FS=48000 order=4
const float afCoefFilterLP_4000_butter[2 * 5 + 1] = { 2
 , 0.010126745894822, 0.020253492422550, 0.010126746041441,-0.368045418945308, 1.184762086337570
, 0.254418788827324, 0.508837561753847, 0.254418785143739,-0.678779457508339, 1.453865657553675
};

// Filter low pass freq= 8000.00 FS=48000 order=4
const float afCoefFilterLP_8000_butter[2 * 5 + 1] = { 2
 , 0.092079174371377, 0.184158354497567, 0.092079175704539,-0.111047444888218, 0.555523722444110
, 0.283209768974632, 0.566419520249068, 0.283209764874198,-0.502162843313881, 0.751081421656940
};
#endif
