/**
*******************************************************************************
** @file    wavfile.cpp
** @version
** @author  Wevioo SE Team
** @brief   records .wav files from the input audio data
*******************************************************************************
**/

#include "WavFile.h"
#include "AudioGet/AudioGet.h"
#include <QtCore>
#include <QFile>
#include <QIODevice>
#include <QByteArray>
#include <QDataStream>
#include <qendian.h>

/**
*******************************************************************************
** @fn  WavFile::WavFile(QObject *parent)
** @param[inout] QObject *parent
** @brief IConstructor of the WavFile class
** @return
**
*******************************************************************************
**/
WavFile::WavFile(QObject *parent) : QObject(parent),
  m_cSNValue("0019"), m_cINAM("0e+0  "), m_cIART("5e-2  "),
  m_cIGNR("5e+0  "), m_cICMT("1e+2  "),
  m_cIPRD ("Mto be defined by the user  "),
  m_llLastDataPosition(0)
{

}

/**
*******************************************************************************
** @fn  void WavFile::Initialize()
** @param[in] void
** @param[out] void
** @brief Initializes wave file chunks
** @return void
**
*******************************************************************************
**/
void WavFile::Initialize(){

  /************************* RIFF Header *************************/
  rWaveFrame.rFileHeader.uliID=ID_RIFF;
  rWaveFrame.rFileHeader.uliLen=0;
  rWaveFrame.rFileHeader.uliFormat=ID_WAVE;
  /************************* fmt chunk ***************************/
  rWaveFrame.rFormatHeader.uliID=ID_FMT;
  rWaveFrame.rFormatHeader.uliLen=0x10000000;
  rWaveFrame.rFormatHeader.uiFormat=0x0300;                         /*!< AudioFormat=3 for Float */
  rWaveFrame.rFormatHeader.uiChannels=0x0100;                       /*!< Number of channels mono =1 */
  rWaveFrame.rFormatHeader.uliSampRate=0x80BB0000;                  /*!< for a sample rate of 48kHz */
  rWaveFrame.rFormatHeader.uliByteRate=0x00ee0200;                  /*!< Byte rate = SampleRate * NumChannels * BitsPerSample/8*/
  rWaveFrame.rFormatHeader.uiBlockAlign=0x0400;                     /*!< The number of bytes for one sample == NumChannels * BitsPerSample/8 */
  rWaveFrame.rFormatHeader.uiBitsPerSamp=0x2000;                    /*!< 32 bits */
  /************************* Data Chunk **************************/
  rWaveFrame.rDataHeader.uliID=ID_DATA;                             /*!< "data" big-endian */
  rWaveFrame.rDataHeader.uliLen=0;
  /************************* Info-List Chunk *********************/
  rWaveFrame.rListHeader.uliID=ID_LIST;
  rWaveFrame.rListHeader.rIART.uliID=ID_IART;
  rWaveFrame.rListHeader.rIART.cData=GetArtist();
  rWaveFrame.rListHeader.rIPRD.uliID=ID_IPRD;
  rWaveFrame.rListHeader.rIPRD.cData=GetProduct();
  rWaveFrame.rListHeader.rINAM.uliID=ID_INAM;
  rWaveFrame.rListHeader.rINAM.cData=GetTitle();
  rWaveFrame.rListHeader.rIGNR.uliID=ID_IGNR;
  rWaveFrame.rListHeader.rIGNR.cData=GetGenre();
  rWaveFrame.rListHeader.rICMT.uliID=ID_ICMT;
  rWaveFrame.rListHeader.rICMT.cData=GetCmnt();
}

/**
*******************************************************************************
** @fn  bool WavFile::Open()
** @param[in] void
** @param[out] void
** @brief Opens the wave file
** @return bool: returns 0 upon success else error closing file
**
*******************************************************************************
**/
bool WavFile::Open(QString cSerialNumber, QString cFileName, TrListHeader rListHeader){ //PatchBetaDynamMetadata
      QAndroidJniObject path;
      m_cSNValue = cSerialNumber.mid(cSerialNumber.indexOf('#',0,Qt::CaseInsensitive)+1,5);

          path = QAndroidJniObject::callStaticObjectMethod("android.os.Environment",
                                                                             "getExternalStorageDirectory",
                                                                             "()Ljava/io/File;");

      if(!cFileName.isEmpty()){
        m_file.setFileName(path.toString()+ QString("/AllianTech/ATomicSound/Projects/ENV/WAV/")
                           + cFileName + QString("_")
                           + QString("ATomic1_SN")
                           + QString(m_cSNValue)
                           + QString("_")
                           + QDateTime::currentDateTime().toString("yyMMdd_hhmmss")
                           + QString(".wav"));
      }
      else{
          m_file.setFileName(path.toString()+ QString("/AllianTech/ATomicSound/Projects/ENV/WAV/")
                             + QString("ATomic1_SN")
                             + QString(m_cSNValue)
                             + QString("_")
                             + QDateTime::currentDateTime().toString("yyMMdd_hhmmss")
                             + QString(".wav"));
        }

  setFilename(m_file.fileName());
  m_file.open(QIODevice::WriteOnly);

  m_out=new QDataStream(&m_file);
  m_out->setFloatingPointPrecision(QDataStream::SinglePrecision);
  m_out->setByteOrder(QDataStream::LittleEndian);

  //PatchBetaDynamMetadata
  qDebug("***** UPDATE METADATA *****");
  SetProduct(rListHeader.rIPRD.cData);
  SetArtist(rListHeader.rIART.cData);
  SetTitle(rListHeader.rINAM.cData);
  SetCmnt(rListHeader.rICMT.cData);
  SetGenre(rListHeader.rIGNR.cData);

  m_file.write(WriteHeader());
  m_file.write(WriteDataHeader(0));

  m_file.flush();
  m_file.seek(m_file.pos());

  m_llLastDataPosition = 44; // = 44 after "data" tag + length

  return(!m_file.isOpen());
}

/**
*******************************************************************************
** @fn  bool WavFile::Close()
** @param[in] void
** @param[out] void
** @brief Closes the wave file
** @return bool: returns 0 upon success else error closing file
**
*******************************************************************************
**/
bool WavFile::Close()
{
  quint32 ListSize=0;
  ListSize=static_cast<quint32>(WriteList().size());

  quint32 sizeContentRiff = static_cast<quint32>(m_file.size());

  //Check if size is EVEN
  if (sizeContentRiff % 2 != 0)
    {
      sizeContentRiff += 1;
    }

  //Update RIFF size
  rWaveFrame.rFileHeader.uliLen=static_cast<quint32>(sizeContentRiff);
  m_file.seek(4); // after "RIFF" Tag
  *m_out<<static_cast<quint32>(rWaveFrame.rFileHeader.uliLen - 8);

  //Update WAV size
  rWaveFrame.rDataHeader.uliLen=static_cast<quint32>(sizeContentRiff-8-36-ListSize);
  m_file.seek(40); // after "data" tag
  *m_out<<static_cast<quint32>(rWaveFrame.rDataHeader.uliLen);

  m_file.flush();

  m_file.close();
  return(m_file.isOpen());

}

/**
*******************************************************************************
** @fn  void WavFile::WriteFloat(QList<double> data)
** @param[in] QList<double> data  audio data from the recorder (ATomic)
** @param[out] qint64
** @brief writes float data into the wave file
** @return the size of the file
**
*******************************************************************************
**/
qint64 WavFile::WriteFloat(QList<double> data){

  if(!data.isEmpty())
    {
      m_file.seek(m_llLastDataPosition);
      for(int i=0; i<data.size()-1;i++)
        {
          *m_out << data.at(i);
        }
      m_llLastDataPosition = m_file.pos();
      m_file.write(WriteList());

      //The metadata is still not present in the file but it will be added at the end
      //so it has to be take into accound for the resulting size
      quint32 ListSize=0;
      ListSize=static_cast<quint32>(WriteList().size());

      //The metadata is still not present in the file but it will be added at the end
      //so it has to be take into accound for the resulting size
      quint32 sizeContentRiff = static_cast<quint32>(m_file.size());

      //Check if size is EVEN
      if (sizeContentRiff % 2 != 0)
        {
          sizeContentRiff += 1;
        }

      //Update RIFF size
      rWaveFrame.rFileHeader.uliLen=static_cast<quint32>(sizeContentRiff);
      m_file.seek(4); // after "RIFF" Tag
      *m_out<<static_cast<quint32>(rWaveFrame.rFileHeader.uliLen - 8);

      //Update WAV size
      rWaveFrame.rDataHeader.uliLen=static_cast<quint32>(sizeContentRiff-8-36-ListSize);
      m_file.seek(40); // after "data" tag
      *m_out<<static_cast<quint32>(rWaveFrame.rDataHeader.uliLen);

      m_file.flush();
    }
  return m_file.size();
}

/**
*******************************************************************************
** @fn  QByteArray WavFile::WriteHeader()
** @param[in] void
** @param[out] QByteArray
** @brief writes float data into the wave file
**        writes the RIFF and fmt_ subchunks into a byte array
** @return the RIFF header byte array
**
*******************************************************************************
**/
QByteArray WavFile::WriteHeader(){
  QByteArray headerData;
  headerData.clear();

  QDataStream out(&headerData, QIODevice::WriteOnly);
  out<<static_cast<quint32>(rWaveFrame.rFileHeader.uliID)
      <<static_cast<quint32>(qToBigEndian(rWaveFrame.rFileHeader.uliLen))
      <<static_cast<quint32>(rWaveFrame.rFileHeader.uliFormat)
      <<static_cast<quint32>(rWaveFrame.rFormatHeader.uliID)
      <<static_cast<quint32>(rWaveFrame.rFormatHeader.uliLen)
      <<static_cast<quint16>(rWaveFrame.rFormatHeader.uiFormat)
      <<static_cast<quint16>(rWaveFrame.rFormatHeader.uiChannels)
      <<static_cast<quint32>(rWaveFrame.rFormatHeader.uliSampRate)
      <<static_cast<quint32>(rWaveFrame.rFormatHeader.uliByteRate)
      <<static_cast<quint16>(rWaveFrame.rFormatHeader.uiBlockAlign)
      <<static_cast<quint16>(rWaveFrame.rFormatHeader.uiBitsPerSamp);

  return headerData;
}

/**
*******************************************************************************
** @fn  QByteArray WavFile::WriteDataHeader(qint64 )
** @param[in] qint64 lliDataSize : Size of data
** @param[out] QByteArray
** @brief writes the data-header subchunk into a byte array
** @return the data header byte array
**
*******************************************************************************
**/
QByteArray WavFile::WriteDataHeader(qint64 lliDatasize){
  QByteArray headerData;
  headerData.clear();
  rWaveFrame.rDataHeader.uliLen=static_cast<quint32>(lliDatasize);

  QDataStream out(&headerData, QIODevice::WriteOnly);
  out<<static_cast<quint32>(rWaveFrame.rDataHeader.uliID);
  out<<static_cast<quint32>(qToBigEndian(rWaveFrame.rDataHeader.uliLen) & 0xffffffff);

  return headerData;
}

/**
*******************************************************************************
** @fn  QByteArray WavFile::WriteList()
** @param[in] void
** @param[out] QByteArray
** @brief writes List-subchunk data into a byte array
** @return the list-subchunk-byte array
**
*******************************************************************************
**/
QByteArray WavFile::WriteList()
{
  QByteArray ListData;
  ListData.clear();
  qint32 lisize;

  //Update Values
  rWaveFrame.rListHeader.rIART.cData=GetArtist();
  rWaveFrame.rListHeader.rIPRD.cData=GetProduct();
  rWaveFrame.rListHeader.rINAM.cData=GetTitle();
  rWaveFrame.rListHeader.rIGNR.cData=GetGenre();
  rWaveFrame.rListHeader.rICMT.cData=GetCmnt();

  qint32 lilen_IPRD, lilen_INAM, lilen_IART, lilen_IGNR, lilen_ICMT;
  lilen_IPRD=static_cast<qint32>(rWaveFrame.rListHeader.rIPRD.cData.length());
  lilen_INAM=static_cast<qint32>(rWaveFrame.rListHeader.rINAM.cData.length());
  lilen_IART=static_cast<qint32>(rWaveFrame.rListHeader.rIART.cData.length());
  lilen_IGNR=static_cast<qint32>(rWaveFrame.rListHeader.rIGNR.cData.length());
  lilen_ICMT=static_cast<qint32>(rWaveFrame.rListHeader.rICMT.cData.length());

  rWaveFrame.rListHeader.rIPRD.cData.resize(lilen_IPRD);
  rWaveFrame.rListHeader.rINAM.cData.resize(lilen_INAM);
  rWaveFrame.rListHeader.rIART.cData.resize(lilen_IART);
  rWaveFrame.rListHeader.rIGNR.cData.resize(lilen_IGNR);
  rWaveFrame.rListHeader.rICMT.cData.resize(lilen_ICMT);
  lisize=lilen_IPRD+lilen_INAM+lilen_IART+lilen_IGNR+lilen_ICMT+20+8+8+8+8;
  rWaveFrame.rListHeader.uliSize=static_cast<quint32>(lisize-8);

  QDataStream out(&ListData,QIODevice::WriteOnly);
  out <<static_cast<quint32>(rWaveFrame.rListHeader.uliID)
      <<static_cast<quint32>(qToBigEndian(rWaveFrame.rListHeader.uliSize))
      <<static_cast<quint32>(ID_INFO)
      <<static_cast<quint32>(rWaveFrame.rListHeader.rIPRD.uliID)
      <<static_cast<quint32>(qToBigEndian(lilen_IPRD))
      <<rWaveFrame.rListHeader.rIPRD.cData.toLocal8Bit()
      <<static_cast<quint32>(rWaveFrame.rListHeader.rINAM.uliID)
      <<static_cast<quint32>(qToBigEndian(lilen_INAM))
      <<rWaveFrame.rListHeader.rINAM.cData.toLocal8Bit()
      <<static_cast<quint32>(rWaveFrame.rListHeader.rIART.uliID)
      <<static_cast<quint32>(qToBigEndian(lilen_IART))
      <<rWaveFrame.rListHeader.rIART.cData.toLocal8Bit()
      <<static_cast<quint32>(rWaveFrame.rListHeader.rIGNR.uliID)
      <<static_cast<quint32>(qToBigEndian(lilen_IGNR))
      <<rWaveFrame.rListHeader.rIGNR.cData.toLocal8Bit()
      <<static_cast<quint32>(rWaveFrame.rListHeader.rICMT.uliID)
      <<static_cast<quint32>(qToBigEndian(lilen_ICMT))
      <<rWaveFrame.rListHeader.rICMT.cData.toLocal8Bit();

  ListData.remove(20,4);
  ListData.remove(20+lilen_IPRD+8,4);
  ListData.remove(20+lilen_IPRD+8+lilen_INAM+8,4);
  ListData.remove(20+lilen_IPRD+8+lilen_INAM+8+lilen_IART+8,4);
  ListData.remove(20+lilen_IPRD+8+lilen_INAM+8+lilen_IART+8+lilen_IGNR+8,4);
  return ListData;
}

/**
*******************************************************************************
** @fn  void WavFile::SetTitle(QString cAttr)
** @param[in] QString cAttr : title string value
** @param[out] void
** @brief sets the title string
** @return void
**
*******************************************************************************
**/
void WavFile::SetTitle(QString cAttr)
{
  if(cAttr.length()%2)
    cAttr.append(" ");
  else cAttr.append("  ");

  m_cINAM=cAttr;
  emit TitleChanged();
}

/**
*******************************************************************************
** @fn  void WavFile::SetArtist(QString cAttr)
** @param[in] QString cAttr : Artist string value
** @param[out] void
** @brief sets the artist string
** @return void
**
*******************************************************************************
**/
void WavFile::SetArtist(QString cAttr)
{
  if(cAttr.length()%2)
    cAttr.append(" ");
  else cAttr.append("  ");

  m_cIART=cAttr;
  emit ArtistChanged();
}

/**
*******************************************************************************
** @fn  void WavFile::SetGenre(QString cAttr)
** @param[in] QString cAttr : genre string value
** @param[out] void
** @brief sets the genre string
** @return void
**
*******************************************************************************
**/
void WavFile::SetGenre(QString cAttr)
{if(cAttr.length()%2)
    cAttr.append(" ");
  else cAttr.append("  ");

  m_cIGNR=cAttr;
  emit GenreChanged();
}

/**
*******************************************************************************
** @fn  void WavFile::SetCmnt(QString cAttr)
** @param[in] QString cAttr : comment string value
** @param[out] void
** @brief sets the comment string
** @return void
**
*******************************************************************************
**/
void WavFile::SetCmnt(QString cAttr)
{
  if(cAttr.length()%2)
    cAttr.append(" ");
  else cAttr.append("  ");

  m_cICMT=cAttr;
  emit CmntChanged();
}

/**
*******************************************************************************
** @fn  void WavFile::SetProduct(QString cAttr)
** @param[in] QString cAttr : product string value
** @param[out] void
** @brief sets the product string
** @return void
**
*******************************************************************************
**/
void WavFile::SetProduct(QString cAttr)
{
  if(cAttr.length()%2)
    cAttr.append(" ");
  else cAttr.append("  ");

  m_cIPRD=cAttr;
  emit ProductChanged();
}

/**
*******************************************************************************
** @fn  QString WavFile::GetTitle()
** @param[in] void
** @param[out] QString title string value
** @brief gets the title string
** @return QString
**
*******************************************************************************
**/
QString WavFile::GetTitle()
{
  return m_cINAM;
}

/**
*******************************************************************************
** @fn  QString WavFile::GetArtist()
** @param[in] void
** @param[out] QString artist string value
** @brief gets the artist string
** @return QString
**
*******************************************************************************
**/
QString WavFile::GetArtist()
{
  return m_cIART;
}

/**
*******************************************************************************
** @fn  QString WavFile::GetGenre()
** @param[in] void
** @param[out] QString genre string value
** @brief gets the genre string
** @return QString
**
*******************************************************************************
**/
QString WavFile::GetGenre()
{
  return m_cIGNR;
}

/**
*******************************************************************************
** @fn  QString WavFile::GetCmnt()
** @param[in] void
** @param[out] QString comment string value
** @brief gets the comment string
** @return QString
**
*******************************************************************************
**/
QString WavFile::GetCmnt()
{
  return m_cICMT;
}

/**
*******************************************************************************
** @fn  QString WavFile::GetProduct()
** @param[in] void
** @param[out] QString product string value
** @brief gets the product string
** @return QString
**
*******************************************************************************
**/
QString WavFile::GetProduct()
{
  return m_cIPRD;
}

/**
*******************************************************************************
** @fn  void WavFile::setFilename()
** @param[in] QString filename string value
** @param[out] void
** @brief sets the file name of the wave file
** @return void
**
*******************************************************************************
**/
void WavFile::setFilename(QString cfileName)
{
  m_cFilename=cfileName;
  emit fileNameChanged();
}

/**
*******************************************************************************
** @fn  QString WavFile::getFilename()
** @param[in] void
** @param[out] QString filename string value
** @brief gets the filename string
** @return QString
**
*******************************************************************************
**/
QString WavFile::getFilename()
{
  return m_cFilename;
}
