/**
*******************************************************************************
** @file    wavfile.h
** @version
** @author  Wevioo SE Team
** @brief   records .wav files from the input audio data
**
*******************************************************************************
**/

#ifndef WAVFILE_H
#define WAVFILE_H

#include <QtCore/QObject>
#include <QByteArray>
#include <QFile>
#include<QBuffer>


#define ID_RIFF 0x52494646  /* 'RIFF' */
#define ID_WAVE 0x57415645  /* 'WAVE' */
#define ID_FMT  0x666D7420  /* 'fmt ' */
#define ID_DATA 0x64617461  /* 'data' */
#define ID_LIST 0x4c495354  /* 'LIST' */
#define ID_INFO 0x494e464f  /* 'INFO' */
#define ID_INAM 0x494e414d  /* 'INAM' */
#define ID_IPRD 0x49505244  /* 'IPRD' */
#define ID_IART 0x49415254  /* 'IART' */
#define ID_ICMT 0x49434d54  /* 'ICMT' */
#define ID_IGNR 0x49474e52  /* 'IGNR' */

#define MAX_WAVE_SIZE qPow(2,30)             /*!< Maximum wave file size to be stored = 1GB */
/**
*******************************************************************************
** @struct  TrFileHeader
** @brief definition of the RIFF header
**
*******************************************************************************
**/
typedef struct {
  quint32 uliID;               /*!< FILE ID "RIFF" */
  quint32 uliLen;              /*!< file length (excluding 8-byte header) */
  quint32 uliFormat;           /*!< Data type 'WAVE' (start of main file) */
} TrFileHeader; /*!< RIFF HEADER 4+4+4 = 12bytes*/


/**
*******************************************************************************
** @struct  TrFormatHeader
** @brief definition of the Format-subchunk structure
**
*******************************************************************************
**/
typedef struct {
  quint32 uliID;               /*!< chunk ID "FMT " */
  quint32 uliLen;              /*!< chunk length */
  quint16 uiFormat;            /*!< format (1 if uncompressed) */
  quint16 uiChannels;          /*!< # of channels (mono, stereo, etc) */
  quint32 uliSampRate;         /*!< # of samples per second */
  quint32 uliByteRate;         /*!< == SampleRate * NumChannels * BitsPerSample/8 */
  quint16 uiBlockAlign;        /*!< == NumChannels * BitsPerSample/8 */
  quint16 uiBitsPerSamp;       /*!< bits per sample */
} TrFormatHeader; /*!< fmt subchunk: describes the sound data's format 4+4+2+2+4+4+2+2 = 24bytes*/

/**
*******************************************************************************
** @struct  TrDataHeader
** @brief definition of data-header-subchunk structure
**
*******************************************************************************
**/
typedef struct {
  quint32 uliID;               /*!< chunk ID "DATA" */
  quint32 uliLen;              /*!< == NumSamples * NumChannels * BitsPerSample/8
                                   This is the number of bytes in the data */
} TrDataHeader; /*!< data subchunk: contains the size of the data and the actual sound 4+4+x=8+x */


/**
*******************************************************************************
** @struct  TrListStruct
** @brief definition of List's data structure
**
*******************************************************************************
**/
typedef struct {
  quint32 uliID;               /*!< chunk ID : information tag */
  quint32 ulisize;             /*!< chunk size */
  QString cData;               /*!< chunk's data */
}TrListStruct;  /*!< LIST subchunk: contains different information about the recorded sound Size: 8+x bytes */

/**
*******************************************************************************
** @struct  TrListHeader
** @brief definition of the List subchunk structure
**
*******************************************************************************
**/
typedef struct {
  quint32 uliID;               /*! chunk ID "LIST" */
  quint32 uliSize;
  TrListStruct rINAM;          /*!< info track file the name of the data stored in the file */
  TrListStruct rIPRD;          /*!< info album title */
  TrListStruct rIART;          /*!< info artist */
  TrListStruct rICMT;          /*!< info Comment */
  TrListStruct rIGNR;          /*!< info Genre */
} TrListHeader; /*!< List_Info subchunck: contains the info of the audio 4+4+8+x+8+x+8+x+8+x+8+x=48+xbytes */

/**
*******************************************************************************
** @struct  TrWaveFrame
** @brief definition of the Wave Frame structure
**
*******************************************************************************
**/
typedef struct{
  TrFileHeader rFileHeader;       /*!< RIFF subchunks structure */
  TrFormatHeader rFormatHeader;   /*!< format subchunks structure */
  TrListHeader rListHeader;       /*!< List subchunks structure */
  TrDataHeader rDataHeader;       /*!< data subchunks */
}TrWaveFrame;      /*!< Source: http://soundfile.sapp.org/doc/WaveFormat/ */

/**
*******************************************************************************
** @class  WavFile
** @brief definition of the wave file class:
**
*******************************************************************************
**/
class WavFile : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString fileName READ getFilename WRITE setFilename NOTIFY fileNameChanged)

  Q_PROPERTY(QString title READ GetTitle WRITE SetTitle NOTIFY TitleChanged)
  Q_PROPERTY(QString artist READ GetArtist WRITE SetArtist NOTIFY ArtistChanged)
  Q_PROPERTY(QString genre READ GetGenre WRITE SetGenre NOTIFY GenreChanged)
  Q_PROPERTY(QString comment READ GetCmnt WRITE SetCmnt NOTIFY CmntChanged)
  Q_PROPERTY(QString product READ GetProduct WRITE SetProduct NOTIFY ProductChanged)
public:
  /**
  *******************************************************************************
  ** @fn  WavFile(QObject *parent)
  ** @param[inout] QObject *parent
  ** @brief IConstructor of the WavFile class
  ** @return
  **
  *******************************************************************************
  **/
  explicit WavFile(QObject *parent = nullptr);     /*!< Constructor of the WavFile class */

  /**
  *******************************************************************************
  ** @fn  void Initialize()
  ** @param[in] void
  ** @param[out] void
  ** @brief Initializes wave file chunks
  ** @return void
  **
  *******************************************************************************
  **/
  void Initialize();

  /**
  *******************************************************************************
  ** @fn  QString getFilename()
  ** @param[in] void
  ** @param[out] QString filename string value
  ** @brief gets the filename string
  ** @return QString
  **
  *******************************************************************************
  **/
  QString getFilename();                           /*!< gets the file's name */

  /**
  *******************************************************************************
  ** @fn  bool WavFile::Close()
  ** @param[in] void
  ** @param[out] void
  ** @brief Closes the wave file
  ** @return bool: returns 0 upon success else error closing file
  **
  *******************************************************************************
  **/
  bool Close();                                     /*!< Closes the file */

  /**
  *******************************************************************************
  ** @fn  bool Open()
  ** @param[in] void
  ** @param[out] void
  ** @brief Opens the wave file
  ** @return bool: returns 0 upon success else error closing file
  **
  *******************************************************************************
  **/
  bool Open(QString ,QString, TrListHeader);//PatchBetaDynamMetadata

  /**
  *******************************************************************************
  ** @fn  void WriteFloat(QList<double> data)
  ** @param[in] QList<double> data  audio data from the recorder (ATomic)
  ** @param[out] qint64
  ** @brief writes float data into the wave file
  ** @return the size of the file
  **
  *******************************************************************************
  **/
  qint64 WriteFloat(QList<double>);                 /*!< writes data from the ATomic card into the wave file every 200ms */

  /**
  *******************************************************************************
  ** @fn  QByteArray WriteHeader()
  ** @param[in] void
  ** @param[out] QByteArray
  ** @brief writes float data into the wave file
  **        writes the RIFF and fmt_ subchunks into a byte array
  ** @return the RIFF header byte array
  **
  *******************************************************************************
  **/
  QByteArray WriteHeader();

  /**
  *******************************************************************************
  ** @fn  QByteArray WavFile::WriteDataHeader(qint64 )
  ** @param[in] qint64 lliDataSize : Size of data
  ** @param[out] QByteArray
  ** @brief writes the data-header subchunk into a byte array
  ** @return the data header byte array
  **
  *******************************************************************************
  **/
  QByteArray WriteDataHeader(qint64 );

  /**
  *******************************************************************************
  ** @fn  QByteArray WavFile::WriteList()
  ** @param[in] void
  ** @param[out] QByteArray
  ** @brief writes List-subchunk data into a byte array
  ** @return the list-subchunk-byte array
  **
  *******************************************************************************
  **/
  QByteArray WriteList();

  /**
  *******************************************************************************
  ** @fn  void SetTitle(QString cAttr)
  ** @param[in] QString cAttr : title string value
  ** @param[out] void
  ** @brief sets the title string
  ** @return void
  **
  *******************************************************************************
  **/
  void SetTitle(QString);                           /*!< sets the INAM string value */

  /**
  *******************************************************************************
  ** @fn  void SetArtist(QString cAttr)
  ** @param[in] QString cAttr : Artist string value
  ** @param[out] void
  ** @brief sets the artist string
  ** @return void
  **
  *******************************************************************************
  **/
  void SetArtist(QString);                          /*!< sets the IART string value */

  /**
  *******************************************************************************
  ** @fn  void SetGenre(QString cAttr)
  ** @param[in] QString cAttr : genre string value
  ** @param[out] void
  ** @brief sets the genre string
  ** @return void
  **
  *******************************************************************************
  **/
  void SetGenre(QString);                           /*!< sets the IGNR string value */

  /**
  *******************************************************************************
  ** @fn  void SetCmnt(QString cAttr)
  ** @param[in] QString cAttr : comment string value
  ** @param[out] void
  ** @brief sets the comment string
  ** @return void
  **
  *******************************************************************************
  **/
  void SetCmnt(QString);                            /*!< sets the ICMT string value */

  /**
  *******************************************************************************
  ** @fn  void SetProduct(QString cAttr)
  ** @param[in] QString cAttr : product string value
  ** @param[out] void
  ** @brief sets the product string
  ** @return void
  **
  *******************************************************************************
  **/
  void SetProduct(QString);                         /*!< sets the IPRD string value */

  /**
  *******************************************************************************
  ** @fn  QString GetTitle()
  ** @param[in] void
  ** @param[out] QString title string value
  ** @brief gets the title string
  ** @return QString
  **
  *******************************************************************************
  **/
  QString GetTitle();                               /*!< gets INAM */

  /**
  *******************************************************************************
  ** @fn  QString GetArtist()
  ** @param[in] void
  ** @param[out] QString artist string value
  ** @brief gets the artist string
  ** @return QString
  **
  *******************************************************************************
  **/
  QString GetArtist();                              /*!< gets IART */

  /**
  *******************************************************************************
  ** @fn  QString GetGenre()
  ** @param[in] void
  ** @param[out] QString genre string value
  ** @brief gets the genre string
  ** @return QString
  **
  *******************************************************************************
  **/
  QString GetGenre();                               /*!< gets IGNR */

  /**
  *******************************************************************************
  ** @fn  QString GetCmnt()
  ** @param[in] void
  ** @param[out] QString comment string value
  ** @brief gets the comment string
  ** @return QString
  **
  *******************************************************************************
  **/
  QString GetCmnt();                                /*!< gets ICMT */

  /**
  *******************************************************************************
  ** @fn  QString GetProduct()
  ** @param[in] void
  ** @param[out] QString product string value
  ** @brief gets the product string
  ** @return QString
  **
  *******************************************************************************
  **/
  QString GetProduct();                             /*!< gets IPRD */

signals:
  void fileNameChanged();                           /*!< Signal emitted upon the change of the file name*/
  void TitleChanged();                              /*!< Signal emitted upon the change of the title */
  void ArtistChanged();                             /*!< Signal emitted upon the change of the artist */
  void GenreChanged();                              /*!< Signal emitted upon the change of the genre */
  void CmntChanged();                               /*!< Signal emitted upon the change of the comment */
  void ProductChanged();                            /*!< Signal emitted upon the change of the product */

public slots:

  /**
  *******************************************************************************
  ** @fn  void setFilename()
  ** @param[in] QString filename string value
  ** @param[out] void
  ** @brief sets the file name of the wave file
  ** @return void
  **
  *******************************************************************************
  **/
  void setFilename(QString filename);               /*!< sets the file name string value */

public:
  QString m_cFilename;                              /*!< the file's name */
  QFile m_file;                                     /*!< current file in use */
  QString m_cSNValue;                               /*!< corresponds to the serial number of the currently connected ATomic card */
private:
  QString m_cINAM;                                   /*!< INAM string value */
  QString m_cIART;                                   /*!< IART string value */
  QString m_cIGNR;                                   /*!< sets the IGNR string value */
  QString m_cICMT;                                   /*!< sets the ICMT string value */
  QString m_cIPRD;                                   /*!< sets the IPRD string value */
  TrWaveFrame rWaveFrame;                            /*!< wave-frame structure */
  QDataStream *m_out;                                /*!< pointer on a QDataStream upon which print bytes to file */
  qint64  m_llLastDataPosition;
};

#endif // WAVFILE_H
