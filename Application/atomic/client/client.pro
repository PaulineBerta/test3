QT += qml quick androidextras gui quickcontrols2 widgets core printsupport xml
QT += svg
QT += remoteobjects
CONFIG += c++11
CONFIG += qwt

#debug directives
CONFIG += debug
CONFIG -= app_bundle


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AudioGraph/TableModelBA.cpp \
    AudioPlot/ChartQuick2BA.cpp \
    AudioPlot/ChartQuick2BAResult.cpp \
    QmlTabs/main.cpp \
    AudioGraph/TableModel.cpp \
    AudioPlot/BarQuick2.cpp \
    AudioPlot/ChartQuick2.cpp \
    AudioRec/AudioRec.cpp \
    AudioScreenShots/ScreenCapture.cpp

REPC_REPLICA += ../atomic.rep \
               ../configuration.rep

RESOURCES += QmlTabs/qml.qrc \
    $$files(AboutImages/*.png)\
    $$files(CalibrationImages/*.png)\
    $$files(ConfigurationImages/*.png)\
    $$files(FileBrowserImages/*.png)\
    $$files(FooterImages/*.png)\
    $$files(GIF/*.gif)\
    $$files(GIF/*.png)\
    $$files(HeaderImages/*.png)\
    $$files(LeftMenuImages/*.png)\
    $$files(LoadingImages/*.png)\
    $$files(MainMenuImages/*.png)\
    $$files(NotImplementedmages/*.png)\
    $$files(Polices/*.ttf)\
    $$files(ReverberationImages/*.png)\
    $$files(RightMenuImages/*.png)\
    $$files(SonometerFrequenciesImages/*.png)\
    $$files(SonometerImages/*.png)\
    $$files(SonometerTablesImages/*.png)\
    $$files(SonometerTemporalImages/*.png)\
    $$files(qml/*.qml)\
    android/res/drawable/icon_foreground.png


# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
#    android/libs/AcousticMeas_armv7/libAcousticMeas.a \
#    android/libs/AcousticMeas_armv7/libAcousticMeas.so \
    android/res/layout/activity_main.xml \
    android/src/com/AllianTech/ATomicSound/ForegroundService.java \
    android/res/drawable/ic_launcher_background.xml \
    android/res/drawable-v24/ic_launcher_foreground.xml \
    android/res/mipmap-anydpi-v26/ic_launcher.xml \
    android/res/mipmap-anydpi-v26/ic_launcher_round.xml \
    android/res/mipmap-hdpi/ic_launcher.png \
    android/res/mipmap-hdpi/ic_launcher_foreground.png \
    android/res/mipmap-hdpi/ic_launcher_round.png \
    android/res/mipmap-mdpi/ic_launcher.png \
    android/res/mipmap-mdpi/ic_launcher_foreground.png \
    android/res/mipmap-mdpi/ic_launcher_round.png \
    android/res/mipmap-xhdpi/ic_launcher.png \
    android/res/mipmap-xhdpi/ic_launcher_foreground.png \
    android/res/mipmap-xhdpi/ic_launcher_round.png \
    android/res/mipmap-xxhdpi/ic_launcher.png \
    android/res/mipmap-xxhdpi/ic_launcher_foreground.png \
    android/res/mipmap-xxhdpi/ic_launcher_round.png \
    android/res/mipmap-xxxhdpi/ic_launcher.png \
    android/res/mipmap-xxxhdpi/ic_launcher_foreground.png \
    android/res/mipmap-xxxhdpi/ic_launcher_round.png \
    android/ic_launcher-web.png \
    QmlTabs/About.qml \
    QmlTabs/AboutScroll.qml \
    QmlTabs/Animation.qml \
    QmlTabs/Calibration.qml \
    QmlTabs/ConfigSonometer.qml \
    QmlTabs/ConfigReverb.qml \
    QmlTabs/ConfigMode.qml \
    QmlTabs/FileBrowser.qml \
    QmlTabs/FirstTable.qml \
    QmlTabs/Footer.qml \
    QmlTabs/Header.qml \
    QmlTabs/main.qml \
    QmlTabs/NotImplemented.qml \
    QmlTabs/SecondTable.qml \
    QmlTabs/Sonometer.qml \
    QmlTabs/SonometerFrequencies.qml \
    QmlTabs/SonometerTermporal.qml \
    QmlTabs/SonometerTables.qml \
    QmlTabs/SwipeSonometer.qml \
    android/src/com/AllianTech/ATomicSound/ATomicUsbManager.java \
    android/res/xml/accessory_filter.xml \
    android/src/com/AllianTech/ATomicSound/ATomicAudioIn.java \


ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

HEADERS += \
    AudioGraph/TableModelBA.h \
    AudioPlot/ChartQuick2BA.h \
    AudioPlot/ChartQuick2BAResult.h \
    AudioScreenShots/ScreenCapture.h \
    AudioGraph/TableModel.h \
    AudioPlot/BarQuick2.h \
    AudioPlot/ChartQuick2.h \ #\
    AudioRec/AudioRec.h


LIBS += -L$$PWD/qwt-6.1.4/lib/ -lqwt
INCLUDEPATH = $$PWD/qwt-6.1.4/include
