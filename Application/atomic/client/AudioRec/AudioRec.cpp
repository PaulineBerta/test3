/**
*******************************************************************************
** @file    AudioRec.cpp
** @version
** @author  Wevioo SE Team
** @brief   This class assures the interaction between the application
**         and the service
**
*******************************************************************************
**/

#include "AudioRec.h"
#include <QFile>
#include <QDataStream>
#include <QtAndroidExtras/QAndroidJniEnvironment>
#include <QAndroidJniObject>
#include <QtAndroid>
#include <QDateTime>
#include <QDebug>


/**
*******************************************************************************
** @fn  AudioRec(QObject *parent, QSharedPointer<atomicReplica> repAtomic,
**               QSharedPointer<configurationReplica> repCfg)
** @param[in] QObject *parent
** @param[in] QSharedPointer<atomicReplica> repAtomic holds a reference to the
**             atomic replica
** @param[in] QSharedPointer<configurationReplica> repAtomic holds a reference
**              to the configuration replica
** @param[out]
** @brief AudioRec's class constructor
**
** @return
**
*******************************************************************************
**/
AudioRec ::AudioRec(QObject* parent, QSharedPointer<atomicReplica> ptr, QSharedPointer<configurationReplica> ptrCfg):QObject(parent),
  m_oAudioRep(ptr), m_oConfigRep(ptrCfg)
{
  QObject::connect(m_oConfigRep.data(), SIGNAL(outOfRange()), this, SLOT(outOfRangePopUp()));
  QObject::connect(m_oConfigRep.data(), SIGNAL(configurationSuccess()), this, SLOT(configurationSuccessPopUp()));
  QObject::connect(m_oConfigRep.data(), SIGNAL(configurationFailure()), this, SLOT(configurationFailurePopUp()));
  QObject::connect(m_oConfigRep.data(), SIGNAL(calibrationTimeOut()), this, SLOT(calibrationTimeOutPopUp()));
  QObject::connect(m_oConfigRep.data(), SIGNAL(calibrationTimeOutSuccess()), this, SLOT(calibrationTimeOutSuccessPopUp()));
  QObject::connect(m_oAudioRep.data(), SIGNAL(insufficientSpaceWarning()), this, SLOT(insufficientSpaceWarningPopUp()));
  //QObject::connect(m_oAudioRep.data(), SIGNAL(showBetaWarning()), this, SLOT(betaWarningPopUp())); //Patch_betapopup //kuba
}

/**
*******************************************************************************
** @fn  ~AudioRec()
** @param[in]
** @param[out]
** @brief AudioRec's class destructor
**
** @return
**
*******************************************************************************
**/
AudioRec::~AudioRec(){
}

/**
*******************************************************************************
** @fn  void AudioRec::quitHandler()
** @param[in] void
** @param[out] void
** @brief in case the card disconnection, the logFile is updated depending on
**        the state and the service is stopped
** @return
**
*******************************************************************************
**/
void AudioRec::quitHandler(){

  QFile logFile;
  logFile.setFileName(m_oAudioRep->path()+QString("/AllianTech/ATomicSound/Log/LogFile.csv"));
  if(logFile.exists()){
      logFile.close();
      logFile.open(QIODevice::WriteOnly | QIODevice::Append);
      QTextStream out(&logFile);
      out.setCodec("UTF-8");
      if(m_oAudioRep->sonoState()!=7){
          if(m_oAudioRep->sonoState()==3){
              out<<QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss.zzz")
                << ";" << "EVENT "
                << ";" << "Stop WAV Recording"
                << "\n";
              out.flush();
            }
          if(m_oAudioRep->sonoState()>1 && m_oAudioRep->sonoState() <4){
              out<<QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss.zzz")
                << ";" << "EVENT "
                << ";" << "Stop CSV Recording"
                << "\n";
              out.flush();
            }
          if(m_oAudioRep->sonoState() ==4){
              out<<QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss.zzz")
                << ";" << "EVENT "
                << ";" << "Stop Calibration"
                << "\n";
              out.flush();
            }
        }
      if (m_oAudioRep->atomicState()>0) {
          out<<QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss.zzz")
            << ";" << "EVENT "
            << ";" << "Stop Acquisition"
            << "\n";
          out.flush();
        }
      out<<QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss.zzz")
        << ";" << "EVENT "
        << ";" << "Close LogFile"
        << "\n";
      out.flush();
    }
  stopService();
}

/**
*******************************************************************************
** @fn  void AudioRec::stopService()
** @param[in] void
** @param[out] void
** @brief stop the running service
** @return
**
*******************************************************************************
**/
void AudioRec::stopService(){
  // stop service
  QAndroidJniObject::callStaticMethod<void>("com/AllianTech/ATomicSound/ForegroundService",
                                            "stopMyService",
                                            "(Landroid/content/Context;)V",
                                            QtAndroid::androidActivity().object());
}

/**
*******************************************************************************
** @fn  void startService()
** @param[in] void
** @param[out] void
** @brief start the service
** @return
**
*******************************************************************************
**/
void AudioRec::startService(){
  // start service
  QAndroidJniObject::callStaticMethod<void>("com/AllianTech/ATomicSound/ForegroundService",
                                            "startMyService",
                                            "(Landroid/content/Context;)V",
                                            QtAndroid::androidActivity().object());
}
/* ------------------------------------------------------------------------------
 *                           POP-UP DEFINITIONS
 * ------------------------------------------------------------------------------
 * */
/**
*******************************************************************************
** @fn  void outOfRangePopUp()
** @param[in] void
** @param[out] void
** @brief display an error pop-up for an out of range laboratory sensitivity
**        value
** @return
**
*******************************************************************************
**/
void AudioRec::outOfRangePopUp(){
  /** Pop up view shown when the laboratory sensitivity is outside the specified range */
  QMessageBox msgBox(QMessageBox::Critical,
                     "ERROR",
                     "The Laboratory Sensitivity introduced is outside the range marked "
                     "by the tolerance of the nominal sensitivity. The configuration will not be applied and saved",
                     QMessageBox::Ok );
  int iRet = msgBox.exec();
  switch (iRet) {
    case QMessageBox::Ok :
      {
        qDebug()<<"Config OK HAS BEEN CLICKED";
        m_oConfigRep->setDefault();
        emit m_oConfigRep->errorConfiguration();
      }
      break;
    default:
      break;
    }
}
/**
*******************************************************************************
** @fn  void AudioRec::configurationSuccessPopUp()
** @param[in] void
** @param[out] void
** @brief display an information pop-up indicating the configuration success
**
** @return void
**
*******************************************************************************
**/
void AudioRec::configurationSuccessPopUp(){
  QMessageBox msgBox(QMessageBox::Information,
                     "New Configuration Applied",
                     "Configuration applied correctly, the acquisition has been restarted with the new parameters",
                     QMessageBox::Ok );

  int iRet = msgBox.exec();
  switch (iRet) {
    case QMessageBox::Ok :
      break;
    default:
      break;
    }
}

/**
*******************************************************************************
** @fn  void AudioRec::configurationFailurePopUp()
** @param[in] void
** @param[out] void
** @brief display a warring pop-up indicating the configuration failure
**
** @return void
**
*******************************************************************************
**/
void AudioRec::configurationFailurePopUp(){
  QMessageBox msgBox(QMessageBox::Warning,
                     "Unable to Apply Configuration",
                     "Something went wrong, the acquisition will be restarted with the default parameters",
                     QMessageBox::Ok );

  int iRet = msgBox.exec();

  switch (iRet) {
    case QMessageBox::Ok :
      m_oConfigRep.data()->setReset();
      m_oConfigRep.data()->configAcousticMeas(true, true);
      break;
    default:
      break;
    }
}
/**
*******************************************************************************
** @fn  void calibrationTimeOutPopUp()
** @param[in] void
** @param[out] void
** @brief display a pop-up indicating calibration failure at timeout
** @return
**
*******************************************************************************
**/
void AudioRec::calibrationTimeOutPopUp(){
  QMessageBox msgBox(QMessageBox::Critical,
                     "Calibration Failure",
                     "Verify that your microphone and calibrator are properly connected.\n\n"
                     "You are authorized to modify the Laboratory Sensitivity (in the configuration screen) "
                     "but a new manufacture calibration is recommended.",
                     QMessageBox::Ok );

  int iRet = msgBox.exec();

  switch (iRet) {
    case QMessageBox::Ok :{
        msgBox.close();
        m_oConfigRep.data()->stopCalibration(true);
      }
      break;
    default:
      break;
    }
}
/**
*******************************************************************************
** @fn  void AudioRec::calibrationTimeOutSuccessPopUp()
** @param[in] void
** @param[out] void
** @brief display a pop-up indicating calibration success after Timeout
** @return
**
*******************************************************************************
**/
void AudioRec::calibrationTimeOutSuccessPopUp(){
  QMessageBox msgBox(QMessageBox::Critical,
                     "Calibration finished correctly",
                     "Are you sure you want to discard the correction value obtained, or do you want to apply it",
                     QMessageBox::Discard | QMessageBox::Apply );

  int iRet = msgBox.exec();

  switch (iRet) {
    case QMessageBox::Discard : //Stop calibration
      {
        m_oConfigRep.data()->stopCalibration(true);
        m_oAudioRep.data()->resetChrono();
        m_oAudioRep.data()->reset();
      }
      break;
    case QMessageBox::Apply:     //Validate the calibration
      {
        m_oConfigRep.data()->validateCalibration();
        m_oAudioRep.data()->resetChrono();
        m_oAudioRep.data()->reset();

      }
    }
}

/**
*******************************************************************************
** @fn  void AudioRec::insufficientSpaceWarningPopUp()
** @param[in] void
** @param[out] void
** @brief display a warning pop-up of an insufficient storage space
** @return
**
*******************************************************************************
**/
void AudioRec::insufficientSpaceWarningPopUp(){
  QMessageBox msgBox(QMessageBox::Warning,
                     "Insufficient Storage Space",
                     "Free some space",
                     QMessageBox::Ok);
  int iRet=msgBox.exec();
  if (iRet==QMessageBox::Ok) return;
}

/**
*******************************************************************************
** @fn  void AudioRec::betaWarningPopUp()
** @param[in] void
** @param[out] void
** @brief display an information pop-up indicating possible issues due to Beta Version
**
** @return void
**
*******************************************************************************
**/
void AudioRec::betaWarningPopUp(){ //Patch_betapopup
  QMessageBox msgBox(QMessageBox::Information,
                     "Warning!",
                     "This is NOT the FINAL VERSION of the application !!!\n\n"
                     "Depending on the model of smartphone where the application"
                     " is running it is possible to experience audio glitches and/or "
                     "alterations in the input signal (filtering or gain changes). ",
                     QMessageBox::Ok );

  int iRet = msgBox.exec();
  switch (iRet) {
    case QMessageBox::Ok :
      break;
    default:
      break;
    }
}
