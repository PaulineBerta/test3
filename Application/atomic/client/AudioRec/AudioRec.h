/**
*******************************************************************************
** @file    AudioRec.h
** @version
** @author  Wevioo SE Team
** @brief   This class assures the interaction between the application
**         and the service
**
*******************************************************************************
**/

#ifndef AUDIOREC_H
#define AUDIOREC_H

#include <QtCore/QObject>
#include <QSharedPointer>
#include <QMessageBox>
#include <QtAndroid>
#include <QAndroidJniObject>
#include <QtAndroidExtras/QAndroidJniEnvironment>

#include "rep_atomic_replica.h"
#include "rep_configuration_replica.h"

/**
 * @brief The AudioRec class
 */

/**
*******************************************************************************
** @class  AudioRec : public QObject
** @brief assures interaction between the GUI and the service
**
*******************************************************************************
**/
class AudioRec : public QObject
{
  Q_OBJECT
public:

  /**
  *******************************************************************************
  ** @fn  AudioRec(QObject *parent, QSharedPointer<atomicReplica> repAtomic,
  **               QSharedPointer<configurationReplica> repCfg)
  ** @param[in] QObject *parent
  ** @param[in] QSharedPointer<atomicReplica> repAtomic holds a reference to the
  **             atomic replica
  ** @param[in] QSharedPointer<configurationReplica> repAtomic holds a reference
  **              to the configuration replica
  ** @param[out]
  ** @brief AudioRec's class constructor
  **
  ** @return
  **
  *******************************************************************************
  **/
  explicit AudioRec(QObject *parent, QSharedPointer<atomicReplica> repAtomic, QSharedPointer<configurationReplica> repCfg);

  /**
  *******************************************************************************
  ** @fn  ~AudioRec()
  ** @param[in]
  ** @param[out]
  ** @brief AudioRec's class destructor
  **
  ** @return
  **
  *******************************************************************************
  **/
  ~AudioRec();

signals:

public slots:
  /**
  *******************************************************************************
  ** @fn  void quitHandler()
  ** @param[in] void
  ** @param[out] void
  ** @brief in case the card disconnection, the logFile is updated depending on
  **        the state and the service is stopped
  ** @return
  **
  *******************************************************************************
  **/
  void quitHandler();

  /**
  *******************************************************************************
  ** @fn  void stopService()
  ** @param[in] void
  ** @param[out] void
  ** @brief stop the running service
  ** @return
  **
  *******************************************************************************
  **/
  void stopService();

  /**
  *******************************************************************************
  ** @fn  void startService()
  ** @param[in] void
  ** @param[out] void
  ** @brief start the service
  ** @return
  **
  *******************************************************************************
  **/
  void startService();

  /**
  *******************************************************************************
  ** @fn  void outOfRangePopUp()
  ** @param[in] void
  ** @param[out] void
  ** @brief display an error pop-up for an out of range laboratory sensitivity
  **        value
  ** @return
  **
  *******************************************************************************
  **/
  void outOfRangePopUp();

  /**
  *******************************************************************************
  ** @fn  void configurationSuccessPopUp()
  ** @param[in] void
  ** @param[out] void
  ** @brief display an information pop-up indicating the configuration success
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void configurationSuccessPopUp();

  /**
  *******************************************************************************
  ** @fn  void configurationFailurePopUp()
  ** @param[in] void
  ** @param[out] void
  ** @brief display a warring pop-up indicating the configuration failure
  ** @return
  **
  *******************************************************************************
  **/
  void configurationFailurePopUp();

  /**
  *******************************************************************************
  ** @fn  void calibrationTimeOutPopUp()
  ** @param[in] void
  ** @param[out] void
  ** @brief display a pop-up indicating calibration failure at timeout
  ** @return
  **
  *******************************************************************************
  **/
  void calibrationTimeOutPopUp();

  /**
  *******************************************************************************
  ** @fn  void calibrationTimeOutSuccessPopUp()
  ** @param[in] void
  ** @param[out] void
  ** @brief display a pop-up indicating calibration success after Timeout
  ** @return
  **
  *******************************************************************************
  **/
  void calibrationTimeOutSuccessPopUp();

  /**
  *******************************************************************************
  ** @fn  void insufficientSpaceWarningPopUp()
  ** @param[in] void
  ** @param[out] void
  ** @brief display a warning pop-up of an insufficient storage space
  ** @return
  **
  *******************************************************************************
  **/
  void insufficientSpaceWarningPopUp();


  /**
  *******************************************************************************
  ** @fn  void betaWarningPopUp()
  ** @param[in] void
  ** @param[out] void
  ** @brief display an information pop-up indicating possible issues due to Beta Version
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void betaWarningPopUp(); //Patch_betapopup

private:
  QSharedPointer<atomicReplica> m_oAudioRep;                     /*!< holds a reference to the atomic replica object */
  QSharedPointer<configurationReplica> m_oConfigRep;             /*!< holds a reference to the configuration replica object */
};

#endif // AUDIOREC_H
