/**
*******************************************************************************
** @file    ScreenCapture.cpp
** @version
** @author  Wevioo SE Team
** @brief   This class enables user to take screenshots of current window on
**          display
**
*******************************************************************************
**/

#include <QPixmap>
#include <QQuickView>
#include <QString>
#include <QGuiApplication>
#include <QWindow>
#include <QWidget>
#include <QStandardPaths>
#include <QDir>
#include "ScreenCapture.h"
#include <QAndroidJniEnvironment>
#include <QtAndroidExtras/QAndroidJniObject>

/**
*******************************************************************************
** @fn  ScreenCapture::ScreenCapture(QQuickWindow *parent = nullptr)
** @param[in] QObject *parent
** @param[out]
** @brief ScreenCapture's class constructor
**
** @return
**
*******************************************************************************
**/
ScreenCapture::ScreenCapture(QQuickWindow *currentView) :QObject(nullptr),
  currentWindow_(currentView)
{
}

/**
*******************************************************************************
** @fn  void capture()
** @param[in] void
** @param[out]  void
** @brief upon the screenshot button clicked, at the bottom right, creates the
**          screenshot folder, if not existed, takes screenshot and save it.
**
** @return
**
*******************************************************************************
**/
void ScreenCapture::capture() const
{
  QString initialPath;
  QAndroidJniObject activity = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative",
                                                                         "activity",
                                                                         "()Landroid/app/Activity;");
  QAndroidJniObject dirs = activity.callObjectMethod("getExternalFilesDirs","(Ljava/lang/String;)[Ljava/io/File;", NULL);

  if (dirs.isValid())
    {
      QAndroidJniObject path;
      path = QAndroidJniObject::callStaticObjectMethod("android.os.Environment",
                                                                             "getExternalStorageDirectory",
                                                                             "()Ljava/io/File;");

      QDir().mkdir(path.toString()+ QString("/AllianTech/ATomicSound/ScreenShots"));
      initialPath = path.toString()
          + QString("/AllianTech/ATomicSound/ScreenShots/")
          + QString("ScreenShot_")
          + QDateTime::currentDateTime().toString("yyMMdd_hhmmss")
          + QString(".jpg");
    }

  QImage img = currentWindow_->grabWindow();         // grabs current active window
  img.save(initialPath);                             // save image to initial path
}
