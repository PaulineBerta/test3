/**
*******************************************************************************
** @file    ScreenCapture.h
** @version
** @author  Wevioo SE Team
** @brief   This class enables user to take screenshots of current window on
**          display
**
*******************************************************************************
**/

#include <QObject>
#include <QDateTime>

class QString;
class QQuickWindow;

/**
*******************************************************************************
** @class  ScreenCapture : public QObject
** @brief enables taking screenshots
**
*******************************************************************************
**/
class ScreenCapture : public QObject
{
    Q_OBJECT
public:

  /**
  *******************************************************************************
  ** @fn  ScreenCapture(QQuickWindow *parent = nullptr)
  ** @param[in] QObject *parent
  ** @param[out]
  ** @brief ScreenCapture's class constructor
  **
  ** @return
  **
  *******************************************************************************
  **/
    explicit ScreenCapture(QQuickWindow *parent = nullptr);

public slots:

  /**
  *******************************************************************************
  ** @fn  void capture()
  ** @param[in] void
  ** @param[out]  void
  ** @brief upon the screenshot button clicked, at the bottom right, creates the
  **          screenshot folder, if not existed, takes screenshot and save it.
  **
  ** @return
  **
  *******************************************************************************
  **/
    void capture() const;

private:
    QQuickWindow *currentWindow_;             /*!< represents the current scene graph on display */
};
