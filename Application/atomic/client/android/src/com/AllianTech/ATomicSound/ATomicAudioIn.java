/**
*******************************************************************************
** An     ATomicAudioIn class for AudioRecord creation, initialization
**         and pulling audio data into a float array
** @author  Wevioo SE Team
**
*******************************************************************************
**/

package com.AllianTech.ATomicSound;
import android.media.AudioRecord;
import android.media.AudioFormat;
import android.content.Context;
import android.media.MediaRecorder;
import android.media.AudioTimestamp;
import android.util.Log;
import java.util.concurrent.atomic.AtomicBoolean;
import android.media.audiofx.AutomaticGainControl;
import android.media.audiofx.AcousticEchoCanceler;

public class ATomicAudioIn {

    public AudioRecord mRecorder;
    private AudioTimestamp outTimestamp;
    private static Context mContext;

    /*public static int FECH = 48000;
    public static int ACQUISITION_BLOCK_DURATION_MS = 200; //Default duration for a block
    public static int ACQUISITION_BLOCK_SIZE = (int)(ACQUISITION_BLOCK_DURATION_MS * FECH/1000) * 2 ;*/ // * 2 and L and R are interlaced

    public int SampleRate;
    public int ChannelCount;
    public int bufferOverflow;

    private int internalMic = 0;
    private int bufferSize;

    public ATomicAudioIn(Context c, int bufferSize, int internalMic){
        mContext = c;
        this.bufferSize = bufferSize;
        this.internalMic = internalMic;
    }

    public void recordConfig() {
        //Config Mic recorder depending on Directives
        //Init Audio Recorder
        outTimestamp = new AudioTimestamp();

        if(internalMic == 0){
            //USB MIC
            //It changes depending on the smartphone
            int iMinBufferSize = AudioRecord.getMinBufferSize(48000, AudioFormat.CHANNEL_IN_STEREO , AudioFormat.ENCODING_PCM_FLOAT);

            //Set Buffer 10 time the minimum size to absorb fluctuations
            int iBufferSize = iMinBufferSize * 10;

            Log.d("QtJavaDebug", "UNPROCESSED PROFILE");
            this.mRecorder = new AudioRecord(MediaRecorder.AudioSource.UNPROCESSED, 48000, AudioFormat.CHANNEL_IN_STEREO, AudioFormat.ENCODING_PCM_FLOAT, iBufferSize);

        } else {
            Log.d("QtJavaDebug", "INTERNAL MIC PROFILE");
            this.mRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, 48000, AudioFormat.CHANNEL_IN_STEREO, AudioFormat.ENCODING_PCM_FLOAT, bufferSize);
        }

        SampleRate = this.mRecorder.getSampleRate();
        ChannelCount = this.mRecorder.getChannelCount();

        //try to disable AGC
        if (AutomaticGainControl.isAvailable()) {
            Log.d("QtJavaDebug", "AudioThread: AGC available");
            AutomaticGainControl agc = AutomaticGainControl.create(this.mRecorder.getAudioSessionId());
            agc.setEnabled(false);
            Log.d("QtJavaDebug", "AudioThread: AGC turn off");
        }else{
            Log.d("QtJavaDebug", "AudioThread: AGC not available");
        }

        //try to disable AcousticEchoCanceler
        if (AcousticEchoCanceler.isAvailable()) {
            Log.d("QtJavaDebug", "AudioThread: AcousticEchoCanceler available");
            AcousticEchoCanceler aec = AcousticEchoCanceler.create(this.mRecorder.getAudioSessionId());
            aec.setEnabled(false);
            Log.d("QtJavaDebug", "AudioThread: AcousticEchoCanceler turn off");
        }else{
            Log.d("QtJavaDebug", "AudioThread: AcousticEchoCanceler not available");
        }

        //Check if instance has been initializated correctly
        if((this.mRecorder == null) || (this.mRecorder.getState() != AudioRecord.STATE_INITIALIZED)){
            return;
        }

        //try to get higher priority
        try {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        } catch (IllegalArgumentException | SecurityException ex) {
            // Ignore
        }
    }

    public void startRecorder() {
        this.mRecorder.startRecording();
        Log.d("QtJavaDebug", ">>>>>>> Start AudioRecorder <<<<<<<<<");
    }

    public float[] readRecorder(int ACQUISITION_BLOCK_SIZE) {
        float[] m_afFlipFlopBuffer = new float[ACQUISITION_BLOCK_SIZE];
        Log.d("QtJavaDebug", "++++++++++++++ RENTRE READ RECORDER +++++++++++++++ ");

        int ret = this.mRecorder.read(m_afFlipFlopBuffer, 0, ACQUISITION_BLOCK_SIZE, AudioRecord.READ_BLOCKING);
        Log.d("QtJavaDebug", ">>> READ RECORDER, ACQUISITION_BLOCK_SIZE :" + String.valueOf(ACQUISITION_BLOCK_SIZE));

        return(m_afFlipFlopBuffer);
    }

    public void stopRecorder() {
        if(this.mRecorder.getState() != AudioRecord.STATE_UNINITIALIZED) {
            this.mRecorder.stop();
            this.mRecorder.release();
            Log.d("QtJavaDebug", ">>>>>>> Stop and Release AudioRecorder <<<<<<<<<");
        }
    }
}
