import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import "./"

import Qt.labs.folderlistmodel 2.2

Item {
    id: fileBrowserScroll
    width:Screen.width
    height:Screen.height

    Connections{
        target: audioget
        onPreviousScreenStateChanged:  {
            console.log(audioget.previousScreenState)
        }

        onButtonClickChanged: {
            if(audioget.screenState === 8) { // FILE BROWSER

                switch(audioget.buttonClick) {
                case 81: // FileBrowser Left Button
                    if(folderListModel.folder.toString() !== config.rootPath.toString())
                    folderListModel.folder = folderListModel.parentFolder
                break

                case 83: // FileBrowser Right Button
                    audioget.screenState = audioget.previousScreenState
                    break

                default:
                    break
                }

                audioget.buttonClick=0 // None
            }
        }
    }

    /*** Header ***/
    Image {
        id: superior_panel
        source: "FileBrowserImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    /* Background */
    Image {
        id: inferior_panel_back
        source: "FooterImages/inferior_panel_back.png"
        x: setX(0)
        y: setY(2458)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }
    Image {
        id: inferior_panel_front
        source: "FooterImages/inferior_panel_front.png"
        x: setX(245)
        y: setY(2425)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    /*** File Browser ***/
    Rectangle {
        id: mainRect
        x: setX(0)
        y: setY(0)+superior_panel.height
        z: 0
        width:Screen.width
        height:applicationWindow.height-superior_panel.height-inferior_panel_front.height+60
        border.color: "transparent"
        color: "#353535"

        ListView {
            y: setY(30)
            z: 1
            width: parent.width
            height: parent.height - 60
            clip: true
            model: FolderListModel {
                id: folderListModel
                showDirsFirst: true
                nameFilters: ["*.xml"]

                //folder: "file:///sdcard/" équivalent à /storage/emulated/0/ dans c++
                rootFolder: config.rootPath
                folder: config.rootPath
            }
            delegate: Button {
                width: parent.width
                height: 50
                text: fileName
                onClicked: {
                    if (fileIsDir) {
                        folderListModel.folder = fileURL
                    } else {
                        console.log("%%%%%%%%%% Select File FileBrowser %%%%%%%%%")
                        config.pathConfigFileName(fileURL.toString())
                        console.log("File URL : "+fileURL.toString())

                        audioget.screenState = audioget.previousScreenState
                    }
                }
                background: Rectangle {
                    color: fileIsDir ? "#dcdcdc" : "#faf0e6"
                    border.color: "#696969"
                }
            }
        }
    }
    Button {
        x: setX(20) //350
        y: setY(300) //350
        anchors.left: mainRect.right
        anchors.leftMargin: 5
        text: "back"
        onClicked: {
            if(folderListModel.folder.toString() !== config.rootPath.toString())
            folderListModel.folder = folderListModel.parentFolder
        }
    }
}
