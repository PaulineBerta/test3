import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1

Item {
    id:reverbTabScreen
    x:0
    y:superior_panel.height
    width:Screen.width
    height:Screen.height

    /** Background **/
    Image {
        id: superior_panel
        source: "ReverberationImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }
    Image {
        id: inferior_panel_front
        source: "ReverberationImages/inferior_panel_front.png"
        visible: false
        x: setX(245)
        y: setY(2425)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    Flickable{
        id: myFlickable
        x: setX(0)
        y: setY(750)-superior_panel.height
        width:Screen.width
        height:applicationWindow.height-superior_panel.height-inferior_panel_front.height-back_tr_counter_area.height-160

        contentHeight: if(config.spectrumConfiguration === "SPECTRUM_3RD_OCTAVE") {
                            680//(1060+inferior_panel_front.height)

                       } else if (config.spectrumConfiguration === "SPECTRUM_OCTAVE") {
                            (560+inferior_panel_front.height)
                       }

        contentWidth: Screen.width
        interactive: true
        boundsMovement: Flickable.StopAtBounds
        boundsBehavior: Flickable.StopAtBounds

        ScrollBar.vertical: ScrollBar {
            policy: ScrollBar.AlwaysOn
        }

        Column{
            spacing: 9

            ReverbTableAvg{
            }
        }
    }

    Image {
        id: back_tr_counter_area
        source: "ReverberationImages/back_tr_counter_area.png"
        x: setX(0)
        y: setY(260)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }







    /** Background **/
    /*Image {
        id: superior_panel
        source: "ReverberationImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }
    Image {
        id: inferior_panel_front
        source: "ReverberationImages/inferior_panel_front.png"
        visible: false
        x: setX(245)
        y: setY(2425)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    Flickable{
        id: myFlickable
        x: setX(0)
        y: setY(650)-superior_panel.height
        width:Screen.width
        height:applicationWindow.height-superior_panel.height-inferior_panel_front.height-back_tr_counter_area.height-160

        contentHeight: if(config.spectrumConfiguration === "SPECTRUM_3RD_OCTAVE") {
                            680//(1060+inferior_panel_front.height)

                       } else if (config.spectrumConfiguration === "SPECTRUM_OCTAVE") {
                            (560+inferior_panel_front.height)
                       }

        contentWidth: Screen.width
        interactive: true
        boundsMovement: Flickable.StopAtBounds
        boundsBehavior: Flickable.StopAtBounds

        ScrollBar.vertical: ScrollBar {
            policy: ScrollBar.AlwaysOn
        }

        Column{
            spacing: 12

            ReverbTableAvg{
            }
        }
    }

    Image {
        id: back_tr_counter_area
        source: "ReverberationImages/back_tr_counter_area.png"
        x: setX(0)
        y: setY(2010)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    Image {
        id: tr_counter_area
        source: "ReverberationImages/tr_counter_area.png"
        anchors.horizontalCenter: parent.horizontalCenter
        y: setY(2087)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    Text {
        id: table_counter_text
        text: "Selected Table: "
        font.pixelSize: 34
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(450)
        anchors.verticalCenter: tr_counter_area.verticalCenter
        //y: setY(2130)-superior_panel.height
        opacity: 1
    }
    Text {
        id: table_counter
        text: "TR Avg."
        font.pixelSize: 34
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(920)
        anchors.verticalCenter: tr_counter_area.verticalCenter
        //y: setY(2130)-superior_panel.height
        opacity: 1
    }*/




    Text {
        id: table_counter_text
        text: "Selected Table: RT Average"
        font.pixelSize: 18
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(365)
        y: setY(660)-superior_panel.height
        anchors.horizontalCenter: parent.horizaontalCenter
        opacity: 1
    }

    /** Window Indicator **/
    Image {
        id: other_window_indicator_01
        source: "ReverberationImages/other_window_indicator_01.png"
        x: setX(632)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_02
        source: "ReverberationImages/other_window_indicator_02.png"
        x: setX(736)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: blue_window_indicator
        source: "ReverberationImages/blue_window_indicator.png"
        x: setX(840)
        y: setY(2321)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
}
