import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQuick.Dialogs 1.1
import "./"

Item {
    id: configItem
    x:0
    y:0
    width:Screen.width
    height:applicationWindow.height

    function getTimeWeightingColor(sArg) {
        if(config.timeWeighting === sArg) {
            this.color = "#0c85c7"

        } else {
            this.color="#ffffff"
        }
    }

    function getSpectrumColor(sArg) {
        if(config.spectrumConfiguration === sArg) {
            this.color = "#0c85c7"

        } else {
            this.color="#ffffff"
        }
    }

    Connections{
        target: config
        onTimeWeightingChanged: {
            switch(config.timeWeighting) {
            case "LP_NONE":
                none.color= "#0c85c7"
                fast.color="#ffffff"
                slow.color="#ffffff"
                impulse.color="#ffffff"

                time_check_outline.x=setX(135)
                time_check_outline.y=setY(2042)
                time_check_center.x=setX(143)
                time_check_center.y=setY( 2050)
                break

            case "LP_SLOW":
                time_check_outline.x=setX(435)
                time_check_outline.y=setY(2042)
                time_check_center.x= setX(443)
                time_check_center.y=setY(2050)

                slow.color= "#0c85c7"
                fast.color="#ffffff"
                impulse.color="#ffffff"
                none.color= "#ffffff"
                break

            case "LP_FAST":
                time_check_outline.x=setX(760)
                time_check_outline.y=setY(2042)
                time_check_center.x= setX(768)
                time_check_center.y=setY(2050)

                fast.color= "#0c85c7"
                slow.color="#ffffff"
                impulse.color="#ffffff"
                none.color= "#ffffff"
                break

            case "LP_IMPULSE":
                time_check_outline.x=setX(1047)
                time_check_outline.y=setY(2042)
                time_check_center.x= setX(1055)
                time_check_center.y=setY(2050)

                impulse.color= "#0c85c7"
                fast.color="#ffffff"
                none.color= "#ffffff"
                slow.color="#ffffff"
                break
            }
        }

        onSpectrumConfigurationChanged: {
            switch(config.spectrumConfiguration) {
            case "SPECTRUM_NO":
                spectrum_check_outline.x=setX(135)
                spectrum_check_outline.y=setY(2267)
                spectrum_check_center.x= setX(143)
                spectrum_check_center.y=setY(2275)

                none1.color= "#0c85c7"
                octave.color="#ffffff"
                thirdoctave.color="#ffffff"
                //fft.color="#ffffff"
                break

            case "SPECTRUM_OCTAVE":
                spectrum_check_outline.x=setX(435)
                spectrum_check_outline.y=setY(2267)
                spectrum_check_center.x= setX(443)
                spectrum_check_center.y=setY(2275)

                octave.color= "#0c85c7"
                thirdoctave.color="#ffffff"
                //fft.color="#ffffff"
                none1.color= "#ffffff"
                break

            case "SPECTRUM_3RD_OCTAVE":
                spectrum_check_outline.x=setX(760)
                spectrum_check_outline.y=setY(2267)
                spectrum_check_center.x= setX(768)
                spectrum_check_center.y=setY(2275)

                thirdoctave.color= "#0c85c7"
                //fft.color="#ffffff"
                none1.color= "#ffffff"
                octave.color="#ffffff"
                break

            case "SPECTRUM_FFT":
                //spectrum_check_outline.x=setX(1143)
                //spectrum_check_outline.y=setY(2267)
                //spectrum_check_center.x= setX(1151)
                //spectrum_check_center.y=setY(2275)

                //fft.color= "#0c85c7"
                //octave.color="#ffffff"
                //thirdoctave.color="#ffffff"
                //none1.color= "#ffffff"
                break
            }
        }
    }

    Connections{
        target: audioget
        onButtonClickChanged: {
            if(audioget.screenState === 6) { // CONFIGURATION

                switch(audioget.buttonClick) {
                case 61:
                    console.log("******** reset ********"+config.cfgState)

                    if(audioget.sonoState===1) {
                        // Modify spectrum
                        spectrum_check_outline.visible=true
                        spectrum_check_center.visible=true

                        spectrum_check_outline.x=setX(760)
                        spectrum_check_outline.y=setY(2267)
                        spectrum_check_center.x= setX(768)
                        spectrum_check_center.y=setY(2275)

                        config.spectrumConfiguration="SPECTRUM_3RD_OCTAVE"
                        thirdoctave.color="#0c85c7"
                        octave.color= "#ffffff"
                        //fft.color="#ffffff"
                        none1.color= "#ffffff"

                        //modify time weighting
                        time_check_outline.visible=true
                        time_check_center.visible=true
                        time_check_outline.x=setX(435)
                        time_check_outline.y=setY(2042)
                        time_check_center.x= setX(443)
                        time_check_center.y=setY(2050)

                        config.timeWeighting="LP_SLOW"
                        slow.color= "#0c85c7"
                        fast.color="#ffffff"
                        impulse.color="#ffffff"
                        none.color= "#ffffff"

                        // Set reset
                        config.configAcousticMeas(true, true)
                    }
                    break

                default:
                    break
                }

                audioget.buttonClick=0 // None
            }
        }
    }

    /** BACKGROUND **/
    Image {
        id: superior_panel
        source: "ConfigurationImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }

    /** Sensor Parameters **/
    Text {
        id: sensorparameters
        text: "Sensor Parameters"
        font.pixelSize: 14
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(104)
        y: setY(680.25) //546.25)
        opacity: 1
    }
    Image {
        id: trace_config_01
        source: "ConfigurationImages/trace_config_01.png"
        x: setX(683)
        y: setY(700)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /* Type Microphone */
    Image {
        id: rect_type_micro
        source: "ConfigurationImages/rect_type_micro.png"
        x: setX(635)
        y: setY(760)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        TextField {
            id: textField1
            text: config.microphone
            padding: 0
            smooth: true
            color: "#ffffff"
            anchors.fill: parent
            font.pixelSize: 10
            font.family: "Tahoma"
            placeholderTextColor:"#ffffff"
            background: Rectangle {
                implicitWidth: rect_type_micro.width
                implicitHeight: rect_type_micro.height
                color:"transparent"
                border.color: "transparent"
            }
            inputMethodHints: Qt.ImhNoPredictiveText

            onAccepted: {
                Qt.inputMethod.hide();
                textField1.focus = false
            }

            onTextEdited: {
                config.microphone=textField1.text
            }
        }
    }
    Text {
        id: typemicrophone
        text: "Type microphone"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(106)
        y: setY(770)
        opacity: 1
    }

    /* Serial Number */
    Image {
        id: rect_serial_number
        source: "ConfigurationImages/rect_serial_number.png"
        x: setX(635)
        y: setY(880)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        TextField {
            id: textFieldSerialNum
            text: config.serialNumber
            padding: 0
            smooth: true
            color: "#ffffff"
            anchors.fill: parent
            font.pixelSize: 10
            font.family: "Tahoma"
            placeholderTextColor:"#ffffff"
            background: Rectangle {
                implicitWidth: rect_serial_number.width
                implicitHeight: rect_serial_number.height
                color:"transparent"
                border.color: "transparent"
            }
            inputMethodHints: Qt.ImhNoPredictiveText
            onAccepted: {
                Qt.inputMethod.hide();
                textFieldSerialNum.focus = false
            }
            onTextEdited: {
                config.serialNumber=textFieldSerialNum.text
            }
        }
    }
    Text {
        id: serialnumber
        text: "Serial Number"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX( 108)
        y: setY(890)
        opacity: 1
    }

    /* Nominal Sensitivity */
    Image {
        id: rect_nominal_sens
        source: "ConfigurationImages/rect_nominal_sens.png"
        x: setX(635)
        y: setY(1000)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1

        TextField {
            id: textFieldNSensivity
            text: config.nominalSensitivity
            padding: 0
            smooth: true
            color: "#ffffff"
            anchors.fill: parent
            x: setX(1090)
            y: setY(917.25)
            font.pixelSize: 10
            font.family: "Tahoma"
            placeholderTextColor:"#ffffff"
            background: Rectangle {
                implicitWidth: rect_nominal_sens.width
                implicitHeight: rect_nominal_sens.height
                color:"transparent"
                border.color: "transparent"
            }

            validator: RegExpValidator {
                // Parse double with . or , as decimal point
                regExp: /(\d*)([.,]\d*)?$/            // /(\d*)([.,]\d*)?$/ /* /[+-]?\d*[\.,]?\d+/    -- /(\d{1,3})([.,]\d{2,3})*/
            }
            inputMethodHints: Qt.ImhFormattedNumbersOnly

            onAccepted: {
                config.nominalSensitivity=textFieldNSensivity.text
                console.log("NSensitivity text",textFieldNSensivity.text,
                            "NSensitivity to C++", config.nominalSensitivity)
                Qt.inputMethod.hide();
                textFieldNSensivity.focus = false
            }

            onEditingFinished: {
                if (acceptableInput) {
                    config.nominalSensitivity=textFieldNSensivity.text
                    console.log("NSensitivity text",textFieldNSensivity.text,
                                "NSensitivity to C++", config.nominalSensitivity)

                } else {
                    invalidMsgBox.open();
                }
            }

        }
    }
    Text {
        id: nominalsensitivity
        text: "Nominal Sensitivity"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(109)
        y: setY(1010)
        opacity: 1
    }
    Text {
        text: "mV/Pa"
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1254)
        y: setY(1010)
        opacity: 1
    }

    /* N. S. Tolerance */
    Image {
        id: rect_tolerance
        source: "ConfigurationImages/rect_tolerance.png"
        x: setX(633)
        y: setY(1120)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        TextField {
            id: textFieldTolerance
            text: config.tolerance
            padding: 0
            smooth: true
            color: "#ffffff"
            anchors.fill: parent
            font.pixelSize: 10
            font.family: "Tahoma"
            placeholderTextColor:"#ffffff"
            background: Rectangle {
                implicitWidth: rect_tolerance.width
                implicitHeight: rect_tolerance.height
                color:"transparent"
                border.color: "transparent"
            }
            validator: RegExpValidator {
                // Parse double with . or , as decimal point
                regExp:/(\d{1,3})([.,]\d*)?$/
            }
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            onAccepted: {
                Qt.inputMethod.hide();
                textFieldTolerance.focus = false
            }
            onEditingFinished: {
                if (acceptableInput) {
                    config.tolerance=textFieldTolerance.text
                    console.log("tolerance",config.tolerance)

                } else {
                    invalidMsgBox.open();
                }
            }
        }
    }
    Text {
        text: "N. S. Tolerance"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(107)
        y: setY(1130)
        opacity: 1
    }
    Text {
        id: db
        text: "dB"
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1255)
        y: setY(1130)
        opacity: 1
    }

    /* Lab Sensitivity */
    Image {
        id: rect_lab_sens
        source: "ConfigurationImages/rect_lab_sens.png"
        x: setX(633)
        y: setY(1240)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1

        TextField {
            id: textFieldlabSensivity
            text: config.labSensitivity
            x: setX(1088)
            y: setY(1163.25)
            padding: 0
            smooth: true
            color: "#ffffff"
            anchors.fill: parent
            font.pixelSize: 10
            font.family: "Tahoma"
            placeholderTextColor:"#ffffff"
            property int iTextEdited: 1
            background: Rectangle {
                implicitWidth: rect_lab_sens.width
                implicitHeight: rect_lab_sens.height
                color:"transparent"
                border.color: "transparent"
            }
            validator: RegExpValidator {
                // Parse double with . or , as decimal point
                regExp:/(\d{1,3})([.,]\d*)?$/
            }
            inputMethodHints: Qt.ImhFormattedNumbersOnly

            onAccepted: {
                Qt.inputMethod.hide();
                textFieldlabSensivity.focus = false
            }

            onEditingFinished:{
                if (acceptableInput) {
                    config.labSensitivity=textFieldlabSensivity.text
                    console.log("labSensitivity",config.labSensitivity)
                    iTextEdited = 1
                } else {
                    invalidMsgBox.open();
                }
            }
            onPressed: {
                if(iTextEdited){
                    iTextEdited=0
                    messageDialog.open()
                }
            }
        }
    }
    Text {
        id: labSensitivity
        text: "Lab. Sensitivity"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(108)
        y: setY(1250)
        opacity: 1
    }
    Text {
        text: "mV/Pa"
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1256)
        y: setY(1250)
        opacity: 1
    }

    /* Lab Calibration Date */
    Image {
        id: rect_lab_calib_date
        source: "ConfigurationImages/rect_lab_calib_date.png"
        x: setX(633)
        y: setY(1360)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: separating_lines
        source: "ConfigurationImages/separating_lines.png"
        x: setX(745)
        y: setY(1362)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        text: "Lab. Calibration Date"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(108)
        y: setY(1370)
        opacity: 1
    }

    Image {
        id: icon_clock
        source: "ConfigurationImages/icon_clock.png"
        x: setX(1137)
        y: setY(1376)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        text: config.calibrationYear
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(981)
        y: setY(1380)
        opacity: 1
    }
    Text {
        text: config.calibrationDay
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(687)
        y: setY(1380)
        opacity: 1
    }
    Text {
        text: config.calibrationMonth
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(812)
        y: setY(1380)
        opacity: 1
    }

    /** Basic Parameters **/
    Text {
        id: basicparameters
        text: "Basic Parameters"
        font.pixelSize: 14
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(106)
        y: setY(1505)
        opacity: 1
    }
    Image {
        id: trace_config_02
        source: "ConfigurationImages/trace_config_02.png"
        x: setX(683)
        y: setY(1525)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /* Integration Time */
    Image {
        id: rect_int_time
        source: "ConfigurationImages/rect_int_time.png"
        x: setX(635)
        y: setY(1585)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1

        TextField {
            id: textFieldtime
            text: config.integrationTime
            padding: 0
            smooth: true
            color: "#ffffff"
            anchors.fill: parent
            font.pixelSize: 10
            font.family: "Tahoma"
            placeholderTextColor:"#ffffff"
            background: Rectangle {
                implicitWidth: rect_files_name.width
                implicitHeight: rect_files_name.height
                color:"transparent"
                border.color: "transparent"
            }
            inputMethodHints: Qt.ImhDigitsOnly
            onAccepted: {
                Qt.inputMethod.hide();
                textFieldtime.focus = false
            }
            onEditingFinished: {
                config.integrationTime=textFieldtime.text
                console.log("integrationTime",config.integrationTime)
            }
        }
    }
    Text {
        id: integrationtime
        text: "Integration Time"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(108)
        y: setY(1595)
        opacity: 1
    }
    Text {
        text: "Second(s)"
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1253)
        y: setY(1590)
        opacity: 1
    }

    /* Files Name */
    Image {
        id: rect_files_name
        source: "ConfigurationImages/rect_files_name.png"
        x: setX(635)
        y: setY(1705)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        TextField {
            id: textFieldname
            text: config.filesName
            padding: 0
            smooth: true
            color: "#ffffff"
            anchors.fill: parent
            font.pixelSize: 10
            font.family: "Tahoma"
            placeholderTextColor:"#ffffff"
            background: Rectangle {
                implicitWidth: rect_files_name.width
                implicitHeight: rect_files_name.height
                color:"transparent"
                border.color: "transparent"
            }
            onAccepted: {
                Qt.inputMethod.hide();
                textFieldname.focus = false
            }
            onTextEdited: {
                if(!textFieldname.text.includes("\\") &&
                        !textFieldname.text.includes("/") &&
                        !textFieldname.text.includes("\"") &&
                        !textFieldname.text.includes(":") &&
                        !textFieldname.text.includes("*") &&
                        !textFieldname.text.includes("?") &&
                        !textFieldname.text.includes("<") &&
                        !textFieldname.text.includes(">") &&
                        !textFieldname.text.includes("|")) {
                    config.filesName=textFieldname.text
                    console.log("filesName",config.filesName)

                } else {
                    invalidTextMsgBox.open()
                }
            }
        }
    }
    Text {
        id: filesname
        text: "Files Name"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(110)
        y: setY(1715)
        opacity: 1
    }

    /** Advanced Parameters **/
    Text {
        id: advancedparameters
        text: "Advanced Parameters"
        font.pixelSize: 14
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(101)
        y: setY(1850)
        opacity: 1
    }
    Image {
        id: trace_config_03
        source: "ConfigurationImages/trace_config_03.png"
        x: setX(683)
        y: setY(1870)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /* Time Weighting */
    Text {
        id: timeweighting
        text: "Time Weighting"
        font.pixelSize: 13
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(106)
        y: setY(1943.75)
        opacity: 1
    }

    Text {
        id: none
        text: "None"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(218)
        y: setY(2045.75)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                time_check_outline.visible=true
                time_check_center.visible=true

                time_check_outline.x=setX(135)
                time_check_outline.y=setY(2042)
                time_check_center.x=setX(143)
                time_check_center.y=setY(2050)

                config.timeWeighting="LP_NONE"
                none.color= "#0c85c7"
                fast.color="#ffffff"
                slow.color="#ffffff"
                impulse.color="#ffffff"
                console.log("time weighting",config.timeWeighting)
            }
        }
        Component.onCompleted: getTimeWeightingColor("LP_NONE")
    }
    Image {
        id: ellipse_none
        source: "ConfigurationImages/ellipse_none.png"
        x: setX(135)
        y: setY(2042)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                time_check_outline.visible=true
                time_check_center.visible=true

                time_check_outline.x=setX(135)
                time_check_outline.y=setY(2042)
                time_check_center.x=setX(143)
                time_check_center.y=setY( 2050)

                config.timeWeighting="LP_NONE"
                none.color= "#0c85c7"
                fast.color="#ffffff"
                slow.color="#ffffff"
                impulse.color="#ffffff"
            }
        }
    }

    Text {
        id: slow
        text: "Slow"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(517)
        y: setY(2043.75)
        opacity: 1
        MouseArea{
            anchors.fill: parent
            onClicked: {
                time_check_outline.visible=true
                time_check_center.visible=true

                time_check_outline.x=setX(435)
                time_check_outline.y=setY(2042)
                time_check_center.x= setX(443)
                time_check_center.y=setY(2050)

                config.timeWeighting="LP_SLOW"
                slow.color= "#0c85c7"
                fast.color="#ffffff"
                impulse.color="#ffffff"
                none.color= "#ffffff"
                console.log("time Weighting",config.timeWeighting)
            }
        }
        Component.onCompleted: getTimeWeightingColor("LP_SLOW")
    }
    Image {
        id: ellipse_slow
        source: "ConfigurationImages/ellipse_slow.png"
        visible: true
        x: setX(435)
        y: setY(2042)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                time_check_outline.visible=true
                time_check_center.visible=true

                time_check_outline.x=setX(435)
                time_check_outline.y=setY(2042)
                time_check_center.x= setX(443)
                time_check_center.y=setY(2050)

                config.timeWeighting="LP_SLOW"
                slow.color= "#0c85c7"
                fast.color="#ffffff"
                impulse.color="#ffffff"
                none.color= "#ffffff"
            }
        }
    }

    Text {
        id: fast
        text: "Fast"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(844)
        y: setY(2045.75)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                time_check_outline.visible=true
                time_check_center.visible=true

                time_check_outline.x=setX(760)
                time_check_outline.y=setY(2042)
                time_check_center.x= setX(768)
                time_check_center.y=setY(2050)

                config.timeWeighting="LP_FAST"
                fast.color= "#0c85c7"
                slow.color="#ffffff"
                impulse.color="#ffffff"
                none.color= "#ffffff"
                console.log("timeWeighting",config.timeWeighting)
            }
        }
        Component.onCompleted: getTimeWeightingColor("LP_FAST")
    }
    Image {
        id: ellipse_fast
        source: "ConfigurationImages/ellipse_fast.png"
        visible: true
        x: setX(760)
        y: setY(2042)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                time_check_outline.visible=true
                time_check_center.visible=true

                time_check_outline.x=setX(760)
                time_check_outline.y=setY(2042)
                time_check_center.x= setX(768)
                time_check_center.y=setY(2050)

                config.timeWeighting="LP_FAST"
                fast.color= "#0c85c7"
                slow.color="#ffffff"
                impulse.color="#ffffff"
                none.color= "#ffffff"
            }
        }
    }

    Text {
        id: impulse
        text: "Impulse"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(1129)
        y: setY(2043.75)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                time_check_outline.visible=true
                time_check_center.visible=true

                time_check_outline.x=setX(1047)
                time_check_outline.y=setY(2042)
                time_check_center.x= setX(1055)
                time_check_center.y=setY(2050)

                config.timeWeighting="LP_IMPULSE"
                impulse.color= "#0c85c7"
                fast.color="#ffffff"
                none.color= "#ffffff"
                slow.color="#ffffff"
                console.log("timeWeighting",config.timeWeighting)
            }
        }
        Component.onCompleted: getTimeWeightingColor("LP_IMPULSE")
    }
    Image {
        id: ellipse_impulse
        source: "ConfigurationImages/ellipse_impulse.png"
        visible: true
        x: setX(1047)
        y: setY(2042)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                time_check_outline.visible=true
                time_check_center.visible=true

                time_check_outline.x=setX(1047)
                time_check_outline.y=setY(2042)
                time_check_center.x= setX(1055)
                time_check_center.y=setY(2050)

                config.timeWeighting="LP_IMPULSE"
                impulse.color= "#0c85c7"
                fast.color="#ffffff"
                slow.color="#ffffff"
                none.color= "#ffffff"
            }
        }
    }

    Image {
        id: time_check_outline
        source: "ConfigurationImages/time_check_outline.png"
        visible: true
        x: {
            if(config.timeWeighting === "LP_SLOW") {
                setX(435)

            } else if(config.timeWeighting === "LP_FAST") {
                setX(760)

            } else if(config.timeWeighting === "LP_NONE") {
                setX(135)

            } else {
                setX(1047)
            }
        }
        y: setY(2042)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    Image {
        id: time_check_center
        source: "ConfigurationImages/time_check_center.png"
        visible: true
        x: {
            if(config.timeWeighting === "LP_SLOW") {
                setX(443)

            } else if(config.timeWeighting === "LP_FAST") {
                setX(768)

            } else if(config.timeWeighting === "LP_NONE") {
                setX(143)

            } else {
                setX(1055)
            }
        }
        y: setY(2050)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /* Spectrum Configuration */
    Text {
        id: spectrumconfiguration
        text: "Spectrum Configuration"
        font.pixelSize: 13
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(107)
        y: setY(2176.75)
        opacity: 1
    }

    Text {
        id: none1
        text: "None"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(218)
        y: setY(2278.75)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                spectrum_check_outline.visible=true
                spectrum_check_center.visible=true

                spectrum_check_outline.x=setX(135)
                spectrum_check_outline.y=setY(2267)
                spectrum_check_center.x= setX(143)
                spectrum_check_center.y=setY(2275)

                config.spectrumConfiguration="SPECTRUM_NO"
                none1.color="#0c85c7"
                octave.color="#ffffff"
                thirdoctave.color="#ffffff"
                // fft.color="#ffffff"
                console.log("spectrum",config.spectrumConfiguration)
            }
        }
        Component.onCompleted: getSpectrumColor("SPECTRUM_NO")
    }
    Image {
        id: ellipse_sp_none
        source: "ConfigurationImages/ellipse_sp_none.png"
        x: setX(135)
        y: setY(2267)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                spectrum_check_outline.visible=true
                spectrum_check_center.visible=true

                spectrum_check_outline.x=setX(135)
                spectrum_check_outline.y=setY(2267)
                spectrum_check_center.x= setX(143)
                spectrum_check_center.y=setY(2275)

                config.spectrumConfiguration="SPECTRUM_NO"
                none1.color="#0c85c7"
                octave.color="#ffffff"
                thirdoctave.color="#ffffff"
                //fft.color="#ffffff"
            }
        }
    }

    Text {
        id: octave
        text: "Octave"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(516)
        y: setY(2277.75)
        opacity: 1
        MouseArea{
            anchors.fill: parent
            onClicked: {
                spectrum_check_outline.visible=true
                spectrum_check_center.visible=true

                spectrum_check_outline.x=setX(435)
                spectrum_check_outline.y=setY(2267)
                spectrum_check_center.x= setX(443)
                spectrum_check_center.y=setY(2275)

                config.spectrumConfiguration="SPECTRUM_OCTAVE"
                octave.color="#0c85c7"
                thirdoctave.color="#ffffff"
                //fft.color="#ffffff"
                none1.color= "#ffffff"
                console.log("octave",config.spectrumConfiguration)
            }
        }
        Component.onCompleted: getSpectrumColor("SPECTRUM_OCTAVE")
    }
    Image {
        id: ellipse_sp_octave
        source: "ConfigurationImages/ellipse_sp_octave.png"
        visible: true
        x: setX(435)
        y: setY(2267)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                spectrum_check_outline.visible=true
                spectrum_check_center.visible=true

                spectrum_check_outline.x=setX(435)
                spectrum_check_outline.y=setY(2267)
                spectrum_check_center.x= setX(443)
                spectrum_check_center.y=setY(2275)

                config.spectrumConfiguration="SPECTRUM_OCTAVE"
                octave.color="#0c85c7"
                thirdoctave.color="#ffffff"
                //fft.color="#ffffff"
                none1.color= "#ffffff"
            }
        }
    }

    Text {
        id:thirdoctave
        text: "3rd Oct."
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(844)
        y: setY(2276.75)
        opacity: 1
        MouseArea{
            anchors.fill: parent
            onClicked: {
                spectrum_check_outline.visible=true
                spectrum_check_center.visible=true

                spectrum_check_outline.x=setX(760)
                spectrum_check_outline.y=setY(2267)
                spectrum_check_center.x= setX(768)
                spectrum_check_center.y=setY(2275)

                config.spectrumConfiguration="SPECTRUM_3RD_OCTAVE"
                thirdoctave.color="#0c85c7"
                //fft.color="#ffffff"
                none1.color= "#ffffff"
                octave.color="#ffffff"
                console.log("spectrumConfiguration",config.spectrumConfiguration)
            }
        }
        Component.onCompleted: getSpectrumColor("SPECTRUM_3RD_OCTAVE")
    }
    Image {
        id: ellipse_sp_3rd_octave
        source: "ConfigurationImages/ellipse_sp_3rd_octave.png"
        visible: true
        x: setX(760)
        y: setY(2267)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                spectrum_check_outline.visible=true
                spectrum_check_center.visible=true

                spectrum_check_outline.x=setX(760)
                spectrum_check_outline.y=setY(2267)
                spectrum_check_center.x= setX(768)
                spectrum_check_center.y=setY(2275)

                config.spectrumConfiguration="SPECTRUM_3RD_OCTAVE"
                thirdoctave.color="#0c85c7"
                octave.color="#ffffff"
                //fft.color="#ffffff"
                none1.color= "#ffffff"
            }
        }
    }

    Text {
        id: fft
        text: "FFT"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "black" //"darkGray"
        smooth: true
        x: setX(1129)
        y: setY(2278.75)
        opacity: 1
        //        MouseArea{
        //            anchors.fill: parent
        //            onClicked: {
        //                spectrum_check_outline.visible=true
        //                spectrum_check_center.visible=true
        //                spectrum_check_outline.x=setX(1143)
        //                spectrum_check_outline.y=setY(2267)
        //                spectrum_check_center.x= setX(1151)
        //                spectrum_check_center.y=setY(2275)

        //                config.spectrumConfiguration="SPECTRUM_FFT"
        //                fft.color="#0c85c7"
        //                octave.color="#ffffff"
        //                thirdoctave.color="#ffffff"
        //                none1.color= "#ffffff"
        //                console.log("spectrumConfiguration",config.spectrumConfiguration)
        //            }
        //        }
        //Component.onCompleted: getSpectrumColor("SPECTRUM_FFT")
    }
    Image {
        id: ellipse_sp_fft
        source: "ConfigurationImages/ellipse_sp_fft.png"
        x: setX(1047)
        y: setY(2267)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        //        MouseArea{
        //            anchors.fill:parent
        //            onClicked: {
        //                spectrum_check_outline.visible=true
        //                spectrum_check_center.visible=true
        //                spectrum_check_outline.x=setX(1143)
        //                spectrum_check_outline.y=setY(2267)
        //                spectrum_check_center.x= setX(1151)
        //                spectrum_check_center.y=setY(2275)

        //                config.spectrumConfiguration="SPECTRUM_FFT"
        //                fft.color="#0c85c7"
        //                octave.color="#ffffff"
        //                thirdoctave.color="#ffffff"
        //                none1.color= "#ffffff"
        //            }
        //        }
    }

    Image {
        id: spectrum_check_outline
        source: "ConfigurationImages/spectrum_check_outline.png"
        visible: true
        x: {
            if(config.spectrumConfiguration === "SPECTRUM_NO") {
                setX(135)

            } else if(config.spectrumConfiguration === "SPECTRUM_3RD_OCTAVE"){
                setX(760)

            } else if(config.spectrumConfiguration === "SPECTRUM_OCTAVE"){
                setX(435)

            } else {
                setX(1143)
            }
        }
        y: setY(2267)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    Image {
        id: spectrum_check_center
        source: "ConfigurationImages/spectrum_check_center.png"
        visible: true
        x: {
            if(config.spectrumConfiguration === "SPECTRUM_NO"){
                setX(143)

            } else if(config.spectrumConfiguration === "SPECTRUM_3RD_OCTAVE"){
                setX(768)

            } else if(config.spectrumConfiguration === "SPECTRUM_OCTAVE"){
                setX(443)

            } else {
                setX(1151)
            }
        }

        y: setY(2275)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /** Message Dialog **/
    MessageDialog {
        id: invalidTextMsgBox
        title: "Warrning: "
        text: "Please make sure the file name does not contain any of the below characters: \n
               \t\t\t\t \\ , / , \" , | , : , * , ? , < , > "
        onAccepted: {
            textFieldname.text=""
            invalidTextMsgBox.close()
        }
        visible: false
    }
    MessageDialog {
        id: invalidMsgBox
        title: "Warrning: "
        text: "Please enter a valid value. "
        onAccepted: {
            invalidMsgBox.close()
        }
        visible: false
    }
    MessageDialog {
        id: messageDialog
        title: "Warrning: "
        text: "This value should only be modified when changing the sensor.\n\n Pressing 'Yes' will erase the calibration
                correction attributed to the previous laboratory sensitivity value. "
        standardButtons: StandardButton.Yes | StandardButton.No
        onYes: {
            config.onLabSensitivityChanged()
            messageDialog.close()
        }
        onNo: {
            Qt.inputMethod.hide();
            textFieldlabSensivity.focus = false
            textFieldlabSensivity.text= "50.00"
            messageDialog.close()
        }

        visible: false
    }
}
