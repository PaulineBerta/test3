import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQuick.Window 2.12
import "./"

Item {
    id : swipeSource
    Loader{
        id :pageLoader
    }

    Image {
        id: superior_panel
        source: "SonometerImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }

    Image {
        id: inferior_panel_front
        source: "SonometerImages/inferior_panel_front.png"
        visible:false
        x: setX(245)
        y: setY(2425)-(applicationWindow.height-inferior_panel_front.height )
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    Item {
        id: swipeView
        x:0
        y:superior_panel.height
        height:applicationWindow.height-superior_panel.height-inferior_panel_front.height
        width:Screen.width

        SwipeView {
            id: swipeViewSonometer
            visible: true
            anchors.fill:parent
            interactive: true
            Sonometer{}
            SonometerTemporal{}
            SonometerFrequencies{}
            SonometerTables{}
        }
    }
}
