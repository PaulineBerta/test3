import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import "./"

Item {
    id :footerItem
    x:0
    y:applicationWindow.height-inferior_panel_front.height
    width:Screen.width
    height:inferior_panel_front.height

    // Reset button Enable variable (User can only reset the last RT Measure)
    property int iResetEnable: 1

    // User click on the Legend Button
    property bool bLegendButton: false

    Connections{
        target: audioget
        onButtonClickChanged: {
            if(audioget.screenState === 5) { // REVERBERATION RESULT

                switch(audioget.buttonClick) {
                case 540:
                    bLegendButton = false
                    break

                case 541:
                    bLegendButton = true
                    break

                default:
                    break
                }
            }
        }
    }

    Connections{
        target: config
        onCalibStateChanged:  {
            if(audioget.screenState === 1) { // CALIBRATION

                console.log("CalibStateChanged to "+ config.calibState)

                switch(config.calibState) {
                case 0:  // Calibration Ready
                    button_left.source="FooterImages/neutral_button.png"
                    button_center.source="FooterImages/play_button_on.png"
                    button_right.source="FooterImages/save_button_off.png"
                    break

                case 1:  // Calibration WORKING
                    button_left.source="FooterImages/neutral_button.png"
                    button_center.source="FooterImages/stop_button_on.png"
                    button_right.source="FooterImages/save_button_off.png"
                    break

                case 2:   // Calibration finished
                    button_left.source="FooterImages/neutral_button.png"
                    button_center.source="FooterImages/stop_button_on.png"
                    button_right.source="FooterImages/save_button_on.png"
                    break

                case 3:  // Calibration disabled
                    button_left.source="FooterImages/neutral_button.png"
                    button_center.source="FooterImages/stop_button_off.png"
                    button_right.source="FooterImages/save_button_off.png"
                    break
                }
            }
        }

        onCfgStateChanged:  {
            if(audioget.screenState === 6) { // CONFIGURATION
                console.log("CfgStateChanged to "+ config.cfgState)

                switch(config.cfgState) {
                case 0:
                    button_left.source="FooterImages/reset_button_on.png"
                    button_right.source="FooterImages/save_button_on.png"
                    break

                case 1:
                    button_left.source="FooterImages/reset_button_off.png"
                    button_right.source="FooterImages/save_button_off.png"
                    break
                }
            }
        }
    }

    Connections{
        target: audioget
        onSonoStateChanged:  {
            console.log("SonoStateChanged to "+ audioget.sonoState)
            switch(audioget.sonoState){
            case 0:
                console.log("***STANDBY***")
                    //Reset Button
                button_left.source="FooterImages/neutral_button.png"
                    //Play Button
                button_center.source="FooterImages/neutral_button.png"
                    //Stop Button
                button_right.source="FooterImages/neutral_button.png"

                audioget.setAudioCodified(0)
                break

            case 1:
                console.log("***READY***")
                if(audioget.screenState === 2) {
                        //Reset Button
                    button_left.source="FooterImages/reset_button_on.png"
                        //Play Button
                    button_center.source="FooterImages/play_button_on.png"
                        //Stop Button
                    button_right.source="FooterImages/rec_button_off.png"
                }

                audioget.setAudioCodified(0)
                break

            case 2:
                console.log("***LOGGING***")
                    //Reset Button
                button_left.source="FooterImages/reset_button_off.png"
                    //Play Button
                button_center.source="FooterImages/stop_button_on.png"
                    //Stop Button
                button_right.source="FooterImages/rec_button_enable.png"

                audioget.setAudioCodified(1)
                break

            case 3:
                console.log("***RECORDING***")
                    //Reset Button
                button_left.source="FooterImages/reset_button_off.png"
                    //Play Button
                button_center.source="FooterImages/stop_button_on.png"
                    //Stop Button
                button_right.source="FooterImages/rec_button_on.png"
                break

            case 4:
                console.log("***CALIBRATION***")
                audioget.setAudioCodified(0)
                break

            case 5:
                console.log("***REVERBERATION TIME READY***")
                audioget.setAudioCodified(0)
                break

            case 6:
                console.log("***REVERBERATION TIME RUNNING***")
                audioget.setAudioCodified(1)
                break

            case 7: //Not Implemented
                console.log("***FACTORY SETTINGS***")
                audioget.setAudioCodified(0)
                break

            case 8:
                console.log("***ERROR***")
                audioget.recover()
                break

            case 9://idle
                console.log("***IDLE MODE***")
                config.configAcousticMeas(false, false)
                audioget.launchNewMode()
                break
            }
        }

        onScreenStateChanged: {
            switch(audioget.screenState){
            case 0: // MAIN MENU
                button_left.source="FooterImages/neutral_button.png"
                button_center.source="FooterImages/neutral_button.png"
                button_right.source="FooterImages/neutral_button.png"

                //button_center.visible=true

                info_button.source="FooterImages/info_button_off.png"
                break

            case 1: // CALIBRATION
                button_left.source="FooterImages/neutral_button.png"
                button_center.source="FooterImages/play_button_on.png"
                button_right.source="FooterImages/save_button_off.png"

                //button_center.visible=true

                info_button.source="FooterImages/info_button_off.png"
                break

            case 2: // SONOMETER
                button_left.source="FooterImages/reset_button_on.png"
                button_center.source="FooterImages/play_button_on.png"
                button_right.source="FooterImages/rec_button_off.png"

                //button_center.visible=true

                info_button.source="FooterImages/info_button_off.png"
                break

            case 3: // REVERBERATION WAITING ROOM
                button_left.source="FooterImages/reset_button_off.png"
                button_center.source="FooterImages/play_button_on.png"
                button_right.source="FooterImages/save_button_off.png"

                //button_center.visible=true

                info_button.source="FooterImages/info_button_off.png"
                break

            case 4: // REVERBERATION MEASURE
                button_left.source="FooterImages/reset_button_off.png"
                button_center.source="FooterImages/play_button_on.png"
                button_right.source="FooterImages/save_button_off.png"

                //button_center.visible=true

                info_button.source="FooterImages/info_button_off.png"

                //Reinitialize Reset button Enable variable
                iResetEnable = 1
                break

            case 5: // REVERBERATION RESULT
                button_left.source="FooterImages/reset_button_on.png"
                button_center.source="FooterImages/play_button_on.png"
                button_right.source="FooterImages/save_button_on.png"

                //button_center.visible=true

                info_button.source="FooterImages/info_button_on.png"
                break

            case 6: // CONFIGURATION
                button_left.source="FooterImages/reset_button_on.png"
                button_center.source="FooterImages/neutral_button.png"
                button_right.source="FooterImages/save_button_on.png"

                //button_center.visible=true

                info_button.source="FooterImages/info_button_off.png"
                break

            case 7: // ABOUT
                button_left.source="FooterImages/neutral_button.png"
                button_center.source="FooterImages/neutral_button.png"
                button_right.source="FooterImages/neutral_button.png"

                //button_center.visible=true

                info_button.source="FooterImages/info_button_off.png"
                break

            case 8: // FILE BROWSER
                button_left.source="FooterImages/button_back_on.png"
                button_center.source="FooterImages/neutral_button.png"
                button_right.source="FooterImages/button_cancel_on.png"

                //button_center.visible=true

                info_button.source="FooterImages/info_button_off.png"
                break

            default:
                break
            }
        }

        onReverbStatusChanged: {
            if (audioget.screenState === 4) { // REVERBERATION MEASURE
                if(audioget.reverbStatus === "Ready") {
                    button_center.source="FooterImages/play_button_on.png"

                } else if(audioget.reverbStatus === "Measuring Noise Level") {
                    button_center.source="FooterImages/stop_button_on.png"

                } else if(audioget.reverbStatus === "Waiting Source") {
                    button_center.source="FooterImages/stop_button_on.png"

                } else if(audioget.reverbStatus === "Waiting Source Stabilized") {
                    button_center.source="FooterImages/stop_button_on.png"

                } else if(audioget.reverbStatus === "Measuring Source Level") {
                    button_center.source="FooterImages/stop_button_on.png"

                } else if(audioget.reverbStatus === "Waiting Stop Source") {
                    button_center.source="FooterImages/stop_button_on.png"

                } else if(audioget.reverbStatus === "Source Decrease Recording") {
                    button_center.source="FooterImages/stop_button_on.png"

                } else if(audioget.reverbStatus === "TR Measure") {
                    button_center.source="FooterImages/stop_button_on.png"

                } else if(audioget.reverbStatus === "Noise Level Too High") {
                    button_center.source="FooterImages/stop_button_on.png"

                } else if(audioget.reverbStatus === "Source Not Detected") {
                    button_center.source="FooterImages/stop_button_on.png"

                } else if(audioget.reverbStatus === "Source Never Stopped") {
                    button_center.source="FooterImages/stop_button_on.png"
                }
            }
        }
    }

    /* Background */
    Image {
        id: inferior_panel_back
        source: "FooterImages/inferior_panel_back.png"
        x: setX(0)
        y: setY(2458)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }
    Image {
        id: inferior_panel_front
        source: "FooterImages/inferior_panel_front.png"
        x: setX(245)
        y: setY(2425)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    /* Footer Button */
    Image {
        id: button_center
        source: "FooterImages/neutral_button.png"
        x: setX(655)
        anchors.verticalCenter: inferior_panel_front.verticalCenter
        //y: setY(2461)-(applicationWindow.height-inferior_panel_front.height)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1

        MouseArea{
            anchors.fill: parent
            property int iState: 1
            onClicked: {
                switch(audioget.screenState) {
                case 1: // CALIBRATION
                    audioget.buttonClick = 12 // Calibration Center Button

                    if(audioget.sonoState === 1){
                        audioget.reset()
                        config.startCalibration()
                        iState === 1

                    } else if(audioget.sonoState === 4){
                        if(config.calibState === 1){
                            config.stopCalibration(true)
                            audioget.resetChrono()

                        } else if(config.calibState === 2 && iState === 1){
                            config.stopAfterCorrection()
                            iState === 0
                        }
                    }
                    break

                case 2: // SONOMETER
                    console.log("***PLAY CLICKED*** State " + audioget.sonoState)

                    switch(audioget.sonoState){
                    case 1://ready
                        console.log("***PLAY|OPEN CSV***")
                        audioget.reset() // pas besoin? on reset déjà au début d'opencsv?
                        audioget.openCsv()

                        audioget.buttonClick = 221
                        break

                    case 2://logging
                        console.log("***PLAY|CLOSE CSV***")
                        audioget.stop()
                        audioget.reset()

                        audioget.buttonClick = 220
                        break

                    case 3://wav recording
                        console.log("***PLAY|Reset***")
                        audioget.onSusspended()
                        audioget.stop()
                        audioget.reset()

                        audioget.buttonClick = 220
                        break

                    default:
                        break
                    }
                    break

                case 3: // REVERBERATION WAITING ROOM
                    audioget.screenState = 4

                    audioget.reinitPlot()
                    //audioget.createDirProject
                    break

                case 4: // REVERBERATION MEASURE
                    console.log("***PLAY REVERBERATION CLICKED*** State " + audioget.sonoState)

                    switch(audioget.sonoState) {
                    case 5: // Reverberation time ready
                        console.log("***PLAY REVERB***")
                        //audioget.reset()
                        audioget.startReverbMode()
                        audioget.openCsv()
                        break

                    case 6: // Reverberation time running
                        console.log("***STOP REVERB***")
                        audioget.stop()
                        audioget.deleteCsv()
                        break

                    default:
                        break
                    }
                    break

                case 5: // REVERBERATION RESULT
                    audioget.screenState = 4

                    switch(audioget.sonoState) {
                    case 5: // Reverberation time ready
                        console.log("***PLAY REVERB***")
                        //audioget.reset()
                        audioget.startReverbMode()
                        audioget.openCsv()
                        break

                    default:
                        break
                    }

                    audioget.reinitPlot()
                    break

                default:
                    break
                }
            }
        }
    }

    Image {
        id: button_left
        source: "FooterImages/neutral_button.png"
        x: setX(358)
        anchors.verticalCenter: inferior_panel_front.verticalCenter
        //y: setY(2458)-(applicationWindow.height-inferior_panel_front.height)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1

        MouseArea{
            anchors.fill:parent
            onClicked: {
                switch(audioget.screenState) {
                case 2: // SONOMETER
                    console.log("***RESET CLICKED*** State" + audioget.sonoState)

                    switch(audioget.sonoState){
                    case 1: // Sonometer Ready
                        console.log("***RESET|RESET***")
                        audioget.onSusspended()
                        audioget.stop()
                        audioget.reset()
                        break

                    default:
                        break
                    }
                    break

                case 5: // REVERBERATION RESULT
                    if(iResetEnable == 1) {
                        audioget.resetLastTrMeasure()
                        iResetEnable = 0

                        button_left.source = "FooterImages/reset_button_off.png"
                    }

                    if(audioget.reverbCounter === 1) {
                        audioget.stopReverbCounter()
                        audioget.screenState = 4 // REVERB MEASURE
                    }

                    break

                case 6: // CONFIGURATION
                    audioget.buttonClick = 61 // Configuration Left Button
                    /** See ConfigSonometer.qml onButtonClickChanged **/
                    break

                case 8: // FILE BROWSER
                    audioget.buttonClick = 81 // FileBrowser Left Button
                    /** See FileBrowser.qml onButtonClickChanged **/
                    break

                default:
                    break
                }
            }

            onPressed: {
                if(audioget.sonoState === 1 && audioget.screenState === 6) { // CONFIGURATION, Ready
                    config.cfgState = 1
                }
            }

            onReleased: {
                if(audioget.sonoState === 1 && audioget.screenState === 6) { // CONFIGURATION, Ready
                    config.cfgState = 0
                }
            }
        }
    }

    Image {
        id: button_right
        source: "FooterImages/neutral_button.png"
        x: setX(951)
        anchors.verticalCenter: inferior_panel_front.verticalCenter
        //y: setY(2458)-(applicationWindow.height-inferior_panel_front.height)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1

        MouseArea{
            anchors.fill: parent
            onClicked: {
                console.log(" Click Right Button! ")
                switch(audioget.screenState) {
                case 1: // CALIBRATION

                    switch(audioget.sonoState){
                    case 4:
                        console.log("save"+config.calibState)

                        if(config.calibState === 2){
                            //Validation de la valeur de correction
                            config.stopCalibration(true)
                            config.validateCalibration()
                            audioget.reset()
                        }
                        break

                    default:
                        break
                    }
                    break

                case 2: // SONOMETER
                    console.log("***REC CLICKED*** State" + audioget.sonoState)

                    switch(audioget.sonoState){
                    case 2: //logging
                        console.log("***REC|OPEN WAV***")
                        audioget.record()
                        break

                    case 3: //wav recording
                        console.log("***REC|CLOSE WAV***")
                        audioget.onSusspended()
                        break

                    default:
                        break
                    }
                    break

                case 5: // REVERBERATION RESULT
                    console.log("increment reverb counter : "+audioget.reverbCounter)

                    audioget.stopReverbCounter()
                    //config.configNewMode()
                    config.configAcousticMeas(false, false) // reset calcul ne suffit pas, pourquoi?
                    audioget.screenState = 3
                    break

                case 6: // CONFIGURATION
                    console.log("save"+config.cfgState)

                    if(audioget.sonoState === 1){
                        console.log("set configuration start")
                        config.setconfiguration()
                        console.log("set configuration done")
                    }
                    break

                case 8: // FILE BROWSER
                    audioget.buttonClick = 83 // FileBrowser Right Button
                    /** See FileBrowser.qml onButtonClickChanged **/
                    break

                default:
                    break
                }
            }

            onPressed: {
                if(audioget.sonoState === 1 && audioget.screenState === 6) { // CONFIGURATION, Ready
                    config.cfgState=1
                }
            }

            onReleased: {
                if(audioget.sonoState === 1 && audioget.screenState === 6) { // CONFIGURATION, Ready
                    config.cfgState=0
                }
            }
        }
    }

    Image {
        id: screenshot_button
        source: "FooterImages/screenshot_button_off.png"
        x: setX(1315)
        y: setY(2514)-(applicationWindow.height-inferior_panel_front.height)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1

        MouseArea{
            anchors.fill: parent
            onClicked: {
                /*console.log("********* save image *********")
                ScreenShot.capture()*/
            }
            //onPressed: screenshot_button.source="FooterImages/screenshot_button_off.png"
            //onReleased: screenshot_button.source = "FooterImages/screenshot_button_enable.png"
        }
    }

    Image {
        id: info_button
        source:"FooterImages/info_button_off.png"
        x: setX(75)
        y: setY(2505)-(applicationWindow.height-inferior_panel_front.height)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1

        MouseArea{
            anchors.fill: parent
            onClicked: {
                console.log(" Click Legend Button! ")

                if(audioget.sonoState===2 || audioget.sonoState===3){
                    switch(audioget.codified){
                    case 1:
                        audioget.setAudioCodified(2)
                        break

                    case 2:
                        audioget.setAudioCodified(1)
                        break

                    default:
                        break
                    }
                }


                switch(audioget.screenState) {
                case 5: // REVERBERATION RESULT
                    if (bLegendButton === false) {
                        audioget.buttonClick = 541; // Legend Button On

                    } else if (bLegendButton === true) {
                        audioget.buttonClick = 540; // Legend Button Off
                    }
                    break

                default:
                    break
                }
            }
        }
    }

    /*Rectangle{
        id:mousezone
        visible: true
        color: "transparent"
        x: setX(0)
        y: setY(2458)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(245)
        height:setY(info_button.height+200)

        MouseArea{
            anchors.fill: parent
            onClicked:{
                if(audioget.sonoState===2 || audioget.sonoState===3){
                    switch(audioget.codified){
                    case 1:
                        audioget.setAudioCodified(2)
                        break

                    case 2:
                        audioget.setAudioCodified(1)
                        break

                    default:
                        break
                    }
                }
            }
        }
    }*/
}
