import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQuick.Window 2.12
import QtQuick.Dialogs 1.1
import "./"

Item {
    id: animationScreen
    x:0
    y:0
    width:Screen.width
    height:applicationWindow.height
    visible: false

    Connections{
        target: audioget
        onAtomicStateChanged:{
            console.log("onAtomicStateChanged : "+ audioget.atomicState)
            switch(audioget.atomicState){
            case 0:
                console.log("ERROR from QML")
                animationSablier.visible=false

                icon_danger.visible=true
                background_error.visible=true
                error.visible=true
                acquisition.visible=true

                acquisition.text=" Not Found"
                error.text="ATomic"

                disconnectMessageDialog.open()
                break

            case 1:
                console.log("ATomic Found")
                disconnectMessageDialog.close()

                icon_danger.visible=false
                background_error.visible=false
                error.visible=false
                acquisition.visible=false

                animationScreen.visible=true
                animationScreen.focus=true
                animationSablier.visible=true
                animationSablier.playing=true
                animationTimer.start()

                config.setConfigurationAtStart()
                audioget.reset()
                break

            default:
                break
            }
        }
    }

    Image{
        id: animationSablierBackground
        source: "GIF/background.png"
        x:0
        y:0
        width: Screen.width
        height:Screen.height
    }

    AnimatedImage {
        id: animationSablier
        source: "GIF/animationSablier.gif"
        anchors.centerIn: animationSablierBackground
        x: (Screen.width/2)
        y: setY(1006)
        visible: false
        playing: false
    }

    Timer{
        id: animationTimer
        running: false
        interval: 5000
        repeat: false
        onTriggered: {
            animationSablier.playing=false
            animationSablier.visible=false

            animationScreen.visible=false
            animationScreen.focus=false

            audioget.resetChrono()
            audioget.reset()
        }
    }

    MessageDialog {
        id: disconnectMessageDialog
        title: "ATomic not found"
        text: "Connect the device or press Cancel to quit"
        standardButtons: StandardButton.Cancel
        onRejected: {
            Qt.quit()
        }
        visible: false
    }
}

