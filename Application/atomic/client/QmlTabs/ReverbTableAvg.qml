import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12

Item {
    id:reverbAvgTableRect
    width:Screen.width
    height:if(config.spectrumConfiguration === "SPECTRUM_3RD_OCTAVE") {
               760
           } else if (config.spectrumConfiguration === "SPECTRUM_OCTAVE") {
               200
           }

    anchors.margins: 10

    Connections {
        target: audioget
        property int iSaveReverbCounter: 0
        onReverbCounterChanged: {
            if(audioget.reverbCounter === 0) {
                // Reset the memory counter
                iSaveReverbCounter = 0

            } else if(audioget.reverbCounter > iSaveReverbCounter) {
                // Keep in memory the reverb counter last index when we add a new RT Measure
                iSaveReverbCounter = audioget.reverbCounter
            }
        }

        property bool bResetReverbMeasure: false
        onReverbMeasureChanged: {
            if(audioget.reverbCounter < iSaveReverbCounter) {
                // Indicator to remove the last RT Measure
                bResetReverbMeasure = true
                // Keep in memory the reverb counter last index when we remove the last RT Measure
                iSaveReverbCounter = audioget.reverbCounter
            } else {
                bResetReverbMeasure = false
            }

            // Update the Rt Measure Plot
            reverbAvgTableModel.updateReverbActiveMeasure(audioget.reverbMeasure, bResetReverbMeasure)
        }
    }

    TableView {
        id: tableView
        anchors.fill: reverbAvgTableRect
        anchors.centerIn: parent
        columnSpacing: 0.0
        rowSpacing: 0.0
        clip: false
        height:if(config.spectrumConfiguration === "SPECTRUM_3RD_OCTAVE") {
                   760
               } else if (config.spectrumConfiguration === "SPECTRUM_OCTAVE") {
                   200
               }

        interactive: false
        model: reverbAvgTableModel

        delegate: Rectangle {

            implicitWidth: Screen.width/4
            implicitHeight: 20
            border.color: "black"
            border.width: 0.6
            color: (heading === true)?"lightgray":"white"

            Connections{
                target: reverbAvgTableModel
                onOverloadChanged:{
                    textId.color="#272727"
                    switch(reverbAvgTableModel.overload) {
                    case 1:
                        if(index>37 & index<74){textId.color="red"}
                        break

                    case 2:
                        if(index>74 & index<111){textId.color="red"}
                        break

                    case 3:
                        if(index>37 & heading === false){textId.color="red"}
                        break

                    default: textId.color="#272727"
                    }
                }
            }

            Text {
                id: reverbTextId
                text: tableData
                font.pixelSize: 11
                font.family: "Tahoma-Bold"
                font.bold: true
                color: "#272727"
                smooth: true
                anchors.centerIn: parent
            }
        }
    }
}
