/**
*******************************************************************************
** @file    main.cpp
** @version
** @author  Wevioo SE Team
** @brief   request needed permissions, connect to the host node in order to
**           properly execute the application
**
*******************************************************************************
**/
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQuickWindow>
#include <QQmlContext>

#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#include <QtAndroid>
#include <QRemoteObjectNode>
#include "rep_atomic_replica.h"
#include "rep_configuration_replica.h"
#include <QDebug>
#include "AudioRec/AudioRec.h"
#include "AudioScreenShots/ScreenCapture.h"
#include "AudioGraph/TableModel.h"
#include "AudioGraph/TableModelBA.h"
#include "AudioPlot/BarQuick2.h"
#include "AudioPlot/ChartQuick2.h"
#include "AudioPlot/ChartQuick2BA.h"
#include "AudioPlot/ChartQuick2BAResult.h"

int main(int argc, char *argv[])
{
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QApplication* app= new QApplication(argc, argv);
  QQmlApplicationEngine engine;

  //////////////////////////////////////////////////////////////////////////////////
  /// Android Permissions //////////////////////////////////////////////////////////
  const QVector<QString> permissions({"android.permission.RECORD_AUDIO",
                                      "android.permission.WRITE_EXTERNAL_STORAGE"});

  for(const QString &permission : permissions){
      auto result = QtAndroid::checkPermission(permission);
      if(result == QtAndroid::PermissionResult::Denied){
          auto resultHash = QtAndroid::requestPermissionsSync(QStringList({permission}));
        }
    }

  //////////////////////////////////////////////////////////////////////////////////
  /// Launch ForegroundService /////////////////////////////////////////////////////

  QAndroidJniObject::callStaticMethod<void>("com/AllianTech/ATomicSound/ForegroundService",
                                            "startMyService",
                                            "(Landroid/content/Context;)V",
                                            QtAndroid::androidActivity().object());

  //////////////////////////////////////////////////////////////////////////////////
  /// Prepare Nodes used to comunicate between client and server(foregroundService)

  QRemoteObjectNode repNode;
  repNode.connectToNode(QUrl(QStringLiteral("local:atomic")));
  QSharedPointer<atomicReplica> rep(repNode.acquire<atomicReplica>());

  QRemoteObjectNode repNode2;
  repNode2.connectToNode(QUrl(QStringLiteral("local:configuration")));
  QSharedPointer<configurationReplica> repCfg(repNode2.acquire<configurationReplica>());

  engine.rootContext()->setContextProperty("audioget", rep.data());
  engine.rootContext()->setContextProperty("config", repCfg.data());

  AudioRec audioRec(nullptr, rep, repCfg);
  engine.rootContext()->setContextProperty("audioRec", &audioRec);

  rep->waitForSource();
  repCfg->waitForSource();

  //QString TRCounter = "TRCounter";

  TableModel tableModel(nullptr, rep, 37);
  TableModel scndTableModel(nullptr, rep, 12);
  TableModelBA reverbTableModel(nullptr, rep, "TRCounter");
  TableModelBA reverbAvgTableModel(nullptr, rep, "TRAverage");
  engine.rootContext()->setContextProperty("tableModel",&tableModel);
  engine.rootContext()->setContextProperty("tableModel2",&scndTableModel);
  engine.rootContext()->setContextProperty("reverbTableModel",&reverbTableModel);
  engine.rootContext()->setContextProperty("reverbAvgTableModel",&reverbAvgTableModel);

  qmlRegisterType<ChartQuick2>("ChartQuick2", 1, 0, "ChartQuick2");
  qmlRegisterType<ChartQuick2BA>("ChartQuick2BA", 1, 0, "ChartQuick2BA");
  qmlRegisterType<ChartQuick2BAResult>("ChartQuick2BAResult", 1, 0, "ChartQuick2BAResult");
  qmlRegisterType<BarQuick2>("BarQuick2", 1, 0, "BarQuick2");

  // Load QML ///////////////////////////////////////////////////////////////////////////
  engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

  /** SONOMETER TABLE MODEL **/
  //QObject::connect(rep.data(),SIGNAL(octaveChanged(int)),&tableModel,SLOT(updateOctave(int)));

  /** REVERBERATION TABLE MODEL **/
  /*QObject::connect(rep.data(),SIGNAL(octaveChanged(int)),&reverbTableModel,SLOT(updateOctave(int)));
  QObject::connect(rep.data(),SIGNAL(octaveChanged(int)),&reverbAvgTableModel,SLOT(updateOctave(int)));*/

  /** BAR QUICK **/
  QObject* plotBar= engine.rootObjects().first()->findChild<QObject *>("plot");
  if(plotBar) {
      qDebug()<<"object value"<<plotBar;
      QObject::connect(rep.data(), SIGNAL(passData(AcousticRes)), plotBar, SLOT(passData(AcousticRes)));
      QObject::connect(rep.data(), SIGNAL(octaveChanged(int)), plotBar, SLOT(setCfgOctave(int)));
  }

  /** CHART QUICK **/
  QObject* plotChart= engine.rootObjects().first()->findChild<QObject *>("plotChart");
  if(plotChart) {
      qDebug()<<"object value chart"<<plotChart;
      QObject::connect(rep.data(), SIGNAL(passDataFIT(QVector<double>)), plotChart, SLOT(passDataFIT(QVector<double>)));
      QObject::connect(rep.data(), SIGNAL(onReset()), plotChart, SLOT(Initialize()));
  }

  /** CHART QUICK BA **/
  QObject* plotChartBA= engine.rootObjects().first()->findChild<QObject *>("plotChartBA");
  if(plotChartBA) {
      qDebug()<<"object value chart reverb"<<plotChartBA;
      QObject::connect(rep.data(), SIGNAL(passDataFITBA(QVector<double>)), plotChartBA, SLOT(populateBA(QVector<double>)));
      QObject::connect(rep.data(), SIGNAL(onReset()), plotChartBA, SLOT(InitializeBA()));
  }

  /** CHART QUICK BA RESULT **/
  QObject* plotChartBAResult= engine.rootObjects().first()->findChild<QObject *>("plotChartBAResult");
  if(plotChartBAResult) {
      qDebug()<<"object value chart reverb"<<plotChartBAResult;
      QObject::connect(rep.data(), SIGNAL(passBufferFITBA(QVector<QVector<double>>,bool,int)), plotChartBAResult, SLOT(populateLAeqCurve(QVector<QVector<double>>,bool,int)));
      QObject::connect(rep.data(), SIGNAL(passRegressionLines(RegressionLines)), plotChartBAResult, SLOT(populateRegLines(RegressionLines)));
      QObject::connect(rep.data(), SIGNAL(passReverberationRes(ReverberationRes, ReverbAverageRes)), plotChartBAResult, SLOT(populateTRValues(ReverberationRes, ReverbAverageRes)));
      QObject::connect(rep.data(), SIGNAL(passCounterSinceStartTR(int)), plotChartBAResult, SLOT(getCounterSinceStartTR(int)));
      QObject::connect(rep.data(), SIGNAL(passReverbNoise(QVector<double>)), plotChartBAResult, SLOT(populateReverbNoise(QVector<double>)));
      QObject::connect(rep.data(), SIGNAL(passReverbSource(QVector<double>)), plotChartBAResult, SLOT(populateReverbSource(QVector<double>)));
      QObject::connect(rep.data(), SIGNAL(saveReverbActiveMeasure()), plotChartBAResult, SLOT(saveReverbActiveMeasure()));
      QObject::connect(rep.data(), SIGNAL(reinitPlotList()), plotChartBAResult, SLOT(reinitPlotList()));
      QObject::connect(rep.data(), SIGNAL(octaveChanged(int)), plotChartBAResult, SLOT(setCfgOctave(int)));
      QObject::connect(rep.data(), SIGNAL(onReset()), plotChartBAResult, SLOT(InitializeBAResult()));

      //QObject::connect(plotChartBAResult, SIGNAL(saveCsv()), rep.data(), SLOT(saveCsv()));
  }

  QObject *topLevel = engine.rootObjects().value(0);
  QQuickWindow *window = qobject_cast<QQuickWindow *>(topLevel);

  ScreenCapture launcher (window);
  engine.rootContext()->setContextProperty("ScreenShot", &launcher);
  window->show();

  QObject::connect(app, SIGNAL(aboutToQuit()), &audioRec, SLOT(quitHandler()));

  if (engine.rootObjects().isEmpty())
    return -1;
  return app->exec();

  qDebug()<<"sortie main";
}
