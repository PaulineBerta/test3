import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQml 2.12
import ChartQuick2 1.0

Item {
    id:sonometerTemporalScreen
    x:0
    y:0
    width:Screen.width
    height:Screen.height

    FontLoader { source: "qrc:/Polices/Tahoma.ttf" }
    FontLoader { source: "qrc:/Polices/digital.ttf" }
    FontLoader { source: "qrc:/Polices/TahomaBold.ttf" }
    FontLoader { source: "qrc:/Polices/AllerBd.ttf" }

    /** Background **/
    Image {
        id: superior_panel
        source: "SonometerTemporalImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }
    Image {
        id: inferior_panel_front
        source: "SonometerTemporalImages/inferior_panel_front.png"
        visible: false
        x: setX(245)
        y: setY(2425)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }
    /*Image {
        id: int_time_and_global_background
        source: "SonometerTemporalImages/int_time_and_global_background.png"
        x: setX(45)
        y: setY(75)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }*/

    /** Decibels Values Background **/
    /*Image {
        id: decibels_values_background_01
        source: "SonometerTemporalImages/decibels_values_background.png"
        x: setX(125)
        y: setY(517)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: decibels_values_background_02
        source: "SonometerTemporalImages/decibels_values_background.png"
        x: setX(796)
        y: setY(517)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }*/

    /** Decibels Values Area **/

    /* Laeq */
    /*Text {
        text: "LAeqT (dB)"
        font.pixelSize: 12
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(403)
        y: setY(744)-superior_panel.height
        opacity: 1
    }
    Text {
        text: audioget.lAeqTUNDERLOADCHAR
        font.pixelSize: 30
        font.family: "Digital-7"
        color: "#f83f07"
        x: setX(133)
        y: setY(558.25)-superior_panel.height
        opacity: 1
    }
    Text {
        text: audioget.lAeqT
        font.pixelSize: 54
        font.family: "Digital-7"
        color: "#f83f07"
        smooth: true
        x: setX(277)
        y: setY(568.25)-superior_panel.height
        opacity: 1
        onTextChanged:
            if(audioget.overload === 3) {
                decibels_values_background_01.source="SonometerTemporalImages/decibels_values_warning_background.png"
                decibels_values_background_02.source="SonometerTemporalImages/decibels_values_warning_background.png"

            } else if(audioget.overload === 2) {
                decibels_values_background_01.source="SonometerTemporalImages/decibels_values_background.png"
                decibels_values_background_02.source="SonometerTemporalImages/decibels_values_warning_background.png"

            } else if(audioget.overload === 1) {
                decibels_values_background_01.source="SonometerTemporalImages/decibels_values_warning_background.png"
                decibels_values_background_02.source="SonometerTemporalImages/decibels_values_background.png"

            } else {
                decibels_values_background_01.source="SonometerTemporalImages/decibels_values_background.png"
                decibels_values_background_02.source="SonometerTemporalImages/decibels_values_background.png"
            }
    }*/

    /* LCeq */
    /*Text {
        text: "LCeqT (dB)"
        font.pixelSize: 12
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(411)
        y: setY(950)-superior_panel.height
        opacity: 1
    }
    Text {
        text: audioget.lCeqTUNDERLOADCHAR
        font.pixelSize: 20
        font.family: "Digital-7"
        color:"#ff8500"
        x: setX(298)
        y: setY(826.25)-superior_panel.height
        opacity: 1
    }
    Text {
        text: audioget.lCeqT
        font.pixelSize: 35
        font.family: "Digital-7"
        color: "#ff8500"
        smooth: true
        x: setX(442)
        y: setY(836.25)-superior_panel.height
        opacity: 1
    }/*

    /* LCpeak */
    /*Text {
        text: "LCpeakT (dB)"
        font.pixelSize: 12
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(360)
        y: setY(1139)-superior_panel.height
        opacity: 1
    }
    Text {
        text: audioget.lCpeakTUNDERLOADCHAR
        font.pixelSize: 20
        font.family: "Digital-7"
        color:"#fdfd2c"
        x: setX(298)
        y: setY(1016.25)-superior_panel.height
        opacity: 1
    }
    Text {
        text: audioget.lCpeakT
        font.pixelSize: 35
        font.family: "Digital-7"
        color: "#fdfd2c"
        smooth: true
        x: setX(442)
        y: setY(1026.25)-superior_panel.height
        opacity: 1
    }*/

    /* LAeq Global */
    /*Text {
        text: "LAeqG (dB)"
        font.pixelSize: 12
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1078)
        y: setY(744)-superior_panel.height
        opacity: 1
    }
    Text {
        text: audioget.lAeqGUNDERLOADCHAR
        font.pixelSize: 30
        font.family: "Digital-7"
        color:"#0092ff"
        x: setX(841)
        y: setY(558.25)-superior_panel.height
        opacity: 1
    }
    Text {
        text: audioget.lAeqG
        font.pixelSize: 54
        font.family: "Digital-7"
        color: "#0092ff"
        smooth: true
        x: setX(985)
        y: setY(568.25)-superior_panel.height
        opacity: 1
    }*/

    /* LCeq Global */
    /*Text {
        text: "LCeqG (dB)"
        font.pixelSize: 12
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1078)
        y: setY(950)-superior_panel.height
        opacity: 1
    }
    Text {
        text: audioget.lCeqGUNDERLOADCHAR
        font.pixelSize: 20
        font.family: "Digital-7"
        color:"#579bd8"
        x: setX(969)
        y:  setY(826)-superior_panel.height
        opacity: 1
    }
    Text {
        text: audioget.lCeqG
        font.pixelSize: 35
        font.family: "Digital-7"
        color: "#579bd8"
        smooth: true
        x: setX(1113)
        y: setY(836)-superior_panel.height
        opacity: 1
    }*/

    /* LCpeak Global */
    /*Text {
        text: "LCpeakG (dB)"
        font.pixelSize: 12
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1027)
        y: setY(1139)-superior_panel.height
        opacity: 1
    }
    Text {
        text: audioget.lCpeakGUNDERLOADCHAR
        font.pixelSize: 20
        font.family: "Digital-7"
        color: "#adcff0"
        x: setX(969)
        y: setY(1016.25)-superior_panel.height
        opacity: 1
    }
    Text {
        text: audioget.lCpeakG
        font.pixelSize: 35
        font.family: "Digital-7"
        color: "#adcff0"
        smooth: true
        x: setX(1113)
        y: setY(1026.25)-superior_panel.height
        opacity: 1
    }*/

    /** Graph Legend **/
    Image {
        id: legend
        source: "SonometerTemporalImages/legend.png"
        x: setX(847)
        y: setY(720)-superior_panel.height //setY(1349)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        id: laeq
        text: "LAeq"
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(902)
        y: setY(720)-superior_panel.height //setY(1349)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lceq
        text: "LCeq"
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1077)
        y: setY(720)-superior_panel.height //setY(1349)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lcpeak
        text: "LCpeak"
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1239)
        y: setY(720)-superior_panel.height //setY(1349)-superior_panel.height
        opacity: 1
    }
    Text {
        id: timehistory
        text: "Time History"
        font.pixelSize: 10
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(143) //133
        y: setY(720)-superior_panel.height //setY(1349)-superior_panel.height
        opacity: 1
    }
    Text {
        id: db
        text: "dB"
        font.pixelSize: 10
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(55)
        y: setY(720)-superior_panel.height //670
        opacity: 1
    }

    /** Temporal Graph **/
    Image {
        id: graph_lines
        source: "SonometerTemporalImages/graph_lines.png"
        x: setX(52)
        y: setY(1387)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        visible: false
        opacity: 0
    }
    ChartQuick2 {
        id: qwtPlot
        objectName: "plotChart"
        x: setX(75)
        y: setY(770)-superior_panel.height //720
        width:graph_lines.width
        height:graph_lines.height*2.2

        Component.onCompleted: {
            initQwtPlot()
            console.log("%%%%%%%% INIT PLOT %%%%%%%%%")
            qwtPlot.chartWidth=qwtPlot.width
        }
    }

    /** Marker Area **/
    Image {
        id: marker_time_area
        source: "SonometerTemporalImages/marker_time_area.png"
        x: setX(632)
        y: setY(2012)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        text: qwtPlot.time
        anchors.horizontalCenter: marker_time_area.horizontalCenter
        font.pixelSize: 10
        font.family: "Arial-BoldMT"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(685)
        y: setY(2034.5)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }

    Image {
        id: marker_values_area
        source: "SonometerTemporalImages/marker_values_area.png"
        x: setX(228)
        y: setY(2087)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: marker_right
        source: "SonometerTemporalImages/marker_right.png"
        x: setX(1333)
        y: setY(2114)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea {
            anchors.fill: parent
            onClicked: qwtPlot.step(1)
        }
    }
    Image {
        id: marker_left
        source: "SonometerTemporalImages/marker_left.png"
        x: setX(86)
        y: setY(2114)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea {
            anchors.fill: parent
            onClicked: qwtPlot.step(-1)
        }
    }

    /* Decibel Values */
    Text {
        text: "LAeq (dB)"
        Layout.alignment :Qt.AlignHCenter | Qt.AlignVCenter
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(351) //x: setX(309)
        y: setY(2200)-superior_panel.height
        opacity: 1
    }
    Text {
        text: qwtPlot.lAeqValue
        Layout.alignment :Qt.AlignHCenter | Qt.AlignVCenter
        font.pixelSize: 29
        font.family: "Digital-7"
        color: "#fa3500"
        smooth: true
        x: setX(351)
        y: setY(2109.5)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }
    Text {
        text: "LCeq (dB)"
        Layout.alignment :Qt.AlignHCenter | Qt.AlignVCenter
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(686) //x: setX(726)
        y: setY(2200)-superior_panel.height
        opacity: 1
    }
    Text {
        text: qwtPlot.lCeqValue
        Layout.alignment :Qt.AlignHCenter | Qt.AlignVCenter
        font.pixelSize: 29
        font.family: "Digital-7"
        color: "#ff7a00"
        smooth: true
        x: setX(686)
        y: setY(2109.5)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }
    Text {
        text: "LCpeak (dB)"
        Layout.alignment :Qt.AlignHCenter | Qt.AlignVCenter
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1021) //x: setX(1025)
        y: setY(2200)-superior_panel.height
        opacity: 1
    }
    Text {
        text: qwtPlot.lCpeakValue
        Layout.alignment :Qt.AlignHCenter | Qt.AlignVCenter
        font.pixelSize: 29
        font.family: "Digital-7"
        color: "#fffc00"
        smooth: true
        x: setX(1021)
        y: setY(2109.5)-superior_panel.height
        opacity: 1
    }

    /** Window Indicator **/
    Image {
        id: other_window_indicator_01
        source: "SonometerTemporalImages/other_window_indicator_01.png"
        x: setX(583)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: blue_window_indicator
        source: "SonometerTemporalImages/blue_window_indicator.png"
        x: setX(679)
        y: setY(2321)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_02
        source: "SonometerTemporalImages/other_window_indicator_02.png"
        x: setX(783)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_03
        source: "SonometerTemporalImages/other_window_indicator_03.png"
        x: setX(883)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
}
