import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import "./"

Item {
    id: aboutscroll
    x:0
    y:0
    width:Screen.width
    height:applicationWindow.height

    Flickable{
        id: myFlickable
        x: setX(0)
        y: setY(0)
        width:Screen.width
        height:applicationWindow.height
        contentWidth:Screen.width
        contentHeight:(applicationWindow.height+11300)
        interactive: true
        boundsBehavior: Flickable.StopAtBounds

        ScrollBar.vertical: ScrollBar {
            policy: ScrollBar.AlwaysOn
        }
        Column{
            About{
            }
        }
    }
}
