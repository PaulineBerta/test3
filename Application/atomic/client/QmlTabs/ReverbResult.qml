import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQml 2.12
import ChartQuick2BAResult 1.0
import QtQuick.Dialogs 1.1

Item {
    id: reverbItem
    x:0
    y:superior_panel.height
    width:Screen.width
    height:Screen.height

    FontLoader { source: "qrc:/Polices/Tahoma.ttf" }
    FontLoader { source: "qrc:/Polices/digital.ttf" }
    FontLoader { source: "qrc:/Polices/TahomaBold.ttf" }
    FontLoader { source: "qrc:/Polices/AllerBd.ttf" }

    Connections{
        target: audioget
        onButtonClickChanged: {
            if(audioget.screenState === 5) { // REVERBERATION RESULT

                switch(audioget.buttonClick) {
                case 540:
                    legendReverb.visible=false
                    console.log(" LEGEND FALSE ")
                    break

                case 541:
                    legendReverb.visible=true
                    console.log(" LEGEND TRUE ")
                    break

                default:
                    break
                }

                audioget.buttonClick=0 // None
            }
        }

        onScreenStateChanged: {
            if(audioget.screenState !== 5) {
                // Reinitialization ReverbResult screen display
                qwtPlotReverbResult.reinitQwtPlotBAResult()

                reverb_type.text = "T20 Measure"

                tr_value.color = "#51A8FF"
                unit_sec.color = "#51A8FF"
            }
        }

        property int iSaveReverbCounter: 0
        onReverbCounterChanged: {
            if(audioget.reverbCounter === 0) {
                // Reset the memory counter
                iSaveReverbCounter = 0

            } else if(audioget.reverbCounter > iSaveReverbCounter) {
                // Keep in memory the reverb counter last index when we add a new RT Measure
                iSaveReverbCounter = audioget.reverbCounter
            }
        }

        property bool bResetReverbMeasure: false
        onReverbMeasureChanged: {
            if(audioget.reverbCounter < iSaveReverbCounter) {
                // Indicator to remove the last RT Measure
                bResetReverbMeasure = true
                // Keep in memory the reverb counter last index when we remove the last RT Measure
                iSaveReverbCounter = audioget.reverbCounter
            } else {
                bResetReverbMeasure = false
            }

            // Update the Rt Measure Plot
            qwtPlotReverbResult.updateReverbActiveMeasure(audioget.reverbMeasure, bResetReverbMeasure)
        }
    }

    Connections{
        target: config
        onChartTimeChanged:  {
            qwtPlotReverbResult.initQwtPlotBAResult(config.chartTime)
        }
    }

    Image {
        id: superior_panel
        source: "ReverberationImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }

    /** Title **/
    Text {
        id: reverb_type
        text: "T20 Measure"
        anchors.horizontalCenter: parent.horizontalCenter
        y: setY(660)-superior_panel.height
        font.pixelSize: 18
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        opacity: 1
    }

    /** Graph Legend **/
    Text {
        id: timehistory
        text: "Time History"
        font.pixelSize: 10
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(143)
        y: setY(720)-superior_panel.height
        opacity: 1
    }
    Text {
        id: db
        text: "dB"
        font.pixelSize: 10
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(55)
        y: setY(720)-superior_panel.height
        opacity: 1
    }

    /** Graph **/
    Image {
        id: graph_lines
        source: "ReverberationImages/graph_lines.png"
        x: setX(52)
        y: setY(1387)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        visible: false
        opacity: 0
    }
    ChartQuick2BAResult {
        id: qwtPlotReverbResult
        objectName: "plotChartBAResult"
        x: setX(75)
        y: setY(770)-superior_panel.height
        width:graph_lines.width
        height:graph_lines.height*2.2

        Component.onCompleted: {
            initQwtPlotBAResult(config.chartTime)
            qwtPlotReverbResult.chartWidth=qwtPlotReverbResult.width
        }
    }

    /** Marker Area **/
    Image {
        id: tr_freq_area
        source: "ReverberationImages/tr_freq_area.png"
        anchors.horizontalCenter: parent.horizontalCenter
        y: setY(2012)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: tr_value_area
        source: "ReverberationImages/tr_value_area.png"
        anchors.horizontalCenter: parent.horizontalCenter
        //x: setX(228)
        y: setY(2087)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: marker_left
        source: "ReverberationImages/marker_left.png"
        x: setX(257)
        y: setY(2114)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea {
            anchors.fill: parent
            onClicked: qwtPlotReverbResult.step(-1)
        }
    }
    Image {
        id: marker_right
        source: "ReverberationImages/marker_right.png"
        x: setX(1163)
        y: setY(2114)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea {
            anchors.fill: parent
            onClicked: qwtPlotReverbResult.step(1)
        }
    }

    /* Frequency Value */
    Text {
        text: qwtPlotReverbResult.frequency
        font.pixelSize: 10
        font.family: "Arial-BoldMT"
        font.bold: true
        color: "#ffffff"
        smooth: true
        anchors.horizontalCenter: tr_freq_area.horizontalCenter
        y: setY(2034.5)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }

    /* TR Value */
    Text {
        id: tr_value
        text: qwtPlotReverbResult.trValue
        font.pixelSize: setX(160)
        font.family: "Digital-7"
        color: "#51A8FF"
        smooth: true
        x: setX(630)
        anchors.verticalCenter: tr_value_area.verticalCenter
        opacity: 1

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if (reverb_type.text === "T20 Measure") {
                        // From Mode T20 to T30
                    reverb_type.text = "T30 Measure"
                    qwtPlotReverbResult.updateModeTR(30)

                    tr_value.color = "#0088FF"
                    unit_sec.color = "#0088FF"

                } else if (reverb_type.text === "T30 Measure") {
                        // From Mode T30 to T60
                    reverb_type.text = "T60 Measure"
                    qwtPlotReverbResult.updateModeTR(60)

                    tr_value.color = "#0066FF"
                    unit_sec.color = "#0066FF"

                } else if (reverb_type.text === "T60 Measure") {
                        // From Mode T60 to T20
                    reverb_type.text = "T20 Measure"
                    qwtPlotReverbResult.updateModeTR(20)

                    tr_value.color = "#51A8FF"
                    unit_sec.color = "#51A8FF"
                }
            }
        }
    }

    Text {
        id: unit_sec
        text: "(sec)"
        font.pixelSize: 11
        font.family: "Tahoma"
        color: "#51A8FF"
        smooth: true
        x: setX(900)
        y: setY(2170)-superior_panel.height
        opacity: 1
    }

    /** Window Indicator **/
    Image {
        id: blue_window_indicator
        source: "ReverberationImages/blue_window_indicator.png"
        x: setX(629)
        y: setY(2321)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_01
        source: "ReverberationImages/other_window_indicator_01.png"
        x: setX(736) // +107
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_02
        source: "ReverberationImages/other_window_indicator_02.png"
        x: setX(840) // +104
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /** Legend Window **/
    Item {
        id: legendReverb
        visible: false

        Image {
            id: legend_background
            source: "ReverberationImages/tr_legend_background.png"
            x: setX(54)
            y: setY(1706)-superior_panel.height
            width:setX(sourceSize.width)
            height:setY(sourceSize.height)
            opacity: 1
        }

        Image {
            id: legend
            source: "ReverberationImages/tr_legend_color.png"
            x: setX(154)
            y: setY(1948)-superior_panel.height
            width:setSize(sourceSize.width)
            height:setSize(sourceSize.height)
            opacity: 1
        }

        Text {
            id: plot_legend
            text: "Plot Legend"
            font.pixelSize: 17
            font.family: "Tahoma-Bold"
            font.bold: true
            color: "#ffffff"
            smooth: true
            anchors.horizontalCenter: legend_background.horizontalCenter //x: setX(388)
            y: setY(1777)-superior_panel.height
            opacity: 1
        }

        Text {
            id: source_level
            text: "Source Level"
            font.pixelSize: 12
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(250)
            y: setY(1950)-superior_panel.height
            opacity: 1
        }
        Text {
            id: noise_level
            text: "Noise Level"
            font.pixelSize: 12
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(250)
            y: setY(2070)-superior_panel.height
            opacity: 1
        }
        Text {
            id: freq_value_legend
            text: qwtPlotReverbResult.frequency
            font.pixelSize: 12
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(250)
            y: setY(2190)-superior_panel.height
            opacity: 1
        }

        Text {
            id: t60
            text: "T60 Reg. Lin."
            font.pixelSize: 12
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(750)
            y: setY(1950)-superior_panel.height
            opacity: 1
        }
        Text {
            id: t30
            text: "T30 Reg. Lin."
            font.pixelSize: 12
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(750)
            y: setY(2070)-superior_panel.height
            opacity: 1
        }
        Text {
            id: t20
            text: "T20 Reg. Lin."
            font.pixelSize: 12
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(750)
            y: setY(2190)-superior_panel.height
            opacity: 1
        }

        Image {
            id: exit_button
            source: "ReverberationImages/exit_button.png"
            x: setX(987)
            y: setY(1750)-superior_panel.height
            width:setSize(sourceSize.width)
            height:setSize(sourceSize.height)
            opacity: 1

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    audioget.buttonClick = 540 // Legend Button Off
                }
            }
        }
    }
}
