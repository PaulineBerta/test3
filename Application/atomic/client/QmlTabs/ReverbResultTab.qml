import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1

Item {
    id:reverbTabScreen
    x:0
    y:superior_panel.height
    width:Screen.width
    height:Screen.height

    Connections{
        target: audioget
        onReverbMeasureChanged: {
            table_counter.text = audioget.reverbMeasure+1
        }

        onButtonClickChanged: {
            if(audioget.screenState === 5) { // REVERBERATION RESULT

                switch(audioget.buttonClick) {
                case 540:
                    tableLegendReverb.visible=false
                    console.log(" LEGEND FALSE ")
                    break

                case 541:
                    tableLegendReverb.visible=true
                    console.log(" LEGEND TRUE ")
                    break

                default:
                    break
                }

                audioget.buttonClick=0 // None
            }
        }
    }

    /** Background **/
    Image {
        id: superior_panel
        source: "ReverberationImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }
    Image {
        id: inferior_panel_front
        source: "ReverberationImages/inferior_panel_front.png"
        visible: false
        x: setX(245)
        y: setY(2425)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    Flickable{
        id: myFlickable
        x: setX(0)
        y: setY(750)-superior_panel.height
        width:Screen.width
        height:applicationWindow.height-superior_panel.height-inferior_panel_front.height-back_tr_counter_area.height-160

        contentHeight: if(config.spectrumConfiguration === "SPECTRUM_3RD_OCTAVE") {
                            680//(1060+inferior_panel_front.height)

                       } else if (config.spectrumConfiguration === "SPECTRUM_OCTAVE") {
                            (560+inferior_panel_front.height)
                       }

        contentWidth: Screen.width
        interactive: true
        boundsMovement: Flickable.StopAtBounds
        boundsBehavior: Flickable.StopAtBounds

        ScrollBar.vertical: ScrollBar {
            policy: ScrollBar.AlwaysOn
        }

        Column{
            spacing: 9

            ReverbTable{
            }
        }
    }

    Image {
        id: back_tr_counter_area
        source: "ReverberationImages/back_tr_counter_area.png"
        x: setX(0)
        y: setY(260)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    /*Image {
        id: tr_counter_area
        source: "ReverberationImages/tr_counter_area.png"
        anchors.horizontalCenter: parent.horizontalCenter
        y: setY(2087)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: marker_left
        source: "ReverberationImages/marker_left.png"
        x: setX(100)
        y: setY(2114)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea {
            anchors.fill: parent
            //onClicked: plot.step(-1)
        }
    }
    Image {
        id: marker_right
        source: "ReverberationImages/marker_right.png"
        x: setX(1300)
        y: setY(2114)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea {
            anchors.fill: parent
            //onClicked: plot.step(1)
        }
    }*/

    Text {
        id: table_counter_text
        text: "Selected Table: RT N°"
        font.pixelSize: 18
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(410)
        y: setY(660)-superior_panel.height
        //anchors.verticalCenter: tr_counter_area.verticalCenter
        opacity: 1
    }
    Text {
        id: table_counter
        text: audioget.reverbCounter
        font.pixelSize: 18
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(1030)
        y: setY(660)-superior_panel.height
        //anchors.verticalCenter: tr_counter_area.verticalCenter
        opacity: 1
    }

    /** Window Indicator **/
    Image {
        id: other_window_indicator_01
        source: "ReverberationImages/other_window_indicator_01.png"
        x: setX(629)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: blue_window_indicator
        source: "ReverberationImages/blue_window_indicator.png"
        x: setX(732.5)
        y: setY(2321)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_02
        source: "ReverberationImages/other_window_indicator_02.png"
        x: setX(840)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /** Legend Table Window **/
    Item {
        id: tableLegendReverb
        visible: false

        Image {
            id: table_legend_background
            source: "ReverberationImages/tr_legend_background.png"
            x: setX(54)
            y: setY(1706)-superior_panel.height
            width:setX(sourceSize.width)
            height:setY(sourceSize.height)
            opacity: 1
        }

        Image {
            id: table_legend
            source: "ReverberationImages/tr_table_legend_color.png"
            x: setX(154)
            y: setY(1948)-superior_panel.height
            width:setSize(sourceSize.width)
            height:setSize(sourceSize.height)
            opacity: 1
        }

        Text {
            id: plot_table_legend
            text: "Table Legend"
            font.pixelSize: 17
            font.family: "Tahoma-Bold"
            font.bold: true
            color: "#ffffff"
            smooth: true
            anchors.horizontalCenter: table_legend_background.horizontalCenter //x: setX(388)
            y: setY(1777)-superior_panel.height
            opacity: 1
        }

        Text {
            id: tr_table_legend
            text: "T20 (D/<)  T30 (D/</C)  T60(D/<)"
            font.pixelSize: 12
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(250)
            y: setY(1950)-superior_panel.height
            opacity: 1
        }
        Text {
            id: d_legend
            text: "D: Insufficient Dynamic"
            font.pixelSize: 12
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(154)
            y: setY(2090)-superior_panel.height //2120
            opacity: 1
        }
        Text {
            id: symbol_legend
            text: "<: Txx too Low"
            font.pixelSize: 12
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(154)
            y: setY(2160)-superior_panel.height
            opacity: 1
        }
        Text {
            id: c_legend
            text: "C: Curvature too Low or High"
            font.pixelSize: 12
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(154)
            y: setY(2230)-superior_panel.height
            opacity: 1
        }

        Image {
            id: exit_table_button
            source: "ReverberationImages/exit_button.png"
            x: setX(987)
            y: setY(1750)-superior_panel.height
            width:setSize(sourceSize.width)
            height:setSize(sourceSize.height)
            opacity: 1

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    audioget.buttonClick = 540 // Table Legend Button Off
                }
            }
        }
    }
}
