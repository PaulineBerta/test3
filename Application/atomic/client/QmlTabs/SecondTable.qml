import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12

Item {
    id:tableRect
    width:Screen.width
    height:if(config.spectrumConfiguration === "SPECTRUM_3RD_OCTAVE") {
               760
           } else if (config.spectrumConfiguration === "SPECTRUM_OCTAVE") {
               260
           }

    anchors.margins: 10

    TableView {
        id: tableView
        anchors.fill: tableRect
        anchors.centerIn: parent
        columnSpacing: 0.0
        rowSpacing: 0.0
        clip: false
        height:if(config.spectrumConfiguration === "SPECTRUM_3RD_OCTAVE") {
                   760
               } else if (config.spectrumConfiguration === "SPECTRUM_OCTAVE") {
                   260
               }

        interactive: false
        model: tableModel

        delegate: Rectangle {

            implicitWidth: Screen.width/3
            implicitHeight: 20
            border.color: "black"
            border.width: 0.6
            color: (heading === true)?"lightgray":"white"

            Connections{
                target: tableModel
                onOverloadChanged:{
                    textId.color="#272727"
                    switch(tableModel.overload) {
                    case 1:
                        if(index>37 & index<74){textId.color="red"}
                        break

                    case 2:
                        if(index>74 & index<111){textId.color="red"}
                        break

                    case 3:
                        if(index>37 & heading === false){textId.color="red"}
                        break

                    default: textId.color="#272727"
                    }
                }
            }

            Text {
                id: textId
                text: tableData
                font.pixelSize: 11
                font.family: "Tahoma-Bold"
                font.bold: true
                color: "#272727"
                smooth: true
                anchors.centerIn: parent
            }
        }
    }
}
