import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQml 2.12
import ChartQuick2BA 1.0
import QtQuick.Dialogs 1.1

Item {
    id: reverbItem
    x:0
    y:superior_panel.height
    width:Screen.width
    height:Screen.height

    FontLoader { source: "qrc:/Polices/Tahoma.ttf" }
    FontLoader { source: "qrc:/Polices/digital.ttf" }
    FontLoader { source: "qrc:/Polices/TahomaBold.ttf" }
    FontLoader { source: "qrc:/Polices/AllerBd.ttf" }

    Connections{
        target: audioget
        onSonoStateChanged: {
            console.log("SonoStateChanged REVERB to "+ audioget.sonoState)
            switch(audioget.sonoState){
            case 9:
                audioget.stop()

                color_box.source="ReverberationImages/box_ready.png"

                reverb_information.text="Ready"
                box_note.visible=false
                break
            }
        }

        onScreenStateChanged: {
            if(audioget.screenState !== 4) {
                qwtPlotReverb.resetCounterFirstSecond()
                //console.log(" EMIT RESET COUNTER FIRST SECOND !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
        }

        onReverbStatusChanged: {
            reverb_information.text=audioget.reverbStatus

            if(audioget.reverbStatus === "Ready") {
                audioget.stop()

                color_box.source="ReverberationImages/box_ready.png"
                color_box.visible=true

                reverb_Level1.visible=false
                reverb_Level2.visible=false
                reverb_Level3.visible=false
                reverb_Level4.visible=false

                box_note.visible=false

            } else if(audioget.reverbStatus === "Measuring Noise Level") {
                color_box.source="ReverberationImages/box_noise_level.png"
                color_box.visible=true

                reverb_Level1.visible=true
                reverb_Level2.visible=false
                reverb_Level3.visible=false
                reverb_Level4.visible=false

                box_note.visible=true
                box_note.text="Silence!"

            } else if(audioget.reverbStatus === "Waiting Source") {
                color_box.source="ReverberationImages/box_go.png"
                color_box.visible=true

                reverb_Level1.visible=true
                reverb_Level2.visible=true
                reverb_Level3.visible=false
                reverb_Level4.visible=false

                box_note.visible=true
                box_note.text="Go!"

            } else if(audioget.reverbStatus === "Waiting Source Stabilized") {
                color_box.source="ReverberationImages/box_wait.png"
                color_box.visible=true

                reverb_Level1.visible=true
                reverb_Level2.visible=true
                reverb_Level3.visible=false
                reverb_Level4.visible=false

                box_note.visible=true
                box_note.text="Wait"

            } else if(audioget.reverbStatus === "Measuring Source Level") {
                color_box.source="ReverberationImages/box_wait.png"
                color_box.visible=true

                reverb_Level1.visible=true
                reverb_Level2.visible=true
                reverb_Level3.visible=false
                reverb_Level4.visible=false

                box_note.visible=true
                box_note.text="Wait"

            } else if(audioget.reverbStatus === "Waiting Stop Source") {
                color_box.source="ReverberationImages/box_off.png"
                color_box.visible=true

                reverb_Level1.visible=true
                reverb_Level2.visible=true
                reverb_Level3.visible=true
                reverb_Level4.visible=false

                box_note.text="Off!"
                box_note.visible=true

            } else if(audioget.reverbStatus === "Source Decrease Recording") {
                color_box.source="ReverberationImages/box_wait.png"
                color_box.visible=true

                reverb_Level1.visible=true
                reverb_Level2.visible=true
                reverb_Level3.visible=true
                reverb_Level4.visible=false

                box_note.visible=true
                box_note.text="Wait"

            } else if(audioget.reverbStatus === "TR Measure") {
                color_box.visible=false

                reverb_Level1.visible=true
                reverb_Level2.visible=true
                reverb_Level3.visible=true
                reverb_Level4.visible=true

                box_note.visible=false

                // Display TR Measure Result
                audioget.screenState=5
                audioget.sonoState=5

            } else if(audioget.reverbStatus === "Noise Level Too High") {
                color_box.visible=false

                reverb_Level1.visible=false
                reverb_Level2.visible=false
                reverb_Level3.visible=false
                reverb_Level4.visible=false

                box_note.visible=false

            } else if(audioget.reverbStatus === "Source Not Detected") {
                color_box.visible=false

                reverb_Level1.visible=false
                reverb_Level2.visible=false
                reverb_Level3.visible=false
                reverb_Level4.visible=false

                box_note.visible=false

            } else if(audioget.reverbStatus === "Source Never Stopped") {
                color_box.visible=false

                reverb_Level1.visible=false
                reverb_Level2.visible=false
                reverb_Level3.visible=false
                reverb_Level4.visible=false

                box_note.visible=false
            }
        }
    }

    Connections{
        target: config
        onChartTimeChanged:  {
            console.log("ChartTimeChanged REVERB to "+ config.chartTime)
            qwtPlotReverb.initQwtPlotBA(config.chartTime)

            if(config.chartTime === "3") {
                audioget.chartTimeResult = 3
            } else if(config.chartTime === "10") {
                audioget.chartTimeResult = 10
            }


        }
    }

    Image {
        id: superior_panel
        source: "ReverberationImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }

    /** Graph Legend **/
    Image {
        id: legend_LAeq
        source: "ReverberationImages/legend_LAeq.png"
        x: setX(1000)
        y: setY(720)-superior_panel.height
        width:setSize(sourceSize.width*1.5)
        height:setSize(sourceSize.height*1.5)
        opacity: 1
    }
    Text {
        id: laeq
        text: "LAeq(dB)"
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1060)
        y: setY(720)-superior_panel.height
        opacity: 1
    }
    Text {
        text: qwtPlotReverb.lAeqValue
        Layout.alignment :Qt.AlignHCenter | Qt.AlignVCenter
        font.pixelSize: 20
        font.family: "Digital-7"
        color: "#ffffff"
        smooth: true
        x: setX(1230)
        y: setY(712)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }
    Text {
        id: timehistory
        text: "Time History"
        font.pixelSize: 10
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(143)
        y: setY(720)-superior_panel.height
        opacity: 1
    }
    Text {
        id: db
        text: "dB"
        font.pixelSize: 10
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(55)
        y: setY(720)-superior_panel.height
        opacity: 1
    }

    /** Graph **/
    Image {
        id: graph_lines
        source: "ReverberationImages/graph_lines.png"
        x: setX(52)
        y: setY(1387)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        visible: false
        opacity: 0
    }
    ChartQuick2BA {
        id: qwtPlotReverb
        objectName: "plotChartBA"
        x: setX(75)
        y: setY(770)-superior_panel.height
        width:graph_lines.width
        height:graph_lines.height*2.2

        Component.onCompleted: {
            initQwtPlotBA(config.chartTime)
            qwtPlotReverb.chartWidth=qwtPlotReverb.width
        }
    }

    /** Noise Gage **/
    Image {
        id: rect_gage
        source: "ReverberationImages/rect_gage.png"
        x: setX(99)
        y: setY(2100)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    Image {
        id: reverb_Level1
        source: "ReverberationImages/reverb_Level1.png"
        x: setX(113)
        y: setY(2110)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        visible: false
    }
    Image {
        id: reverb_Level2
        source: "ReverberationImages/reverb_Level2.png"
        x: setX(431)
        y: setY(2110)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        visible: false
    }
    Image {
        id: reverb_Level3
        source: "ReverberationImages/reverb_Level3.png"
        x: setX(750)
        y: setY(2110)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        visible: false
    }
    Image {
        id: reverb_Level4
        source: "ReverberationImages/reverb_Level4.png"
        x: setX(1071)
        y: setY(2110)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        visible: false
    }

    Image {
        id: separating_lines_gage
        source: "ReverberationImages/separating_lines_gage.png"
        x: setX(428)
        y: setY(2090)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /** Reverb Information **/
    Image {
        id: color_box
        source: "ReverberationImages/box_ready.png"
        x: setX(1000)
        y: setY(2260)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        id: reverb_information
        text: "Ready"
        font.pixelSize: 16
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(220)
        y: setY(2280)-superior_panel.height
        opacity: 1
    }
    Text {
        id: box_note
        visible: false
        text: ""
        font.pixelSize: 14
        font.family: "Tahoma-Bold"
        font.italic: true
        font.bold: true
        color: "#ffffff"
        smooth: true
        anchors.horizontalCenter: color_box.horizontalCenter
        //x: setX(1045)
        y: setY(2275)-superior_panel.height
        opacity: 1
    }
}
