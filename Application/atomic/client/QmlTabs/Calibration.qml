import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQuick.Dialogs 1.1

Item {
    id: calibItem
    x:0
    y:0
    width:Screen.width
    height:applicationWindow.height

    property int iState: 1

    function getClassColor(sArg) {
        if(config.classConfig === sArg) {
            this.color = "#0c85c7" //blue

        } else {
            this.color="#ffffff"
        }
    }

    function getLevelColor(sArg) {
        if(config.calibrationLevel === sArg) {
            this.color = "#0c85c7" //blue

        } else {
            this.color="#ffffff"
        }
    }

    Connections{
        target: config
        onClassConfigChanged : {
            if(config.classConfig === "CLASS_1") {
                class2.color="#ffffff"
                class1.color="#0c85c7"
                class0.color="#ffffff"

                class_check_outline.x=setX(841)
                class_check_center.x= setX(850)

            } else if (config.classConfig === "CLASS_2") {
                class2.color="#0c85c7"
                class1.color="#ffffff"
                class0.color="#ffffff"

                class_check_outline.x=setX(1116)
                class_check_center.x= setX(1125)

            } else if(config.classConfig === "CLASS_0") {
                class2.color="#ffffff"
                class1.color="#ffffff"
                class0.color="#0c85c7"

                class_check_outline.x=setX(566)
                class_check_center.x= setX(575)
            }
        }

        onCalibrationLevelChanged: {
            if(config.calibrationLevel === 94) {
                other.visible=true

                ellipse_other.visible=true
                calib_check_outline.visible=true
                calib_check_center.visible=true
                calib_check_outline.x=setX(566)
                calib_check_center.x=setX(575)

                textFieldother.text=""

                other.color="#ffffff"
                class114.color="#ffffff"
                class94.color="#0c85c7" 

            } else if(config.calibrationLevel === 114) {
                other.visible=true

                ellipse_other.visible=true
                calib_check_outline.visible=true
                calib_check_center.visible=true
                calib_check_outline.x=setX(841)
                calib_check_center.x= setX(850)

                textFieldother.text=""

                other.color="#ffffff"
                class94.color="#ffffff"
                class114.color="#0c85c7"

            } else {
                other.visible=false

                calib_check_outline.x=setX(1116)
                calib_check_center.x= setX(1125)

                class94.color = "#ffffff"
                class114.color = "#ffffff"
                textFieldother.color = "#0c85c7"
            }
        }

        onCalibStateChanged:  {
            console.log("CalibStateChanged to "+ config.calibState)

            switch(config.calibState) {
            case 0: // Calibration Ready
                calibrationdoneclickonvalidate.text=""

                calib_Level1.visible=false
                calib_Level2.visible=false
                calib_Level3.visible=false
                calib_Level4.visible=false
                break

            case 1: // Calibration WORKING
                break

            case 2: // Calibration finished
                break

            case 3: // Calibration disabled
                calibrationdoneclickonvalidate.text=""

                calib_Level1.visible=false
                calib_Level2.visible=false
                calib_Level3.visible=false
                calib_Level4.visible=false
                break
            }
        }

        onLibCalibStatusChanged: {
            switch(config.libCalibStatus) {
            case 0: //CAL_NOT_RUNNING
                calibrationdoneclickonvalidate.text="Preparing calibration ..."
                realSensText.text="Real Sensitivity: "

                calib_Level1.visible=false
                calib_Level2.visible=false
                calib_Level3.visible=false
                calib_Level4.visible=false

                calibRealSens.visible=false
                calibratedText.visible=false
                calibrated.visible=false
                calibrationcorrection2.visible=false
                break

            case 1: //SEARCHING_LEVEL
                if(config.classConfig === "CLASS_0") {
                    calibrationdoneclickonvalidate.text="Search for calibration level +/-1,0dB (Class 0)"

                } else if(config.classConfig === "CLASS_1") {
                    calibrationdoneclickonvalidate.text="Search for calibration level +/-1,5dB (Class 1)"

                } else if(config.classConfig === "CLASS_2") {
                    calibrationdoneclickonvalidate.text="Search for calibration level +/-2,0dB (Class 2)"
                }

                realSensText.text="Real Sensitivity: "

                lCeqTextId.color="#53CE6E"
                calibRealSens.color="#53CE6E"
                calibrationcorrection2.color="#53CE6E"
                calibRealSens.color="#53CE6E"

                calib_Level1.visible=true
                calib_Level2.visible=false
                calib_Level3.visible=false
                calib_Level4.visible=false

                calibratedText.visible=false
                calibrated.visible=false
                calibRealSens.visible=true
                calibrationcorrection2.visible=false
                break

            case 2: //LOOK_FOR_STABILIZATION
                calibrationdoneclickonvalidate.text="Look for a stable value during 5s ..."
                realSensText.text="Real Sensitivity: "

                lCeqTextId.color="#A5B847"
                calibrationcorrection2.color="#A5B847"
                calibRealSens.color="#A5B847"

                calib_Level1.visible=true
                calib_Level2.visible=true
                calib_Level3.visible=false
                calib_Level4.visible=false

                calibRealSens.visible=true
                calibrationcorrection2.visible=false
                calibratedText.visible=false
                calibrated.visible=false
                break

            case 3: //WAIT_FOR_STABILIZED
                calibrationdoneclickonvalidate.text="Look for a stable value during 10s ..."
                realSensText.text="Real Sensitivity: "

                lCeqTextId.color="#FBB750"
                calibrationcorrection2.color="#FBB750"
                calibRealSens.color="#FBB750"

                calib_Level1.visible=true
                calib_Level2.visible=true
                calib_Level3.visible=true
                calib_Level4.visible=false

                calibRealSens.visible=true
                calibrationcorrection2.visible=false
                calibratedText.visible=false
                calibrated.visible=false
                break

            case 4: //CALIBRATION_DONE
                calibrationdoneclickonvalidate.text="Calibration done: Click on Validate"
                realSensText.text="Real Sens.: "

                lCeqTextId.color="#E38768"
                calibrationcorrection2.color="#E38768"
                calibRealSens.color="#E38768"

                calib_Level1.visible=true
                calib_Level2.visible=true
                calib_Level3.visible=true
                calib_Level4.visible=true

                calibRealSens.visible=true
                calibrationcorrection2.visible=true
                calibratedText.visible=true
                calibrated.visible=true
                break

            case 5: //TIMEOUT_CALIB
                calibrationdoneclickonvalidate.text="Time out of calibration after 60s ..."
                break
            }
        }
    }

    Connections{
        target: audioget
        onButtonClickChanged: {
            if(audioget.screenState === 1 && audioget.sonoState === 4) { // CALIBRATION, Calibration

                switch(audioget.buttonClick) {
                case 12:
                    if(audioget.sonoState === 1) {
                        iState === 1

                    } else if(audioget.sonoState === 4) {
                        if(config.calibState === 2 && iState === 1){
                            calibMessageDialog.open()
                            iState === 0
                        }
                    }
                    break

                default:
                    break
                }

                audioget.buttonClick=0 // None
            }
        }
    }

    /** AREA LAST CALIBRATION **/
    Text {
        id: lastcalibration
        text: "Last Calibration"
        font.pixelSize: 14
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(100)
        y: setY(680.25)
        opacity: 1
    }
    Image {
        id: trace_calib_01
        source: "CalibrationImages/trace_calib_01.png"
        x: setX(618)
        y: setY(700)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /* Laboratory Sensitivity */
    Image {
        id: rect_lab_sens
        source: "CalibrationImages/rect_lab_sens.png"
        x: setX(700)
        y: setY(782)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        id: laboratorysensitivity
        text: "Laboratory Sensitivity"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(104)
        y: setY(791.75)
        opacity: 1
    }
    Text {
        text: config.calibLabSensitivity
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1155)
        y: setY(800)
        opacity: 1
    }
    Text {
        text: "mV/Pa"
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1323)
        y: setY(800.75)
        opacity: 1
    }

    /* Calibration Correction */
    Image {
        id: rect_calib_corr
        source: "CalibrationImages/rect_calib_corr.png"
        x: setX(700)
        y: setY(907)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        text: "Calibration Correction"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(101)
        y: setY(916.75)
        opacity: 1
    }
    Text {
        text: config.calibrationCorrection1
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1179)
        y: setY(924.25)
        opacity: 1
    }
    Text {
        text: "dB"
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1322)
        y: setY(929.75)
        opacity: 1
    }

    /* Real Sensitivity */
    Image {
        id: rect_real_sens
        source: "CalibrationImages/rect_real_sens.png"
        x: setX(700)
        y: setY(1032)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        id: realsensitivity
        text: "Real Sensitivity"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(104)
        y: setY(1041.75)
        opacity: 1
    }
    Text {
        text:config.realSensivity
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1155)
        y: setY(1049.25)
        opacity: 1
    }
    Text {
        text: "mV/Pa"
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX( 1323)
        y: setY(1050.75)
        opacity: 1
    }

    /* Last Calibration Date*/
    Image {
        id: rect_lab_calib_date
        source: "CalibrationImages/rect_lab_calib_date.png"
        x: setX(700)
        y: setY(1157)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: separating_lines
        source: "CalibrationImages/separating_lines.png"
        x: setX(812)
        y: setY(1159)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        id: lastcalibrationdate
        text: "Last Calibration Date"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(104)
        y: setY(1166.75)
        opacity: 1
    }
    Image {
        id: icon_clock
        source: "CalibrationImages/icon_clock.png"
        x: setX(1204)
        y: setY(1173)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        text: config.lastCalibrationDateDay
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(754)
        y: setY(1179.25)
        opacity: 1
    }
    Text {
        text: config.lastcalibrationDateMonth
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(878)
        y: setY(1179.25)
        opacity: 1
    }
    Text {
        text: config.lastcalibrationDateYear
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1048)
        y: setY(1179.25)
        opacity: 1
    }

    /** NEW CALIBRATION **/
    Text {
        id: newcalibration
        text: "New Calibration"
        font.pixelSize: 14
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(100)
        y: setY(1309.25)
        opacity: 1
    }
    Image {
        id: trace_calib_02
        source: "CalibrationImages/trace_calib_02.png"
        x: setX(618)
        y: setY(1329)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /* Calibration Level */
    Image {
        id:rect_calib_level
        source: "CalibrationImages/rect_calib_level.png"
        x: setX(1104)
        y: setY(1407)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        TextField {
            id: textFieldother
            text: if(config.calibrationLevel !== 94 && config.calibrationLevel !== 114) {
                      config.calibrationLevel
                  } else {
                      ""
                  }
            padding: 20
            smooth: true
            color: "#ffffff"
            anchors.fill: parent
            font.pixelSize: 13
            font.family: "Tahoma"
            placeholderTextColor:"#ffffff"
            background: Rectangle {
                implicitWidth: rect_calib_level.width
                implicitHeight: rect_calib_level.height
                color:"transparent"
                border.color: "transparent"
            }
            inputMethodHints: Qt.ImhDigitsOnly
            onAccepted: {
                Qt.inputMethod.hide();
                textFieldother.focus = false
            }
            onPressed: {
                other.visible=false
                ellipse_other.visible=true //false
                class114.color="#ffffff"
                class94.color="#ffffff"
                calib_check_outline.visible=true
                calib_check_center.visible=true
            }
            onTextChanged: {
                var modifiedText = text.replace(",",".")
                config.calibrationLevel = modifiedText
            }
        }
        Component.onCompleted:{
            if(config.calibrationLevel !== 94 && config.calibrationLevel !== 114) {
                other.color="#0c85c7"
                other.visible=true

            } else {
                other.color="#ffffff"
            }
        }
    }
    Text {
        id: calibrationlevel
        text: "Calibration Level"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(101)
        y: setY(1420.75)
        opacity: 1
    }
    Image {
        id: calib_check_outline
        source: "CalibrationImages/calib_check_outline.png"
        x: {
            if(config.calibrationLevel === 94){
                setX(566)

            } else if(config.calibrationLevel === 114){
                setX(841)

            } else {
                setX(1116)
            }
        }
        y: setY(1419)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: calib_check_center
        source: "CalibrationImages/calib_check_center.png"
        x: {
            if(config.calibrationLevel === 94){
                setX(575)

            } else if(config.calibrationLevel === 114){
                setX(850)

            } else {
                setX(1125)
            }

        }
        y: setY(1427)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    Text {
        id: class94
        text: "94dB"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#0c85c7"
        smooth: true
        x: setX (647)
        y: setY(1420.75)
        opacity: 1
        Component.onCompleted: getLevelColor(94)
    }
    Image {
        id: ellipse_94dB
        source: "CalibrationImages/ellipse_94dB.png"
        visible: false
        x: setX(566)
        y: setY(1419)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill: parent
            onClicked:{
                config.calibrationLevel=94

                ellipse_94dB.visible=false
                ellipse_114dB.visible=true
                ellipse_other.visible=true

                other.color="#ffffff"
                class114.color="#ffffff"
                class94.color="#0c85c7"
            }
        }
    }

    Text {
        id: class114
        text: "114dB"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(926)
        y: setY(1420.75)
        opacity: 1
        Component.onCompleted: getLevelColor(114)
    }
    Image {
        id: ellipse_114dB
        source: "CalibrationImages/ellipse_114dB.png"
        x: setX(841)
        y: setY(1419)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill: parent
            onClicked:{
                config.calibrationLevel=114

                ellipse_94dB.visible=true
                ellipse_114dB.visible=false
                ellipse_other.visible=true

                other.color="#ffffff"
                class94.color="#ffffff"
                class114.color="#0c85c7"
            }
        }
    }

    Text {
        id: other
        text: {
            if(config.calibrationLevel !== 94 && config.calibrationLevel !== 114) {
                config.calibrationLevel
            } else {
                "Other"
            }
        }
        font.pixelSize: 13
        font.family: "Tahoma"
        //        color: "#ffffff"
        smooth: true
        x: setX(1197)
        y: setY(1420.75)
        opacity: 1
        Component.onCompleted: {
            if(config.calibrationLevel !== 94 && config.calibrationLevel !== 114) {
                other.color="#0c85c7"
                other.visible=true
            } else {
                other.color="#ffffff"
            }
        }
    }
    Image {
        id: ellipse_other
        source: "CalibrationImages/ellipse_other.png"
        x: setX(1116)
        y: setY(1419)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill: parent
            onClicked:{
                config.calibrationLevel=textFieldother.text

                ellipse_94dB.visible=true
                ellipse_114dB.visible=true
                ellipse_other.visible=false

                class114.color="#ffffff"
                class94.color="#ffffff"
                other.color="#0c85c7"
            }
        }
    }

    /* Class */
    Text {
        text: "Class"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(101)
        y: setY(1545.75)
        opacity: 1
    }

    Image {
        id: class_check_outline
        source: "CalibrationImages/class_check_outline.png"
        x: if(config.classConfig === "CLASS_0") {
                setX(566)

            } else if(config.classConfig === "CLASS_1") {
                setX(841)

            } else if(config.classConfig === "CLASS_2") {
                setX(1116)
            }
        y: setY(1544)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: class_check_center
        source: "CalibrationImages/class_check_center.png"
        x: if(config.classConfig === "CLASS_0"){
                setX(575)

            } else if(config.classConfig === "CLASS_1") {
                setX(850)

            } else if(config.classConfig === "CLASS_2") {
                setX(1125)
            }
        y: setY(1552)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    Text {
        id: class0
        text: "Class0"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(647)
        y: setY(1545.75)
        opacity: 1
        color: "#ffffff"
        Component.onCompleted: getClassColor('CLASS_0')
    }
    Image {
        id: ellipse_class0
        source: "CalibrationImages/ellipse_class0.png"
        x: setX(566)
        y: setY(1544)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill: parent
            onClicked:{
                config.classConfig = "CLASS_0"

                ellipse_class0.visible=false
                ellipse_class1.visible=true
                ellipse_class2.visible=true

                class2.color="#ffffff"
                class1.color="#ffffff"
                class0.color="#0c85c7"
            }
        }
    }

    Text {
        id: class1
        text: "Class1"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(922)
        y: setY(1545.75)
        opacity: 1
        color: "#ffffff"
        Component.onCompleted: getClassColor('CLASS_1')
    }
    Image {
        id: ellipse_class1
        visible: false
        source: "CalibrationImages/ellipse_class1.png"
        x: setX(841)
        y: setY(1544)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill: parent
            onClicked:{
                config.classConfig = "CLASS_1"

                ellipse_class0.visible=true
                ellipse_class1.visible=false
                ellipse_class2.visible=true

                class2.color="#ffffff"
                class1.color="#0c85c7"
                class0.color="#ffffff"
            }
        }
    }

    Text {
        id: class2
        text: "Class2"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(1197)
        y: setY(1545.75)
        opacity: 1
        color: "#ffffff"
        Component.onCompleted: getClassColor('CLASS_2')
    }
    Image {
        id: ellipse_class2
        source: "CalibrationImages/ellipse_class2.png"
        x: setX(1116)
        y: setY(1544)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill: parent
            onClicked:{
                config.classConfig = "CLASS_2"

                ellipse_class0.visible=true
                ellipse_class1.visible=true
                ellipse_class2.visible=false

                class2.color="#0c85c7"
                class1.color="#ffffff"
                class0.color="#ffffff"
            }
        }
    }

    /* Class Tolerance */
    Text {
        id: classtolerance
        text: "Class Tolerance (dB)"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(101)
        y: setY(1670.75)
        opacity: 1
    }
    Text {
        text: "(+/- 1,0)"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(619)
        y: setY(1670.75)
        opacity: 1
    }
    Text {
        text: "(+/- 1,5)"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(894)
        y: setY(1670.75)
        opacity: 1
    }
    Text {
        text: "(+/- 2,0)"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1169)
        y: setY(1670.75)
        opacity: 1
    }

    /* LCeq */
    Image {
        id: rect_lceq
        source: "CalibrationImages/rect_lceq.png"
        x: setX(700)
        y: setY(1786)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        id: calibrated
        text: "[Calibrated]"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#E38768" //"#33c548"
        smooth: true
        x: setX(275)
        y: setY(1795.75)
        opacity: 1
        visible: false
    }
    Text {
        id: lceq
        text: "LCeq"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(104)
        y: setY(1796.75)
        opacity: 1
    }
    Text {
        id: lCeqTextId
        text: config.lceq//"94,00"
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#33c548"
        smooth: true
        x: setX(1155)
        y: setY(1803.25)
        opacity: 1
    }
    Text {
        id: db
        text: "dB"
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1322)
        y: setY(1808.75)
        opacity: 1
    }

    /* Calibration Correction */
    Image {
        id: rect_calib_corr2
        source: "CalibrationImages/rect_calib_corr2.png"
        x: setX(700)
        y: setY(1911)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        id: calibrationcorrection
        text: "Calibration Correction"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(101)
        y: setY(1920.75)
        opacity: 1
    }
    Text {
        id: calibrationcorrection2
        text: config.calibrationCorrection2//"1,90"
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#33c548"
        smooth: true
        x: setX(1155) //setX(1183)
        y: setY(1928.25)
        opacity: 1
    }
    Text {
        id: db1
        text: "dB"
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1322)
        y: setY(1933.75)
        opacity: 1
    }

    /* Real Sensitivity */
    Image {
        id: rect_real_sens2
        source: "CalibrationImages/rect_real_sens2.png"
        x: setX(700)
        y: setY(2036)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        id: realSensText
        text: "Real Sens."
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(104)
        y: setY(2045.75)
        opacity: 1
    }
    Text {
        id: calibratedText
        text: "[Calibrated]"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#E38768" //"#33c548"
        smooth: true
        x: setX(389)
        y: setY(2045.75)
        opacity: 1
        visible: false
    }
    Text {
        id: calibRealSens
        text: config.realSens
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#53CE6E" //"#33c548"
        smooth: true
        x: setX(1183)
        y: setY(2053.25)
        opacity: 1
    }
    Text {
        text: "mV/Pa"
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1323)
        y: setY(2054.75)
        opacity: 1
    }

    /* Counter */
    Image {
        id: rect_counter
        source: "CalibrationImages/rect_counter.png"
        x: setX(700)
        y: setY(2161)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        id: counter
        text: "Counter"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(101)
        y: setY(2171.75)
        opacity: 1
    }
    Text {
        id: calibCounter
        text: config.counter//"30"
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#24ffff"
        smooth: true
        x: setX(1219)
        y: setY(2178.25)
        opacity: 1
    }
    Text {
        text: "S"
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1322)
        y: setY(2183.75)
        opacity: 1
    }

    /** CALIBRATION GAGE **/
    Image {
        id: rect_gage
        source: "CalibrationImages/rect_gage.png"
        x: setX(99)
        y: setY(2291)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    Image {
        id: calib_Level1
        source: "CalibrationImages/calib_Level1.png"
        x: setX(113)
        y: setY(2302)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        visible: false
    }
    Image {
        id: calib_Level2
        source: "CalibrationImages/calib_Level2.png"
        x: setX(431)
        y: setY(2302)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        visible: false
    }
    Image {
        id: calib_Level3
        source: "CalibrationImages/calib_Level3.png"
        x: setX(750)
        y: setY(2302)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        visible: false
    }
    Image {
        id: calib_Level4
        source: "CalibrationImages/calib_Level4.png"
        x: setX(1071)
        y: setY(2302)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        visible: false
    }

    Image {
        id: separating_lines_gage
        source: "CalibrationImages/separating_lines_gage.png"
        x: setX(428)
        y: setY(2281)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    Text {
        id: calibrationdoneclickonvalidate
        text: " "
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(406)
        y: setY(2461.25)
        opacity: 1
    }

    MessageDialog {
        id: calibMessageDialog
        icon: StandardIcon.Question
        title: "Calibration finished correctly: "
        text: "Are you sure you want to cancel calibration or do you want to apply it? : " + calibrationcorrection2.text
        standardButtons: StandardButton.Apply | StandardButton.Cancel
        onApply: {
            console.log("NO CLICKED")
            config.validateCalibration()
            audioget.reset()
            calibMessageDialog.close()
        }

        onRejected: {
            console.log("CANCEL CLICKED")
            audioget.reset()
            calibMessageDialog.close()
        }

        visible: false
    }
}
