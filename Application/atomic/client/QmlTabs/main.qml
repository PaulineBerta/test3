import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQuick.Window 2.12
import QtQuick.Dialogs 1.1
import "./"

ApplicationWindow {
    id:applicationWindow
    visibility: "AutomaticVisibility"
    width: Screen.width
    height: Screen.height
    visible: true
    title: qsTr("alliantech")

    Connections{
            target: audioget
            onScreenStateChanged: {
                switch(audioget.screenState){
                case 0: // MAIN MENU
                    // Main Menu
                    mainMenuView.visible=true
                    mainMenuView.focus=true
                    // Calibration
                    calibView.visible=false
                    calibView.focus=false
                    // Sonometer
                    sonometerView.visible=false
                    sonometerView.focus=false
                    // Reverberation Waiting Room
                    reverbWaitView.visible=false
                    reverbWaitView.focus=false
                    // Reverberation Measure
                    reverbMeasureView.visible=false
                    reverbMeasureView.focus=false
                    // Reverberation Result
                    reverbResultView.visible=false
                    reverbResultView.focus=false
                    // Configuration
                    configView.visible=false
                    configView.focus=false
                    // About
                    aboutView.visible=false
                    aboutView.focus=false
                    // File Browser
                    fileBrowserView.visible=false
                    fileBrowserView.focus=false
                    // Not Implemented
                    notImplementedView.visible=false
                    notImplementedView.focus=false
                    break

                case 1: // CALIBRATION
                    // Main Menu
                    mainMenuView.visible=false
                    mainMenuView.focus=false
                    // Calibration
                    calibView.visible=true
                    calibView.focus=true
                    // Sonometer
                    sonometerView.visible=false
                    sonometerView.focus=false
                    // Reverberation Waiting Room
                    reverbWaitView.visible=false
                    reverbWaitView.focus=false
                    // Reverberation Measure
                    reverbMeasureView.visible=false
                    reverbMeasureView.focus=false
                    // Reverberation Result
                    reverbResultView.visible=false
                    reverbResultView.focus=false
                    // Configuration
                    configView.visible=false
                    configView.focus=false
                    // About
                    aboutView.visible=false
                    aboutView.focus=false
                    // File Browser
                    fileBrowserView.visible=false
                    fileBrowserView.focus=false
                    // Not Implemented
                    notImplementedView.visible=false
                    notImplementedView.focus=false
                    break

                case 2: // SONOMETER
                    // Main Menu
                    mainMenuView.visible=false
                    mainMenuView.focus=false
                    // Calibration
                    calibView.visible=false
                    calibView.focus=false
                    // Sonometer
                    sonometerView.visible=true
                    sonometerView.focus=true
                    // Reverberation Waiting Room
                    reverbWaitView.visible=false
                    reverbWaitView.focus=false
                    // Reverberation Measure
                    reverbMeasureView.visible=false
                    reverbMeasureView.focus=false
                    // Reverberation Result
                    reverbResultView.visible=false
                    reverbResultView.focus=false
                    // Configuration
                    configView.visible=false
                    configView.focus=false
                    // About
                    aboutView.visible=false
                    aboutView.focus=false
                    // File Browser
                    fileBrowserView.visible=false
                    fileBrowserView.focus=false
                    // Not Implemented
                    notImplementedView.visible=false
                    notImplementedView.focus=false
                    break

                case 3: // REVERBERATION WAITING ROOM
                    // Main Menu
                    mainMenuView.visible=false
                    mainMenuView.focus=false
                    // Calibration
                    calibView.visible=false
                    calibView.focus=false
                    // Sonometer
                    sonometerView.visible=false
                    sonometerView.focus=false
                    // Reverberation Waiting Room
                    reverbWaitView.visible=true
                    reverbWaitView.focus=true
                    // Reverberation Measure
                    reverbMeasureView.visible=false
                    reverbMeasureView.focus=false
                    // Reverberation Result
                    reverbResultView.visible=false
                    reverbResultView.focus=false
                    // Configuration
                    configView.visible=false
                    configView.focus=false
                    // About
                    aboutView.visible=false
                    aboutView.focus=false
                    // File Browser
                    fileBrowserView.visible=false
                    fileBrowserView.focus=false
                    // Not Implemented
                    notImplementedView.visible=false
                    notImplementedView.focus=false
                    break

                case 4: // REVERBERATION MEASURE
                    // Main Menu
                    mainMenuView.visible=false
                    mainMenuView.focus=false
                    // Calibration
                    calibView.visible=false
                    calibView.focus=false
                    // Sonometer
                    sonometerView.visible=false
                    sonometerView.focus=false
                    // Reverberation Waiting Room
                    reverbWaitView.visible=false
                    reverbWaitView.focus=false
                    // Reverberation Measure
                    reverbMeasureView.visible=true
                    reverbMeasureView.focus=true
                    // Reverberation Result
                    reverbResultView.visible=false
                    reverbResultView.focus=false
                    // Configuration
                    configView.visible=false
                    configView.focus=false
                    // About
                    aboutView.visible=false
                    aboutView.focus=false
                    // File Browser
                    fileBrowserView.visible=false
                    fileBrowserView.focus=false
                    // Not Implemented
                    notImplementedView.visible=false
                    notImplementedView.focus=false
                    break

                case 5: // REVERBERATION RESULT
                    // Main Menu
                    mainMenuView.visible=false
                    mainMenuView.focus=false
                    // Calibration
                    calibView.visible=false
                    calibView.focus=false
                    // Sonometer
                    sonometerView.visible=false
                    sonometerView.focus=false
                    // Reverberation Waiting Room
                    reverbWaitView.visible=false
                    reverbWaitView.focus=false
                    // Reverberation Measure
                    reverbMeasureView.visible=false
                    reverbMeasureView.focus=false
                    // Reverberation Result
                    reverbResultView.visible=true
                    reverbResultView.focus=true
                    // Configuration
                    configView.visible=false
                    configView.focus=false
                    // About
                    aboutView.visible=false
                    aboutView.focus=false
                    // File Browser
                    fileBrowserView.visible=false
                    fileBrowserView.focus=false
                    // Not Implemented
                    notImplementedView.visible=false
                    notImplementedView.focus=false
                    break

                case 6: // CONFIGURATION
                    // Main Menu
                    mainMenuView.visible=false
                    mainMenuView.focus=false
                    // Calibration
                    calibView.visible=false
                    calibView.focus=false
                    // Sonometer
                    sonometerView.visible=false
                    sonometerView.focus=false
                    // Reverberation Waiting Room
                    reverbWaitView.visible=false
                    reverbWaitView.focus=false
                    // Reverberation Measure
                    reverbMeasureView.visible=false
                    reverbMeasureView.focus=false
                    // Reverberation Result
                    reverbResultView.visible=false
                    reverbResultView.focus=false
                    // Configuration
                    configView.visible=true
                    configView.focus=true
                    // About
                    aboutView.visible=false
                    aboutView.focus=false
                    // File Browser
                    fileBrowserView.visible=false
                    fileBrowserView.focus=false
                    // Not Implemented
                    notImplementedView.visible=false
                    notImplementedView.focus=false
                    break

                case 7: // ABOUT
                    // Main Menu
                    mainMenuView.visible=false
                    mainMenuView.focus=false
                    // Calibration
                    calibView.visible=false
                    calibView.focus=false
                    // Sonometer
                    sonometerView.visible=false
                    sonometerView.focus=false
                    // Reverberation Waiting Room
                    reverbWaitView.visible=false
                    reverbWaitView.focus=false
                    // Reverberation Measure
                    reverbMeasureView.visible=false
                    reverbMeasureView.focus=false
                    // Reverberation Result
                    reverbResultView.visible=false
                    reverbResultView.focus=false
                    // Configuration
                    configView.visible=false
                    configView.focus=false
                    // About
                    aboutView.visible=true
                    aboutView.focus=true
                    // File Browser
                    fileBrowserView.visible=false
                    fileBrowserView.focus=false
                    // Not Implemented
                    notImplementedView.visible=false
                    notImplementedView.focus=false
                    break

                case 8: // FILE BROWSER
                    // Main Menu
                    mainMenuView.visible=false
                    mainMenuView.focus=false
                    // Calibration
                    calibView.visible=false
                    calibView.focus=false
                    // Sonometer
                    sonometerView.visible=false
                    sonometerView.focus=false
                    // Reverberation Waiting Room
                    reverbWaitView.visible=false
                    reverbWaitView.focus=false
                    // Reverberation Measure
                    reverbMeasureView.visible=false
                    reverbMeasureView.focus=false
                    // Reverberation Result
                    reverbResultView.visible=false
                    reverbResultView.focus=false
                    // Configuration
                    configView.visible=false
                    configView.focus=false
                    // About
                    aboutView.visible=false
                    aboutView.focus=false
                    // File Browser
                    fileBrowserView.visible=true
                    fileBrowserView.focus=true
                    // Not Implemented
                    notImplementedView.visible=false
                    notImplementedView.focus=false
                    break

                case 99: // NOT IMPLEMENTED
                    // Main Menu
                    mainMenuView.visible=false
                    mainMenuView.focus=false
                    // Calibration
                    calibView.visible=false
                    calibView.focus=false
                    // Sonometer
                    sonometerView.visible=false
                    sonometerView.focus=false
                    // Reverberation Waiting Room
                    reverbWaitView.visible=false
                    reverbWaitView.focus=false
                    // Reverberation Measure
                    reverbMeasureView.visible=false
                    reverbMeasureView.focus=false
                    // Reverberation Result
                    reverbResultView.visible=false
                    reverbResultView.focus=false
                    // Configuration
                    configView.visible=false
                    configView.focus=false
                    // About
                    aboutView.visible=false
                    aboutView.focus=false
                    // File Browser
                    fileBrowserView.visible=false
                    fileBrowserView.focus=false
                    // Not Implemented
                    notImplementedView.visible=true
                    notImplementedView.focus=true
                    break
                }
            }
        }

    Image {
        id: background
        source: "MainMenuImages/background.png"
        x: setX(0)
        y: setY(-1)
        z: 0
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    /** Loading **/
    Item {
        id:loadingScreen
        width:Screen.width
        height:applicationWindow.height

        Loader{
            id:pageLoader
        }

        Image {
            id: background_loading
            source: "LoadingImages/background_loading.png"
            fillMode: Image.Stretch
            x: 0
            y: -1
            width:Screen.width
            height:applicationWindow.height
            opacity: 1
        }
        Text {
            id: loading
            x: setX(646)
            y: setY(1538)
            text: "Loading…"
            font.pixelSize: 12
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            opacity: 1
        }
        Image {
            id: loading_empty
            source: "LoadingImages/loading_empty.png"
            x: setX(287)
            y: setY(1450)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: loading_bar
            source: "LoadingImages/loading_full.png"
            x: setX(287)
            y: setY(1450)
            width:10//setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Timer{
            id:timer_width1
            running: true
            interval: 700/*1250*/
            repeat: true
            onTriggered: {
                loading_bar.width=loading_bar.width+((setX(loading_bar.sourceSize.width))*1/3)
            }
        }

        /** Logo AllianTech **/
        Image {
            id: logo_trace
            source: "LoadingImages/logo_trace.png"
            x:setX(554)
            y: setY(925)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: logo_01
            source: "LoadingImages/logo_01.png"
            x: setX(130)
            y: setY(755)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: logo_02
            source: "LoadingImages/logo_02.png"
            x: setX(861)
            y: setY(924)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: logo_03
            source: "LoadingImages/logo_03.png"
            x: setX(542)
            y: setY(741)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        /** Logo Footer **/
        Image {
            id: logo_footer
            source: "LoadingImages/logo_footer.png"
            x: setX(155)
            y: setY(2233)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }

        Timer{
            id:timer
            running: true
            interval: 2000
            repeat: false
            onTriggered:{
                timer_width1.stop()

                loadingScreen.visible=false

                mainMenuView.visible=true
                footersScreen.visible=true
                headerScreen.visible=true

                footersScreen.enabled=true
                headerScreen.enabled=true

                console.log("ATomic state : " + audioget.atomicState)

                if(audioget.atomicState === 0){
                    audioget.atomicState=2

                } else {
                    config.setConfigurationAtStart()
                    audioget.reset()
                }
            }
        }
    }

    /** Main Menu **/
    header:Item {
        id: headerScreen
        z: 2
        Header{
        }
        visible: false
    }

    Item {
        id: animationSablier
        z: 1
        Animation{
        }
        visible: false
    }

    Item {
        id: mainMenuView
        z: 1
        MainMenu{
        }
        visible: false
    }

    Item {
        id: calibView
        z: 1
        Calibration{
        }
        visible: false
    }

    Item {
        id: sonometerView
        z: 1
        SwipeSonometer{
        }
        visible: false
    }

    Item {
        id: reverbWaitView
        z: 1
        ReverbWaitingRoom{
        }
        visible: false
    }

    Item {
        id: reverbMeasureView
        z: 1
        ReverbMeasure{
        }
        visible: false
    }

    Item {
        id: reverbResultView
        z: 1
        SwipeReverbResult{
        }
        visible: false
    }

    Item {
        id: configView
        z: 1
        SwipeConfiguration{
        }
        visible: false
    }

    Item {
        id: aboutView
        z: 1
        AboutScroll{
        }
        visible: false
    }

    Item {
        id: fileBrowserView
        z: 1
        FileBrowser{
        }
        visible: false
    }

    Item {
        id: notImplementedView
        z: 1
        NotImplemented{
        }
        visible: false
    }

    Item {
        id: footersScreen
        z: 2
        Footer{
        }
        visible: false
    }

    //Use for Item parameter: x
    function setX(x){
        return (x/1500*Screen.width)
        /* - Item Width Size (on Adobe): L=(360*2)=720
           - For Now: Size*2 to improve Quality (It's possible to comeback to the original size but we have to export all
           the Adobe's Items another time)
           - Conversion to Pixels Size: L=1500

           - Use for Background Item parameters Width
        */
    }

    //Use for Item parameter: y
    function setY(y){
        return (y/2667*applicationWindow.height)
        /* - Item Height Size (on Adobe): H=(640*2)=1280
           - For Now: Size*2 to improve Quality (It's possible to comeback to the original size but we have to export all
           the Adobe's Items another time)
           - Conversion to Pixels Size: H=2667

           - Use for Background Item parameters Height
        */
    }

    //Use for Item parameters: width and height (To evenly ajust the size)
    function setSize(s){
        return ((s*Screen.width)/1500)
        /* - Based exclusively on the width
           - Item Width Size (on Adobe): L=(360*2)=720
           - For Now: Size*2 to improve Quality (It's possible to comeback to the original size but we have to export all
           the Adobe's Items another time)
           - Conversion to Pixels Size: L=1503
        */
    }

    function setheight(w,ws,hs){
        return (w/(ws/hs))
    }

    onClosing: {
        close.accepted=false
    }
}
