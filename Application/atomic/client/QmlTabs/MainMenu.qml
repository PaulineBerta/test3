import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQml 2.12
import QtQuick.Dialogs 1.1

Item {
    id: mainMenuItem
    x:0
    y:superior_panel.height
    width:Screen.width
    height:applicationWindow.height-superior_panel.height-inferior_panel_front.height

    Image {
        id: superior_panel
        source: "MainMenuImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }
    Image {
        id: inferior_panel_front
        source: "MainMenuImages/inferior_panel_front.png"
        visible: false
        x: setX(245)
        y: setY(2425)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    Text {
        id: config_modes
        text:"Configuration Modes"
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 25
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        y: setY(700)-superior_panel.height
        opacity: 1
    }

    Image {
        id: calib_logo
        source: "MainMenuImages/logocalib.png"
        //x: setX(500) //525
        anchors.horizontalCenter: parent.horizontalCenter
        y: setY(850)-superior_panel.height
        width:setSize(sourceSize.width*1.1)
        height:setSize(sourceSize.height*1.1)
        opacity: 1
        MouseArea {
            anchors.fill: parent
            onClicked: {
                //audioget.screenState=1
                audioget.setModeApp(1);
                console.log("%%%%%%%% SONOMETER CONFIG FOR CALIBRATION %%%%%%%%%")

                audioget.screenState=1
                console.log("%%%%%%%% CALIBRATION AREA %%%%%%%%%")
            }
        }
    }
    Image {
        id: sonometer_logo
        source: "MainMenuImages/logosono.png"
        //x: setX(490) //15
        anchors.horizontalCenter: parent.horizontalCenter
        y: setY(1300)-superior_panel.height
        width:setSize(sourceSize.width*1.1)
        height:setSize(sourceSize.height*1.1)
        opacity: 1
        MouseArea {
            anchors.fill: parent
            onClicked: {
                audioget.screenState=2

                audioget.setModeApp(1)
                console.log("%%%%%%%% SONOMETER AREA %%%%%%%%%")
            }
        }
    }
    Image {
        id: reverb_logo
        source: "MainMenuImages/logoreverb.png"
        //x: setX(490) //15
        anchors.horizontalCenter: parent.horizontalCenter
        y: setY(1765)-superior_panel.height
        width:setSize(sourceSize.width*1.1)
        height:setSize(sourceSize.height*1.1)
        opacity: 1
        MouseArea {
            anchors.fill: parent
            onClicked: {
                audioget.screenState=3

                audioget.setModeApp(2)
                console.log("%%%%%%%% REVERBERATION AREA %%%%%%%%%")
            }
        }
    }
}
