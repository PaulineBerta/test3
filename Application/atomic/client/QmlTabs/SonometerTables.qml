import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1

Item {
    id:sonometerFrequenciesScreen
    x:0
    y:0
    width:Screen.width
    height:applicationWindow.height-superior_panel.height-inferior_panel_front.height

    /** Background **/
    Image {
        id: superior_panel
        source: "SonometerTablesImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }
    Image {
        id: inferior_panel_front
        source: "SonometerTablesImages/inferior_panel_front.png"
        visible: false
        x: setX(245)
        y: setY(2425)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    Flickable{
        id: myFlickable
        x: setX(0)
        y: setY(650)-superior_panel.height
        height : applicationWindow.height-superior_panel.height-inferior_panel_front.height
        width : Screen.width

        contentHeight: if(config.spectrumConfiguration === "SPECTRUM_3RD_OCTAVE") {
                            (1060+inferior_panel_front.height)

                       } else if (config.spectrumConfiguration === "SPECTRUM_OCTAVE") {
                            (560+inferior_panel_front.height)
                       }

        contentWidth: Screen.width
        interactive: true
        boundsMovement: Flickable.StopAtBounds
        boundsBehavior: Flickable.StopAtBounds

        ScrollBar.vertical: ScrollBar {
            policy: ScrollBar.AlwaysOn
        }

        Column{
            spacing: 12

            SecondTable{
            }

            FirstTable{
            }
        }
    }

    /** Window Indicator **/
    Image {
        id: other_window_indicator_01
        source: "SonometerTablesImages/other_window_indicator_01.png"
        x: setX(583)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_02
        source: "SonometerTablesImages/other_window_indicator_02.png"
        x: setX(683)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_03
        source: "SonometerTablesImages/other_window_indicator_03.png"
        x: setX(783)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: blue_window_indicator
        source: "SonometerTablesImages/blue_window_indicator.png"
        x: setX(879)
        y: setY(2321)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
}
