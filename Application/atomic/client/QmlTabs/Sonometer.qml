import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1

Item {
    id:sonometerScreen
    x:0
    y:0
    width:Screen.width
    height:Screen.height

    Connections{
        target: audioget
        onLAeqTChanged: {
            lAeqTOver1.text= audioget.lAeqTUNDERLOADCHAR
            lAeqT1.text=audioget.lAeqT
        }
        onLCeqTChanged: {
            lCeqOver1.text= audioget.lCeqTUNDERLOADCHAR
            lCeq1.text= audioget.lCeqT
        }
        onLCpeakTChanged: {
            lCpeakTOver1.text= audioget.lCpeakTUNDERLOADCHAR
            lCpeakT1.text= audioget.lCpeakT
        }

        onLAeqGChanged: {
            lAeqGOver1.text= audioget.lAeqGUNDERLOADCHAR
            lAeqG1.text= audioget.lAeqG
        }
        onLCeqGChanged: {
            lCeqGOver1.text= audioget.lCeqGUNDERLOADCHAR
            lCeqG1.text= audioget.lCeqG
        }
        onLCpeakGChanged: {
            lCpeakGOver1.text= audioget.lCpeakGUNDERLOADCHAR
            lCpeakG1.text= audioget.lCpeakG
        }
    }

    Image {
        id: superior_panel
        source: "SonometerImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }
    Image {
        id: inferior_panel_front
        source: "SonometerImages/inferior_panel_front.png"
        visible: false
        x: setX(245)
        y: setY(2425)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    /** Integration Time **/
    Text {
        text: "Integration Time"
        font.pixelSize: 16
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(167)
        y: setY(681.75)-superior_panel.height
        opacity: 1
    }

    Image {
        id: decibels_values_background_01
        source: "SonometerImages/decibels_values_background.png"
        x: setX(64)
        y: setY(813)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }
    Image {
        id:decibels_values_background_02
        source: "SonometerImages/decibels_values_background.png"
        x: setX(775)
        y: setY(813)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    /* LAeq */
    Text {
        text: "LAeqT (dB)"
        font.pixelSize: 22
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(180) //201
        y: setY(1103.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lAeqTOver1
        text: audioget.lAeqTUNDERLOADCHAR
        font.pixelSize: 40
        font.family: "Digital-7"
        color: "#f83f07"
        x: setX(89)
        y: setY(864.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lAeqT1
        text: audioget.lAeqT
        font.pixelSize: 71
        font.family: "Digital-7"
        color: "#f83f07"
        smooth: true
        x: setX(233)//185
        y: setY(874.25)-superior_panel.height
        opacity: 1
        horizontalAlignment: Text.AlignHCenter
        onTextChanged: {
            if(audioget.overload===3) {
                decibels_values_background_01.source="SonometerImages/decibels_values_warning_background.png"
                decibels_values_background_02.source="SonometerImages/decibels_values_warning_background.png"

            } else if(audioget.overload===2) {
                decibels_values_background_01.source="SonometerImages/decibels_values_background.png"
                decibels_values_background_02.source="SonometerImages/decibels_values_warning_background.png"

            } else if(audioget.overload===1) {
                decibels_values_background_01.source="SonometerImages/decibels_values_warning_background.png"
                decibels_values_background_02.source="SonometerImages/decibels_values_background.png"

            } else {
                decibels_values_background_01.source= "SonometerImages/decibels_values_background.png"
                decibels_values_background_02.source= "SonometerImages/decibels_values_background.png"
            }
        }
    }

    /* LCeq */
    Text {
        text: "LCeqT (dB)"
        font.pixelSize: 22
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(180)
        y: setY(1468.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCeqOver1
        text: audioget.lCeqTUNDERLOADCHAR
        font.pixelSize: 30
        font.family: "Digital-7"
        color:"#ff8500"
        x: setX(200)
        y: setY(1285.75)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCeq1
        text: audioget.lCeqT
        font.pixelSize: 52
        font.family: "Digital-7"
        color: "#ff8500"
        smooth: true
        x: setX(344)
        y: setY(1295.75)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }

    /* LCpeak */
    Text {
        text: "LCpeakT (dB)"
        font.pixelSize: 22
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(180)
        y: setY(1823.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCpeakTOver1
        text: audioget.lCpeakTUNDERLOADCHAR
        font.pixelSize: 30
        font.family: "Digital-7"
        color:"#fdfd2c"
        x: setX(200)
        y: setY(1638.75)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCpeakT1
        text: audioget.lCpeakT
        font.pixelSize: 52
        font.family: "Digital-7"
        color: "#fdfd2c"
        smooth: true
        x: setX(344)
        y: setY(1648.75)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }

    /** Global **/
    Text {
        id: global
        text: "Global"
        font.pixelSize: 16
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1195)
        y: setY(679.75)-superior_panel.height
        opacity: 1
    }

    /* LAeq Global */
    Text {
        text: "LAeqG (dB)"
        font.pixelSize: 22
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(893)
        y: setY(1103.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lAeqGOver1
        text: audioget.lAeqGUNDERLOADCHAR
        font.pixelSize: 40
        font.family: "Digital-7"
        color:"#0092ff"
        x: setX(793)
        y: setY(864)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lAeqG1
        text: audioget.lAeqG
        font.pixelSize: 71
        font.family: "Digital-7"
        color: "#0092ff"
        smooth: true
        x: setX(937)
        y: setY(874)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }

    /* LCeq Global */
    Text {
        text: "LCeqG (dB)"
        font.pixelSize: 22
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(893)
        y: setY(1468.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCeqGOver1
        text: audioget.lCeqGUNDERLOADCHAR
        font.pixelSize: 30
        font.family: "Digital-7"
        color:"#579bd8"
        x: setX(915)
        y:  setY(1284.75)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCeqG1
        text: audioget.lCeqG
        font.pixelSize: 52
        font.family: "Digital-7"
        color: "#579bd8"
        smooth: true
        x: setX(1059)
        y: setY(1294.75)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }

    /* LCpeak Global */
    Text {
        text: "LCpeakG (dB)"
        font.pixelSize: 22
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(893)
        y: setY(1823.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCpeakGOver1
        text: audioget.lCpeakGUNDERLOADCHAR
        font.pixelSize: 30
        font.family: "Digital-7"
        color: "#adcff0"
        x: setX(915)
        y: setY(1638.75)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCpeakG1
        text: audioget.lCpeakG
        font.pixelSize: 52
        font.family: "Digital-7"
        color: "#adcff0"
        smooth: true
        x: setX(1059)
        y: setY(1648.75)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }

    /** Window Indicator **/
    Image {
        id: blue_window_indicator
        source: "SonometerImages/blue_window_indicator.png"
        x: setX(579)
        y: setY(2321)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_01
        source: "SonometerImages/other_window_indicator_01.png"
        x: setX(783)
        y: setY(2325)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_02
        source: "SonometerImages/other_window_indicator_02.png"
        x: setX(883)
        y: setY(2325)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_03
        source: "SonometerImages/other_window_indicator_03.png"
        x: setX(683)
        y: setY(2325)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }
}
