import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQuick.Dialogs 1.1
import "./"

Item {
    id: configReverbItem
    x:0
    y:0
    width:Screen.width
    height:applicationWindow.height

    function getSourceColor(sArg) {
        if(config.source === sArg) {
            this.color="#0c85c7"

        } else {
            this.color="#ffffff"
        }
    }

    function getSpectrumTypeColor(sArg) {
        if(config.spectrumType === sArg) {
            this.color="#0c85c7"

        } else {
            this.color="#ffffff"
        }
    }

    function getChartTimeColor(sArg) {
        if(config.chartTime === sArg) {
            this.color="#0c85c7"

        } else {
            this.color="#ffffff"
        }
    }

    Connections{
        target: config
        onSourceChanged: {
            switch(config.source){
            case "INTERRUPTED":
                continuous.color="#0c85c7"
                impulse.color="#ffffff"

                source_outline.x=setX(135)
                source_outline.y=setY(724)
                source_center.x=setX(143)
                source_center.y=setY(732)
                break

            case "IMPULSE":
                continuous.color="#ffffff"
                impulse.color="#0c85c7"

                source_outline.x=setX(435)
                source_outline.y=setY(724)
                source_center.x= setX(443)
                source_center.y=setY(732)
                break
            }
        }

        onSpectrumTypeChanged: {
            switch(config.spectrumType){
            case "SPECTRUM_OCTAVE":
                octave.color="#0c85c7"
                thirdoctave.color="#ffffff"

                type_spectrum_outline.x=setX(135)
                type_spectrum_outline.y=setY(957)
                type_spectrum_center.x=setX(143)
                type_spectrum_center.y=setY(965)
                break

            case "SPECTRUM_3RD_OCTAVE":
                octave.color="#ffffff"
                thirdoctave.color="#0c85c7"

                type_spectrum_outline.x=setX(435)
                type_spectrum_outline.y=setY(957)
                type_spectrum_center.x= setX(443)
                type_spectrum_center.y=setY(965)
                break
            }
        }

        onChartTimeChanged: {
            switch(config.chartTime){
            case "3":
                second4.color="#0c85c7"
                second11.color="#ffffff"

                chart_time_outline.x=setX(135)
                chart_time_outline.y=setY(2042)
                chart_time_center.x=setX(143)
                chart_time_center.y=setY(2050)
                break

            case "10":
                second4.color="#ffffff"
                second11.color="#0c85c7"

                chart_time_outline.x=setX(435)
                chart_time_outline.y=setY(2042)
                chart_time_center.x= setX(443)
                chart_time_center.y=setY(2050)
                break
            }
        }
    }

    /** BACKGROUND **/
    Image {
        id: trace_config_01
        source: "ConfigurationImages/trace_config_01.png"
        x: setX(683)
        y: setY(565)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /** Test Parameters **/
    Text {
        id: testparameters
        text: "Test Parameters"
        font.pixelSize: 14
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(104)
        y: setY(546.25)
        opacity: 1
    }

    /* Source */
    Text {
        id: source
        text: "Source"
        font.pixelSize: 13
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(106)
        y: setY(625.75)
        opacity: 1
    }

    Text {
        id: continuous
        text: "Continuous"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(218)
        y: setY(727.75)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                source_outline.visible=true
                source_center.visible=true

                source_outline.x=setX(135)
                source_outline.y=setY(724)
                source_center.x=setX(143)
                source_center.y=setY(732)

                config.source="INTERRUPTED"
                continuous.color="#0c85c7"
                impulse.color="#ffffff"
                console.log("source",config.source)
            }
        }
        Component.onCompleted: getSourceColor("INTERRUPTED")
    }
    Image {
        id: ellipse_continuous_reverb
        source: "ConfigurationImages/ellipse_continuous_reverb.png"
        x: setX(135)
        y: setY(724)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                source_outline.visible=true
                source_center.visible=true

                source_outline.x=setX(135)
                source_outline.y=setY(724)
                source_center.x=setX(143)
                source_center.y=setY(732)

                config.source="INTERRUPTED"
                continuous.color="#0c85c7"
                impulse.color="#ffffff"
            }
        }
    }

    Text {
        id: impulse
        text: "Impulse"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(517)
        y: setY(727.75)
        opacity: 1
        MouseArea{
            anchors.fill: parent
            onClicked: {
                source_outline.visible=true
                source_center.visible=true

                source_outline.x=setX(435)
                source_outline.y=setY(724)
                source_center.x= setX(443)
                source_center.y=setY(732)

                config.source="IMPULSE"
                continuous.color="#ffffff"
                impulse.color="#0c85c7"
                console.log("source",config.source)
            }
        }
        Component.onCompleted: getSourceColor("IMPULSE")
    }
    Image {
        id: ellipse_impulse_reverb
        source: "ConfigurationImages/ellipse_impulse_reverb.png"
        visible: true
        x: setX(435)
        y: setY(724)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                source_outline.visible=true
                source_center.visible=true

                source_outline.x=setX(435)
                source_outline.y=setY(724)
                source_center.x= setX(443)
                source_center.y=setY(732)

                config.source="IMPULSE"
                continuous.color="#ffffff"
                impulse.color="#0c85c7"
            }
        }
    }

    Image {
        id: source_outline
        source: "ConfigurationImages/source_outline.png"
        visible: true
        x: if(config.source==="INTERRUPTED") {
                setX(435)

            } else if(config.source==="IMPULSE") {
                setX(760)
            }
        y: setY(724)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    Image {
        id: source_center
        source: "ConfigurationImages/source_center.png"
        visible: true
        x: if(config.source==="INTERRUPTED") {
                setX(443)

            } else if(config.source==="IMPULSE") {
                setX(768)
            }
        y: setY(732)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /* Type of Spectrum */
    Text {
        id: spectrumType
        text: "Type of Spectrum"
        font.pixelSize: 13
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(106)
        y: setY(858.75)
        opacity: 1
    }

    Text {
        id: octave
        text: "Octave"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(218)
        y: setY(960.75)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                type_spectrum_outline.visible=true
                type_spectrum_center.visible=true

                type_spectrum_outline.x=setX(135)
                type_spectrum_outline.y=setY(957)
                type_spectrum_center.x=setX(143)
                type_spectrum_center.y=setY(965)

                config.spectrumType="SPECTRUM_OCTAVE"
                octave.color="#0c85c7"
                thirdoctave.color="#ffffff"
                console.log("source",config.spectrumType)
            }
        }
        Component.onCompleted: getSpectrumTypeColor("SPECTRUM_OCTAVE")
    }
    Image {
        id: ellipse_octave_reverb
        source: "ConfigurationImages/ellipse_octave_reverb.png"
        x: setX(135)
        y: setY(957)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                type_spectrum_outline.visible=true
                type_spectrum_center.visible=true

                type_spectrum_outline.x=setX(135)
                type_spectrum_outline.y=setY(957)
                type_spectrum_center.x=setX(143)
                type_spectrum_center.y=setY(965)

                config.spectrumType="SPECTRUM_OCTAVE"
                octave.color="#0c85c7"
                thirdoctave.color="#ffffff"
            }
        }
    }

    Text {
        id: thirdoctave
        text: "3rd Octave"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(517)
        y: setY(960.75)
        opacity: 1
        MouseArea{
            anchors.fill: parent
            onClicked: {
                type_spectrum_outline.visible=true
                type_spectrum_center.visible=true

                type_spectrum_outline.x=setX(435)
                type_spectrum_outline.y=setY(957)
                type_spectrum_center.x=setX(443)
                type_spectrum_center.y=setY(965)

                config.spectrumType="SPECTRUM_3RD_OCTAVE"
                octave.color="#ffffff"
                thirdoctave.color="#0c85c7"
                console.log("source",config.spectrumType)
            }
        }
        Component.onCompleted: getSpectrumTypeColor("SPECTRUM_3RD_OCTAVE")
    }
    Image {
        id: ellipse_3rd_octave_reverb
        source: "ConfigurationImages/ellipse_3rd_octave_reverb.png"
        visible: true
        x: setX(435)
        y: setY(957)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                type_spectrum_outline.visible=true
                type_spectrum_center.visible=true

                type_spectrum_outline.x=setX(435)
                type_spectrum_outline.y=setY(957)
                type_spectrum_center.x=setX(443)
                type_spectrum_center.y=setY(965)

                config.spectrumType="SPECTRUM_3RD_OCTAVE"
                octave.color="#ffffff"
                thirdoctave.color="#0c85c7"
            }
        }
    }

    Image {
        id: type_spectrum_outline
        source: "ConfigurationImages/type_spectrum_outline.png"
        visible: true
        x: if(config.spectrumType === "SPECTRUM_OCTAVE") {
                setX(435)

            } else if(config.spectrumType==="SPECTRUM_3RD_OCTAVE") {
                setX(760)
            }
        y: setY(957)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    Image {
        id: type_spectrum_center
        source: "ConfigurationImages/type_spectrum_center.png"
        visible: true
        x: if(config.spectrumType === "SPECTRUM_OCTAVE") {
                setX(443)

            } else if(config.spectrumType === "SPECTRUM_3RD_OCTAVE") {
                setX(768)
            }
        y: setY(965)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /* Trigger Threshold */
    Text {
        text: "Trigger Threshold"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(107)
        y: setY(1090.75)
        opacity: 1
    }

    /* Decrease Time */
    Text {
        text: "Decrease Time"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(107)
        y: setY(1150.75)
        opacity: 1
    }

    /* Chart Time */
    Text {
        text: "Chart Time"
        font.pixelSize: 13
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(107)
        y: setY(1210.75)
        opacity: 1
    }

    /* Seconds */
    Text {
        id: timeseconds
        text: "Time (s)"
        font.pixelSize: 13
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(106)
        y: setY(1943.75)
        opacity: 1
    }

    Text {
        id: second4
        text: "4 seconds"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(218)
        y: setY(2045.75)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                chart_time_outline.visible=true
                chart_time_center.visible=true

                chart_time_outline.x=setX(135)
                chart_time_outline.y=setY(2042)
                chart_time_center.x=setX(143)
                chart_time_center.y=setY(2050)

                config.chartTime="3"
                second4.color= "#0c85c7"
                second11.color="#ffffff"
                console.log("time weighting",config.chartTime)
            }
        }
        Component.onCompleted: getChartTimeColor("3")
    }
    Image {
        id: ellipse_4_seconds
        source: "ConfigurationImages/ellipse_none.png"
        x: setX(135)
        y: setY(2042)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                chart_time_outline.visible=true
                chart_time_center.visible=true

                chart_time_outline.x=setX(135)
                chart_time_outline.y=setY(2042)
                chart_time_center.x=setX(143)
                chart_time_center.y=setY(2050)

                config.chartTime="3"
                second4.color= "#0c85c7"
                second11.color="#ffffff"
            }
        }
    }

    Text {
        id: second11
        text: "11 seconds"
        font.pixelSize: 13
        font.family: "Tahoma"
        smooth: true
        x: setX(517)
        y: setY(2043.75)
        opacity: 1
        MouseArea{
            anchors.fill: parent
            onClicked: {
                chart_time_outline.visible=true
                chart_time_center.visible=true

                chart_time_outline.x=setX(435)
                chart_time_outline.y=setY(2042)
                chart_time_center.x= setX(443)
                chart_time_center.y=setY(2050)

                config.chartTime="10"
                second4.color= "#0c85c7"
                second11.color="#ffffff"
                console.log("chartTime",config.chartTime)
            }
        }
        Component.onCompleted: getChartTimeColor("10")
    }
    Image {
        id: ellipse_11_seconds
        source: "ConfigurationImages/ellipse_slow.png"
        visible: true
        x: setX(435)
        y: setY(2042)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea{
            anchors.fill:parent
            onClicked: {
                chart_time_outline.visible=true
                chart_time_center.visible=true

                chart_time_outline.x=setX(435)
                chart_time_outline.y=setY(2042)
                chart_time_center.x= setX(443)
                chart_time_center.y=setY(2050)

                config.chartTime="10"
                second4.color= "#0c85c7"
                second11.color="#ffffff"
            }
        }
    }

    Image {
        id: chart_time_outline
        source: "ConfigurationImages/time_check_outline.png"
        visible: true
        x: if(config.chartTime === "3") {
                setX(435)

            } else if(config.chartTime === "10") {
                setX(760)
            }
        y: setY(2042)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    Image {
        id: chart_time_center
        source: "ConfigurationImages/time_check_center.png"
        visible: true
        x: if(config.chartTime === "3") {
                setX(443)

            } else if(config.chartTime === "10") {
                setX(768)
            }
        y: setY(2050)
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
}
