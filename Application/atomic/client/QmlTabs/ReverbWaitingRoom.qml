import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQml 2.12
import QtQuick.Dialogs 1.1

import QtQuick 2.6
import QtQuick.Window 2.2

Item {
    id: reverbItem
    x:0
    y:superior_panel.height
    width:Screen.width
    height:Screen.height

    Connections {
        target: audioget
        onProjectNameChanged:{
            text_project_name.text = audioget.projectName
            console.log("project Name : "+audioget.projectName)
        }

        onRoomTypeChanged:{
            text_room_type.text = audioget.roomType
            console.log("room Type : "+audioget.roomType)
        }

        onRoomNumberChanged:{
            text_room_number.text = audioget.roomNumber
            console.log("room Number : "+audioget.roomNumber)
        }
    }

    Image {
        id: superior_panel
        source: "ReverberationImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }

    /* Project Parameters */
    Text {
        id: project_param
        text: "Project Parameters"
        font.pixelSize: 17
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(106)
        y: setY(723)-superior_panel.height
        opacity: 1
    }
    Image {
        id: trace_project_parameters
        source: "ReverberationImages/trace_project_parameters.png"
        x: setX(904)
        y: setY(748)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }

    /* Project Name */
    Text {
        id: project_name
        text: "Project Name"
        font.pixelSize: 15
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(228)
        y: setY(920)-superior_panel.height
        opacity: 1
    }
    Image {
        id: rect_project_name
        source: "ReverberationImages/rect_project_name.png"
        x: setX(92)
        y: setY(1018)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        visible: false
    }
    Item {
        id: text_project_name
        property alias text: name.text
        x: setX(92)
        y: setY(1018)-superior_panel.height
        width: rect_project_name.width
        height: rect_project_name.height
        BorderImage {
            source: "ReverberationImages/rect_project_name.png"
            anchors.fill: parent
        }
        TextInput {
            id: name
            text: "Default" // Value by Default
            color: "#ffffff" //"#ABABAB"
            selectionColor: "blue"
            font.pixelSize: 14
            width: parent.width-16
            maximumLength: 16
            leftPadding: 15
            anchors.centerIn: parent
            validator: RegExpValidator {}

            onTextChanged: {
                audioget.projectName = text;
                console.log("name_text")
                console.log(text)
            }
        }
    }

    /* Room Type */
    Text {
        id: room_type
        text: "Room Type"
        font.pixelSize: 15
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(228)
        y: setY(1292)-superior_panel.height
        opacity: 1
    }
    Image {
        id: rect_room_type
        source: "ReverberationImages/rect_room_type.png"
        x: setX(92)
        y: setY(1388)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        visible: false
    }
    Item {
        id: text_room_type
        property alias text: type.text
        x: setX(92)
        y: setY(1388)-superior_panel.height
        width: rect_room_type.width
        height: rect_room_type.height
        BorderImage {
            source: "ReverberationImages/rect_room_type.png"
            anchors.fill: parent
        }
        TextInput {
            id: type
            text: "Workshop" // Value by Default
            color: "#ffffff" //"#ABABAB"
            selectionColor: "blue"
            font.pixelSize: 14
            width: parent.width-16
            maximumLength: 16
            leftPadding: 15
            anchors.centerIn: parent
            validator: RegExpValidator {}

            onTextChanged: {
                audioget.roomType = text;
                console.log("type_text")
                console.log(text)
            }
        }
    }

    /* Room Number */
    Text {
        id: room_number
        text: "Room Number"
        font.pixelSize: 15
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(228)
        y: setY(1662)-superior_panel.height
        opacity: 1
    }
    Image {
        id: rect_room_number
        source: "ReverberationImages/rect_room_number.png"
        x: setX(92)
        y: setY(1764)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        visible: false
    }
    Item {
        id: text_room_number
        property alias text: number.text
        x: setX(92)
        y: setY(1764)-superior_panel.height
        width: rect_room_number.width
        height: rect_room_number.height
        BorderImage {
            source: "ReverberationImages/rect_room_number.png"
            anchors.fill: parent
        }
        TextInput {
            id: number
            text: "1" // Value by Default
            color: "#ffffff" //"#ABABAB"
            selectionColor: "blue"
            font.pixelSize: 14
            width: parent.width-16
            maximumLength: 16
            leftPadding: 15
            anchors.centerIn: parent
            validator: IntValidator {
                            top: 100;
                            bottom: 1;
            }

            onTextChanged: {
                audioget.roomNumber = text;
                console.log("number_text")
                console.log(text)
            }
        }
    }
}
