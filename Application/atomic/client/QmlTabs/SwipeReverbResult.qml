import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQuick.Window 2.12
import "./"

Item {
    id : swipeReverbSource
    Loader{
        id :pageLoader
    }

    Image {
        id: superior_panel
        source: "ReverberationImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }

    Image {
        id: inferior_panel_front
        source: "ReverberationImages/inferior_panel_front.png"
        visible:false
        x: setX(245)
        y: setY(2425)-(applicationWindow.height-inferior_panel_front.height )
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }

    Item {
        id: swipeReverbView
        x:0
        y:superior_panel.height
        height:applicationWindow.height-superior_panel.height-inferior_panel_front.height
        width:Screen.width

        SwipeView {
            id: swipeViewReverb
            visible: true
            anchors.fill:parent
            interactive: true
            ReverbResult{}
            ReverbResultTab{}
            ReverbResultTabAvg{}
        }
    }
}
