﻿import QtQuick.Window 2.1
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQml 2.12
import BarQuick2 1.0

Item {
    id:sonometerFrequenciesScreen
    x:0
    y:0
    width:Screen.width
    height:Screen.height

    /** Background **/
    Image {
        id: superior_panel
        source: "SonometerFrequenciesImages/superior_panel.png"
        visible: false
        x: setX(0)
        y: setY(0)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
    }
    Image {
        id: inferior_panel_front
        source: "SonometerFrequenciesImages/inferior_panel_front.png"
        visible: false
        x: setX(245)
        y: setY(2425)-(applicationWindow.height-inferior_panel_front.height)
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        opacity: 1
    }
    /*Image {
        id: int_time_and_global_background
        source: "SonometerFrequenciesImages/int_time_and_global_background.png"
        x: setX(45)
        y: setY(75)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }*/

    /** Decibels Values Background **/
    /*Image {
        id: decibels_values_background_01
        source: "SonometerFrequenciesImages/decibels_values_background.png"
        x: setX(125)
        y: setY(517)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: decibels_values_background_02
        source: "SonometerFrequenciesImages/decibels_values_background.png"
        x: setX(796)
        y: setY(517)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }*/

    /** Decibels Values Area **/

    /* Laeq */
    /*Text {
        id: lAeqTPlotLegend
        text: "LAeqT (dB)"
        font.pixelSize: 12
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(403)
        y: setY(744)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lAeqUnderloadPlotInd
        text: audioget.lAeqTUNDERLOADCHAR
        font.pixelSize: 30
        font.family: "Digital-7"
        color: "#f83f07"
        x: setX(133)
        y: setY(558.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lAeqTPlotInd
        text: audioget.lAeqT
        font.pixelSize: 54
        font.family: "Digital-7"
        color: "#f83f07"
        smooth: true
        x: setX(277)
        y: setY(568.25)-superior_panel.height
        opacity: 1
        onTextChanged: {
            if(audioget.overload === 3) {
                decibels_values_background_01.source="SonometerFrequenciesImages/decibels_values_warning_background.png"
                decibels_values_background_02.source="SonometerFrequenciesImages/decibels_values_warning_background.png"

            } else if(audioget.overload === 2) {
                decibels_values_background_01.source="SonometerFrequenciesImages/decibels_values_background.png"
                decibels_values_background_02.source="SonometerFrequenciesImages/decibels_values_warning_background.png"

            } else if(audioget.overload === 1) {
                decibels_values_background_01.source="SonometerFrequenciesImages/decibels_values_warning_background.png"
                decibels_values_background_02.source="SonometerFrequenciesImages/decibels_values_background.png"

            } else {
                decibels_values_background_01.source= "SonometerFrequenciesImages/decibels_values_background.png"
                decibels_values_background_02.source= "SonometerFrequenciesImages/decibels_values_background.png"
            }
        }
    }*/

    /* LCeq */
    /*Text {
        id: lCeqTPlotLegend
        text: "LCeqT (dB)"
        font.pixelSize: 12
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(411)
        y: setY(950)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCeqUnderloadPlotInd
        text: audioget.lCeqTUNDERLOADCHAR
        font.pixelSize: 20
        font.family: "Digital-7"
        color:"#ff8500"
        x: setX(298)
        y: setY(826.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCeqTPlotInd
        text: audioget.lCeqT
        font.pixelSize: 35
        font.family: "Digital-7"
        color: "#ff8500"
        smooth: true
        x: setX(442)
        y: setY(836.25)-superior_panel.height
        opacity: 1
    }*/

    /* LCpeak */
    /*Text {
        id: lCpeakTPlotLegend
        text: "LCpeakT (dB)"
        font.pixelSize: 12
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(360)
        y: setY(1139)-superior_panel.height
        opacity: 1
    }
    Text {
        id:lCpeakTUnderloadPlotInd
        text: audioget.lCpeakTUNDERLOADCHAR
        font.pixelSize: 20
        font.family: "Digital-7"
        color:"#fdfd2c"
        x: setX(298)
        y: setY(1016.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCpeakTPlotInd
        text: audioget.lCpeakT
        font.pixelSize: 35
        font.family: "Digital-7"
        color: "#fdfd2c"
        smooth: true
        x: setX(442)
        y: setY(1026.25)-superior_panel.height
        opacity: 1
    }*/

    /* LAeq Global */
    /*Text {
        id: lAeqGPlotLegend
        text: "LAeqG (dB)"
        font.pixelSize: 12
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1078)
        y: setY(744)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lAeqGUnderloadPlotInd
        text: audioget.lAeqGUNDERLOADCHAR
        font.pixelSize: 30
        font.family: "Digital-7"
        color:"#0092ff"
        x: setX(841)
        y: setY(558.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lAeqGPlotInd
        text: audioget.lAeqG
        font.pixelSize: 54
        font.family: "Digital-7"
        color: "#0092ff"
        smooth: true
        x: setX(985)
        y: setY(568.25)-superior_panel.height
        opacity: 1
    }*/

    /* LCeq Global */
    /*Text {
        id: lCeqGUnderloadPlotInd
        text: audioget.lCeqGUNDERLOADCHAR
        font.pixelSize: 20
        font.family: "Digital-7"
        color:"#579bd8"
        x: setX(969)
        y:  setY(826)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCeqGPlotInd
        text: audioget.lCeqG
        font.pixelSize: 35
        font.family: "Digital-7"
        color: "#579bd8"
        smooth: true
        x: setX(1113)
        y: setY(836.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCeqGPlotLegend
        text: "LCeqG (dB)"
        font.pixelSize: 12
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1078)
        y: setY(950)-superior_panel.height
        opacity: 1
    }*/

    /* LCpeak Global */
    /*Text {
        id: lCpeakGUnderloadPlotInd
        text: audioget.lCpeakGUNDERLOADCHAR
        font.pixelSize: 20
        font.family: "Digital-7"
        color: "#adcff0"
        x: setX(969)
        y: setY(1016.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCpeakGPlotInd
        text: audioget.lCpeakG
        font.pixelSize: 35
        font.family: "Digital-7"
        color: "#adcff0"
        smooth: true
        x: setX(1113)
        y: setY(1026.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: lCpeakGPlotLegend
        text: "LCpeakG (dB)"
        font.pixelSize: 12
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1027)
        y: setY(1139)-superior_panel.height
        opacity: 1
    }*/

    /* Graph Legend */
    Image {
        id: legend_global
        source: "SonometerFrequenciesImages/legend_global.png"
        x: setX(1222)
        y: setY(720)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        id: global
        text: "Global"
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1275)
        y: setY(720)-superior_panel.height
        opacity: 1
    }

    Image {
        id: legend_int_time
        source: "SonometerFrequenciesImages/legend_int_time.png"
        x: setX(964)
        y: setY(720)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Text {
        id: intTime
        text: "Int. Time"
        font.pixelSize: 10
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(1016)
        y: setY(720)-superior_panel.height
        opacity: 1
    }

    Text {
        id: spectrum
        text: "Spectrum"
        font.pixelSize: 10
        font.family: "Arial-BoldMT"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(143)
        y: setY(720)-superior_panel.height //setY(1342.25)-superior_panel.height
        opacity: 1
    }
    Text {
        id: db
        text: "dB"
        font.pixelSize: 10
        font.family: "Tahoma-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(55)
        y: setY(720)-superior_panel.height
        opacity: 1
    }

    /** Graph **/
    Image {
        id: graph_lines
        source: "SonometerFrequenciesImages/graph_lines.png"
        x: setX(52)
        y: setY(1387)-superior_panel.height
        width:setX(sourceSize.width)
        height:setY(sourceSize.height)
        visible: false
    }

    Connections{
        target: audioget
        onOctaveChanged: {
            plot.update()
        }
    }

    BarQuick2 {
        id: plot
        objectName: "plot"
        x: setX(75)
        y: setY(770)-superior_panel.height
        width:graph_lines.width
        height:graph_lines.height*2.2
        focus: true

        Component.onCompleted: {
            initBarPlot()
            plot.barWidth=plot.width
        }
    }

    /** Marker Area **/
    Image {
        id: marker_time_area
        source: "SonometerFrequenciesImages/marker_time_area.png"
        x: setX(632)
        y: setY(2012)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: marker_values_area
        source: "SonometerFrequenciesImages/marker_values_area.png"
        x: setX(228)
        y: setY(2087)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: marker_right
        source: "SonometerFrequenciesImages/marker_right.png"
        x: setX(1333)
        y: setY(2114)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea {
            anchors.fill: parent
            onClicked: plot.step(1)
        }
    }
    Image {
        id: marker_left
        source: "SonometerFrequenciesImages/marker_left.png"
        x: setX(86)
        y: setY(2114)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
        MouseArea {
            anchors.fill: parent
            onClicked: plot.step(-1)
        }
    }

    /* Frequency Value */
    Text {
        text: plot.frequency
        anchors.horizontalCenter: marker_values_area.horizontalCenter
        font.pixelSize: 10
        font.family: "Arial-BoldMT"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: setX(685)
        y: setY(2034.5)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }

    /* Decibel Values */
    Text {
        text: plot.leqGValue
        Layout.alignment :Qt.AlignHCenter | Qt.AlignVCenter
        font.pixelSize: 29
        font.family: "Digital-7"
        color: "#0092ff"
        smooth: true
        x: setX(917)
        y: setY(2105.5)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }
    Text {
        text: "LeqG (dB)"
        Layout.alignment :Qt.AlignHCenter | Qt.AlignVCenter
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(917) //x: setX(978)
        y: setY(2200)-superior_panel.height //2207
        opacity: 1
    }

    Text {
        text: plot.leqTValue
        Layout.alignment :Qt.AlignHCenter | Qt.AlignVCenter
        font.pixelSize: 29
        font.family: "Digital-7"
        color: "#adcff0"
        smooth: true
        x: setX(400)
        y: setY(2105.5)-superior_panel.height
        horizontalAlignment: Text.AlignHCenter
        opacity: 1
    }
    Text {
        text: "LeqT (dB)"
        Layout.alignment :Qt.AlignHCenter | Qt.AlignVCenter
        font.pixelSize: 8
        font.family: "Tahoma"
        color: "#ffffff"
        smooth: true
        x: setX(400) //x: setX(439)
        y: setY(2200)-superior_panel.height
        opacity: 1
    }

    /** Window Indicator **/
    Image {
        id: other_window_indicator_01
        source: "SonometerFrequenciesImages/other_window_indicator_01.png"
        x: setX(583)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_02
        source: "SonometerFrequenciesImages/other_window_indicator_02.png"
        x: setX(683)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: blue_window_indicator
        source: "SonometerFrequenciesImages/blue_window_indicator.png"
        x: setX(779)
        y: setY(2321)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
    Image {
        id: other_window_indicator_03
        source: "SonometerFrequenciesImages/other_window_indicator_03.png"
        x: setX(883)
        y: setY(2325)-superior_panel.height
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
}
