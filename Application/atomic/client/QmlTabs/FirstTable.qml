import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12

Item {
    id:tableRect2
    width:Screen.width
    height:300
    anchors.margins: 10

    TableView {
        id: tableView2
        anchors.fill: parent
        anchors.centerIn: parent

        columnSpacing: 0.0
        rowSpacing: 0.0
        clip: false
        interactive: false
        model: tableModel2

        delegate: Rectangle {

            implicitWidth: Screen.width/3 //120
            implicitHeight: 20
            border.color: "black"
            border.width: 0.6
            color: (heading==true)?"lightgray":"white"

            Connections{
                target: tableModel2
                onOverloadChanged:{
                    textId2.color="#272727"
                    switch(tableModel2.overload) {
                    case 1:
                        if(index>12 & index<24){textId2.color="red"}
                        break

                    case 2:
                        if(index>24 & index<36){textId2.color="red"}
                        break

                    case 3:
                        if(index>12 & heading==false){textId2.color="red"}
                        break

                    default: textId2.color="#272727"
                    }
                }
            }

            Text {
                id: textId2
                text: tableData
                font.pixelSize: 11
                font.family: "Tahoma-Bold"
                font.bold: true
                color: "#272727"
                smooth: true
                anchors.centerIn: parent
                //                onTextChanged: {
                //                    switch(tableModel2.overload){
                //                    case 1:
                //                        if(index>12 & index<24){textId2.color="red"}
                //                        break
                //                    case 2:
                //                        if(index>24 & index<36){textId2.color="red"}
                //                        break
                //                    case 3:
                //                        if(index>12 & heading==false){textId2.color="red"}
                //                        break
                //                    default: textId2.color="#272727"
                //                    }
                //                }
            }
        }
    }
}

