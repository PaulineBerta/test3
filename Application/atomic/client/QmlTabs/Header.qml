import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
import QtQuick.Dialogs 1.1
import "./"

Item {
    id:headeritem
    x:0
    y:0
    width:Screen.width
    height:superior_panel.height

    Connections{
        target: config
        onConfigFileNameChanged: {
            config_xml.text = config.configFileName.toString()
            console.log("config_xml : "+config_xml.text)
        }
    }

    Connections {
        target: audioget
        onChronometerSecondoChanged:{
            chronoSecond1.text=audioget.chronometerSecondo
        }
        onChronometerMinutesoChanged:{
            chronoMinutes1.text=audioget.chronometerMinuteso
        }
        onChronometerHoursoChanged:{
            chronoHours1.text= audioget.chronometerHourso
        }

        onChronometerSecondtChanged:{
            chronoSecond2.text=audioget.chronometerSecondt
        }
        onChronometerMinutestChanged:{
            chronoMinutes2.text=audioget.chronometerMinutest
        }
        onChronometerHourstChanged:{
            chronoHours2.text=audioget.chronometerHourst
        }

        onBlockLostChanged:  {
            console.log("Block Lost from QML")
            if(audioget.blockLost){
                icon_danger.visible=true
                background_error.visible=true
                error.visible=true
                acquisition.visible=true
                error.text="Block"
                acquisition.text="Lost"
                audioget.recover()
            }
            else {
                icon_danger.visible=false
                background_error.visible=false
                error.visible=false
                acquisition.visible=false
            }
        }

        onErrorChanged: {
            console.log("ERROR from QML")
            icon_danger.visible=audioget.isDanger
            background_error.visible=audioget.isDanger
            error.visible=audioget.isDanger
            acquisition.visible=audioget.isDanger
            acquisition.text=audioget.acquisition
            error.text=audioget.error
        }

        onButtonClickChanged: {
            switch(audioget.buttonClick) {
            case 220: // Sonometer Stop
                file_browser_config.visible=true
                break

            case 221: // Sonometer Play
                file_browser_config.visible=false
                break

            default:
                break
            }

            audioget.buttonClick=0 // None
        }

        onScreenStateChanged: {
            console.log("SCREEN STATE CHANGED :"+audioget.screenState)

            switch(audioget.screenState) {
            case 0: // MAIN MENU
                // Position Left Menu Indicator
                selected_icon_left.visible=true
                selected_icon_left.x=0
                selected_icon_left.y=setY(756)
                selected_icon_left.height=calibrationArea.y-configurationArea.y

                selected_icon_right.visible=false

                // Position Left Menu Indicator
                icon_advanced_parameters.visible=true
                white_logo_mode.visible=false

                configXml.visible=false
                file_browser_config.visible=false

                chrono_box.visible=false
                reverbCounter.visible=false
                comboBoxCounter.visible=false

                headerIndicator.visible=false
                break

            case 1: // CALIBRATION
                // Position Left Menu Indicator
                selected_icon_left.visible=true
                selected_icon_left.x=0
                selected_icon_left.y=setY(989.8)
                selected_icon_left.height=sonometerArea.y-calibrationArea.y

                selected_icon_right.visible=false

                // Header Information
                icon_advanced_parameters.visible=false
                white_logo_mode.visible=true
                white_logo_mode.source="HeaderImages/whitelogocalibsimple.png"
                white_logo_mode.x=setX(178)
                white_logo_mode.y=setY(212)

                configXml.visible=false
                file_browser_config.visible=false

                chrono_box.visible=true
                reverbCounter.visible=false
                comboBoxCounter.visible=false

                headerIndicator.visible=true
                break

            case 2: // SONOMETER
                // Position Left Menu Indicator
                selected_icon_left.visible=true
                selected_icon_left.x=0
                selected_icon_left.y=setY(1218)
                selected_icon_left.height=reverberationArea.y-sonometerArea.y

                selected_icon_right.visible=false

                // Header Information
                icon_advanced_parameters.visible=false
                white_logo_mode.visible=true
                white_logo_mode.source="HeaderImages/whitelogosonosimple.png"
                white_logo_mode.x=setX(174)
                white_logo_mode.y=setY(194)

                configXml.visible=true
                file_browser_config.visible=true

                chrono_box.visible=true
                reverbCounter.visible=false
                comboBoxCounter.visible=false

                headerIndicator.visible=true
                break

            case 3: // REVERBERATION WAITING ROOM
                // Position Left Menu Indicator
                selected_icon_left.visible=true
                selected_icon_left.x=0
                selected_icon_left.y=setY(1445)

                selected_icon_right.visible=false

                // Header Information
                icon_advanced_parameters.visible=false
                white_logo_mode.visible=true
                white_logo_mode.source="HeaderImages/whitelogoreverbsimple.png"
                white_logo_mode.x=setX(112)
                white_logo_mode.y=setY(212)

                configXml.visible=true
                file_browser_config.visible=true

                chrono_box.visible=false
                reverbCounter.visible=true
                comboBoxCounter.visible=false

                headerIndicator.visible=true

                rt_count.text = "RT T60 --"
                rt_count.y = setY(284)
                break

            case 4: // REVERBERATION MEASURE
                white_logo_mode.visible=true
                white_logo_mode.source="HeaderImages/whitelogoreverbsimple.png"

                file_browser_config.visible=false

                chrono_box.visible=false
                reverbCounter.visible=true
                comboBoxCounter.visible=false

                rt_count.text = "RT T60 N°"+(audioget.reverbCounter+1)
                rt_count.y = setY(270)
                break

            case 5: // REVERBERATION RESULT
                white_logo_mode.visible=true
                white_logo_mode.source="HeaderImages/whitelogoreverbsimple.png"

                file_browser_config.visible=false

                chrono_box.visible=false
                reverbCounter.visible=false
                comboBoxCounter.visible=true

                headerIndicator.visible=false
                break

            case 6: // CONFIGURATION
                // Position Left Menu Indicator
                selected_icon_left.visible=false

                selected_icon_right.visible=false

                // Header Information
                icon_advanced_parameters.visible=false
                white_logo_mode.visible=false

                            // a modifier
                configXml.visible=false
                file_browser_config.visible=false

                chrono_box.visible=true
                reverbCounter.visible=false
                comboBoxCounter.visible=false

                headerIndicator.visible=true
                break

            case 7: // ABOUT
                // Position Left Menu Indicator
                selected_icon_left.visible=false

                selected_icon_right.visible=true
                selected_icon_right.x=setX(670)
                selected_icon_right.y=setY(756)

                // Position Left Menu Indicator
                icon_advanced_parameters.visible=false
                white_logo_mode.visible=false

                configXml.visible=false
                file_browser_config.visible=false

                chrono_box.visible=false
                reverbCounter.visible=false
                comboBoxCounter.visible=false

                headerIndicator.visible=false
                break

            default:
                break
            }
        }

        onSonoStateChanged: {
            switch(audioget.sonoState) {
            case 0:
                console.log("***STANDBY HEADER***")

                    //Header Information
                csv.color="#c5c5c5"
                wav.color="#c5c5c5"
                break

            case 1:
                console.log("***READY HEADER***")

                    //Header Information
                csv.color="#c5c5c5"
                wav.color="#c5c5c5"
                csv_indicator.source="HeaderImages/header_indicator_off.png"
                wav_indicator.source="HeaderImages/header_indicator_off.png"
                break

            case 2:
                console.log("***LOGGING HEADER***")

                    //Header Information
                wav.color="#c5c5c5"
                csv.color="#ffffff"
                csv_indicator.source="HeaderImages/csv_indicator_on.png"
                wav_indicator.source="HeaderImages/header_indicator_off.png"
                break

            case 3:
                console.log("***RECORDING HEADER***")

                    //Header Information
                wav.color="#ffffff"
                wav_indicator.source="HeaderImages/wav_indicator_on.png"
                break

            case 4:
                console.log("***CALIBRATION***")

                    //Header Information
                csv.color="#c5c5c5"
                wav.color="#c5c5c5"
                csv_indicator.source="HeaderImages/header_indicator_off.png"
                wav_indicator.source="HeaderImages/header_indicator_off.png"
                break

            case 5:
                console.log("***REVERBERATION TIME READY HEADER***")

                    //Header Information
                csv.color="#c5c5c5"
                wav.color="#c5c5c5"
                csv_indicator.source="HeaderImages/header_indicator_off.png"
                wav_indicator.source="HeaderImages/header_indicator_off.png"
                break

            case 6:
                console.log("***REVERBERATION TIME RUNNING HEADER***")

                    //Header Information
                csv.color="#ffffff"
                wav.color="#c5c5c5"
                csv_indicator.source="HeaderImages/csv_indicator_on.png"
                wav_indicator.source="HeaderImages/header_indicator_off.png"
                break

            default:
                break
            }
        }

        property int iSaveReverbCounter: 0
        onReverbCounterChanged: {
            if(audioget.reverbCounter === 0) {
                list_combo_box_counter.clear()
                iSaveReverbCounter = 0

            //} else if(audioget.reverbCounter !== iSaveReverbCounter) {
            } else {
                if(audioget.reverbCounter < iSaveReverbCounter) {
                    // Delete the last combo box counter item
                    list_combo_box_counter.remove(iSaveReverbCounter-1)

                } else if(audioget.reverbCounter >= iSaveReverbCounter) {
                    // Add a new combo box counter item
                    list_combo_box_counter.append({"text": "RT T60 N°"+audioget.reverbCounter})
                }

                // Update the combo box counter display
                combo_box_counter.currentIndex = audioget.reverbCounter-1
                // Keep in memory the reverb counter last index
                iSaveReverbCounter = audioget.reverbCounter
            }
        }
    }

    /* Timer */
    Timer{
        id: animationTimer
        running: false
        interval: 5000
        repeat: false
        onTriggered: {
            animationSablier.playing=false
            animationSablier.visible=false
            animationScreen.visible=false
            animationScreen.focus=false
            audioget.resetChrono()
            audioget.reset()
        }
    }

    Item{
        id:headerView
        width:Screen.width
        height:applicationWindow.height

        /* Background */
        Image {
            id: superior_panel
            source: "HeaderImages/superior_panel.png"
            x: setX(0)
            y: setY(-1)
            width:setX(sourceSize.width)
            height:setY(sourceSize.height)
            opacity: 1
        }

        /* Advanced Parameters */
        Image {
            id: icon_advanced_parameters
            source: "HeaderImages/logoadvancedconfig.png"
            x: setX(80)
            y: setY(270)
            width:setSize(sourceSize.width)
            height:setSize(sourceSize.height)
            opacity: 1
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    /** BLOCKED DEMO VERSION **/
                    /*audioget.screenState=6 // CONFIGURATION

                    selected_icon_left.visible=false*/

                    /** PASS MODE NOT IMPLEMENTED **/
                    audioget.screenState=99 // NOT IMPLEMENTED

                    selected_icon_left.visible=false
                }
            }
        }

        /* White Logo */
        Image {
            id: white_logo_mode
            source: "HeaderImages/whitelogocalibsimple.png"
            x: setX(174)
            y: setY(194)
            width:setSize(sourceSize.width)
            height:setSize(sourceSize.height)
            opacity: 1
            visible: false
        }

        /* Chrono */
        Item {
            id: chrono_box
            visible: false

            Image {
                id: back_chrono
                source: "HeaderImages/chrono_text_box.png"
                x: setX(474)
                y: setY(246)
                width:setSize(sourceSize.width)
                height:setSize(sourceSize.height)
                opacity: 1
            }
            Text {
                font.pointSize: 70
                font.pixelSize: setX(100)
                font.family: "Digital-7"
                font.bold: false
                color: "white"
                smooth: true
                x: setX(661.3)
                y: setY(284)
                text: ":"
            }
            Text {
                font.pointSize: 70
                font.pixelSize: setX(100)
                font.family: "Digital-7"
                font.bold: false
                color: "white"
                smooth: true
                x: setX(845.3)
                y: setY(284)
                text: ":"
            }
            Image {
                id: timer_chrono
                source: "HeaderImages/timer_chrono.png"
                x: setX(474)
                y: setY(296)
                width:setSize(sourceSize.width)
                height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
                opacity: 1
                visible: false
            }

            Text {
                id: chronoHours2
                font.pointSize: 70
                font.pixelSize: setX(100)
                font.family: "Digital-7"
                font.bold: false
                color: "white"
                smooth: true
                x: if (audioget.chronometerHourst==="1"){setX(550.3)}else {setX(523.3)}//520
                y: setY(284)
                width: timer_chrono.width
                height: timer_chrono.height
                text: audioget.chronometerHourst
            }
            Text {
                id: chronoHours1
                font.pointSize: 70
                font.pixelSize: setX(100)
                font.family: "Digital-7"
                font.bold: false
                color: "white"
                smooth: true
                x: if (audioget.chronometerHourso==="1"){setX(596.3)}else {setX(569.3)}//520
                y: setY(284)
                width: timer_chrono.width
                height: timer_chrono.height
                text: audioget.chronometerHourso
            }
            Text {
                id: chronoMinutes2
                font.pointSize: 70
                font.pixelSize: setX(100)
                font.family: "Digital-7"
                font.bold: false
                color: "white"
                smooth: true
                x: if (audioget.chronometerMinutest==="1"){setX(734.3)}else {setX(707.3)}
                y: setY(284)
                width: timer_chrono.width
                height: timer_chrono.height
                text: audioget.chronometerMinutest
            }
            Text {
                id: chronoMinutes1
                font.pointSize: 70
                font.pixelSize: setX(100)
                font.family: "Digital-7"
                font.bold: false
                color: "white"
                smooth: true
                x: if (audioget.chronometerMinuteso==="1"){setX(780.3)}else {setX(753.3)}
                y: setY(284)
                width: timer_chrono.width
                height: timer_chrono.height
                text: audioget.chronometerMinuteso
            }
            Text {
                id: chronoSecond2
                font.pointSize: 70
                font.pixelSize: setX(100)
                font.family: "Digital-7"
                font.bold: false
                color: "white"
                smooth: true
                x: if (audioget.chronometerSecondt==="1"){setX(918.3)}else {setX(891.3)}
                y: setY(284)
                width: timer_chrono.width
                height: timer_chrono.height
                text: audioget.chronometerSecondt
            }
            Text {
                id: chronoSecond1
                font.pointSize: 70
                font.pixelSize: setX(100)
                font.family: "Digital-7"
                font.bold: false
                color: "white" //"#c5c5c5"
                smooth: true
                x: if (audioget.chronometerSecondo==="1"){setX(964.3)}else {setX(937.3)}
                y: setY(284)
                width: timer_chrono.width
                height: timer_chrono.height
                text: audioget.chronometerSecondo
            }
        }

        /* Combo Box Counter */
        Item {
            id: comboBoxCounter
            visible: false

            Image {
                id: back_combo_box_counter
                source: "HeaderImages/counter_text_box_result.png"
                x: setX(474)
                y: setY(246)
                width:setSize(sourceSize.width)
                height:setSize(sourceSize.height)
                opacity: 1
                visible: false
            }

            ComboBox {
                id: combo_box_counter
                font.pixelSize: 15
                font.family: "Tahoma"
                smooth: true
                x: setX(426)
                y: setY(246)
                width: back_combo_box_counter.width
                height: back_combo_box_counter.height
                opacity: 1

                model: ListModel {
                    id: list_combo_box_counter
                    //ListElement { text: "RT T60 N°1" } //Index 0
                }
                currentIndex: 0

                delegate: ItemDelegate {
                    width: combo_box_counter.width
                    contentItem: Text {
                        text: modelData
                        color: "#858585"
                        font.family: "Tahoma-Bold"
                        elide: Text.ElideRight
                        verticalAlignment: Text.AlignVCenter
                    }
                    highlighted: combo_box_counter.highlightedIndex === index
                }

                indicator: Canvas {
                    id: canvas_num_loop
                    x: combo_box_counter.width - width - combo_box_counter.rightPadding
                    y: combo_box_counter.topPadding + (combo_box_counter.availableHeight - height) / 2
                    width: 12
                    height: 8
                    contextType: "2d"
                    Connections {
                        target: combo_box_counter
                        onPressedChanged: canvas_num_loop.requestPaint()
                    }
                    onPaint: {
                        context.reset();
                        context.moveTo(0, 0);
                        context.lineTo(width, 0);
                        context.lineTo(width / 2, height);
                        context.closePath();
                        context.fillStyle = "#ffffff";
                        context.fill();
                    }
                }
                contentItem: Text {
                    leftPadding: 25
                    rightPadding: combo_box_counter.indicator.width + combo_box_counter.spacing
                    text: combo_box_counter.displayText
                    font.pointSize: 70
                    font.pixelSize: setX(100)
                    font.family: "Digital-7"
                    color: "#ffffff"
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                background: Rectangle {
                    implicitWidth: 120
                    implicitHeight: 40
                    color: "#282828"
                    border.color: "#A0A0A0"
                    border.width: 0.4
                    radius: 5
                }
                popup: Popup {
                    y: combo_box_counter.height - 1
                    width: combo_box_counter.width
                    implicitHeight: contentItem.implicitHeight
                    padding: 1
                    contentItem: ListView {
                        clip: true
                        implicitHeight: contentHeight
                        model: combo_box_counter.popup.visible ? combo_box_counter.delegateModel : null
                        currentIndex: combo_box_counter.highlightedIndex

                        ScrollIndicator.vertical: ScrollIndicator { }
                    }
                    background: Rectangle {
                        border.color: "#A0A0A0"
                        color: "#282828"
                        radius: 2
                    }
                }
                onCurrentTextChanged: {
                    audioget.reverbMeasure = combo_box_counter.currentIndex
                    console.log("combo_box_counter.currentIndex : "+combo_box_counter.currentIndex)
                }
            }
        }

        /* Counter */
        Item {
            id: reverbCounter
            visible: false

            Image {
                id: back_counter
                source: "HeaderImages/counter_text_box.png"
                x: setX(474)
                y: setY(246)
                width:setSize(sourceSize.width)
                height:setSize(sourceSize.height)
                opacity: 1
            }

            Image {
                id: scroll_menu_counter
                source: "HeaderImages/counter_scroll_menu.png"
                x: setX(960)
                y: setY(282)
                width:setSize(sourceSize.width)
                height:setSize(sourceSize.height)
                opacity: 1
                visible: false
            }

            Image {
                id: tr_counter_text
                source: "HeaderImages/tr_counter_text.png"
                x: setX(474)
                y: setY(296)
                width:setSize(sourceSize.width)
                height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
                opacity: 1
                visible: false
            }
            Image {
                id: tr_counter
                source: "HeaderImages/tr_counter.png"
                x: setX(774)
                y: setY(296)
                width:setSize(sourceSize.width)
                height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
                opacity: 1
                visible: false
            }

            Text {
                id: rt_count
                text: "RT T60 --"
                font.pointSize: 70
                font.pixelSize: setX(100)
                font.family: "Digital-7"
                font.bold: false
                color: "white" //"#c5c5c5"
                smooth: true
                //x: setX(520)
                anchors.horizontalCenter: back_counter.horizontalCenter
                y: setY(284)
                //anchors.verticalCenter: back_counter.verticalCenter
                //width: tr_counter_text.width
                //height: tr_counter_text.height
            }
        }

        /* Configuration */
        Item {
            id: configXml
            visible: false

            Image {
                id: rect_config_xml
                source: "HeaderImages/rect_config_xml.png"
                x: setX(100)
                y: setY(450)
                width:setSize(sourceSize.width)
                height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
                opacity: 1
            }
            Image {
                id: file_browser_config
                source: "HeaderImages/file_browser_config.png"
                x: setX(1290)
                y: setY(450)
                width:setSize(sourceSize.width)
                height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
                opacity: 1
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        audioget.previousScreenState = audioget.screenState
                        audioget.screenState = 8
                        console.log("%%%%%%%% File Explorer %%%%%%%%%")
                    }
                }
            }
            Text {
                id: config_xml
                text:""
                font.pixelSize: 12
                font.family: "Tahoma-Bold"
                font.bold: true
                color: "#ffffff"
                smooth: true
                x: setX(160)
                y: setY(470) //480
                opacity: 1
            }
        }

        /* Error Acquisition */
        Image {
            id: icon_danger
            source: "HeaderImages/icon_danger.png"
            x: setX(41)
            y: setY(279)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
            visible: false
        }
        Image {
            id: background_error
            source: "HeaderImages/background_error.png"
            x: setX(104)
            y: setY(283)
            width:setX(sourceSize.width)
            height:setheight(setX(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
            visible: false
        }
        Text {
            id: error
            text:""
            font.pixelSize: 8
            font.family: "Tahoma-Bold"
            font.bold: true
            color: "#ffffff"
            smooth: true
            x: setX(248)
            y: setY(294.75)
            opacity: 1
            visible: false
        }
        Text {
            id: acquisition
            text:""
            font.pixelSize: 8
            font.family: "Tahoma-Bold"
            font.bold: true
            color: "#ffffff"
            smooth: true
            x: setX(188)
            y: setY(335.75)
            opacity: 1
            visible: false
        }

        /* Header Indicator */
        Item {
            id: headerIndicator
            visible: false

            Text {
                id: csv
                text: "CSV"
                font.pixelSize: 12
                font.family: "Aller-Bold"
                font.bold: true
                color: "#c5c5c5"
                smooth: true
                x: setX(1135)
                y: setY(314)
                opacity: 1
            }
            Image {
                id: csv_indicator
                source: "HeaderImages/header_indicator_off.png"
                x: setX(1095)
                y: setY(319)
                width:setSize(sourceSize.width)
                height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
                opacity: 1
            }

            Text {
                id: wav
                text: "WAV"
                font.pixelSize: 12
                font.family: "Aller-Bold"
                font.bold: true
                color: "#c5c5c5"
                smooth: true
                x: setX(1333)
                y: setY(314)
                opacity: 1
            }
            Image {
                id: wav_indicator
                source: "HeaderImages/header_indicator_off.png"
                x: setX(1295)
                y: setY(319)
                width:setSize(sourceSize.width)
                height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
                opacity: 1
            }
        }

        /* Left Menu */
        Image {
            id: icon_left_menu
            source: "HeaderImages/icon_left_menu.png"
            x: setX(101)
            y: setY(41)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
            MouseArea {
                anchors.fill: parent
                onClicked:  {
                    if(audioget.sonoState === 4){ //calibration
                        calibrationDialog.visible=true

                    } else {
                        configurationArea.visible=true
                        calibrationArea.visible=true
                        sonometerArea.visible=true
                        reverberationArea.visible=true

                        menu.open()
                    }
                }
            }
        }

        /* Right Menu */
        Image {
            id: icon_right_menu
            source: "HeaderImages/icon_right_menu.png"
            x: setX(1334)
            y: setY(41)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
            MouseArea {
                anchors.fill: parent
                onClicked:  {
                    if(audioget.sonoState === 4){
                        calibrationDialog.visible=true

                    } else {
                        //appFilesArea.visible=true
                        //shareArea.visible=true
                        //photoArea.visible=true
                        //gpsArea.visible=true
                        aboutArea.visible=true

                        rightmenu.open()
                    }
                }
            }
        }

        MessageDialog {
            id: calibrationDialog
            title: "Warning: "
            text: "It is not possible to access the rest of the screens during the calibration process. "
            onAccepted: {
                messageDialog.close()
            }
            visible: false
        }

        /** Logo AllianTech **/
        Image {
            id: logo_trace_head
            source: "HeaderImages/logo_trace.png"
            x: setX(666)
            y: setY(117)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: logo_01_head
            source: "HeaderImages/logo_01.png"
            x: setX(487)
            y: setY(47)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: logo_02_head
            source: "HeaderImages/logo_02.png"
            x: setX(798)
            y: setY(119)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: logo_03_head
            source: "HeaderImages/logo_03.png"
            x: setX(662)
            y: setY(42)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
    }

    /** Left Menu **/
    Drawer {
        id: menu
        visible: false
        interactive:true
        dragMargin: 0

        Rectangle{
            id:configurationArea
            visible: false
            x: setX(0)
            y: setY(756)
            width:selected_icon_left.width
            height:selected_icon_left.height

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    audioget.screenState=0 // MAIN MENU

                    configurationArea.visible=false
                    calibrationArea.visible=false
                    sonometerArea.visible=false
                    reverberationArea.visible=false

                    console.log("%%%%%%%% CONFIGURATION AREA %%%%%%%%%")

                    switch(audioget.screenState) {
                    case 5: // REVERBERATION RESULT
                        console.log("increment reverb counter : "+audioget.reverbCounter)

                        audioget.stopReverbCounter()
                        break
                    }
                }
            }
        }

        Rectangle{
            id:calibrationArea
            visible: false
            x: setX(0)
            y: setY(989.8)
            width:selected_icon_left.width
            height:selected_icon_left.height

            MouseArea{
                anchors.fill: parent
                onClicked:{
                    // Set Sonometer Mode
                    audioget.setModeApp(1);
                    console.log("%%%%%%%% SONOMETER CONFIG FOR CALIBRATION %%%%%%%%%")

                    audioget.screenState=1 // CALIBRATION
                    //audioget.resetChrono()

                    configurationArea.visible=false
                    calibrationArea.visible=false
                    sonometerArea.visible=false
                    reverberationArea.visible=false

                    console.log("%%%%%%%% CALIBRATION AREA %%%%%%%%%")

                    switch(audioget.screenState) {
                    case 5: // REVERBERATION RESULT
                        console.log("increment reverb counter : "+audioget.reverbCounter)

                        audioget.stopReverbCounter()
                        break
                    }
                }
            }
        }

        Rectangle{
            id:sonometerArea
            visible: false
            x: setX(0)
            y: setY(1218)
            width:selected_icon_left.width
            height:selected_icon_left.height

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    audioget.screenState=2 // SONOMETER

                    configurationArea.visible=false
                    calibrationArea.visible=false
                    sonometerArea.visible=false
                    reverberationArea.visible=false

                    // Set Sonometer Mode
                    audioget.setModeApp(1);
                    console.log("%%%%%%%% SONOMETER AREA %%%%%%%%%")

                    switch(audioget.screenState) {
                    case 5: // REVERBERATION RESULT
                        console.log("increment reverb counter dans click sono : "+audioget.reverbCounter)

                        audioget.stopReverbCounter()
                        break
                    }
                }
            }
        }

        Rectangle{
            id:reverberationArea
            visible: false
            x: setX(0)
            y: setY(1445)
            width:selected_icon_left.width
            height:selected_icon_left.height

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    audioget.screenState=3 // REVERBERATION WAITING ROOM

                    configurationArea.visible=false
                    calibrationArea.visible=false
                    sonometerArea.visible=false
                    reverberationArea.visible=false

                    // Set Reverberation Mode
                    audioget.setModeApp(2);
                    console.log("%%%%%%%% REVERBERATION AREA %%%%%%%%%")

                    switch(audioget.screenState) {
                    case 5: // REVERBERATION RESULT
                        console.log("increment reverb counter : "+audioget.reverbCounter)

                        audioget.stopReverbCounter()
                        break
                    }
                }
            }
        }

        Image {
            id: left_menu_background
            source: "LeftMenuImages/left_menu_background.png"
            x: setX(0)
            y: setY(0)
            width:setX(sourceSize.width)
            height:setY(sourceSize.height)
            opacity: 1
        }

        /* LIGNES */
        Image {
            id: separating_lines_01_left
            source: "LeftMenuImages/separating_lines_01_left.png"
            x: setX(4)
            y: setY(756)
            width:setX(sourceSize.width)
            height:setY(sourceSize.height*2)
            opacity: 1
        }
        Image {
            id: separating_lines_02_left
            source: "LeftMenuImages/separating_lines_02_left.png"
            x: setX(4)
            y: setY(990)
            width:setX(sourceSize.width)
            height:setY(sourceSize.height*2)
            opacity: 1
        }
        Image {
            id: separating_lines_03_left
            source: "LeftMenuImages/separating_lines_03_left.png"
            x: setX(4)
            y: setY(1218)
            width:setX(sourceSize.width)
            height:setY(sourceSize.height*2)
            opacity: 1
        }
        Image {
            id: separating_lines_04_left
            source: "LeftMenuImages/separating_lines_04_left.png"
            x: setX(4)
            y: setY(1444)
            width:setX(sourceSize.width)
            height:setY(sourceSize.height*2)
            opacity: 1
        }
        Image {
            id: separating_lines_05_left
            source: "LeftMenuImages/separating_lines_05_left.png"
            x: setX(4)
            y: setY(1668)
            width:setX(sourceSize.width)
            height:setY(sourceSize.height*2)
            opacity: 1
        }

        /* Configuration AREA */
        Text {
            id: configuration
            text: "Configuration"
            font.pixelSize: 14
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(284)
            y: setY(848.5)
            z: 2
            opacity: 1
        }
        Image {
            id: icon_config_01
            source: "LeftMenuImages/logoconfig.png"
            x: setX(63)
            anchors.verticalCenter: configurationArea.verticalCenter
            //y: setY(795)
            z: 2
            width:setSize(sourceSize.width*0.9)
            height:setSize(sourceSize.height*0.9) //setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }

        /* Calibration AREA */
        Text {
            id: calibration
            text: "Calibration"
            font.pixelSize: 14
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(284)
            y: setY(1077.5)
            z: 2
            opacity: 1
        }
        Image {
            id: icon_calib
            source: "LeftMenuImages/whitelogocalibsimple.png"
            x: setX(85)
            anchors.verticalCenter: calibrationArea.verticalCenter
            //y: setY(1017)
            z: 2
            width:setSize(sourceSize.width*0.9)
            height:setSize(sourceSize.height*0.9) //setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }

        /* Sonometer AREA */
        Text {
            id: sonometer
            text: "Sonometer"
            font.pixelSize: 14
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(285)
            y: setY(1307.5)
            z: 2
            opacity: 1
        }
        Image {
            id: icon_sonometer
            source: "LeftMenuImages/whitelogosonosimple.png"
            x: setX(90)
            anchors.verticalCenter: sonometerArea.verticalCenter
            //y: setY(1230)
            z: 2
            width:setSize(sourceSize.width*0.9)
            height:setSize(sourceSize.height*0.9) //setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }

        /* Reverberation AREA */
        Text {
            id: reverberation
            text: "Reverberation"
            font.pixelSize: 14
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(287)
            y: setY(1531.5)
            z: 2
            opacity: 1
        }
        Image {
            id: icon_reverb_01
            source: "LeftMenuImages/whitelogoreverbsimple.png"
            x: setX(40)
            anchors.verticalCenter: reverberationArea.verticalCenter
            //y: setY(1491)
            z: 2
            width:setSize(sourceSize.width*0.9)
            height:setSize(sourceSize.height*0.9) //setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }

        /** Logo AllianTech Left Menu **/
        Image {
            id: logo_trace_left
            source: "LeftMenuImages/logo_trace.png"
            x: setX(278)
            y: setY(484)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: logo_01_left
            source: "LeftMenuImages/logo_01.png"
            x: setX(65)
            y: setY(400)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: logo_02_left
            source: "LeftMenuImages/logo_02.png"
            x: setX(435)
            y: setY(485)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: logo_03_left
            source: "LeftMenuImages/logo_03.png"
            x: setX(274)
            y: setY(393)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }

        /* Logo Footer Left Menu */
        Image {
            id: logo_footer
            source: "LeftMenuImages/logo_footer.png"
            x: setX(181)
            y: setY(2483)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }

        /* Selected Icon Left */
        Image {
            id: selected_icon_left
            source: "LeftMenuImages/selected_icon_left.png"
            x: setX(0)
            y: setY(756)
            z: 1
            width:setSize(sourceSize.width)
            height:separating_lines_02_left.y-separating_lines_01_left.y
            opacity: 1
        }
    }

    /** Right Menu **/
    Drawer {
        id: rightmenu
        visible: false
        interactive:true
        dragMargin: 0

        /*Rectangle{
            id:appFilesArea
            visible: false
            x: setX(670)
            y: setY(785)
            width:Screen.width-appFilesArea.x
            height:shareArea.y-appFilesArea.y

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    selected_icon_left.visible=false

                    selected_icon_right.visible=true
                    selected_icon_right.x=setX(670)
                    selected_icon_right.y=setY(785)
                    selected_icon_right.height=shareArea.y-appFilesArea.y

                    appFilesArea.visible=false
                    //shareArea.visible=true
                    //photoArea.visible=false
                    //gpsArea.visible=false
                    aboutArea.visible=false

                    console.log("%%%%%%%% APP FILES AREA %%%%%%%%%")
                }
            }
        }*/

        /*Rectangle{
            id:shareArea
            visible: false
            x: setX(670)
            y: setY(1018.3)
            width:Screen.width-appFilesArea.x
            height:photoArea.y-shareArea.y

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    selected_icon_left.visible=false

                    selected_icon_right.visible=true
                    selected_icon_right.x=setX(670)
                    selected_icon_right.y=setY(1018.3)
                    selected_icon_right.height=photoArea.y-shareArea.y

                    appFilesArea.visible=false
                    shareArea.visible=true
                    photoArea.visible=false
                    gpsArea.visible=false
                    aboutArea.visible=false

                    console.log("%%%%%%%% SHARE AREA %%%%%%%%%")
                }
            }
        }*/

        /*Rectangle{
            id:photoArea
            visible: false
            x: setX(670)
            y: setY(1247)
            width:Screen.width-appFilesArea.x
            height:gpsArea.y-photoArea.y

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    selected_icon_left.visible=false

                    selected_icon_right.visible=true
                    selected_icon_right.x=setX(670)
                    selected_icon_right.y=setY(1247)
                    selected_icon_right.height=gpsArea.y-photoArea.y

                    appFilesArea.visible=false
                    shareArea.visible=true
                    photoArea.visible=false
                    gpsArea.visible=false
                    aboutArea.visible=false

                    console.log("%%%%%%%% PHOTO AREA %%%%%%%%%")
                }
            }
        }*/

        /*Rectangle{
            id:gpsArea
            visible: false
            x: setX(670)
            y: setY(1474)
            width:Screen.width-appFilesArea.x
            height:aboutArea.y-gpsArea.y

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    selected_icon_left.visible=false

                    selected_icon_right.visible=true
                    selected_icon_right.x=setX(670)
                    selected_icon_right.y=setY(1474)
                    selected_icon_right.height=aboutArea.y-gpsArea.y

                    appFilesArea.visible=false
                    shareArea.visible=true
                    photoArea.visible=false
                    gpsArea.visible=false
                    aboutArea.visible=false

                    console.log("%%%%%%%% GPS AREA %%%%%%%%%")
                }
            }
        }*/

        Rectangle{
            id:aboutArea
            visible: false
            x: setX(670) //670
            y: setY(756) //1697.8
            width:selected_icon_right.width
            height:selected_icon_right.height

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    audioget.screenState=7 // ABOUT

                    //appFilesArea.visible=false
                    //shareArea.visible=false
                    //photoArea.visible=false
                    //gpsArea.visible=false
                    aboutArea.visible=false

                    console.log("%%%%%%%% ABOUT AREA %%%%%%%%%")
                }
            }
        }

        Image {
            id: right_menu_background
            source: "RightMenuImages/right_menu_background.png"
            x: setX(666)
            y: setY(0)
            width:setX(sourceSize.width)
            height:setY(sourceSize.height)
            opacity: 1
        }

        /* LIGNES */
        Image {
            id: separating_lines_01_right
            source: "RightMenuImages/separating_lines_01_right.png"
            x: setX(670)
            y: setY(756)
            width:setSize(sourceSize.width)
            height:setY(sourceSize.height*2)
            opacity: 1
        }
        Image {
            id: separating_lines_02_right
            source: "RightMenuImages/separating_lines_02_right.png"
            x: setX(670)
            y: setY(990)
            width:setSize(sourceSize.width)
            height:setY(sourceSize.height*2)
            opacity: 1
        }

        /* App Files AREA */
        /*Text {
            id: appfiles
            text: "App Files"
            font.pixelSize: 13
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(949)
            y: setY(877.5)
            z: 2
            opacity: 1
        }
        Image {
            id: icon_app_files
            source: "RightMenuImages/icon_app_files.png"
            x: setX(778)
            y: setY(849)
            z: 2
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }*/

        /* Share AREA */
        /*Text {
            id: share
            text: "Share"
            font.pixelSize: 13
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(952)
            y: setY(1106.5)
            z: 2
            opacity: 1
        }
        Image {
            id: icon_share
            source: "RightMenuImages/icon_share.png"
            x: setX(778)
            y: setY(1083)
            z: 2
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }*/

        /* Photo AREA */
        /*Text {
            id: photo
            text: "Photo"
            font.pixelSize: 13
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(954)
            y: setY(1336.5)
            z: 2
            opacity: 1
        }
        Image {
            id: icon_photo
            source: "RightMenuImages/icon_photo.png"
            x: setX(787)
            y: setY(1317)
            z: 2
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }*/

        /* GPS AREA */
        /*Text {
            id: gps
            text: "GPS"
            font.pixelSize: 13
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(951)
            y: setY(1562.5)
            z: 2
            opacity: 1
        }
        Image {
            id: icon_gps
            source: "RightMenuImages/icon_gps.png"
            x: setX(791)
            y: setY(1549)
            z: 2
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }*/

        /* About AREA */
        Text {
            id: about
            text: "About"
            font.pixelSize: 13
            font.family: "Tahoma"
            color: "#ffffff"
            smooth: true
            x: setX(949)
            y: setY(848.5) //1781.5
            z: 2
            opacity: 1
        }
        Image {
            id: logoabout
            source: "RightMenuImages/logoabout.png"
            x: setX(760) //816
            y: setY(790) //770 //840
            z: 2
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        /*Image {
            id: icon_about_02
            source: "RightMenuImages/icon_about_02.png"
            x: setX(836)
            y: setY(903) //833
            z: 2
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }*/

        /** Logo AllianTech Right Menu **/
        Image {
            id: logo_trace_right
            source: "RightMenuImages/logo_trace.png"
            x: setX(949)
            y: setY(513)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: logo_01menu
            source: "RightMenuImages/logo_01.png"
            x: setX(736)
            y: setY(429)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: logo_02menu
            source: "RightMenuImages/logo_02.png"
            x: setX(1105)
            y: setY(514)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }
        Image {
            id: logo_03_right
            source: "RightMenuImages/logo_03.png"
            x: setX(944)
            y: setY(422)
            width:setSize(sourceSize.width)
            height:setheight(setSize(sourceSize.width),sourceSize.width,sourceSize.height)
            opacity: 1
        }

        /* Selected Icon Right */
        Image {
            id: selected_icon_right
            source: "RightMenuImages/selected_icon_right.png"
            visible: false
            x: setX(670)
            y: setY(756)
            z: 1
            width:setSize(sourceSize.width)
            height:separating_lines_02_right.y-separating_lines_01_right.y
            opacity: 1
        }
    }
}
