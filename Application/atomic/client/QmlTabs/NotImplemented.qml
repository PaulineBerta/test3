import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.5
import Qt.labs.settings 1.0
import QtQuick.Window 2.1
Item {
    width:Screen.width
    height:applicationWindow.height

    Text {
        id: comingsoon
        text: "Coming soon..."
        font.pixelSize: 20
        font.family: "Tahoma"
        font.bold: true
        color: "#ffffff"
        smooth: true
        //x: setX(458)
        anchors.horizontalCenter: parent.horizontalCenter
        y: setY(1423.25)
        opacity: 1
    }
    Image {
        id: icon_clock
        source: "NotImplementedmages/icon_clock.png"
        //x: setX(620)
        anchors.horizontalCenter: parent.horizontalCenter
        y: setY(1120) //1092
        width:setSize(sourceSize.width)
        height:setSize(sourceSize.height)
        opacity: 1
    }
}
