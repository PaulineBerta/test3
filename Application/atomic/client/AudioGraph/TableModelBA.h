/**
*******************************************************************************
** @file    TableModelBA.h
** @version
** @author  PB
** @brief   This class provides a table model implementation for the tables
**          displayed in QML.
**
*******************************************************************************
**/

#ifndef TABLEMODELBA_H
#define TABLEMODELBA_H

#include <QAbstractTableModel>
#include <QtQuick>
#include "rep_atomic_replica.h"

#define UNDERLOAD_CHAR  "\u25BC" //(0x2bc6)                  /*!< defines the underload character to add to the table view */

/**
*******************************************************************************
** @class  TableModelBA : public QAbstractTableModel
** @brief defines a data model for the tableView element in QML
**
*******************************************************************************
**/
class TableModelBA : public QAbstractTableModel
{
  Q_OBJECT
  Q_PROPERTY(QList<QVector<QString>> tableData READ tableData WRITE setTableData NOTIFY TableDataChanged)
  Q_PROPERTY(int overload READ GetOverload WRITE setOverload NOTIFY OverloadChanged)
  Q_PROPERTY(int octave READ octave WRITE setOctave NOTIFY octaveChanged)

public:
  enum TeRole {
    eTableRole=Qt::UserRole+1,
    eHeaderRole,
    eAuiFlagTrRole
  };  /*!< defines the different roles of the table model */

  /**
  *******************************************************************************
  ** @fn  TableModelBA::TableModelBA(QObject *parent,QSharedPointer<atomicReplica>,int,QString)
                 : QAbstractTableModel(parent)
  ** @param[in] QObject *parent
  ** @param[in] QSharedPointer<atomicReplica> audioGet holds a reference to the
  **            atomic replica instance
  ** @param[in] int iRow represents the table-row count;
  **            is either 12 or 37
  ** @param[in] int iMode represents the application mode;
  **            1 for Sonometer Mode and 2 for Reverberation Mode
  ** @param[out]
  ** @brief TableModelBA's class constructor, constructs a table model with
  **       the given parent, row count.
  **
  ** @return
  **
  *******************************************************************************
  **/
  explicit TableModelBA(QObject *parent, QSharedPointer<atomicReplica>, QString);

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::updateReverbActiveMeasure(int, bool)
  ** @param[in] QVector<double> adAcousticRes
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  Q_INVOKABLE void updateReverbActiveMeasure(int, bool);

  /**
  *******************************************************************************
  ** @fn  TableModelBA::rowCount(const QModelIndex &parent = QModelIndex()) const
  ** @param[in] QModelIndex &parent = QModelIndex()
  ** @param[out] int
  ** @brief this function overrides the rowCount function of QAbstractTableModel
  **        returns the number of rows
  **
  ** @return returns the number of rows under the given parent
  **
  *******************************************************************************
  **/
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;

  /**
  *******************************************************************************
  ** @fn  TableModelBA::columnCount(const QModelIndex &parent = QModelIndex()) const
  ** @param[in] QModelIndex &parent = QModelIndex()
  ** @param[out] int
  ** @brief this function overrides the columnCount function of QAbstractTableModel
  **        returns the number of columns
  **
  ** @return returns the number of columns under the given parent
  **
  *******************************************************************************
  **/
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;

  /**
  *******************************************************************************
  ** @fn  QVariant TableModelBA::data(const QModelIndex &index, int role) const
  ** @param[in] const QModelIndex &index
  ** @param[in] int role
  ** @param[out] QVariant
  ** @brief this function overrides the data function of QAbstractTableModel that
  **        is invoked from QML
  **
  ** @return Returns the data stored under the given role for the item referred
  **         to by the index.
  **
  *******************************************************************************
  **/
  QVariant data(const QModelIndex &index, int role) const override;

  /**
  *******************************************************************************
  ** @fn  Qt::ItemFlags TableModelBA::flags(const QModelIndex &index) const
  ** @param[in] const QModelIndex &index
  ** @param[out] Qt::ItemFlags
  ** @brief this function overrides the flags function of QAbstractTableModel that
  **        is invoked from QML
  **
  ** @return Returns the item flags for the given index.
  **
  *******************************************************************************
  **/
  Qt::ItemFlags flags(const QModelIndex &index) const override;

  /**
  *******************************************************************************
  ** @fn  QHash<int, QByteArray> TableModelBA::roleNames() const
  ** @param[in]  void
  ** @param[out] QHash<int, QByteArray>
  ** @brief this function overrides the roleNames function of QAbstractTableModel
  **        that is invoked from QML
  **
  ** @return Returns the model's role names
  **
  *******************************************************************************
  **/
  QHash<int, QByteArray> roleNames() const override;

  /**
  *******************************************************************************
  ** @fn  int TableModelBA::GetOverload()
  ** @param[in] void
  ** @param[out] int
  ** @brief this function returns the Overload status of the tables
  **
  ** @return Returns the overload's value; getter
  **         if overload value is:
  **            0 : none of the table' columns are overloaded
  **            1 : the first column, only, is overloaded
  **            2 : the second column, only, is overloaded
  **            3 : both columns are overloaded
  **
  *******************************************************************************
  **/
  int GetOverload();

  /**
   *******************************************************************************
   ** @fn  void TableModelBA::setOverload(int iValue)
   ** @param[in] int, enters the value to be setted to overload
   ** @param[out] void
   ** @brief a setter of the overload's value and in case of changes emets the
   **        OverloadChanged() signal to notify the QML part of this change.
   **
   ** @return void
   **
   *******************************************************************************
   **/
  void setOverload(const int);

  /**
  *******************************************************************************
  ** @fn  int TableModelBA::Octave()
  ** @param[in] void
  ** @param[out] int
  ** @brief this function returns the Octave value
  **
  ** @return Returns the Octave's value
  **
  *******************************************************************************
  **/
  int octave() {return m_iOctave;}

  /**
   *******************************************************************************
   ** @fn  void TableModelBA::setOctave(int iValue)
   ** @param[in] int, enters the value to be setted to octave
   ** @param[out] void
   ** @brief a setter of the Octave's value and in case of changes emets the
   **        OctaveChanged() signal to notify the QML part of this change.
   **
   ** @return void
   **
   *******************************************************************************
   **/
   void setOctave(int);

  /**
  *******************************************************************************
  ** @fn  TableModelBA:: setTableData(QList<QVector<QString> > data)
  ** @param[in] int, enters the value to be setted to overload
  ** @param[out] void
  ** @brief a setter of the overload's value and in case of changes emets the
  **        OverloadChanged() signal to notify the QML part of this change.
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setTableData(QList<QVector<QString>>);

  /**
  *******************************************************************************
  ** @fn  QList<QVector<QString>> TableModelBA:: TableModelBA::tableData()
  ** @param[in] void
  ** @param[out] QList<QVector<QString> >
  ** @brief a getter of the table data
  **
  ** @return returns the total table data
  **
  *******************************************************************************
  **/
  QList<QVector<QString>> tableData();

public slots:

  /**
  *******************************************************************************
  ** @fn  void TableModelBA::reinitPlotList()
  ** @param[in] void
  ** @param[oinitializes the data buffer at start and at reset clicked
  **
  ** @return
  **
  *******************************************************************************
  **/
  void reinitPlotList();

  /**
  *******************************************************************************
  ** @fn  QList<QVector<QString>> TableModelBA:: TableModelBA::tableData()
  ** @param[in] void
  ** @param[out] QList<QVector<QString>>
  ** @brief a getter of the table data
  **
  ** @return returns the total table data
  **
  *******************************************************************************
  **/
  void reinitReverbList();

  /**
  *******************************************************************************
  ** @fn  void TableModelBA::passReverberationRes(TrReverberationRes rReverberationRes)
  ** @param[in] TrReverberationRes rReverberationRes
  ** @param[out] void
  ** @brief slot called every second from AudioGet class to pass
  **        the Reverberation result structure and updates the table's data
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void passReverberationRes(ReverberationRes, ReverbAverageRes);

  /**
  *******************************************************************************
  ** @fn  void TableModelBA::updateOctave(int iOctave)
  ** @param[in] int iOctave this value can be either 12 correspond to octave
  **                                              or 36 correspond to third octave
  ** @param[out] void
  ** @brief slot called to update the sonometer's tables upon every change
  **        in configuration
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void updateOctave(int);

  /**
  *******************************************************************************
  ** @fn  void TableModelBA::saveReverbActiveMeasure()
  ** @param[in] int iStep: the direction to whitch the cursor should move
  **                       if(iStep > 0) moves forward
  **                       else if(iStep < 0) moves backward
  ** @param[out] void
  ** @brief locates the cursor inside the chart depending on number of times
  **        the corresponding button is clicked
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void saveReverbActiveMeasure();


signals:
  void TableDataChanged();                    /*!< signal emitted every one second to update the table's data */
  void OverloadChanged();                     /*!< signal emitted every one second to update the table's data */
  void octaveChanged();

public:
  /* Mode Reverberation */
  QList<ReverberationRes>            m_listReverbRes;
  ReverberationRes                   m_oReverbRes;
  QList<ReverbAverageRes>            m_listAverageRes;
  ReverbAverageRes                   m_oAverageRes;
  int                                m_iListIndex = 0;

  QList<QVector<QString>> m_reverbData;         /*!< table's data */
  QList<QVector<bool>>    m_auiFlagTr;         /*!< table's auiFlagTr */
  int                     m_iReverbColumn=4;    /*!< total reverb column count */
  int                     m_iOverload;          /*!< overload status:
                                                         0 : none of the table' columns are overloaded
                                                         1 : the first column, only, is overloaded
                                                         2 : the second column, only, is overloaded
                                                         3 : both columns are overloaded
                                                 */

  int m_iRow=28; //37-9 First Frequency                               /*!< total row count */
  int m_iFreqCountOffset=0;
  //int m_iTabIndexStart;                   /*!< first index tab lines */
  //int m_iTabIndexEnd;                        /*!< last index tab lines */
  int m_iMode;                               /*!< application mode (1: Sonometer, 2: Reverberation) */
  int m_iOctave=36;
  QString m_cTableMode;
  QSharedPointer<atomicReplica>  m_oAudioGet=nullptr;   /*!< holds a reference to the AudioGet class */
};

#endif // TABLEMODELBA_H
