/**
*******************************************************************************
** @file    TableModelBA.cpp
** @version
** @author  Wevioo SE Team
** @brief  This class provides a table model implementation for the tables
**          displayed in QML.
**
*******************************************************************************
**/

#include <QtDebug>
#include "TableModelBA.h"

static float afFrequency[] = {
    6.3f, 8.0f, 10.0f,
    12.5f, 16.0f, 20.0f,
    25.0f, 31.5f, 40.0f,
    50.0f, 63.0f, 80.0f,
    100.0f, 125.0f, 160.0f,
    200.0f, 250.0f, 315.0f,
    400.0f, 500.0f, 630.0f,
    800.0f, 1000.0f, 1250.0f,
    1600.0f, 2000.0f, 2500.0f,
    3150.0f, 4000.0f, 5000.0f,
    6300.0f, 8000.0f, 10000.0f,
    12500.0f, 16000.0f, 20000.0f};

static float afFrequencyOctave[] = {
    8.0f, 16.0f, 31.5f,
    63.0f,125.0f,250.0f,
    500.0f, 1000.0f, 2000.0f,
    4000.0f, 8000.0f, 16000.0f};

/**
*******************************************************************************
** @fn  QString SetParamBA(double dArg)
** @param[in] double dArg
** @param[out] QString
** @brief In case of underload (negative values) addes the underload character
**        on the right side of the value
**
** @return QString
**
*******************************************************************************
**/
QString SetParamBA(double dArg) {

    if(dArg<=0) {
        return(QString::number(qAbs(dArg),'f', 0));

    } else {
        return QString::number(dArg,'f', 2);
    }
}

/**
*******************************************************************************
** @fn  TableModelBA::TableModelBA(QObject *parent,
**                             QSharedPointer<atomicReplica> audioGet, int iRow, QString tableMode)
**                            : QAbstractTableModel(parent)
** @param[in] QObject *parent
** @param[in] QSharedPointer<atomicReplica> audioGet holds a reference to
**                          the atomic replica
** @param[in] int iRow represents the table-row count;
**            is either 12 or 37
** @param[in] int iMode represents the application mode;
**            1 for Sonometer Mode and 2 for Reverberation Mode
** @param[out]
** @brief TableModelBA's class constructor, constructs a table model with
**       the given parent, row count.
**
** @return
**
*******************************************************************************
**/
TableModelBA::TableModelBA(QObject *parent, QSharedPointer<atomicReplica> audioGet, QString tableMode)
    : QAbstractTableModel(parent), m_oAudioGet(audioGet) {

    m_cTableMode=tableMode;

    /*!< Third Octave Mode Reverb */
    QVector<QString> newList=QVector<QString>(m_iReverbColumn);
    QVector<bool> newFlag=QVector<bool>(m_iReverbColumn);

    m_reverbData.append({"Frequency (Hz)", "T20 (sec)", "T30 (sec)", "T60 (sec)"});
    m_auiFlagTr.append({false, false, false, false});

    for (int iRow=0;iRow<m_iRow-1;iRow++) {
        for (int iCol=0; iCol<m_iRow; iCol++) {
            if (!iCol) {
                newList.insert(iCol,QString::number(static_cast<double>(afFrequency[iRow])));
                newFlag.insert(iCol,false);

            } else {
                newList.insert(iCol,"-");
                newFlag.insert(iCol,false);
            }

            m_reverbData.append(newList);
            m_auiFlagTr.append(newFlag);
        }
    }

    /*!< Connecting the AudioGet's signal passData(TrReverberationRes) to the TableModelBA's slot
   * passReverberationRes(TrReverberationRes)
   */
    connect(m_oAudioGet.data(), SIGNAL(octaveChanged(int)), this, SLOT(updateOctave(int)));

    connect(m_oAudioGet.data(), SIGNAL(passReverberationRes(ReverberationRes, ReverbAverageRes)), this, SLOT(passReverberationRes(ReverberationRes, ReverbAverageRes)));
    connect(m_oAudioGet.data(), SIGNAL(saveReverbActiveMeasure()), this, SLOT(saveReverbActiveMeasure()));
    connect(m_oAudioGet.data(), SIGNAL(reinitPlotList()), this, SLOT(reinitPlotList()));

    m_iOverload=0;
}

/**
*******************************************************************************
** @fn  void TableModelBA::reinitPlotList()
** @param[in] void
** @param[oinitializes the data buffer at start and at reset clicked
**
** @return
**
*******************************************************************************
**/
void TableModelBA::reinitPlotList() {
    for (int i=m_iListIndex-1; i>=0; i--) {
        m_listReverbRes.removeAt(i);
        m_listAverageRes.removeAt(i);
    }

    m_iListIndex = 0;
}

/**
*******************************************************************************
** @fn  TableModelBA::rowCount(const QModelIndex &parent = QModelIndex()) const
** @param[in] QModelIndex &parent = QModelIndex()
** @param[out] int
** @brief this function overrides the rowCount function of QAbstractTableModel
**        returns the number of rows
**
** @return returns the number of rows under the given parent
**
*******************************************************************************
**/
int TableModelBA::rowCount(const QModelIndex &/*parent*/) const {
    return m_iRow;
}

/**
*******************************************************************************
** @fn  TableModelBA::columnCount(const QModelIndex &parent = QModelIndex()) const
** @param[in] QModelIndex &parent = QModelIndex()
** @param[out] int
** @brief this function overrides the columnCount function of QAbstractTableModel
**        returns the number of columns
**
** @return returns the number of columns under the given parent
**
*******************************************************************************
**/
int TableModelBA::columnCount(const QModelIndex &/*parent*/) const {
    return m_iReverbColumn;
}

/**
*******************************************************************************
** @fn  QVariant TableModelBA::data(const QModelIndex &index, int role) const
** @param[in] const QModelIndex &index
** @param[in] int role
** @param[out] QVariant
** @brief this function overrides the data function of QAbstractTableModel that
**        is invoked from QML
**
** @return Returns the data stored under the given role for the item referred
**         to by the index.
**
*******************************************************************************
**/
QVariant TableModelBA::data(const QModelIndex &index, int role) const {
    if(!index.isValid()) {
        return QVariant();
        qDebug()<<" INDEX INVALID! ";
    }

    switch(role) {
    case eTableRole: {
        return m_reverbData[index.row()][index.column()];
    }

    case eHeaderRole: {
        if(index.row()==0){
            return true;
        } else {
            return false;
        }
    }

    case eAuiFlagTrRole: {
        return m_auiFlagTr[index.row()][index.column()];
    }
    }

    return QVariant();
}

/**
*******************************************************************************
** @fn  Qt::ItemFlags TableModelBA::flags(const QModelIndex &index) const
** @param[in] const QModelIndex &index
** @param[out] Qt::ItemFlags
** @brief this function overrides the flags function of QAbstractTableModel that
**        is invoked from QML
**
** @return Returns the item flags for the given index.
**
*******************************************************************************
**/
Qt::ItemFlags TableModelBA::flags(const QModelIndex &index) const {
    /*!< this function enables the item and allows it to be selected */

    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable ;
}

/**
*******************************************************************************
** @fn  QHash<int, QByteArray> TableModelBA::roleNames() const
** @param[in]  void
** @param[out] QHash<int, QByteArray>
** @brief this function overrides the roleNames function of QAbstractTableModel
**        that is invoked from QML
**
** @return Returns the model's role names
**
*******************************************************************************
**/
QHash<int, QByteArray> TableModelBA::roleNames() const {
    /*!<
   *  The role names are set to
   *    - tableData: represents all the data inside the table
   *    - heading: represents the table-header data
   * */

    QHash<int, QByteArray> roles;
    roles[eTableRole] = "tableData";
    roles[eHeaderRole] = "heading";
    roles[eAuiFlagTrRole] = "auiFlagTr";
    return roles;
}

/**
*******************************************************************************
** @fn  int TableModelBA::GetOverload()
** @param[in] void
** @param[out] int
** @brief this function returns the Overload status of the tables
**
** @return Returns the overload's value; getter
**         if overload value is:
**            0 : none of the table' columns are overloaded
**            1 : the first column, only, is overloaded
**            2 : the second column, only, is overloaded
**            3 : both columns are overloaded
**
*******************************************************************************
**/
int TableModelBA::GetOverload() {
    return m_iOverload;
}

/**
*******************************************************************************
** @fn  void TableModelBA::setOverload(int iValue)
** @param[in] int, enters the value to be setted to overload
** @param[out] void
** @brief a setter of the overload's value and in case of changes emets the
**        OverloadChanged() signal to notify the QML part of this change.
**
** @return void
**
*******************************************************************************
**/
void TableModelBA::setOverload(int iValue) {
    if(iValue != m_iOverload) {
        m_iOverload=iValue;
        emit OverloadChanged();

    } else {
        return;
    }
}

/**
 *******************************************************************************
 ** @fn  void TableModelBA::setOctave(int iValue)
 ** @param[in] int, enters the value to be setted to octave
 ** @param[out] void
 ** @brief a setter of the Octave's value and in case of changes emets the
 **        OctaveChanged() signal to notify the QML part of this change.
 **
 ** @return void
 **
 *******************************************************************************
 **/
void TableModelBA::setOctave(int iOctave) {
    m_iOctave=iOctave;
    reinitReverbList();
    emit octaveChanged();
}

/**
*******************************************************************************
** @fn  TableModelBA:: setTableData(QList<QVector<QString>> data)
** @param[in] int, enters the value to be setted to overload
** @param[out] void
** @brief a setter of the overload's value and in case of changes emets the
**        OverloadChanged() signal to notify the QML part of this change.
**
** @return void
**
*******************************************************************************
**/
void TableModelBA::setTableData(QList<QVector<QString>> data) {
    m_reverbData=data;
    emit TableDataChanged();
}

/**
*******************************************************************************
** @fn  QList<QVector<QString>> TableModelBA:: TableModelBA::tableData()
** @param[in] void
** @param[out] QList<QVector<QString>>
** @brief a getter of the table data
**
** @return returns the total table data
**
*******************************************************************************
**/
QList<QVector<QString>> TableModelBA::tableData() {
    return m_reverbData;
}

/**
*******************************************************************************
** @fn  QList<QVector<QString>> TableModelBA:: TableModelBA::tableData()
** @param[in] void
** @param[out] QList<QVector<QString>>
** @brief a getter of the table data
**
** @return returns the total table data
**
*******************************************************************************
**/
void TableModelBA::reinitReverbList() {
    QVector<QString> newList=QVector<QString>(m_iReverbColumn);
    QVector<bool> newFlag=QVector<bool>(m_iReverbColumn);

    m_reverbData.clear();
    m_auiFlagTr.clear();

    m_reverbData.append({"Frequency (Hz)", "T20 (sec)", "T30 (sec)", "T60 (sec)"});
    m_auiFlagTr.append({false, false, false, false});

    for (int iRow=0;iRow<m_iRow-1;iRow++) {
        for (int iCol=0; iCol<m_iRow; iCol++) {
            if (!iCol) {
                newList.insert(iCol,QString::number(static_cast<double>(afFrequency[iRow])));
                newFlag.insert(iCol,false);

            } else {
                newList.insert(iCol,"-");
                newFlag.insert(iCol,false);
            }

            m_reverbData.append(newList);
            m_auiFlagTr.append(newFlag);
        }
    }
}

/**
*******************************************************************************
** @fn  void TableModelBA::passReverberationRes(TrReverberationRes rReverberationRes, ReverbAverageRes rAverageRes)
** @param[in] TrReverberationRes rReverberationRes
** @param[out] void
** @brief slot called every second from AudioGet class to pass
**        the Reverberation result structure and updates the table's data
**
** @return void
**
*******************************************************************************
**/
void TableModelBA::passReverberationRes(ReverberationRes rReverberationRes, ReverbAverageRes rAverageRes) {
    QList<QVector<QString>> DataToPaint;
    QList<QVector<bool>> FlagToPaint;

    m_oReverbRes = rReverberationRes;
    m_oAverageRes = rAverageRes;

    /**
  *****************************************************************************
  **  Tables Update
  *****************************************************************************
  **/

    /*!< updates the Third Octave table */

    if(octave() == 36) {
        qDebug()<<"           octave :             "<<octave();
        m_iFreqCountOffset = 9;

        for (int i=0; i<rowCount(); i++) {

            if (m_cTableMode == "TRCounter") {
                DataToPaint.append({QString::number(static_cast<double>(afFrequency[i+m_iFreqCountOffset])),
                                    SetParamBA(rReverberationRes.afT20().at(i+m_iFreqCountOffset)),
                                    SetParamBA(rReverberationRes.afT30().at(i+m_iFreqCountOffset)),
                                    SetParamBA(rReverberationRes.afT60().at(i+m_iFreqCountOffset))});

                FlagToPaint.append({false, rReverberationRes.afFlagT20().at(i+m_iFreqCountOffset),
                                    rReverberationRes.afFlagT30().at(i+m_iFreqCountOffset),
                                    rReverberationRes.afFlagT60().at(i+m_iFreqCountOffset)});

            } else if (m_cTableMode == "TRAverage") {
                DataToPaint.append({QString::number(static_cast<double>(afFrequency[i+m_iFreqCountOffset])),
                                    SetParamBA(rAverageRes.afAvgT20().at(i+m_iFreqCountOffset)),
                                    SetParamBA(rAverageRes.afAvgT30().at(i+m_iFreqCountOffset)),
                                    SetParamBA(rAverageRes.afAvgT60().at(i+m_iFreqCountOffset))});

                FlagToPaint.append({false, false, false, false});

            } else {
                DataToPaint.append({QString::number(static_cast<double>(afFrequency[i+m_iFreqCountOffset])),
                                    SetParamBA(0),
                                    SetParamBA(0),
                                    SetParamBA(0)});

                FlagToPaint.append({false, false, false, false});
            }
        }

        DataToPaint.prepend({"Frequency (Hz)","T20 (sec)","T30 (sec)","T60 (sec)"});
        FlagToPaint.prepend({false, false, false, false});

    } else {
        qDebug()<<"           octave :             "<<octave();
        m_iFreqCountOffset = 3;

        for (int i=0; i<9; i++) {

            if (m_cTableMode == "TRCounter") {
                DataToPaint.append({QString::number(static_cast<double>(afFrequencyOctave[i+m_iFreqCountOffset])),
                                    SetParamBA(rReverberationRes.afT20().at(i+m_iFreqCountOffset)),
                                    SetParamBA(rReverberationRes.afT30().at(i+m_iFreqCountOffset)),
                                    SetParamBA(rReverberationRes.afT60().at(i+m_iFreqCountOffset))});

                FlagToPaint.append({false, rReverberationRes.afFlagT20().at(i+m_iFreqCountOffset),
                                    rReverberationRes.afFlagT30().at(i+m_iFreqCountOffset),
                                    rReverberationRes.afFlagT60().at(i+m_iFreqCountOffset)});

            } else if (m_cTableMode == "TRAverage") {
                DataToPaint.append({QString::number(static_cast<double>(afFrequencyOctave[i+m_iFreqCountOffset])),
                                    SetParamBA(rAverageRes.afAvgT20().at(i+m_iFreqCountOffset)),
                                    SetParamBA(rAverageRes.afAvgT30().at(i+m_iFreqCountOffset)),
                                    SetParamBA(rAverageRes.afAvgT60().at(i+m_iFreqCountOffset))});

                FlagToPaint.append({false, false, false, false});

            } else {
                DataToPaint.append({QString::number(static_cast<double>(afFrequencyOctave[i+m_iFreqCountOffset])),
                                    SetParamBA(0),
                                    SetParamBA(0),
                                    SetParamBA(0)});

                FlagToPaint.append({false, false, false, false});
            }
        }

        for (int i=9; i<rowCount(); i++) {
            DataToPaint.append({"","",""});
            FlagToPaint.append({false, false, false, false});
        }

        DataToPaint.prepend({"Frequency (Hz)","T20 (sec)","T30 (sec)","T60 (sec)"});
        FlagToPaint.prepend({false, false, false, false});
    }

    setTableData(DataToPaint);
    m_auiFlagTr = FlagToPaint;

    /*!< updates the overload status */
    setOverload(rReverberationRes.iOverload());

    /*!< Updates the QML TableView */
    QModelIndex topIndex=createIndex(0,0, this);
    QModelIndex BottomIndex=createIndex(rowCount()-1,columnCount()-1, this);

    emit dataChanged(topIndex, BottomIndex);  /*!< signal defined in the QAbstractTableModel's class to notify that changes had
                                                 been made in the table between the entered indexes */
}

/**
*******************************************************************************
** @fn  void updateOctave(int iOctave)
** @param[in] int iOctave this value can be either 12 correspond to octave
**                                              or 36 correspond to third octave
** @param[out] void
** @brief slot called to update the sonometer's tables upon every change
**        in configuration
**
** @return void
**
*******************************************************************************
**/
void TableModelBA::updateOctave(int iOctave) {
    setOctave(iOctave);
    //qDebug()<<"    UPDATE OCTAVE TABLE MODEL BA            octave : "<<octave();
}

/**
*******************************************************************************
** @fn  void TableModelBA::updateReverbActiveMeasure(int indexReverbActiveMeasure, bool resetReverbMeasure)
** @param[in] QVector<double> adAcousticRes
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void TableModelBA::updateReverbActiveMeasure(int indexReverbActiveMeasure, bool resetReverbMeasure) {
    if(resetReverbMeasure) {
        m_listReverbRes.removeLast();
        m_listAverageRes.removeLast();
        m_iListIndex--;
    }

    m_oReverbRes = m_listReverbRes.at(indexReverbActiveMeasure);
    m_oAverageRes = m_listAverageRes.last();

    passReverberationRes(m_oReverbRes, m_oAverageRes);
}

/**
*******************************************************************************
** @fn  void TableModelBA::saveReverbActiveMeasure()
** @param[in] int iStep: the direction to whitch the cursor should move
**                       if(iStep > 0) moves forward
**                       else if(iStep < 0) moves backward
** @param[out] void
** @brief locates the cursor inside the chart depending on number of times
**        the corresponding button is clicked
**
** @return void
**
*******************************************************************************
**/
void TableModelBA::saveReverbActiveMeasure() {
    m_listReverbRes.append(m_oReverbRes);
    m_listAverageRes.append(m_oAverageRes);
    m_iListIndex++;
    qDebug()<<"Save active measure, index :"<<m_iListIndex;
}
