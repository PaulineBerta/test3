/**
*******************************************************************************
** @file    TableModel.h
** @version
** @author  PB
** @brief   This class provides a table model implementation for the tables
**          displayed in QML.
**
*******************************************************************************
**/

#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <QAbstractTableModel>
#include <QtQuick>
#include "rep_atomic_replica.h"

#define UNDERLOAD_CHAR  "\u25BC" //(0x2bc6)                  /*!< defines the underload character to add to the table view */

/**
*******************************************************************************
** @class  TableModel : public QAbstractTableModel
** @brief defines a data model for the tableView element in QML
**
*******************************************************************************
**/
class TableModel : public QAbstractTableModel {
    Q_OBJECT
    Q_PROPERTY(QList<QVector<QString>> tableData READ tableData WRITE setTableData NOTIFY TableDataChanged)
    Q_PROPERTY(int overload READ GetOverload WRITE setOverload NOTIFY OverloadChanged)
    Q_PROPERTY(int octave READ octave WRITE setOctave NOTIFY octaveChanged)


public:
    enum TeRole {
        eTableRole=Qt::UserRole+1,
        eHeaderRole
    };  /*!< defines the different roles of the table model */


    /**
  *******************************************************************************
  ** @fn  TableModel::TableModel(QObject *parent,QSharedPointer<atomicReplica>,int)
                 : QAbstractTableModel(parent)
  ** @param[in] QObject *parent
  ** @param[in] QSharedPointer<atomicReplica> audioGet holds a reference to the
  **            atomic replica instance
  ** @param[in] int iRow represents the table-row count;
  **            is either 12 or 37
  ** @param[in] int iMode represents the application mode;
  **            1 for Sonometer Mode and 2 for Reverberation Mode
  ** @param[out]
  ** @brief TableModel's class constructor, constructs a table model with
  **       the given parent, row count.
  **
  ** @return
  **
  *******************************************************************************
  **/
    explicit TableModel(QObject *parent, QSharedPointer<atomicReplica>, int);


    /**
  *******************************************************************************
  ** @fn  TableModel::columnCount(const QModelIndex &parent = QModelIndex()) const
  ** @param[in] QModelIndex &parent = QModelIndex()
  ** @param[out] int
  ** @brief this function overrides the columnCount function of QAbstractTableModel
  **        returns the number of columns
  **
  ** @return returns the number of columns under the given parent
  **
  *******************************************************************************
  **/
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;


    /**
  *******************************************************************************
  ** @fn  QVariant TableModel::data(const QModelIndex &index, int role) const
  ** @param[in] const QModelIndex &index
  ** @param[in] int role
  ** @param[out] QVariant
  ** @brief this function overrides the data function of QAbstractTableModel that
  **        is invoked from QML
  **
  ** @return Returns the data stored under the given role for the item referred
  **         to by the index.
  **
  *******************************************************************************
  **/
    QVariant data(const QModelIndex &index, int role) const override;


    /**
  *******************************************************************************
  ** @fn  Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
  ** @param[in] const QModelIndex &index
  ** @param[out] Qt::ItemFlags
  ** @brief this function overrides the flags function of QAbstractTableModel that
  **        is invoked from QML
  **
  ** @return Returns the item flags for the given index.
  **
  *******************************************************************************
  **/
    Qt::ItemFlags flags(const QModelIndex &index) const override;


    /**
  *******************************************************************************
  ** @fn  int TableModel::GetOverload()
  ** @param[in] void
  ** @param[out] int
  ** @brief this function returns the Overload status of the tables
  **
  ** @return Returns the overload's value; getter
  **         if overload value is:
  **            0 : none of the table' columns are overloaded
  **            1 : the first column, only, is overloaded
  **            2 : the second column, only, is overloaded
  **            3 : both columns are overloaded
  **
  *******************************************************************************
  **/
    int GetOverload();


    /**
  *******************************************************************************
  ** @fn  int TableModel::Octave()
  ** @param[in] void
  ** @param[out] int
  ** @brief this function returns the Octave value
  **
  ** @return Returns the Octave's value
  **
  *******************************************************************************
  **/
    int octave() {return m_iOctave;}


    /**
  *******************************************************************************
  ** @fn  QHash<int, QByteArray> TableModel::roleNames() const
  ** @param[in]  void
  ** @param[out] QHash<int, QByteArray>
  ** @brief this function overrides the roleNames function of QAbstractTableModel
  **        that is invoked from QML
  **
  ** @return Returns the model's role names
  **
  *******************************************************************************
  **/
    QHash<int, QByteArray> roleNames() const override;


    /**
  *******************************************************************************
  ** @fn  TableModel::rowCount(const QModelIndex &parent = QModelIndex()) const
  ** @param[in] QModelIndex &parent = QModelIndex()
  ** @param[out] int
  ** @brief this function overrides the rowCount function of QAbstractTableModel
  **        returns the number of rows
  **
  ** @return returns the number of rows under the given parent
  **
  *******************************************************************************
  **/
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;


    /**
   *******************************************************************************
   ** @fn  void TableModel::setOctave(int iValue)
   ** @param[in] int, enters the value to be setted to octave
   ** @param[out] void
   ** @brief a setter of the Octave's value and in case of changes emets the
   **        OctaveChanged() signal to notify the QML part of this change.
   **
   ** @return void
   **
   *******************************************************************************
   **/
    void setOctave(int);


    /**
   *******************************************************************************
   ** @fn  void TableModel::setOverload(int iValue)
   ** @param[in] int, enters the value to be setted to overload
   ** @param[out] void
   ** @brief a setter of the overload's value and in case of changes emets the
   **        OverloadChanged() signal to notify the QML part of this change.
   **
   ** @return void
   **
   *******************************************************************************
   **/
    void setOverload(const int);


    /**
  *******************************************************************************
  ** @fn  TableModel:: setTableData(QList<QVector<QString> > data)
  ** @param[in] int, enters the value to be setted to overload
  ** @param[out] void
  ** @brief a setter of the overload's value and in case of changes emets the
  **        OverloadChanged() signal to notify the QML part of this change.
  **
  ** @return void
  **
  *******************************************************************************
  **/
    void setTableData(QList<QVector<QString>>);


    /**
  *******************************************************************************
  ** @fn  QList<QVector<QString>> TableModel:: TableModel::tableData()
  ** @param[in] void
  ** @param[out] QList<QVector<QString> >
  ** @brief a getter of the table data
  **
  ** @return returns the total table data
  **
  *******************************************************************************
  **/
    QList<QVector<QString>> tableData();


signals:
    void TableDataChanged();                    /*!< signal emitted every one second to update the table's data */
    void OverloadChanged();                     /*!< signal emitted every one second to update the table's data */
    void octaveChanged();


public slots:
    /**
  *******************************************************************************
  ** @fn  void TableModel::passData(TrAcousticRes rAcousticRes)
  ** @param[in] TrAcousticRes rAcousticRes
  ** @param[out] void
  ** @brief slot called every second from AudioGet class to pass
  **        the Acoustic result structure and updates the table's data
  **
  ** @return void
  **
  *******************************************************************************
  **/
    void passData(AcousticRes);

    /**
  *******************************************************************************
  ** @fn  void updateOctave(int iOctave)
  ** @param[in] int iOctave this value can be either 12 correspond to octave
  **                                              or 36 correspond to third octave
  ** @param[out] void
  ** @brief slot called to update the sonometer's tables upon every change
  **        in configuration
  **
  ** @return void
  **
  *******************************************************************************
  **/
    void updateOctave(int);


public:
    /* Mode Sonometer */
    QList<QVector<QString>> m_data;            /*!< Table's data */

    int m_iColumn=3;                           /*!< Total column count */
    int m_iOverload;                           /*!< Overload status:
                                                         0 : None of the table' columns are overloaded
                                                         1 : The first column, only, is overloaded
                                                         2 : The second column, only, is overloaded
                                                         3 : Both columns are overloaded
                                             */

    int m_iRow;                                /*!< Total row count */
    int m_iMode;                               /*!< Application mode (1: Sonometer, 2: Reverberation) */
    int m_iOctave=36;                          /*!< Spectrum configuration by default, third octave */

    QSharedPointer<atomicReplica>  m_oAudioGet=nullptr;   /*!< Holds a reference to the AudioGet class */
};

#endif // TABLEMODEL_H
