/**
*******************************************************************************
** @file    TableModel.cpp
** @version
** @author  Wevioo SE Team
** @brief   This class provides a table model implementation for the tables
**          displayed in QML.
**
*******************************************************************************
**/

#include <QtDebug>
#include "TableModel.h"

static float afFrequency[] = {
  6.3f, 8.0f, 10.0f,
  12.5f, 16.0f, 20.0f,
  25.0f, 31.5f, 40.0f,
  50.0f, 63.0f, 80.0f,
  100.0f, 125.0f, 160.0f,
  200.0f, 250.0f, 315.0f,
  400.0f, 500.0f, 630.0f,
  800.0f, 1000.0f, 1250.0f,
  1600.0f, 2000.0f, 2500.0f,
  3150.0f, 4000.0f, 5000.0f,
  6300.0f, 8000.0f, 10000.0f,
  12500.0f, 16000.0f, 20000.0f};

static float afFrequencyOctave[] = {
  8.0f, 16.0f, 31.5f,
  63.0f,125.0f,250.0f,
  500.0f, 1000.0f, 2000.0f,
  4000.0f, 8000.0f, 16000.0f};

/**
*******************************************************************************
** @fn  QString SetParam(double dArg)
** @param[in] double dArg
** @param[out] QString
** @brief In case of underload (negative values) addes the underload character
**        on the right side of the value
**
** @return QString
**
*******************************************************************************
**/
QString SetParam(double dArg) {

  if(dArg<=0) {
      return(QString(UNDERLOAD_CHAR)+QString::number(qAbs(dArg),'f', 2));

  } else {
      return QString::number(dArg,'f', 2);
  }
}

/**
*******************************************************************************
** @fn  TableModel::TableModel(QObject *parent,
**                             QSharedPointer<atomicReplica> audioGet, int iRow)
**                            : QAbstractTableModel(parent)
** @param[in] QObject *parent
** @param[in] QSharedPointer<atomicReplica> audioGet holds a reference to
**                          the atomic replica
** @param[in] int iRow represents the table-row count;
**            is either 12 or 37
** @param[in] int iMode represents the application mode;
**            1 for Sonometer Mode and 2 for Reverberation Mode
** @param[out]
** @brief TableModel's class constructor, constructs a table model with
**       the given parent, row count.
**
** @return
**
*******************************************************************************
**/
TableModel::TableModel(QObject *parent, QSharedPointer<atomicReplica> audioGet, int iRow)
  : QAbstractTableModel(parent), m_oAudioGet(audioGet) {

  m_iRow=iRow;

  /*!< Setting of the table data */
  if (iRow == 37) {
      /*!< Third Octave */
      QVector<QString> newList=QVector<QString>(m_iColumn);
      m_data.append({"Frequency (Hz)","Level (dB)","Global (dB)"});

      for (int iRow=0;iRow<m_iRow-1;iRow++) {

          for (int iCol=0; iCol<m_iColumn; iCol++) {
              if (!iCol) {
                  newList.insert(iCol,QString::number(static_cast<double>(afFrequency[iRow])));

              } else {
                  newList.insert(iCol,"-");
              }

          m_data.append(newList);
          }
      }

  } else if (iRow==12) {
      /*!< Octave selected */
      m_data.append({"","Int.Time (dB)","Global (dB)"});
      m_data.append({"LAeq","-","-"});
      m_data.append({"LCeq","-","-"});
      m_data.append({"LZeq","-","-"});
      m_data.append({"LCpeak","-","-"});
      m_data.append({"LZpeak","-","-"});
      m_data.append({"LAS","-","-"});
      m_data.append({"LCS","-","-"});
      m_data.append({"LZS","-","-"});
      m_data.append({"LASmax","-","-"});
      m_data.append({"LCSmax","-","-"});
      m_data.append({"LZSmax","-","-"});

  } else {
      qDebug()<<"invalid Row";
  }

  /*!< Connecting the AudioGet's signal passData(TrAcousticRes) to the TableModel's slot
   * passData(TrAcousticRes)
   */
  connect(m_oAudioGet.data(), SIGNAL(passData(AcousticRes)), this, SLOT(passData(AcousticRes)));
  connect(m_oAudioGet.data(), SIGNAL(octaveChanged(int)), this, SLOT(updateOctave(int)));

  m_iOverload=0;
}

/**
*******************************************************************************
** @fn  TableModel::rowCount(const QModelIndex &parent = QModelIndex()) const
** @param[in] QModelIndex &parent = QModelIndex()
** @param[out] int
** @brief this function overrides the rowCount function of QAbstractTableModel
**        returns the number of rows
**
** @return returns the number of rows under the given parent
**
*******************************************************************************
**/
int TableModel::rowCount(const QModelIndex &/*parent*/) const {
    return m_iRow;
}

/**
*******************************************************************************
** @fn  TableModel::columnCount(const QModelIndex &parent = QModelIndex()) const
** @param[in] QModelIndex &parent = QModelIndex()
** @param[out] int
** @brief this function overrides the columnCount function of QAbstractTableModel
**        returns the number of columns
**
** @return returns the number of columns under the given parent
**
*******************************************************************************
**/
int TableModel::columnCount(const QModelIndex &/*parent*/) const {
    return m_iColumn;
}

/**
*******************************************************************************
** @fn  QVariant TableModel::data(const QModelIndex &index, int role) const
** @param[in] const QModelIndex &index
** @param[in] int role
** @param[out] QVariant
** @brief this function overrides the data function of QAbstractTableModel that
**        is invoked from QML
**
** @return Returns the data stored under the given role for the item referred
**         to by the index.
**
*******************************************************************************
**/
QVariant TableModel::data(const QModelIndex &index, int role) const {
  if(!index.isValid()) {
      return QVariant();
  }

  switch(role){
  case eTableRole:{
      return m_data[index.row()][index.column()];
  }

  case eHeaderRole:{
      if(index.row()==0){
          return true;
      } else {
          return false;
      }
  }
  }

  return QVariant();
}

/**
*******************************************************************************
** @fn  Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
** @param[in] const QModelIndex &index
** @param[out] Qt::ItemFlags
** @brief this function overrides the flags function of QAbstractTableModel that
**        is invoked from QML
**
** @return Returns the item flags for the given index.
**
*******************************************************************************
**/
Qt::ItemFlags TableModel::flags(const QModelIndex &index) const {
  /*!< this function enables the item and allows it to be selected */

  if (!index.isValid())
    return Qt::ItemIsEnabled;

  return Qt::ItemIsEnabled | Qt::ItemIsSelectable ;
}

/**
*******************************************************************************
** @fn  QHash<int, QByteArray> TableModel::roleNames() const
** @param[in]  void
** @param[out] QHash<int, QByteArray>
** @brief this function overrides the roleNames function of QAbstractTableModel
**        that is invoked from QML
**
** @return Returns the model's role names
**
*******************************************************************************
**/
QHash<int, QByteArray> TableModel::roleNames() const {
  /*!<
   *  The role names are set to
   *    - tableData: represents all the data inside the table
   *    - heading: represents the table-header data
   * */

  QHash<int, QByteArray> roles;
  roles[eTableRole] = "tableData";
  roles[eHeaderRole] = "heading";
  return roles;
}

/**
*******************************************************************************
** @fn  int TableModel::GetOverload()
** @param[in] void
** @param[out] int
** @brief this function returns the Overload status of the tables
**
** @return Returns the overload's value; getter
**         if overload value is:
**            0 : none of the table' columns are overloaded
**            1 : the first column, only, is overloaded
**            2 : the second column, only, is overloaded
**            3 : both columns are overloaded
**
*******************************************************************************
**/
int TableModel::GetOverload() {
  return m_iOverload;
}

/**
*******************************************************************************
** @fn  void TableModel::setOverload(int iValue)
** @param[in] int, enters the value to be setted to overload
** @param[out] void
** @brief a setter of the overload's value and in case of changes emets the
**        OverloadChanged() signal to notify the QML part of this change.
**
** @return void
**
*******************************************************************************
**/
void TableModel::setOverload(int iValue) {
  if(iValue != m_iOverload) {
      m_iOverload=iValue;
      emit OverloadChanged();

  } else {
      return;
  }
}

/**
 *******************************************************************************
 ** @fn  void TableModel::setOctave(int iValue)
 ** @param[in] int, enters the value to be setted to octave
 ** @param[out] void
 ** @brief a setter of the Octave's value and in case of changes emets the
 **        OctaveChanged() signal to notify the QML part of this change.
 **
 ** @return void
 **
 *******************************************************************************
 **/
void TableModel::setOctave(int iOctave) {
    m_iOctave=iOctave;
    emit octaveChanged();
}

/**
*******************************************************************************
** @fn  TableModel:: setTableData(QList<QVector<QString>> data)
** @param[in] int, enters the value to be setted to overload
** @param[out] void
** @brief a setter of the overload's value and in case of changes emets the
**        OverloadChanged() signal to notify the QML part of this change.
**
** @return void
**
*******************************************************************************
**/
void TableModel::setTableData(QList<QVector<QString>> data) {
  m_data=data;
  emit TableDataChanged();
}

/**
*******************************************************************************
** @fn  QList<QVector<QString>> TableModel:: TableModel::tableData()
** @param[in] void
** @param[out] QList<QVector<QString> >
** @brief a getter of the table data
**
** @return returns the total table data
**
*******************************************************************************
**/
QList<QVector<QString>> TableModel::tableData() {
  return m_data;
}

/**
*******************************************************************************
** @fn  void TableModel::passData(TrAcousticRes rAcousticRes)
** @param[in] TrAcousticRes rAcousticRes
** @param[out] void
** @brief slot called every second from AudioGet class to pass
**        the Acoustic result structure and updates the table's data
**
** @return void
**
*******************************************************************************
**/
void TableModel::passData(AcousticRes rAcousticRess) {

  QList<QVector<QString>> DataToPaint;

  /**
  *****************************************************************************
  **  Tables Update
  *****************************************************************************
  **/

  if (m_iRow == 37) {
      /*!< updates the Third Octave table */

      if (octave() == 36) {
          for (int i=0; i<rowCount(); i++) {
              DataToPaint.append({QString::number(static_cast<double>(afFrequency[i])),
                                  SetParam(rAcousticRess.adLeq().at(i)),
                                  SetParam(rAcousticRess.adLeqG().at(i))});
          }
          DataToPaint.prepend({"Frequency (Hz)","Level (dB)","Global (dB)"});

      } else {
          for (int i=0; i<octave(); i++) {
              DataToPaint.append({QString::number(static_cast<double>(afFrequencyOctave[i])),
                                  SetParam(rAcousticRess.adLeq().at(i)),
                                  SetParam(rAcousticRess.adLeqG().at(i))});
          }

          for (int i=octave(); i<rowCount(); i++) {
              DataToPaint.append({"","",""});
          }
          DataToPaint.prepend({"Frequency (Hz)","Level (dB)","Global (dB)"});
      }

  } else if (m_iRow == 12) {
      /*!< updates the Octave table */

      DataToPaint.append({"LAeq",
                          SetParam(rAcousticRess.adLeq2().at(0).at(0)),
                          SetParam(rAcousticRess.adLeq2().at(0).at(1))});
      DataToPaint.append({"LCeq",
                          SetParam(rAcousticRess.adLeq2().at(1).at(0)),
                          SetParam(rAcousticRess.adLeq2().at(1).at(1))});
      DataToPaint.append({"LZeq",
                          SetParam(rAcousticRess.adLeq2().at(2).at(0)),
                          SetParam(rAcousticRess.adLeq2().at(2).at(1))});
      DataToPaint.append({"LCpeak",
                          SetParam(rAcousticRess.adLeq2().at(3).at(0)),
                          SetParam(rAcousticRess.adLeq2().at(3).at(1))});
      DataToPaint.append({"LZpeak",
                          SetParam(rAcousticRess.adLeq2().at(4).at(0)),
                          SetParam(rAcousticRess.adLeq2().at(4).at(1))});
      DataToPaint.append({"LAS",
                          SetParam(rAcousticRess.adLeq2().at(5).at(0)),
                          SetParam(rAcousticRess.adLeq2().at(5).at(1))});
      DataToPaint.append({"LCS",
                          SetParam(rAcousticRess.adLeq2().at(6).at(0)),
                          SetParam(rAcousticRess.adLeq2().at(6).at(1))});
      DataToPaint.append({"LZS",
                          SetParam(rAcousticRess.adLeq2().at(7).at(0)),
                          SetParam(rAcousticRess.adLeq2().at(7).at(1))});
      DataToPaint.append({"LASmax",
                          SetParam(rAcousticRess.adLeq2().at(8).at(0)),
                          SetParam(rAcousticRess.adLeq2().at(8).at(1))});
      DataToPaint.append({"LCSmax",
                          SetParam(rAcousticRess.adLeq2().at(9).at(0)),
                          SetParam(rAcousticRess.adLeq2().at(9).at(1))});
      DataToPaint.append({"LZSmax",
                          SetParam(rAcousticRess.adLeq2().at(10).at(0)),
                          SetParam(rAcousticRess.adLeq2().at(10).at(1))});
      DataToPaint.prepend({"","Int.Time (dB)","Global (dB)"});
  }

  setTableData(DataToPaint);

  /*!< updates the overload status */
  setOverload(rAcousticRess.iOverload());

  /*!< Updates the QML TableView */
  QModelIndex topIndex=createIndex(0,0, this);
  QModelIndex BottomIndex=createIndex(rowCount()-1,columnCount()-1, this);

  emit dataChanged(topIndex, BottomIndex);  /*!< signal defined in the QAbstractTableModel's class to notify that changes had
                                                 been made in the table between the entered indexes */

}

/**
*******************************************************************************
** @fn  void updateOctave(int iOctave)
** @param[in] int iOctave this value can be either 12 correspond to octave
**                                              or 36 correspond to third octave
** @param[out] void
** @brief slot called to update the sonometer's tables upon every change
**        in configuration
**
** @return void
**
*******************************************************************************
**/
void TableModel::updateOctave(int iOctave) {
  setOctave(iOctave);
}
