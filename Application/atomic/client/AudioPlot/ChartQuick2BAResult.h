#ifndef CHARTQUICK2BARESULT_H
#define CHARTQUICK2BARESULT_H

/**
*******************************************************************************
** @file    ChartQuick2BAResult.h
** @version
** @author  Wevioo SE Team
** @brief   This class provides the implementation of the temporal charts
**         presented in QML based upon QQuickPaintedItem and QWT.
**
*******************************************************************************
**/
#include <QtQuick>
#include "qwt_plot.h"
#include "qwt_plot_curve.h"
#include "qwt_scale_draw.h"
#include <qwt_plot_marker.h>
#include "rep_atomic_replica.h"

#define BUFFER_RESULT_SIZE_3  375                 /*!< 3x1x125 = 375 [(3 x 1second x 125 points/serie)] with 1s/8ms = 125points */
#define BUFFER_RESULT_SIZE_10  1250               /*!< 10x1x125 = 1250 [(10 x 1second x 125 points/serie)] with 1s/8ms = 125points */
#define FIT_VALUE_BA   8                          /*!< 8ms FIT */

/**
*******************************************************************************
** @class  class TimeScaleDrawBAResult : public QwtScaleDraw
** @brief  provide a customized implementation of the abscisse scale
**         based on the current time formatted in "hh:mm:ss" for the
**         temporal graph.
**
*******************************************************************************
**/
class TimeScaleDrawBAResult : public QwtScaleDraw
{
public:
  /**
  *******************************************************************************
  ** @fn  TimeScaleDraw(const QTime &base)
  ** @param[in] QTime &base : pass the current time
  ** @param[out] void
  ** @brief TimeScaleDraw's constructor
  **
  ** @return void
  **
  *******************************************************************************
  **/
  TimeScaleDrawBAResult(const QTime &base) :
    baseTime(base)
  {
    /*!< set's scale tick lengthes to zero */
    enableComponent(QwtAbstractScaleDraw::Ticks, false);
  }


  /**
  *******************************************************************************
  ** @fn  virtual QwtText label(double v) const
  ** @param[in] double v: holds the current index
  ** @param[out] QwtText
  ** @brief returns the v labels
  **
  ** @return QwtText: returns the label at the index v
  **
  *******************************************************************************
  **/
  virtual QwtText label(double v) const
  {
    QTime upTime = baseTime.addSecs(static_cast<int> (v));
    return upTime.toString("hh:mm:ss");
  }

private:
  QTime baseTime;   /*!< holds the time for the abscisse scale */
};

/*!<
 * The QQuickPaintedItem makes it possible to use the QPainter API
 * with the QML Scene Graph. It sets up a textured rectangle in the
 * Scene Graph and uses a QPainter to paint onto the texture. When
 * the render target is a QImage,QPainter first renders into the image
 * then the content is uploaded to the texture.
 * Call update() to trigger a repaint.
 * */

/**
*******************************************************************************
** @class  class ChartQuick2BAResult : public QQuickPaintedItem
** @brief  sets the temporal graphs using the Qwt library
**          and uplodes it into QML
**
*******************************************************************************
**/
class ChartQuick2BAResult : public QQuickPaintedItem
{
  Q_OBJECT
  Q_PROPERTY(int octave READ octave WRITE setOctave NOTIFY octaveChanged)
  Q_PROPERTY(int chartWidth READ chartWidth WRITE setChartWidth NOTIFY chartWidthChanged)

  /*!< Data to be set in the panel under the plot */
  Q_PROPERTY(QString frequency READ frequency WRITE setFrequency NOTIFY frequencyChanged)
  Q_PROPERTY(QString lAeqValue READ lAeqValue WRITE setLAeqValue NOTIFY lAeqValueChanged)
  Q_PROPERTY(QString trValue READ trValue WRITE setTrValue NOTIFY trValueChanged)

public:
  /**
  *******************************************************************************
  ** @fn  ChartQuick2BAResult::ChartQuick2BAResult(QQuickItem* parent) : QQuickPaintedItem(parent)
  ** @param[in] QQuickItem *parent: holds an instance of the QQuickItem which
  **            defines all the attributes that defines a visual item
  **            (position, height, width, ..)
  ** @param[out]
  ** @brief ChartQuick2's class constructor, derives from QQuickPaintedItem
  **        enabling the widget to be painted in QML.
  **
  ** @return
  **
  *******************************************************************************
  **/
  ChartQuick2BAResult(QQuickItem* parent = nullptr);

  /**
  *******************************************************************************
  ** @fn  ChartQuick2BAResult::~ChartQuick2BAResult()
  ** @param[in]
  ** @param[out]
  ** @brief destructor of ChartQuick2 class: deletes the pointer to the plot
  **
  ** @return
  **
  *******************************************************************************
  **/
  virtual ~ChartQuick2BAResult();

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::paint(QPainter* painter)
  ** @param[in] QPainter* painter
  ** @param[out] void
  ** @brief this function is called by the QML Scene Graph which paints the
  **        Qwt widgets to screen.
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void paint(QPainter* painter);

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::initQwtPlotBAResult(QString chartTime)
  ** @param[in] void
  ** @param[out] void
  ** @brief initializes and sets different parameters of the plot:
  **        - Style of the plot interface
  **        - layout of different Axes and grid
  **        - initializes the curves to be painted in the graph
  **
  ** @return void
  **
  *******************************************************************************
  **/
  Q_INVOKABLE void initQwtPlotBAResult(QString chartTime);

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::reinitQwtPlotBAResult()
  ** @param[in] void
  ** @param[out] void
  ** @brief initializes and sets different parameters of the plot:
  **        - Style of the plot interface
  **        - layout of different Axes and grid
  **        - initializes the curves to be painted in the graph
  **
  ** @return void
  **
  *******************************************************************************
  **/
  Q_INVOKABLE void reinitQwtPlotBAResult();

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::step(int iStep)
  ** @param[in] int iStep: the direction to whitch the cursor should move
  **                       if(iStep > 0) moves forward
  **                       else if(iStep < 0) moves backward
  ** @param[out] void
  ** @brief locates the cursor inside the chart depending on number of times
  **        the corresponding button is clicked
  **
  ** @return void
  **
  *******************************************************************************
  **/
  Q_INVOKABLE void step(int);

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::updateModeTR(int iMode)
  ** @param[in] int iStep: the direction to whitch the cursor should move
  **                       if(iStep > 0) moves forward
  **                       else if(iStep < 0) moves backward
  ** @param[out] void
  ** @brief locates the cursor inside the chart depending on number of times
  **        the corresponding button is clicked
  **
  ** @return void
  **
  *******************************************************************************
  **/
  Q_INVOKABLE void updateModeTR(int);

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::updateReverbActiveMeasure(int, bool)
  ** @param[in] QVector<double> adAcousticRes
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  Q_INVOKABLE void updateReverbActiveMeasure(int, bool);

public slots:
  /**
  *******************************************************************************
  ** @fn  void BarQuick2::setCfgOctave(int iOctave)
  ** @param[in] int iOctave: Spectrum type configuration
  ** @param[out] void
  ** @brief slot called everytime the spectrum type configuration is changed
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setCfgOctave(int iOctave) {
    if(iOctave) {
      setOctave(iOctave);
      //qDebug()<<"    UPDATE OCTAVE CHART QUICK BA            octave : "<<octave();
    }
  }

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::populateLAeqCurve(QVector<QVector<double>>, bool, int)
  ** @param[in] QVector<double>
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void populateLAeqCurve(QVector<QVector<double>>, bool, int);

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::populateRegLines(RegressionLines)
  ** @param[in] QVector<double>
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void populateRegLines(RegressionLines);

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::populateTRValues(ReverberationRes, ReverbAverageRes)
  ** @param[in] QVector<double> adAcousticRes
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void populateTRValues(ReverberationRes, ReverbAverageRes);

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::populateReverbNoise(QVector<double>)
  ** @param[in] QVector<double>
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void populateReverbNoise(QVector<double>);

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::populateReverbSource(QVector<double>)
  ** @param[in] QVector<double>
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void populateReverbSource(QVector<double>);

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::getCounterSinceStartTR(int)
  ** @param[in] QVector<double>
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void getCounterSinceStartTR(int);

  /**
  *******************************************************************************
  ** @fn  int BarQuick2::octave()
  ** @param[in] void
  ** @param[out] int
  ** @brief returns the current octave parameter
  **
  ** @return the current-octave configuration
  **
  *******************************************************************************
  **/
  int octave() {
      return m_iOctaveConfig;
  }

  /**
  *******************************************************************************
  ** @fn  BarQuick2::setOctave(int iOctave)
  ** @param[in] int
  ** @param[out] void
  ** @brief sets the current octave parameter
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setOctave(int iOctave) {
    if(iOctave==12 || iOctave==36){
        m_iOctaveConfig=iOctave;
        emit octaveChanged();
    }
  }

  /**
  *******************************************************************************
  ** @fn  int chartWidth()
  ** @param[in] void
  ** @param[out] int: width
  ** @brief gets the width of the device's screen
  **
  ** @return void
  **
  *******************************************************************************
  **/
  int chartWidth() {
      return m_uiWidth;
  }

  /**
  *******************************************************************************
  ** @fn  void setChartWidth(const int width)
  ** @param[in] int
  ** @param[out] void
  ** @brief sets the width attributes
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setChartWidth(const int width) {
    if(width){
        m_uiWidth=width;
        emit chartWidthChanged();
    }
  }

  /**
  *******************************************************************************
  ** @fn  QString frequency()
  ** @param[in] void
  ** @param[out] QString
  ** @brief returns the position of the marker in the frequential chart
  **
  ** @return the current market position : frequency
  **
  *******************************************************************************
  **/
  QString frequency() {
      return m_cFrequency;
  }

  /**
  *******************************************************************************
  ** @fn  void setFrequency(const QString cFrequency)
  ** @param[in] const QString cFrequency: current position to pass to the QML part
  ** @param[out] void
  ** @brief sets the frequency to be displayed in the panel under the frequential
  **          chart
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setFrequency(const QString cFrequency) {
    m_cFrequency=cFrequency;
    emit frequencyChanged();
  }

  /**
  *******************************************************************************
  ** @fn  void InitializeBAResult()
  ** @param[in] void
  ** @param[oinitializes the data buffer at start and at reset clicked
  **
  ** @return
  **
  *******************************************************************************
  **/
  void InitializeBAResult();

  /**
  *******************************************************************************
  ** @fn  void reinitPlotList()
  ** @param[in] void
  ** @param[oinitializes the data buffer at start and at reset clicked
  **
  ** @return
  **
  *******************************************************************************
  **/
  void reinitPlotList();

  /**
  *******************************************************************************
  ** @fn  QString time()
  ** @param[in] void
  ** @param[out] QString
  ** @brief returns the x coordinate of the marker
  ** @return the x coordinate of the marker
  **
  *******************************************************************************
  **/
  QString time() {
      return m_cTime;
  }

  /**
  *******************************************************************************
  ** @fn  void setTime(const QString cTime)
  ** @param[in] const QString  current x-coordinate of marker
  **             to pass to the QML part
  ** @param[out] void
  ** @brief sets the x-corrdinate of the marker
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setTime(const QString cTime) {
    m_cTime=cTime;
    emit timeChanged();
  }

  /**
  *******************************************************************************
  ** @fn  QString lAeqValue()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the lAeq Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  QString lAeqValue() {
      return m_cLAeqValue;
  }

  /**
  *******************************************************************************
  ** @fn  void setLAeqValue(const QString cLAeq)
  ** @param[in] const QString cLAeq
  ** @param[out] void
  ** @brief sets the lAeq Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setLAeqValue(const QString cLAeq) {
    m_cLAeqValue=cLAeq;
    emit lAeqValueChanged();
  }

  /**
  *******************************************************************************
  ** @fn  QString lAeqValue()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the lAeq Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  QString trValue() {
      return m_cTrValue;
  }

  /**
  *******************************************************************************
  ** @fn  void setLAeqValue(const QString cLAeq)
  ** @param[in] const QString cLAeq
  ** @param[out] void
  ** @brief sets the lAeq Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setTrValue(const QString cTr) {
    m_cTrValue=cTr;
    emit trValueChanged();
  }

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::saveReverbActiveMeasure()
  ** @param[in] int iStep: the direction to whitch the cursor should move
  **                       if(iStep > 0) moves forward
  **                       else if(iStep < 0) moves backward
  ** @param[out] void
  ** @brief locates the cursor inside the chart depending on number of times
  **        the corresponding button is clicked
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void saveReverbActiveMeasure();

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::saveReverbMeasures()
  ** @param[in] int iStep: the direction to whitch the cursor should move
  **                       if(iStep > 0) moves forward
  **                       else if(iStep < 0) moves backward
  ** @param[out] void
  ** @brief locates the cursor inside the chart depending on number of times
  **        the corresponding button is clicked
  **
  ** @return void
  **
  *******************************************************************************
  **/
  //void saveReverbMeasures();

signals:
  void octaveChanged();                   /*!<  */
  void chartWidthChanged();               /*!< emitted when the screen's width has changed */
  void timeChanged();                     /*!< called every 200ms or upon moving the marker to update its x coordinate */
  void frequencyChanged();                /*!<  */
  void lAeqValueChanged();                /*!< called everytime the marker is moved to update the LAeq Value*/
  void trValueChanged();                  /*!<  */

private:
  QwtPlot*                  m_qwtPlot;              /*!< holds a reference to the two-dimentional graph */
  QwtPlotMarker*            m_qwtMarkerNoise;       /*!< a marker that is displayed due to user's touch at the touched point */
  QwtPlotMarker*            m_qwtMarkerSource;      /*!< a marker that is displayed due to user's touch at the touched point */
  QwtPlotCurve*             m_curveLAeq;            /*!< holds a reference to the LAeq-plot item that represents a series of points */
  QwtPlotCurve*             m_curveT20;
  QwtPlotCurve*             m_curveT30;
  QwtPlotCurve*             m_curveT60;
  TimeScaleDrawBAResult*    timeScaleBAResult;      /*!< corresponds to the time scale of the temporal chart */

  double    m_afLAeqBuffer3[BUFFER_RESULT_SIZE_3];            /*!< stores the data of LAeq to be painted in the LAeqCurve, maximum size: BUFFER_RESULT_SIZE*/
  double    m_xDataBuffer3[BUFFER_RESULT_SIZE_3];             /*!< stores the data to be painted, maximum size: BUFFER_RESULT_SIZE*/

  double    m_afT20Buffer3[BUFFER_RESULT_SIZE_3];             /*!< stores the data of LAeq to be painted in the LAeqCurve, maximum size: BUFFER_RESULT_SIZE*/
  double    m_afT30Buffer3[BUFFER_RESULT_SIZE_3];             /*!< stores the data of LAeq to be painted in the LAeqCurve, maximum size: BUFFER_RESULT_SIZE*/
  double    m_afT60Buffer3[BUFFER_RESULT_SIZE_3];             /*!< stores the data of LAeq to be painted in the LAeqCurve, maximum size: BUFFER_RESULT_SIZE*/

  double    m_afLAeqBuffer10[BUFFER_RESULT_SIZE_10];            /*!< stores the data of LAeq to be painted in the LAeqCurve, maximum size: BUFFER_RESULT_SIZE*/
  double    m_xDataBuffer10[BUFFER_RESULT_SIZE_10];             /*!< stores the data to be painted, maximum size: BUFFER_RESULT_SIZE*/

  double    m_afT20Buffer10[BUFFER_RESULT_SIZE_10];             /*!< stores the data of LAeq to be painted in the LAeqCurve, maximum size: BUFFER_RESULT_SIZE*/
  double    m_afT30Buffer10[BUFFER_RESULT_SIZE_10];             /*!< stores the data of LAeq to be painted in the LAeqCurve, maximum size: BUFFER_RESULT_SIZE*/
  double    m_afT60Buffer10[BUFFER_RESULT_SIZE_10];             /*!< stores the data of LAeq to be painted in the LAeqCurve, maximum size: BUFFER_RESULT_SIZE*/


  QList<QVector<QVector<double>>>    m_listLeqOctave;
  QVector<QVector<double>>           m_afLeqOctave2sBlock;

  QList<RegressionLines>             m_listRegLines;
  RegressionLines                    m_oRegLines;

  QList<ReverberationRes>            m_listReverbRes;
  ReverberationRes                   m_oReverbRes;
  ReverbAverageRes                   m_oAverageRes;

  QList<QVector<double>>             m_listReverbNoise;
  QVector<double>                    m_oReverbNoise;

  QList<QVector<double>>             m_listReverbLevel;
  QVector<double>                    m_oReverbLevel;

  QList<int>                         m_listBlockCounter;
  int                                m_iBlockCounterStartPlot = 0;

  int                                m_iListIndex = 0;

  QString   m_cModeTR = "T20";
  int       m_iFrequencyIndex = 22;                 /*!< default frequency : 1 kHz */

  int       m_iOctaveConfig;                        /*!< current configuration of the octave parameter */
  int       m_uiWidth;                              /*!< corresponds to the width of the device's screen */

  QString   m_cTime;                                /*!< corresponds to the x-coordinate of the marker */
  QString   m_cTrValue;                            /*!<  */
  QString   m_cLAeqValue;                           /*!< corresponds to the LAeq coordinate at the marker position */
  QString   m_cFrequency;                           /*!< the frequency value at the marker position */
  bool      m_bAtStart=true;                        /*!< set the marker position at launching the application */

  int       m_iChartLength=3;
  double    m_afAxisScale=1.0;

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::replotAndUpdateBAResult()
  ** @param[in] void
  ** @param[out] void
  ** @brief refresches the plot
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void replotAndUpdateBAResult();

private slots:
  /**
  *******************************************************************************
  ** @fn  void ChartQuick2::updatePlotSizeBAResult()
  ** @param[in] void
  ** @param[out] void
  ** @brief updates the graphic size
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void updatePlotSizeBAResult();

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::updatePlotSizeBAResult()
  ** @param[in] void
  ** @param[out] void
  ** @brief slot that updates the graphic size, called everytime the width or
  **        height has been changed in the QQuickPaintedItem
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void updateReverbNoise();

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::updatePlotSizeBAResult()
  ** @param[in] void
  ** @param[out] void
  ** @brief slot that updates the graphic size, called everytime the width or
  **        height has been changed in the QQuickPaintedItem
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void updateReverbLevel();

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::updatePlotFreqBAResult()
  ** @param[in] QVector<double> adAcousticRes
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void updatePlotFreqBAResult();

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::updatePlotRegBAResult()
  ** @param[in] QVector<double> adAcousticRes
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void updatePlotRegBAResult();

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BAResult::updatePlotValuesBAResult()
  ** @param[in] QVector<double> adAcousticRes
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void updatePlotValuesBAResult();
};

#endif // CHARTQUICK2BARESULT_H
