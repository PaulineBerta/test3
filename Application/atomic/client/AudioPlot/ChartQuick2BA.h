#ifndef CHARTQUICK2BA_H
#define CHARTQUICK2BA_H

/**
*******************************************************************************
** @file    ChartQuick2BA.h
** @version
** @author  Wevioo SE Team
** @brief   This class provides the implementation of the temporal charts
**         presented in QML based upon QQuickPaintedItem and QWT.
**
*******************************************************************************
**/
#include <QtQuick>
#include "qwt_plot.h"
#include "qwt_plot_curve.h"
#include "qwt_scale_draw.h"
#include <qwt_plot_marker.h>
#include "rep_atomic_replica.h"

#define BUFFER_SIZE_4  500                          /*!< 4x1x125 = 500 [(4 x 1second x 125 points/serie)] with 1s/8ms = 125points */
#define BUFFER_SIZE_11  1375                        /*!< 11x1x125 = 1375 [(11 x 1second x 125 points/serie)] with 1s/8ms = 125points */
#define FIT_VALUE_BA   8                               /*!< 8ms FIT */

/**
*******************************************************************************
** @class  class TimeScaleDrawBA : public QwtScaleDraw
** @brief  provide a customized implementation of the abscisse scale
**         based on the current time formatted in "hh:mm:ss" for the
**         temporal graph.
**
*******************************************************************************
**/
class TimeScaleDrawBA : public QwtScaleDraw
{
public:
  /**
  *******************************************************************************
  ** @fn  TimeScaleDraw(const QTime &base)
  ** @param[in] QTime &base : pass the current time
  ** @param[out] void
  ** @brief TimeScaleDraw's constructor
  **
  ** @return void
  **
  *******************************************************************************
  **/
  TimeScaleDrawBA(const QTime &base) :
    baseTime(base)
  {
    /*!< set's scale tick lengthes to zero */
    enableComponent(QwtAbstractScaleDraw::Ticks, false);
  }


  /**
  *******************************************************************************
  ** @fn  virtual QwtText label(double v) const
  ** @param[in] double v: holds the current index
  ** @param[out] QwtText
  ** @brief returns the v labels
  **
  ** @return QwtText: returns the label at the index v
  **
  *******************************************************************************
  **/
  virtual QwtText label(double v) const
  {
    QTime upTime = baseTime.addSecs(static_cast<int> (v));
    return upTime.toString("hh:mm:ss");
  }

private:
  QTime baseTime;   /*!< holds the time for the abscisse scale */
};

/*!<
 * The QQuickPaintedItem makes it possible to use the QPainter API
 * with the QML Scene Graph. It sets up a textured rectangle in the
 * Scene Graph and uses a QPainter to paint onto the texture. When
 * the render target is a QImage,QPainter first renders into the image
 * then the content is uploaded to the texture.
 * Call update() to trigger a repaint.
 * */

/**
*******************************************************************************
** @class  class ChartQuick2BA  : public QQuickPaintedItem
** @brief  sets the temporal graphs using the Qwt library
**          and uplodes it into QML
**
*******************************************************************************
**/
class ChartQuick2BA : public QQuickPaintedItem
{
  Q_OBJECT
  Q_PROPERTY(int chartWidth READ chartWidth WRITE setChartWidth NOTIFY chartWidthChanged)

  /*!< Data to be set in the panel under the plot */
  Q_PROPERTY(QString time READ time WRITE setTime NOTIFY timeChanged)
  Q_PROPERTY(QString lAeqValue READ lAeqValue WRITE setLAeqValue NOTIFY lAeqValueChanged)
  //Q_PROPERTY(QString lCeqValue READ lCeqValue WRITE setLCeqValue NOTIFY lCeqValueChanged)
  //Q_PROPERTY(QString lCpeakValue READ lCpeakValue WRITE setLCpeakValue NOTIFY lCpeakValueChanged)


public:
  /**
  *******************************************************************************
  ** @fn  ChartQuick2::ChartQuick2(QQuickItem* parent) : QQuickPaintedItem(parent)
  ** @param[in] QQuickItem *parent: holds an instance of the QQuickItem which
  **            defines all the attributes that defines a visual item
  **            (position, height, width, ..)
  ** @param[out]
  ** @brief ChartQuick2's class constructor, derives from QQuickPaintedItem
  **        enabling the widget to be painted in QML.
  **
  ** @return
  **
  *******************************************************************************
  **/
  ChartQuick2BA(QQuickItem* parent = nullptr);

  /**
  *******************************************************************************
  ** @fn  ChartQuick2::~ChartQuick2()
  ** @param[in]
  ** @param[out]
  ** @brief destructor of ChartQuick2 class: deletes the pointer to the plot
  **
  ** @return
  **
  *******************************************************************************
  **/
  virtual ~ChartQuick2BA();

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2::paint(QPainter* painter)
  ** @param[in] QPainter* painter
  ** @param[out] void
  ** @brief this function is called by the QML Scene Graph which paints the
  **        Qwt widgets to screen.
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void paint(QPainter* painter);

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BA::initQwtPlotBA(QString chartTime)
  ** @param[in] void
  ** @param[out] void
  ** @brief initializes and sets different parameters of the plot:
  **        - Style of the plot interface
  **        - layout of different Axes and grid
  **        - initializes the curves to be painted in the graph
  **
  ** @return void
  **
  *******************************************************************************
  **/
  Q_INVOKABLE void initQwtPlotBA(QString chartTime);

  /**
  *******************************************************************************
  ** @fn  void step(int iStep)
  ** @param[in] int iStep: could be 1: moves ahead
  **                                -1: moves back
  ** @param[out] void
  ** @brief moves the marker in the graph by one or multiple steps ahead or back,
  **        depending on which direction is clicked on.
  **
  ** @return void
  **
  *******************************************************************************
  **/
  Q_INVOKABLE void step(int iStep){
    qDebug()<<"Step :" << iStep << "new Value :" << m_qwtMarker->value().x()+iStep*1;

    double val=m_qwtMarker->value().x()+iStep*1;
    if( val >=0 &&  val <=4){
      m_qwtMarker->setXValue(m_qwtMarker->value().x()+iStep*1);
      /*setTime((QTime::currentTime().addSecs(-4 + static_cast<int>(val))).toString("hh:mm:ss"));
      setLAeqValue(QString::number(m_afLAeqBuffer[static_cast<int>(val*5)],'f',1));
      setLCeqValue(QString::number(m_afLCeqBuffer[static_cast<int>(val*5)],'f',1));
      setLCpeakValue(QString::number(m_afLCpeakBuffer[static_cast<int>(val*5)],'f',1));
      m_qwtMarker->attach(m_qwtPlot);*/
    }
    replotAndUpdateBA();
  }

  Q_INVOKABLE void resetCounterFirstSecond();

public slots:

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2::populate(QVector<double> )
  ** @param[in] QVector<double>
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void populateBA(QVector<double>);

  /**
  *******************************************************************************
  ** @fn  int chartWidth()
  ** @param[in] void
  ** @param[out] int: width
  ** @brief gets the width of the device's screen
  **
  ** @return void
  **
  *******************************************************************************
  **/
  int chartWidth(){return m_uiWidth;}

  /**
  *******************************************************************************
  ** @fn  void setChartWidth(const int width)
  ** @param[in] int
  ** @param[out] void
  ** @brief sets the width attributes
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setChartWidth(const int width){
    if(width){m_uiWidth=width;
      emit chartWidthChanged();}}

  /**
  *******************************************************************************
  ** @fn  void setMarker()
  ** @param[in] void
  ** @param[out] void
  ** @brief every 200ms the Marker is updated and moved with the chart
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setMarker(){
        m_qwtMarker->setXValue(120);
        m_qwtMarker->attach(m_qwtPlot);
        setTime(QTime::currentTime().toString("hh:mm:ss"));
        if (m_iChartLength == 4) {
            setLAeqValue(QString::number(m_afLAeqBuffer4[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
        } else if (m_iChartLength == 11) {
            setLAeqValue(QString::number(m_afLAeqBuffer11[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
        }
  }

  /**
  *******************************************************************************
  ** @fn  void Initialize()
  ** @param[in] void
  ** @param[oinitializes the data buffer at start and at reset clicked
  **
  ** @return
  **
  *******************************************************************************
  **/
  void InitializeBA();
  /**
  *******************************************************************************
  ** @fn  QString time()
  ** @param[in] void
  ** @param[out] QString
  ** @brief returns the x coordinate of the marker
  ** @return the x coordinate of the marker
  **
  *******************************************************************************
  **/
  QString time(){return m_cTime;}

  /**
  *******************************************************************************
  ** @fn  void setTime(const QString cTime)
  ** @param[in] const QString  current x-coordinate of marker
  **             to pass to the QML part
  ** @param[out] void
  ** @brief sets the x-corrdinate of the marker
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setTime(const QString cTime){
    m_cTime=cTime;
        emit timeChanged();}

  /**
  *******************************************************************************
  ** @fn  QString lAeqValue()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the lAeq Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  QString lAeqValue(){return m_cLAeqValue;}

  /**
  *******************************************************************************
  ** @fn  void setLAeqValue(const QString cLAeq)
  ** @param[in] const QString cLAeq
  ** @param[out] void
  ** @brief sets the lAeq Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setLAeqValue(const QString cLAeq){
    m_cLAeqValue=cLAeq;
        emit lAeqValueChanged();}

  /**
  *******************************************************************************
  ** @fn  QString lCeqValue()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the lCeq Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  //QString lCeqValue(){return m_cLCeqValue;}

  /**
  *******************************************************************************
  ** @fn  void setLCeqValue(const QString cLCeq)
  ** @param[in] const QString cLCeq
  ** @param[out] void
  ** @brief sets the lCeq Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  /*void setLCeqValue(const QString cLAeq){
    m_cLCeqValue=cLAeq;
        emit lCeqValueChanged();}*/

  /**
  *******************************************************************************
  ** @fn  QString lCpeakValue()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the lCpeak Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  //QString lCpeakValue(){return m_cLCpeakValue;}

  /**
  *******************************************************************************
  ** @fn  void setLCpeakValue(const QString cLCpeak)
  ** @param[in] const QString cLCpeak
  ** @param[out] void
  ** @brief sets the LCpeak Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  /*void setLCpeakValue(const QString cLCpeak){
    m_cLCpeakValue=cLCpeak;
        emit lCpeakValueChanged();}*/

signals:
  void chartWidthChanged();               /*!< emitted when the screen's width has changed */
  void timeChanged();                     /*!< called every 200ms or upon moving the marker to update its x coordinate */
  void lAeqValueChanged();                /*!< called everytime the marker is moved to update the LAeq Value*/
  void lCeqValueChanged();                /*!< called everytime the marker is moved to update the LCeq Value*/
  void lCpeakValueChanged();              /*!< called everytime the marker is moved to update the LCpeak Value*/

protected:
  /**
  *******************************************************************************
  ** @fn  void ChartQuick2::populate(QVector<double> )
  ** @param[in] QVector<double>
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  //virtual void populateBA(QVector<double>);

protected:
  /**
  *******************************************************************************
  ** @fn  virtual void touchEvent(QTouchEvent *event)
  ** @param[in] QTouchEvent *event: the event corresponding to screen touching
  **              on the bar chart
  ** @param[out] void
  ** @brief virtual function called everytime a touch has been sensed on the chart
  **
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void touchEvent(QTouchEvent *event);


private:
  QwtPlot*         m_qwtPlot;                         /*!< holds a reference to the two-dimentional graph */
  QwtPlotMarker*   m_qwtMarker;                       /*!< a marker that is displayed due to user's touch at the touched point */
  QwtPlotCurve*    m_curveLAeq;                       /*!< holds a reference to the LAeq-plot item that represents a series of points */
  TimeScaleDrawBA* timeScaleBA;                       /*!< corresponds to the time scale of the temporal chart */

  double m_afLAeqBuffer4[BUFFER_SIZE_4];              /*!< stores the data of LAeq to be painted in the LAeqCurve, maximum size: BUFFER_SIZE*/
  double m_xDataBuffer4[BUFFER_SIZE_4];               /*!< stores the data to be painted, maximum size: BUFFER_SIZE*/

  double m_afLAeqBuffer11[BUFFER_SIZE_11];            /*!< stores the data of LAeq to be painted in the LAeqCurve, maximum size: BUFFER_SIZE*/
  double m_xDataBuffer11[BUFFER_SIZE_11];             /*!< stores the data to be painted, maximum size: BUFFER_SIZE*/

  int   m_uiWidth;                                    /*!< corresponds to the width of the device's screen */

  QString   m_cTime;                                  /*!< corresponds to the x-coordinate of the marker */
  QString   m_cLAeqValue;                             /*!< corresponds to the LAeq coordinate at the marker position */
  bool      m_bAtStart=true;                          /*!< set the marker position at launching the application */

  int       m_iChartLength=4;
  double    m_afAxisScale=1.0;

  int       m_iCounterFirstSecond=0;

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2BA::replotAndUpdateBA()
  ** @param[in] void
  ** @param[out] void
  ** @brief refresches the plot
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void replotAndUpdateBA();

private slots:
  /**
  *******************************************************************************
  ** @fn  void ChartQuick2::updatePlotSize()
  ** @param[in] void
  ** @param[out] void
  ** @brief updates the graphic size
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void updatePlotSizeBA();
};

#endif // CHARTQUICK2BA_H
