/**
*******************************************************************************
** @file    BarQuick2.h
** @version
** @author  Wevioo SE Team
** @brief   This class provides the implementation of the frequency-bar chart
**         in the QML based upon QQuickPaintedItem and QWT.
**
*******************************************************************************
**/
#ifndef BARQUICK2_H
#define BARQUICK2_H

#include <QtQuick>
#include <qwt_plot.h>
#include <qwt_plot_multi_barchart.h>
#include "qwt_scale_draw.h"
#include <qwt_plot_marker.h>
#include "rep_atomic_replica.h"

#define IT_VALUE    1000                            /*!< Integration time: 1 second IT */

/**
*******************************************************************************
** @class  class BarQuick2 : public QQuickPaintedItem
** @brief  sets the frequential chart using the Qwt library
**          and uplodes it into QML
**
*******************************************************************************
**/
class BarQuick2: public QQuickPaintedItem
{
  Q_OBJECT
  Q_PROPERTY(int octave READ octave WRITE setOctave NOTIFY octaveChanged)
  Q_PROPERTY(int barWidth READ barWidth WRITE setBarWidth NOTIFY barWidthChanged)

  /*!< Data to be set in the panel under the plot */
  Q_PROPERTY(QString frequency READ frequency WRITE setFrequency NOTIFY frequencyChanged)
  Q_PROPERTY(QString leqTValue READ leqTValue WRITE setLeqTValue NOTIFY leqTValueChanged)
  Q_PROPERTY(QString leqGValue READ leqGValue WRITE setLeqGValue NOTIFY leqGValueChanged)

public:
  /**
  *******************************************************************************
  ** @fn  BarQuick2::BarQuick2(QQuickItem* parent) : QQuickPaintedItem(parent)
  ** @param[in] QQuickItem *parent: holds an instance of the QQuickItem which
  **            defines all the attributes that defines a visual item
  **            (position, height, width, ..)
  ** @param[out]
  ** @brief BarQuick2's class constructor, derives from QQuickPaintedItem
  **        enabling the widget to be painted in QML.
  **
  ** @return
  **
  *******************************************************************************
  **/
  BarQuick2(QQuickItem* parent=nullptr);

  /**
  *******************************************************************************
  ** @fn  BarQuick2::~BarQuick2()
  ** @param[in]
  ** @param[out]
  ** @brief destructor of BarQuick2 class: deletes the pointer to the bar-plot item
  **         and the overall plot
  **
  ** @return
  **
  *******************************************************************************
  **/
  virtual ~BarQuick2();

  /**
  *******************************************************************************
  ** @fn  void BarQuick2::paint(QPainter* painter)
  ** @param[in] QPainter* painter
  ** @param[out] void
  ** @brief this function is called by the QML Scene Graph which paints the
  **        Qwt widgets to screen.
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void paint(QPainter* painter);

  /**
  *******************************************************************************
  ** @fn  void BarQuick2::initQwtPlot()
  ** @param[in] void
  ** @param[out] void
  ** @brief initializes and sets different parameters of the bar plot:
  **        - Style of the plot interface
  **        - layout of different Axes and grid
  **        - initializes tha bar chart items and attaches to the 2D-plot
  **
  ** @return void
  **
  *******************************************************************************
  **/
  Q_INVOKABLE void initBarPlot();

  /**
  *******************************************************************************
  ** @fn  void BarQuick2::step(int iStep)
  ** @param[in] int iStep: the direction to whitch the cursor should move
  **                       if(iStep > 0) moves forward
  **                       else if(iStep < 0) moves backward
  ** @param[out] void
  ** @brief locates the cursor inside the chart depending on number of times
  **        the corresponding button is clicked
  **
  ** @return void
  **
  *******************************************************************************
  **/
  Q_INVOKABLE void step(int iStep);

  /**
  *******************************************************************************
  ** @fn  int BarQuick2::octave()
  ** @param[in] void
  ** @param[out] int
  ** @brief returns the current octave parameter
  **
  ** @return the current-octave configuration
  **
  *******************************************************************************
  **/
  int octave();

  /**
  *******************************************************************************
  ** @fn  BarQuick2::setOctave(int iOctave)
  ** @param[in] int
  ** @param[out] void
  ** @brief sets the current octave parameter
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setOctave(int iOctave);

  /**
  *******************************************************************************
  ** @fn  int barWidth()
  ** @param[in] void
  ** @param[out] int
  ** @brief returns the widget's width in the QML part
  **          this parameter depends on the used device
  **
  ** @return the current widget's width
  **
  *******************************************************************************
  **/
  int barWidth() {
      return m_uiWidth;
  }

  /**
  *******************************************************************************
  ** @fn  void setBarWidth(const int width)
  ** @param[in] int
  ** @param[out] void
  ** @brief sets the width attributes of the BarQuick2 class from the QML part
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setBarWidth(const int width) {
    if(width) {
        m_uiWidth=width;
        emit barWidthChanged();
    }
  }

  /**
  *******************************************************************************
  ** @fn  QString frequency()
  ** @param[in] void
  ** @param[out] QString
  ** @brief returns the position of the marker in the frequential chart
  **
  ** @return the current market position : frequency
  **
  *******************************************************************************
  **/
  QString frequency() {
      return m_cFrequency;
  }

  /**
  *******************************************************************************
  ** @fn  void setFrequency(const QString cFrequency)
  ** @param[in] const QString cFrequency: current position to pass to the QML part
  ** @param[out] void
  ** @brief sets the frequency to be displayed in the panel under the frequential
  **          chart
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setFrequency(const QString cFrequency){
    m_cFrequency=cFrequency;
    emit frequencyChanged();}

  /**
  *******************************************************************************
  ** @fn  QString leqTValue()
  ** @param[in] void
  ** @param[out] QString
  ** @brief returns the y coordinate at the marker position of LeqT bar chart
  **
  ** @return the LeqT value at the market position
  **
  *******************************************************************************
  **/
  QString leqTValue(){return m_cLeqTValue;}

  /**
  *******************************************************************************
  ** @fn  void setLeqTValue(const QString cLeqTValue)
  ** @param[in] const QString cLeqTValue: current coordinate at marker position
  **            of LeqT to pass to the QML part
  ** @param[out] void
  ** @brief sets the LeqT value at the marker position
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setLeqTValue(const QString cLeqTValue){
    m_cLeqTValue=cLeqTValue;
    emit leqTValueChanged();}

  /**
  *******************************************************************************
  ** @fn  QString leqGValue()
  ** @param[in] void
  ** @param[out] QString
  ** @brief returns the y coordinate at the marker position of LeqG bar chart
  **
  ** @return the LeqT value at the market position
  **
  *******************************************************************************
  **/
  QString leqGValue(){return m_cLeqGValue;}

  /**
  *******************************************************************************
  ** @fn  void setLeqGValue(const QString dLeqGValue)
  ** @param[in] const QString dLeqGValue: current coordinate at marker position
  **            of LeqG to pass to the QML part
  ** @param[out] void
  ** @brief sets the LeqG value at the marker position
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setLeqGValue(const QString cLeqGValue){
    m_cLeqGValue=cLeqGValue;
    emit leqGValueChanged();}


signals:
  /**
  *******************************************************************************
  ** @fn  void octaveChanged();
  ** @param[in] void
  ** @param[out] void
  ** @brief signal emitted whenever the octave parameter has been changed
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void octaveChanged();

  /**
  *******************************************************************************
  ** @fn  void barWidthChanged();
  ** @param[in] void
  ** @param[out] void
  ** @brief signal emitted whenever the widget's width parameter had changed
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void barWidthChanged();

  /**
  *******************************************************************************
  ** @fn  void frequencyChanged();
  ** @param[in] void
  ** @param[out] void
  ** @brief signal emitted whenever the frequency parameter had changed
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void frequencyChanged();

  /**
  *******************************************************************************
  ** @fn  void leqTValueChanged();
  ** @param[in] void
  ** @param[out] void
  ** @brief signal emitted whenever the leqT value parameter had changed
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void leqTValueChanged();

  /**
  *******************************************************************************
  ** @fn  void leqGValueChanged();
  ** @param[in] void
  ** @param[out] void
  ** @brief signal emitted whenever the leqG value parameter had changed
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void leqGValueChanged();
protected:
  /**
  *******************************************************************************
  ** @fn  void BarQuick2::replotAndUpdate()
  ** @param[in] void
  ** @param[out] void
  ** @brief refresches the plot
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void replotAndUpdate();

protected:
  /**
  *******************************************************************************
  ** @fn  virtual void event(QEvent *event)
  ** @param[in] QEvent *event: the event corresponding to screen touching
  **              on the bar chart
  ** @param[out] void
  ** @brief virtual function called everytime a touch has been sensed on the chart
  **
  ** @return void
  **
  *******************************************************************************
  **/
  virtual bool event(QEvent *);

private slots:
  /**
  *******************************************************************************
  ** @fn  void updatePlotSize()
  ** @param[in] void
  ** @param[out] void
  ** @brief slot that updates the graphic size, called everytime the width or
  **        height has been changed in the QQuickPaintedItem
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void updatePlotSize();

public slots:
  /**
  *******************************************************************************
  ** @fn  void BarQuick2::passData(AcousticRes rAcousticRes)
  ** @param[in] TrAcousticRes rAcousticRes
  ** @param[out] void
  ** @brief slot called every 200ms to pass data from the TrAcousticRes
  **        structure to the plot and updates the frequential-bar graph
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void passData(AcousticRes);

  /**
  *******************************************************************************
  ** @fn  void BarQuick2::setCfgOctave(int iOctave)
  ** @param[in] int iOctave: Spectrum type configuration
  ** @param[out] void
  ** @brief slot called everytime the spectrum type configuration is changed
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setCfgOctave(int iOctave)
  {
    if(iOctave){
      m_barItem->setVisible(true);
      setOctave(iOctave);
      initBarPlot();
    }
    else {
        m_barItem->setVisible(false);
        setFrequency("");
        setLeqTValue("NaN");
        setLeqGValue("NaN");
      }
  }

private:
  /**
  *******************************************************************************
  ** @fn  void BarQuick2::populate(TrAcousticRes rAcousticRes)
  ** @param[in] TrAcousticRes rAcousticRes
  ** @param[out] void
  ** @brief sets the frequential-bar graph's data with data from TrAcousticRes
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void populate(AcousticRes);


  QwtPlot*         m_qwtPlot;                         /*!< holds a reference to the two-dimentional graph */
  QwtPlotMultiBarChart *m_barItem;                    /*!< holds a reference to the bar items of the graph */
  QwtPlotMarker*   m_qwtMarker;                       /*!< a marker that is displayed due to user's touch at the touched point */
  int m_iOctaveConfig;                                /*!< current configuration of the octave parameter */
  int m_uiWidth;                                      /*!< the widget's width in the QML interface */
  QString m_cFrequency;                                /*!< the frequency value at the marker position */
  QString m_cLeqTValue;                                /*!< the leqT value at the marker position */
  QString m_cLeqGValue;                                /*!< the leqG value at the marker position */
};

/**
*******************************************************************************
** @class  class FrequencyScaleDraw: public QwtScaleDraw
** @brief  provide a customized implementation of the frequency scale for
**          frequency graph.
**
*******************************************************************************
**/
class FrequencyScaleDraw: public QwtScaleDraw
{
public:
  /**
  *******************************************************************************
  ** @fn  FrequencyScaleDraw(int iOctave)
  ** @param[in] int iOctave: the current octave configuration
  ** @param[out]
  ** @brief FrequencyScaleDraw's constructor
  **
  ** @return void
  **
  *******************************************************************************
  **/
  FrequencyScaleDraw(int iOctave):m_iOctave(iOctave)
  {
    /*!< disable scale's tick  */
    enableComponent(QwtAbstractScaleDraw::Ticks, false);
  }

  /**
  *******************************************************************************
  ** @fn  virtual QwtText label(double v) const
  ** @param[in] double v: holds the current index
  ** @param[out] QwtText
  ** @brief returns the frequency label at the v index
  **
  ** @return QwtText: returns the label at the index v depending on the
  **         current octave configuration
  **
  *******************************************************************************
  **/
  virtual QwtText label(double v) const
  {
    if(m_iOctave==36){
        switch(int(v))
          {
          case 0:
            return QString("");
          case 1:
            return QString("");
          case 2:
            return QString("6.3");
          case 3:
            return QString("8");
          case 4:
            return QString("10");
          case 5:
            return QString("12.5");
          case 6:
            return QString("16");
          case 7:
            return QString("20");
          case 8:
            return QString("25");
          case 9:
            return QString("31.5");
          case 10:
            return QString("40");
          case 11:
            return QString("50");
          case 12:
            return QString("63");
          case 13:
            return QString("80");
          case 14:
            return QString("100");
          case 15:
            return QString("125");
          case 16:
            return QString("160");
          case 17:
            return QString("200");
          case 18:
            return QString("250");
          case 19:
            return QString("315");
          case 20:
            return QString("400");
          case 21:
            return QString("500");
          case 22:
            return QString("630");
          case 23:
            return QString("800");
          case 24:
            return QString("1K");
          case 25:
            return QString("1.25K");
          case 26:
            return QString("1.6K");
          case 27:
            return QString("2K");
          case 28:
            return QString("2.5K");
          case 29:
            return QString("3.15K");
          case 30:
            return QString("4K");
          case 31:
            return QString("5K");
          case 32:
            return QString("6.3K");
          case 33:
            return QString("8K");
          case 34:
            return QString("10K");
          case 35:
            return QString("12.5K");
          case 36:
            return QString("16K");
          case 37:
            return QString("20K");
          case 38:
            return QString("");
          case 39:
            return QString("");

          default: return QString();
          }
      }
    else {
        switch(int(v))
          {
          case 0:
            return QString("");
          case 1:
            return QString("8");
          case 2:
            return QString("16");
          case 3:
            return QString("31.5");
          case 4:
            return QString("63");
          case 5:
            return QString("125");
          case 6:
            return QString("250");
          case 7:
            return QString("500");
          case 8:
            return QString("1K");
          case 9:
            return QString("2K");
          case 10:
            return QString("4K");
          case 11:
            return QString("8K");
          case 12:
            return QString("16K");
          case 13:
            return QString();

          default: return QString();
          }
      }
  }
  int m_iOctave;               /*!< holds the current octave configuration */
};

#endif // BARQUICK2_H
