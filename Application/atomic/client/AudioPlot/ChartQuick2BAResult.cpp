/**
*******************************************************************************
** @file    ChartQuick2.cpp
** @version
** @author  Wevioo SE Team
** @brief   This class provides the implementation of the temporal charts
**         presented in QML based QQuickPaintedItem and QWT.
**
*******************************************************************************
**/
#include "ChartQuick2BAResult.h"

#include <qwt_plot.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_renderer.h>
#include <qwt_scale_div.h>
#include <qwt_scale_engine.h>
#include <QDebug>

/**
*******************************************************************************
** @fn  ChartQuick2::ChartQuick2(QQuickItem* parent) : QQuickPaintedItem(parent)
** @param[in] QQuickItem *parent: holds an instance of the QQuickItem which
**            defines all the attributes that defines a visual item
**            (position, height, width, ..)
** @param[out]
** @brief ChartQuick2's class constructor, derives from QQuickPaintedItem
**        enabling the widget to be painted in QML.
**
** @return
**
*******************************************************************************
**/
ChartQuick2BAResult::ChartQuick2BAResult(QQuickItem* parent) : QQuickPaintedItem(parent)
, m_qwtPlot(nullptr), m_iOctaveConfig(36)//, m_cTrValue(QString::number(m_oReverbRes.afT20().at(m_iFrequencyIndex)))
, m_cFrequency("1 kHz") {

  setFlag(QQuickItem::ItemHasContents, true);
  setAcceptTouchEvents(true);
  setAcceptedMouseButtons(Qt::MouseButton::AllButtons);

  connect(this, &QQuickPaintedItem::widthChanged, this, &ChartQuick2BAResult::updatePlotSizeBAResult);
  connect(this, &QQuickPaintedItem::heightChanged, this, &ChartQuick2BAResult::updatePlotSizeBAResult);

  for (int i = 0; i < BUFFER_RESULT_SIZE_3; i++) {
      m_xDataBuffer3[i] = i/(125.0);
      m_afLAeqBuffer3[i] = 0;
  }

  for (int i = 0; i < BUFFER_RESULT_SIZE_10; i++) {
      m_xDataBuffer10[i] = i/(125.0);
      m_afLAeqBuffer10[i] = 0;
  }
}

/**
*******************************************************************************
** @fn  ChartQuick2BAResult::~ChartQuick2BAResult()
** @param[in]
** @param[out]
** @brief destructor of ChartQuick2 class: delets the pointer to the plot
**
** @return
**
*******************************************************************************
**/
ChartQuick2BAResult::~ChartQuick2BAResult() {
  delete m_qwtPlot;
  m_qwtPlot = nullptr;
}

/**
*******************************************************************************
** @fn  QString SetParamValues(double dArg)
** @param[in] double dArg
** @param[out] QString
** @brief In case of underload (negative values) addes the underload character
**        on the right side of the value
**
** @return QString
**
*******************************************************************************
**/
QString SetParamValues(double dArg) {
    if(dArg<=0) {
        return(QString::number(qAbs(dArg),'f', 2));
    } else {
        return QString::number(dArg,'f', 2);
    }
}


/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::InitializeBAResult(QString chartTime)
** @param[in] void
** @param[oinitializes the data buffer at start and at reset clicked
**
** @return
**
*******************************************************************************
**/
void ChartQuick2BAResult::InitializeBAResult() {
    for (int i = 0; i < BUFFER_RESULT_SIZE_3; i++) {
        m_xDataBuffer3[i] = i/(125.0);
        m_afLAeqBuffer3[i] = 0;
    }

    for (int i = 0; i < BUFFER_RESULT_SIZE_10; i++) {
        m_xDataBuffer10[i] = i/(125.0);
        m_afLAeqBuffer10[i] = 0;
    }

  QVector<double> initVector;
  int bufferSize=0;

  if (m_iChartLength == 3) {
      bufferSize = BUFFER_RESULT_SIZE_3;
  } else if (m_iChartLength == 10) {
      bufferSize = BUFFER_RESULT_SIZE_10;
  }

  for (int indexLAeqValue=0; indexLAeqValue<bufferSize; indexLAeqValue++) {
      initVector.insert(indexLAeqValue,0);
  }
  for (int indexFreqOctave=0; indexFreqOctave<octave()+1; indexFreqOctave++) { //37
      m_afLeqOctave2sBlock.insert(indexFreqOctave,initVector);
  }

  m_bAtStart=true;
  replotAndUpdateBAResult();
}
/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::replotAndUpdateBAResult()
** @param[in] void
** @param[out] void
** @brief refresches the plot
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::replotAndUpdateBAResult() {
  m_qwtPlot->updateAxes();
  m_qwtPlot->replot();
  update();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::initQwtPlotBAResult(QString chartTime)
** @param[in] void
** @param[out] void
** @brief initializes and sets different parameters of the plot:
**        - Style of the plot interface
**        - layout of different Axes and grid
**        - initializes the curves to be painted in the graph
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::initQwtPlotBAResult(QString chartTime) {
    if (chartTime == "3") {
        m_iChartLength = 3;
        m_afAxisScale = 1.0;
    } else if (chartTime == "10") {
        m_iChartLength = 10;
        m_afAxisScale = 2.0;
    }

  m_qwtPlot = new QwtPlot();
  m_qwtPlot->setAttribute(Qt::WA_AcceptTouchEvents, true);
  m_qwtPlot->setAttribute(Qt::WA_StaticContents, true);

  // after replot() we need to call update() - so disable auto replot
  m_qwtPlot->setAutoReplot(false);
  m_qwtPlot->setStyleSheet("background-color:#353535; color:white; border-radius: 0px; font: 9pt; QwtPlot { padding: 0px }");
  m_qwtPlot->plotLayout()->setCanvasMargin(0);
  m_qwtPlot->plotLayout()->setAlignCanvasToScales(true);
  m_qwtPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  QFont font;
  font.setPixelSize(7);
  font.setFamily("Arial-BoldMT");
  font.setBold(true);
  font.setWeight(QFont::ExtraBold);
  font.setHintingPreference(QFont::HintingPreference::PreferNoHinting);
  m_qwtPlot->setFont(font);
  m_qwtPlot->setAxisFont(QwtPlot::xBottom, font);
  m_qwtPlot->setAxisFont(QwtPlot::yLeft, font);

  updatePlotSizeBAResult();

  /*!< Set Grid */
  QwtPlotGrid *grid = new QwtPlotGrid();
  //enable the x and y axis lines
  grid->enableY(true);
  grid->enableX(true);

  QPen pen;
  pen.setStyle(Qt::SolidLine);
  pen.setColor(Qt::white);
  pen.setWidthF(0.5);
  grid->setPen(pen);
  grid->attach(m_qwtPlot);

  /*!< Set X axis */
  timeScaleBAResult=new TimeScaleDrawBAResult(QTime::currentTime().addSecs(-m_iChartLength));
  m_qwtPlot->setAxisScaleDraw(QwtPlot::xBottom, timeScaleBAResult);
  timeScaleBAResult->setLength(m_iChartLength);
  m_qwtPlot->setAxisScale(QwtPlot::xBottom, 0, m_iChartLength, m_afAxisScale);
  m_qwtPlot->axisScaleEngine( QwtPlot::xBottom )->setAttribute( QwtScaleEngine::Inverted, true);
  m_qwtPlot->axisScaleEngine( QwtPlot::xBottom )->setMargins(0,0);

  /*!< Set Y axis */
  QwtScaleDraw* qwtScale= new QwtScaleDraw;
  qwtScale->setTickLength(QwtScaleDiv::MajorTick,0);
  qwtScale->setTickLength(QwtScaleDiv::MinorTick,0);
  m_qwtPlot->setAxisScaleDraw(QwtPlot::yLeft,qwtScale);
  m_qwtPlot->setAxisMaxMinor(QwtPlot::yLeft, 0);
  m_qwtPlot->setAxisScale(QwtPlot::yLeft, +0, +140);

  /*!< LAeq temporal Curve */
  m_curveLAeq = new QwtPlotCurve();
  m_curveLAeq->setRenderHint(QwtPlotItem::RenderAntialiased,true);
  m_curveLAeq->setPen(QPen(QColor("#ffffff"))); //white curve
  m_curveLAeq->setStyle(QwtPlotCurve::Lines);
  m_curveLAeq->setRenderHint(QwtPlotItem::RenderAntialiased);
  m_curveLAeq->attach(m_qwtPlot);

  /*!< T20 Curve */
  m_curveT20 = new QwtPlotCurve();
  m_curveT20->setRenderHint(QwtPlotItem::RenderAntialiased,true);
  m_curveT20->setPen(QColor("#51A8FF"), 2.0, Qt::DashLine);
  m_curveT20->setStyle(QwtPlotCurve::Lines);
  m_curveT20->setRenderHint(QwtPlotItem::RenderAntialiased);
  m_curveT20->attach(m_qwtPlot);

  /*!< T30 Curve */
  m_curveT30 = new QwtPlotCurve();
  m_curveT30->setRenderHint(QwtPlotItem::RenderAntialiased,true);
  m_curveT30->setPen(QColor("#0088FF"), 2.0, Qt::DashLine);
  m_curveT30->setStyle(QwtPlotCurve::Lines);
  m_curveT30->setRenderHint(QwtPlotItem::RenderAntialiased);
  m_curveT30->attach(m_qwtPlot);

  /*!< T60 Curve */
  m_curveT60 = new QwtPlotCurve();
  m_curveT60->setRenderHint(QwtPlotItem::RenderAntialiased,true);
  m_curveT60->setPen(QColor("#0066FF"), 2.0, Qt::DashLine);
  m_curveT60->setStyle(QwtPlotCurve::Lines);
  m_curveT60->setRenderHint(QwtPlotItem::RenderAntialiased);
  m_curveT60->attach(m_qwtPlot);

  /*!< set the line noise level marker */
  m_qwtMarkerNoise= new QwtPlotMarker;
  m_qwtMarkerNoise->setLineStyle(QwtPlotMarker::HLine);
  m_qwtMarkerNoise->setLabelAlignment(Qt::AlignRight | Qt::AlignTop);
  m_qwtMarkerNoise->setLinePen(QColor(255, 175, 0),2,Qt::SolidLine);

  if(octave() == 12) {
      m_qwtMarkerNoise->setYValue(7);
  } else if (octave() == 36) {
      m_qwtMarkerNoise->setYValue(22);
  }

  m_qwtMarkerNoise->attach(m_qwtPlot);

  /*!< set the line source level marker */
  m_qwtMarkerSource= new QwtPlotMarker;
  m_qwtMarkerSource->setLineStyle(QwtPlotMarker::HLine);
  m_qwtMarkerSource->setLabelAlignment(Qt::AlignRight | Qt::AlignTop);
  m_qwtMarkerSource->setLinePen(QColor(Qt::red),2,Qt::SolidLine);

  if(octave() == 12) {
      m_qwtMarkerNoise->setYValue(7);
  } else if (octave() == 36) {
      m_qwtMarkerNoise->setYValue(22);
  }

  m_qwtMarkerSource->attach(m_qwtPlot);

  replotAndUpdateBAResult();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::reinitQwtPlotBAResult()
** @param[in] void
** @param[out] void
** @brief initializes and sets different parameters of the plot:
**        - Style of the plot interface
**        - layout of different Axes and grid
**        - initializes the curves to be painted in the graph
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::reinitQwtPlotBAResult() {
    // Reinitialization LAeq curve style for ReverbMeasure Screen
    //m_curveLAeq->setPen(QColor("#ffffff"), 1.0, Qt::SolidLine);

    // Reinitialization TR Value Area
    m_cModeTR = "T20";
    setTrValue(m_cModeTR);

    if(octave() == 12) {
        m_iFrequencyIndex = 7;
    } else if (octave() == 36) {
        m_iFrequencyIndex = 22;
    }
    setFrequency("1 kHz");
}

/**
*******************************************************************************
** @fn  void reinitPlotList()
** @param[in] void
** @param[oinitializes the data buffer at start and at reset clicked
**
** @return
**
*******************************************************************************
**/
void ChartQuick2BAResult::reinitPlotList() {
    for (int i=m_iListIndex-1; i>=0; i--) {
        m_listReverbNoise.removeAt(i);
        m_listReverbLevel.removeAt(i);
        m_listLeqOctave.removeAt(i);
        m_listRegLines.removeAt(i);
        m_listReverbRes.removeAt(i);
        m_listBlockCounter.removeAt(i);
    }

    m_iListIndex = 0;
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::step(int iStep)
** @param[in] int iStep: the direction to whitch the cursor should move
**                       if(iStep > 0) moves forward
**                       else if(iStep < 0) moves backward
** @param[out] void
** @brief locates the cursor inside the chart depending on number of times
**        the corresponding button is clicked
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::step(int iStep) {
  qDebug()<<"octave : "<<octave();
    if(octave() == 36){
      if(iStep == 1) {
          if(m_iFrequencyIndex >= 9 && m_iFrequencyIndex < octave()-1) {
              m_iFrequencyIndex++;

          } else {
              m_iFrequencyIndex = 35;
          }

      } else if (iStep == -1) {
          if(m_iFrequencyIndex > 9 && m_iFrequencyIndex <= octave()-1) {
              m_iFrequencyIndex--;

          } else {
              m_iFrequencyIndex = 9;
          }
      }

      switch(m_iFrequencyIndex) {
        case 0:
          setFrequency("6.3 Hz");break;
        case 1:
          setFrequency("8 Hz");break;
        case 2:
          setFrequency("10 Hz");break;
        case 3:
          setFrequency("12.5 Hz");break;
        case 4:
          setFrequency("16 Hz");break;
        case 5:
          setFrequency("20 Hz");break;
        case 6:
          setFrequency("25 Hz");break;
        case 7:
          setFrequency("31.5 Hz");break;
        case 8:
          setFrequency("40 Hz");break;
        case 9:
          setFrequency("50 Hz");break;
        case 10:
          setFrequency("63 Hz");break;
        case 11:
          setFrequency("80 Hz");break;
        case 12:
          setFrequency("100 Hz");break;
        case 13:
          setFrequency("125 Hz");break;
        case 14:
          setFrequency("160 Hz");break;
        case 15:
          setFrequency("200 Hz");break;
        case 16:
          setFrequency("250 Hz");break;
        case 17:
          setFrequency("315 Hz");break;
        case 18:
          setFrequency("400 Hz");break;
        case 19:
          setFrequency("500 Hz");break;
        case 20:
          setFrequency("630 Hz");break;
        case 21:
          setFrequency("800 Hz");break;
        case 22:
          setFrequency("1 KHz");break;
        case 23:
          setFrequency("1.25 KHz");break;
        case 24:
          setFrequency("1.6 KHz");break;
        case 25:
          setFrequency("2 KHz");break;
        case 26:
          setFrequency("2.5 KHz");break;
        case 27:
          setFrequency("3.15 KHz");break;
        case 28:
          setFrequency("4 KHz");break;
        case 29:
          setFrequency("5 KHz");break;
        case 30:
          setFrequency("6.3 KHz");break;
        case 31:
          setFrequency("8 KHz");break;
        case 32:
          setFrequency("10 KHz");break;
        case 33:
          setFrequency("12.5 KHz");break;
        case 34:
          setFrequency("16 KHz");break;
        case 35:
          setFrequency("20 KHz"); break;
        case 36:
          setFrequency("Average");break;

        default: setFrequency("");break;
        }

  } else {
        if(iStep == 1) {
            if(m_iFrequencyIndex >= 3 && m_iFrequencyIndex < octave()-1) {
                m_iFrequencyIndex++;

            } else {
                m_iFrequencyIndex = 11;
            }

        } else if (iStep == -1) {
            if(m_iFrequencyIndex > 3 && m_iFrequencyIndex <= octave()-1) {
                m_iFrequencyIndex--;

            } else {
                m_iFrequencyIndex = 3;
            }
        }

      switch(m_iFrequencyIndex) {
        case 0:
          setFrequency("8 Hz");break;
        case 1:
          setFrequency("16 Hz");break;
        case 2:
          setFrequency("31.5 Hz");break;
        case 3:
          setFrequency("63 Hz");break;
        case 4:
          setFrequency("125 Hz");break;
        case 5:
          setFrequency("250 Hz");break;
        case 6:
          setFrequency("500 Hz");break;
        case 7:
          setFrequency("1 KHz");break;
        case 8:
          setFrequency("2 KHz");break;
        case 9:
          setFrequency("4 KHz");break;
        case 10:
          setFrequency("8 KHz");break;
        case 11:
          setFrequency("16 KHz");break;
        case 12:
          setFrequency("Average");break;

        default: setFrequency("");break;
        }
  }

  updateReverbNoise();
  updateReverbLevel();
  updatePlotFreqBAResult();
  updatePlotValuesBAResult();
  updatePlotRegBAResult();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::updateModeTR(int iMode)
** @param[in] int iStep: the direction to whitch the cursor should move
**                       if(iStep > 0) moves forward
**                       else if(iStep < 0) moves backward
** @param[out] void
** @brief locates the cursor inside the chart depending on number of times
**        the corresponding button is clicked
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::updateModeTR(int iMode) {
    QString modeValue = "";

    if (iMode == 20) {
        modeValue = "T20";
    } else if (iMode == 30) {
        modeValue = "T30";
    } else if (iMode == 60) {
        modeValue = "T60";
    }
    m_cModeTR = modeValue;

    updatePlotValuesBAResult();
    updatePlotRegBAResult();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::paint(QPainter* painter)
** @param[in] QPainter* painter
** @param[out] void
** @brief this function is called by the QML Scene Graph which paints the
**        Qwt widgets to screen.
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::paint(QPainter* painter) {
  if(m_qwtPlot) {
      painter->save();
      QwtPlotRenderer renderer;
      renderer.setLayoutFlag(QwtPlotRenderer::FrameWithScales);
      QFont font;
      font.setPixelSize(7);
      font.setFamily("Arial-BoldMT");
      font.setBold(true);
      font.setWeight(QFont::ExtraBold);
      font.setHintingPreference(QFont::HintingPreference::PreferNoHinting);
      painter->setFont(font);
      renderer.render(m_qwtPlot, painter, m_qwtPlot->rect());

      painter->restore();
  }
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::populateReverbNoise(QVector<double> adReverbNoise)
** @param[in] QVector<double>
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::populateReverbNoise(QVector<double> adReverbNoise) {
    m_oReverbNoise = adReverbNoise;
    updateReverbNoise();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::populateReverbSource(QVector<double> adReverbSource)
** @param[in] QVector<double>
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::populateReverbSource(QVector<double> adReverbSource) {
    m_oReverbLevel = adReverbSource;
    updateReverbLevel();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::populateLAeqCurve(QVector<QVector<double>> adAcousticRes, bool lastBuffer, int index)
** @param[in] QVector<double> adAcousticRes
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::populateLAeqCurve(QVector<QVector<double>> adAcousticRes, bool lastBuffer, int index) {
  QVector<double> afLeqOctave200msBlock;
  int counter;

  for (int indexFreqOctave=0; indexFreqOctave<octave()+1; indexFreqOctave++) {
      counter = 0;

      QVector<double> afLeqOctave200msBlockStockage(m_afLeqOctave2sBlock.at(indexFreqOctave));

      afLeqOctave200msBlock = adAcousticRes.at(indexFreqOctave);

      for (int indexLAeqValue = (0+(index*25)); indexLAeqValue < (25+(index*25)); indexLAeqValue++) {
          afLeqOctave200msBlockStockage.insert(indexLAeqValue,qAbs(afLeqOctave200msBlock.at(counter)));
          counter++;
      }

      m_afLeqOctave2sBlock.replace(indexFreqOctave,afLeqOctave200msBlockStockage);
  }

  if (lastBuffer) {
      updatePlotFreqBAResult();
  }
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::populateRegLines(RegressionLines rRegLines)
** @param[in] QVector<double> adAcousticRes
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::populateRegLines(RegressionLines rRegLines) {
  m_oRegLines = rRegLines;
  updatePlotRegBAResult();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::populateTRValues(ReverberationRes rReverberationRes)
** @param[in] QVector<double> adAcousticRes
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::populateTRValues(ReverberationRes rReverberationRes, ReverbAverageRes rAverageRes) {
  m_oReverbRes = rReverberationRes;
  m_oAverageRes = rAverageRes; // We don't use it for now!
  updatePlotValuesBAResult();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::updatePlotSizeBAResult()
** @param[in] void
** @param[out] void
** @brief slot that updates the graphic size, called everytime the width or
**        height has been changed in the QQuickPaintedItem
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::updatePlotSizeBAResult() {
  if(m_qwtPlot) {
      m_qwtPlot->setGeometry(0, 0, static_cast<int>(width()), static_cast<int>(height()));
  }
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::updatePlotSizeBAResult()
** @param[in] void
** @param[out] void
** @brief slot that updates the graphic size, called everytime the width or
**        height has been changed in the QQuickPaintedItem
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::updateReverbNoise() {
    m_qwtMarkerNoise->setYValue(m_oReverbNoise.at(m_iFrequencyIndex));
    m_qwtMarkerNoise->attach(m_qwtPlot);
    replotAndUpdateBAResult();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::updatePlotSizeBAResult()
** @param[in] void
** @param[out] void
** @brief slot that updates the graphic size, called everytime the width or
**        height has been changed in the QQuickPaintedItem
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::updateReverbLevel() {
    m_qwtMarkerSource->setYValue(m_oReverbLevel.at(m_iFrequencyIndex));
    m_qwtMarkerSource->attach(m_qwtPlot);
    replotAndUpdateBAResult();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::updatePlotFreqBAResult(int frequencyIndex)
** @param[in] QVector<double> adAcousticRes
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::updatePlotFreqBAResult() {
    QVector<double> afLeqOctave2sBlock;
    afLeqOctave2sBlock = m_afLeqOctave2sBlock.at(m_iFrequencyIndex);

    if (m_iChartLength == 3) {
        for (int j=0; j<BUFFER_RESULT_SIZE_3; j++) {
            m_afLAeqBuffer3[j] = qAbs(afLeqOctave2sBlock.at(j));
        }

        // Modification LAeq curve style for ReverbResult screen
        //m_curveLAeq->setPen(QColor("#ffffff"), 2.0, Qt::SolidLine);

        m_curveLAeq->setRawSamples(m_xDataBuffer3, m_afLAeqBuffer3, BUFFER_RESULT_SIZE_3);

    } else if (m_iChartLength == 10) {
        for (int j=0; j<BUFFER_RESULT_SIZE_10; j++) {
            m_afLAeqBuffer10[j] = qAbs(afLeqOctave2sBlock.at(j));
        }

        // Modification LAeq curve style for ReverbResult screen
        //m_curveLAeq->setPen(QColor("#ffffff"), 2.0, Qt::SolidLine);

        m_curveLAeq->setRawSamples(m_xDataBuffer10, m_afLAeqBuffer10, BUFFER_RESULT_SIZE_10);
    }

    m_qwtPlot->setAxisScaleDraw(QwtPlot::xBottom,new TimeScaleDrawBAResult(QTime::currentTime().addSecs(-m_iChartLength)));

    replotAndUpdateBAResult();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::updatePlotFreqBAResult(int frequencyIndex, QString modeTR)
** @param[in] QVector<double> adAcousticRes
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::updatePlotRegBAResult() {
    int iTrRegLineIndex = 0;
    int iTrXIndex = m_iBlockCounterStartPlot;
    int iTrStart = m_oRegLines.aiStartFitTr().at(m_iFrequencyIndex) + m_oRegLines.indexFirstFitRecordDecrease();
    int iT20End = m_oRegLines.aiEndFitTr20().at(m_iFrequencyIndex) + m_oRegLines.indexFirstFitRecordDecrease();
    int iT30End = m_oRegLines.aiEndFitTr30().at(m_iFrequencyIndex) + m_oRegLines.indexFirstFitRecordDecrease();
    int iT60End = m_oRegLines.aiEndFitTr60().at(m_iFrequencyIndex) + m_oRegLines.indexFirstFitRecordDecrease();

    QVector<double> afLeqOctave2sBlock;
    afLeqOctave2sBlock = m_afLeqOctave2sBlock.at(m_iFrequencyIndex);

    if(m_iChartLength == 3) {
        for (int j=0; j<BUFFER_RESULT_SIZE_3; j++) {
            // T20 Curve
            if (m_cModeTR == "T20") {
                if ((iTrXIndex >= iTrStart) && (iTrXIndex <= iT20End)) {

                    /* - Debut d'idee pour changer le style de la courbe mais qui ne marche pas ici
                    puisque l'on affiche la coubre dans son intégralité à la fin de la boucle for - */
                    /*if (iTrXIndex == iTrStart) {
                        m_curveT20->setPen(QColor("#51A8FF"), 2.0, Qt::SolidLine);
                        m_curveT20->attach(m_qwtPlot);
                        m_qwtPlot->replot();
                        qDebug()<<"Je passe Mode TrStart!";

                    } else if (iTrXIndex == iT20End) {
                        m_curveT20->setPen(QColor("#51A8FF"), 2.0, Qt::DashLine);
                        m_curveT20->attach(m_qwtPlot);
                        m_qwtPlot->replot();
                        qDebug()<<"Je passe Mode TrEnd!";
                    }*/

                    m_afT20Buffer3[j] = m_oRegLines.afLevelStartFitTr20().at(m_iFrequencyIndex) - (m_oRegLines.afSlopeTr20().at(m_iFrequencyIndex) * (8*0.001) * (iTrRegLineIndex++));

                } else {
                    m_afT20Buffer3[j] = -1;
                }

                m_afT30Buffer3[j] = -1;
                m_afT60Buffer3[j] = -1;

            // T30 Curve
            } else if (m_cModeTR == "T30") {
                if ((iTrXIndex >= iTrStart) && (iTrXIndex <= iT30End)) {
                    m_afT30Buffer3[j] = m_oRegLines.afLevelStartFitTr30().at(m_iFrequencyIndex) - (m_oRegLines.afSlopeTr30().at(m_iFrequencyIndex) * (8*0.001) * (iTrRegLineIndex++));

                } else {
                    m_afT30Buffer3[j] = -1;
                }

                m_afT20Buffer3[j] = -1;
                m_afT60Buffer3[j] = -1;

            // T60 Curve
            } else if (m_cModeTR == "T60") {
                if ((iTrXIndex >= iTrStart) && (iTrXIndex <= iT60End)) {
                    m_afT60Buffer3[j] = m_oRegLines.afLevelStartFitTr60().at(m_iFrequencyIndex) - (m_oRegLines.afSlopeTr60().at(m_iFrequencyIndex) * (8*0.001) * (iTrRegLineIndex++));

                } else {
                    m_afT60Buffer3[j] = -1;
                }

                m_afT20Buffer3[j] = -1;
                m_afT30Buffer3[j] = -1;
            }

            iTrXIndex++;
        }

        m_curveT20->setRawSamples(m_xDataBuffer3, m_afT20Buffer3, BUFFER_RESULT_SIZE_3);
        m_curveT20->attach(m_qwtPlot);
        m_curveT30->setRawSamples(m_xDataBuffer3, m_afT30Buffer3, BUFFER_RESULT_SIZE_3);
        m_curveT30->attach(m_qwtPlot);
        m_curveT60->setRawSamples(m_xDataBuffer3, m_afT60Buffer3, BUFFER_RESULT_SIZE_3);
        m_curveT60->attach(m_qwtPlot);

    } else if (m_iChartLength == 10) {
        for (int j=0; j<BUFFER_RESULT_SIZE_10; j++) {
            // T20 Curve
            if (m_cModeTR == "T20") {
                if ((iTrXIndex >= iTrStart) && (iTrXIndex <= iT20End)) {
                    m_afT20Buffer10[j] = m_oRegLines.afLevelStartFitTr20().at(m_iFrequencyIndex) - (m_oRegLines.afSlopeTr20().at(m_iFrequencyIndex) * (8*0.001) * (iTrRegLineIndex++));

                } else {
                    m_afT20Buffer10[j] = -1;
                }

                m_afT30Buffer10[j] = -1;
                m_afT60Buffer10[j] = -1;

            // T30 Curve
            } else if (m_cModeTR == "T30") {
                if ((iTrXIndex >= iTrStart) && (iTrXIndex <= iT30End)) {
                    m_afT30Buffer10[j] = m_oRegLines.afLevelStartFitTr30().at(m_iFrequencyIndex) - (m_oRegLines.afSlopeTr30().at(m_iFrequencyIndex) * (8*0.001) * (iTrRegLineIndex++));

                } else {
                    m_afT30Buffer10[j] = -1;
                }

                m_afT20Buffer10[j] = -1;
                m_afT60Buffer10[j] = -1;

            // T60 Curve
            } else if (m_cModeTR == "T60") {
                if ((iTrXIndex >= iTrStart) && (iTrXIndex <= iT60End)) {
                    m_afT60Buffer10[j] = m_oRegLines.afLevelStartFitTr60().at(m_iFrequencyIndex) - (m_oRegLines.afSlopeTr60().at(m_iFrequencyIndex) * (8*0.001) * (iTrRegLineIndex++));

                } else {
                    m_afT60Buffer10[j] = -1;
                }

                m_afT20Buffer10[j] = -1;
                m_afT30Buffer10[j] = -1;
            }

            iTrXIndex++;
        }

        m_curveT20->setRawSamples(m_xDataBuffer10, m_afT20Buffer10, BUFFER_RESULT_SIZE_10);
        m_curveT20->attach(m_qwtPlot);
        m_curveT30->setRawSamples(m_xDataBuffer10, m_afT30Buffer10, BUFFER_RESULT_SIZE_10);
        m_curveT30->attach(m_qwtPlot);
        m_curveT60->setRawSamples(m_xDataBuffer10, m_afT60Buffer10, BUFFER_RESULT_SIZE_10);
        m_curveT60->attach(m_qwtPlot);
    }

    replotAndUpdateBAResult();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::updatePlotValuesBAResult()
** @param[in] QVector<double> adAcousticRes
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::updatePlotValuesBAResult() {
    if (m_cModeTR == "T20") {
        setTrValue(SetParamValues(m_oReverbRes.afT20().at(m_iFrequencyIndex)));

    } else if (m_cModeTR == "T30") {
        setTrValue(SetParamValues(m_oReverbRes.afT30().at(m_iFrequencyIndex)));

    } else if (m_cModeTR == "T60") {
        setTrValue(SetParamValues(m_oReverbRes.afT60().at(m_iFrequencyIndex)));
    }
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::updateReverbActiveMeasure(int indexReverbActiveMeasure, bool resetReverbMeasure)
** @param[in] QVector<double> adAcousticRes
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::updateReverbActiveMeasure(int indexReverbActiveMeasure, bool resetReverbMeasure) {
    qDebug()<<"indexReverbActiveMeasure chart : "<<indexReverbActiveMeasure;

    if(resetReverbMeasure) {
        m_listReverbNoise.removeLast();
        m_listReverbLevel.removeLast();
        m_listLeqOctave.removeLast();
        m_listRegLines.removeLast();
        m_listReverbRes.removeLast();
        m_listBlockCounter.removeLast();

        m_iListIndex--;
    }

    m_oReverbNoise = m_listReverbNoise.at(indexReverbActiveMeasure);
    m_oReverbLevel = m_listReverbLevel.at(indexReverbActiveMeasure);
    m_afLeqOctave2sBlock = m_listLeqOctave.at(indexReverbActiveMeasure);
    m_oRegLines = m_listRegLines.at(indexReverbActiveMeasure);
    m_oReverbRes = m_listReverbRes.at(indexReverbActiveMeasure);
    m_iBlockCounterStartPlot = m_listBlockCounter.at(indexReverbActiveMeasure);

    updateReverbNoise();
    updateReverbLevel();
    updatePlotFreqBAResult();
    updatePlotRegBAResult();
    updatePlotValuesBAResult();
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::getCounterSinceStartTR(int counter)
** @param[in] QVector<double>
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::getCounterSinceStartTR(int counter) {
    m_iBlockCounterStartPlot = counter - (2*25); // 25 points -> 200ms
                                                 // LAeq curve display start 400ms before state "Source Before Recording"
}

/**
*******************************************************************************
** @fn  void ChartQuick2BAResult::saveReverbActiveMeasure()
** @param[in] int iStep: the direction to whitch the cursor should move
**                       if(iStep > 0) moves forward
**                       else if(iStep < 0) moves backward
** @param[out] void
** @brief locates the cursor inside the chart depending on number of times
**        the corresponding button is clicked
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BAResult::saveReverbActiveMeasure() {
    m_listReverbNoise.append(m_oReverbNoise);
    m_listReverbLevel.append(m_oReverbLevel);
    m_listLeqOctave.append(m_afLeqOctave2sBlock);
    m_listRegLines.append(m_oRegLines);
    m_listReverbRes.append(m_oReverbRes);
    m_listBlockCounter.append(m_iBlockCounterStartPlot);
    m_iListIndex++;
    qDebug()<<"Save active measure, index :"<<m_iListIndex;
}
