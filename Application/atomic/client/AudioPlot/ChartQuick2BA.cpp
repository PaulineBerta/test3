/**
*******************************************************************************
** @file    ChartQuick2.cpp
** @version
** @author  Wevioo SE Team
** @brief   This class provides the implementation of the temporal charts
**         presented in QML based QQuickPaintedItem and QWT.
**
*******************************************************************************
**/
#include "ChartQuick2BA.h"

#include <qwt_plot.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_renderer.h>
#include <qwt_scale_div.h>
#include <qwt_scale_engine.h>
#include <QDebug>

/**
*******************************************************************************
** @fn  ChartQuick2::ChartQuick2(QQuickItem* parent) : QQuickPaintedItem(parent)
** @param[in] QQuickItem *parent: holds an instance of the QQuickItem which
**            defines all the attributes that defines a visual item
**            (position, height, width, ..)
** @param[out]
** @brief ChartQuick2's class constructor, derives from QQuickPaintedItem
**        enabling the widget to be painted in QML.
**
** @return
**
*******************************************************************************
**/
ChartQuick2BA::ChartQuick2BA(QQuickItem* parent) : QQuickPaintedItem(parent)
, m_qwtPlot(nullptr) {

  setFlag(QQuickItem::ItemHasContents, true);
  setAcceptTouchEvents(true);
  setAcceptedMouseButtons(Qt::MouseButton::AllButtons);

  connect(this, &QQuickPaintedItem::widthChanged, this, &ChartQuick2BA::updatePlotSizeBA);
  connect(this, &QQuickPaintedItem::heightChanged, this, &ChartQuick2BA::updatePlotSizeBA);

  for (int i = 0; i < BUFFER_SIZE_4; i++) {
      m_xDataBuffer4[i] = i/125.0;
      m_afLAeqBuffer4[i] = 0;
  }

  for (int i = 0; i < BUFFER_SIZE_11; i++) {
      m_xDataBuffer11[i] = i/125.0;
      m_afLAeqBuffer11[i] = 0;
  }
}

/**
*******************************************************************************
** @fn  ChartQuick2BA::~ChartQuick2BA()
** @param[in]
** @param[out]
** @brief destructor of ChartQuick2 class: delets the pointer to the plot
**
** @return
**
*******************************************************************************
**/
ChartQuick2BA::~ChartQuick2BA() {
  delete m_qwtPlot;
  m_qwtPlot = nullptr;
}

/**
*******************************************************************************
** @fn  void ChartQuick2BA::InitializeBA()
** @param[in] void
** @param[oinitializes the data buffer at start and at reset clicked
**
** @return
**
*******************************************************************************
**/
void ChartQuick2BA::InitializeBA() {
  for (int i = 0; i < BUFFER_SIZE_4; i++) {
    m_xDataBuffer4[i] = i/125.0;
    m_afLAeqBuffer4[i] = 0;
  }

  for (int i = 0; i < BUFFER_SIZE_11; i++) {
    m_xDataBuffer11[i] = i/125.0;
    m_afLAeqBuffer11[i] = 0;
  }

  m_bAtStart=true;
  replotAndUpdateBA();
}
/**
*******************************************************************************
** @fn  void ChartQuick2BA::replotAndUpdateBA()
** @param[in] void
** @param[out] void
** @brief refresches the plot
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BA::replotAndUpdateBA() {
  m_qwtPlot->updateAxes();
  m_qwtPlot->replot();
  update();
}

void ChartQuick2BA::resetCounterFirstSecond() {
    m_iCounterFirstSecond = 0;
}

/**
*******************************************************************************
** @fn  void ChartQuick2BA::initQwtPlotBA(QString chartTime)
** @param[in] void
** @param[out] void
** @brief initializes and sets different parameters of the plot:
**        - Style of the plot interface
**        - layout of different Axes and grid
**        - initializes the curves to be painted in the graph
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BA::initQwtPlotBA(QString chartTime) {
  if (chartTime == "3") {
      m_iChartLength = 3+1;
      m_afAxisScale = 1.0;
  } else if (chartTime == "10") {
      m_iChartLength = 10+1;
      m_afAxisScale = 2.0;
  }

  m_qwtPlot = new QwtPlot();
  m_qwtPlot->setAttribute(Qt::WA_AcceptTouchEvents, true);
  m_qwtPlot->setAttribute(Qt::WA_StaticContents, true);

  // after replot() we need to call update() - so disable auto replot
  m_qwtPlot->setAutoReplot(false);
  m_qwtPlot->setStyleSheet("background-color:#353535; color:white; border-radius: 0px; font: 9pt; QwtPlot { padding: 0px }");
  m_qwtPlot->plotLayout()->setCanvasMargin(0);
  m_qwtPlot->plotLayout()->setAlignCanvasToScales(true);
  m_qwtPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  QFont font;
  font.setPixelSize(7);
  font.setFamily("Arial-BoldMT");
  font.setBold(true);
  font.setWeight(QFont::ExtraBold);
  font.setHintingPreference(QFont::HintingPreference::PreferNoHinting);
  m_qwtPlot->setFont(font);
  m_qwtPlot->setAxisFont(QwtPlot::xBottom, font);
  m_qwtPlot->setAxisFont(QwtPlot::yLeft, font);

  updatePlotSizeBA();

  /*!< Set Grid */
  QwtPlotGrid *grid = new QwtPlotGrid();
  //enable the x and y axis lines
  grid->enableY(true);
  grid->enableX(true);

  QPen pen;
  pen.setStyle(Qt::SolidLine);
  pen.setColor(Qt::white);
  pen.setWidthF(0.5);
  grid->setPen(pen);
  grid->attach(m_qwtPlot);

  /*!< Set X axis */
  timeScaleBA=new TimeScaleDrawBA(QTime::currentTime().addSecs(-m_iChartLength));
  m_qwtPlot->setAxisScaleDraw(QwtPlot::xBottom, timeScaleBA);
  timeScaleBA->setLength(m_iChartLength);
  m_qwtPlot->setAxisScale(QwtPlot::xBottom, 0, m_iChartLength, m_afAxisScale);
  m_qwtPlot->axisScaleEngine(QwtPlot::xBottom)->setAttribute(QwtScaleEngine::Inverted, true);
  m_qwtPlot->axisScaleEngine(QwtPlot::xBottom)->setMargins(0,0);

  /*!< set Y axis */
  QwtScaleDraw* qwtScale= new QwtScaleDraw;
  qwtScale->setTickLength(QwtScaleDiv::MajorTick,0);
  qwtScale->setTickLength(QwtScaleDiv::MinorTick,0);
  m_qwtPlot->setAxisScaleDraw(QwtPlot::yLeft,qwtScale);
  m_qwtPlot->setAxisMaxMinor(QwtPlot::yLeft, 0);
  m_qwtPlot->setAxisScale(QwtPlot::yLeft, 0, +140);

  /*!< LAeq temporal Curve */
  m_curveLAeq = new QwtPlotCurve();
  m_curveLAeq->setRenderHint(QwtPlotItem::RenderAntialiased,true);
  m_curveLAeq->setPen(QPen(QColor("#ffffff"))); //white curve
  m_curveLAeq->setStyle(QwtPlotCurve::Lines);
  m_curveLAeq->setRenderHint(QwtPlotItem::RenderAntialiased);
  m_curveLAeq->attach(m_qwtPlot);

  /*!< set the line marker */
  m_qwtMarker= new QwtPlotMarker;
  m_qwtMarker->setLineStyle(QwtPlotMarker::VLine);
  m_qwtMarker->setLabelAlignment(Qt::AlignRight | Qt::AlignTop);
  m_qwtMarker->setLinePen(QColor(Qt::cyan),2,Qt::SolidLine);

  m_qwtMarker->setXValue(m_iChartLength-1);

  m_qwtMarker->attach(m_qwtPlot);

  replotAndUpdateBA();
}

/**
*******************************************************************************
** @fn  void ChartQuick2::paint(QPainter* painter)
** @param[in] QPainter* painter
** @param[out] void
** @brief this function is called by the QML Scene Graph which paints the
**        Qwt widgets to screen.
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BA::paint(QPainter* painter) {
  if(m_qwtPlot) {
      painter->save();
      QwtPlotRenderer renderer;
      renderer.setLayoutFlag(QwtPlotRenderer::FrameWithScales);
      QFont font;
      font.setPixelSize(7);
      font.setFamily("Arial-BoldMT");
      font.setBold(true);
      font.setWeight(QFont::ExtraBold);
      font.setHintingPreference(QFont::HintingPreference::PreferNoHinting);
      painter->setFont(font);
      renderer.render(m_qwtPlot, painter, m_qwtPlot->rect());

      painter->restore();
    }
}

/**
*******************************************************************************
** @fn  void ChartQuick2::updatePlotSize()
** @param[in] void
** @param[out] void
** @brief slot that updates the graphic size, called everytime the width or
**        height has been changed in the QQuickPaintedItem
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BA::updatePlotSizeBA() {
  if(m_qwtPlot) {
      m_qwtPlot->setGeometry(0, 0, static_cast<int>(width()), static_cast<int>(height()));
  }
}

/**
*******************************************************************************
** @fn  void ChartQuick2::populateBA(QVector<double> adAcousticRes)
** @param[in] QVector<double> adAcousticRes
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BA::populateBA(QVector<double> adAcousticRes) {
    m_iCounterFirstSecond++;

    if (m_iCounterFirstSecond > 5) { // provisoire!!!!!!!!!!!!!!

        for (int j=0; j<25; j++) {

            if (m_iChartLength == 4) {
               for (int i = 0; i < BUFFER_SIZE_4 - 1; i++) {
                  m_afLAeqBuffer4[i] = m_afLAeqBuffer4[i+1];
               }

               m_afLAeqBuffer4[BUFFER_SIZE_4 - 1] = qAbs(adAcousticRes.at(j));
               m_curveLAeq->setRawSamples(m_xDataBuffer4, m_afLAeqBuffer4, BUFFER_SIZE_4);

             } else if (m_iChartLength == 11) {
               for (int i = 0; i < BUFFER_SIZE_11 - 1; i++) {
                  m_afLAeqBuffer11[i] = m_afLAeqBuffer11[i+1];
               }

               m_afLAeqBuffer11[BUFFER_SIZE_11 - 1] = qAbs(adAcousticRes.at(j));
               m_curveLAeq->setRawSamples(m_xDataBuffer11, m_afLAeqBuffer11, BUFFER_SIZE_11);
             }

            m_qwtPlot->setAxisScaleDraw(QwtPlot::xBottom,new TimeScaleDrawBA(QTime::currentTime().addSecs(-m_iChartLength)));

            if(m_bAtStart){
                setMarker();
                m_qwtMarker->setXValue(m_iChartLength-1);
                m_bAtStart=false;
            }

            if (m_iChartLength == 4) {
                setLAeqValue(QString::number(m_afLAeqBuffer4[static_cast<int>(m_qwtMarker->value().x()*125)],'f',1));
            } else if (m_iChartLength == 11) {
                setLAeqValue(QString::number(m_afLAeqBuffer11[static_cast<int>(m_qwtMarker->value().x()*125)],'f',1));
            }
            m_qwtMarker->attach(m_qwtPlot);

            replotAndUpdateBA();
        }
    }
}

/**
*******************************************************************************
** @fn  void ChartQuick2::touchEvent(QTouchEvent *event)
** @param[in] QTouchEvent *event: the event corresponding to screen touching
**              on the bar chart
** @param[out] void
** @brief virtual function called everytime a touch has been sensed on the chart
**        and displays a marker at the touched point
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2BA::touchEvent(QTouchEvent *event) {
  qDebug() << " *** touch event temporal chart *** ";

  switch (event->type()) {
    case QEvent::TouchBegin:
    case QEvent::TouchUpdate:
    case QEvent::TouchEnd:{

        if(m_qwtPlot) {
            QList<QTouchEvent::TouchPoint> touchPoints = static_cast<QTouchEvent *>(event)->touchPoints();
            foreach (const QTouchEvent::TouchPoint &touchPoint, touchPoints) {

                switch (touchPoint.state()) {
                  case Qt::TouchPointStationary:
                  case Qt::TouchPointMoved:
                  case Qt::TouchPointPressed: continue;
                  case Qt::TouchPointReleased:{
                    const QTouchEvent::TouchPoint &touchPoint = touchPoints.first( );
                    QPointF currentTouchPointPos=touchPoint.pos();

                    if(currentTouchPointPos.isNull()) {
                        qDebug() << "touch is NULL" ;
                        break;
                    }

                    double scalePosX=currentTouchPointPos.x()*m_iChartLength/m_uiWidth;
                    m_qwtMarker->setXValue(static_cast<int>(scalePosX));

                    if(scalePosX>=0 && scalePosX<=0.25) {
                        m_qwtMarker->setXValue(0);
                    } else if(scalePosX>0.25 && scalePosX<=0.75) {
                        m_qwtMarker->setXValue(0.5);
                    } else if(scalePosX>0.75 && scalePosX<=1.25) {
                        m_qwtMarker->setXValue(1);
                    } else if(scalePosX>1.25 && scalePosX<=1.75) {
                        m_qwtMarker->setXValue(1.5);
                    } else if(scalePosX>1.75 && scalePosX<=2.25) {
                        m_qwtMarker->setXValue(2);
                    } else if(scalePosX>2.25 && scalePosX<=2.75) {
                        m_qwtMarker->setXValue(2.5);
                    } else if(scalePosX>2.75 && scalePosX<=3.25) {
                        m_qwtMarker->setXValue(3);
                    } else if(scalePosX>3.25 && scalePosX<=3.75) {
                        m_qwtMarker->setXValue(3.5);
                    } else if(scalePosX>3.75 && scalePosX<=4.25) {
                        m_qwtMarker->setXValue(4);
                    } else if(scalePosX>4.25 && scalePosX<=4.75) {
                        m_qwtMarker->setXValue(4.5);
                    } else if(scalePosX>4.75 && scalePosX<=5.25) {
                        m_qwtMarker->setXValue(5);
                    } else if(scalePosX>5.25 && scalePosX<=5.75) {
                        m_qwtMarker->setXValue(5.5);
                    } else if(scalePosX>5.75 && scalePosX<=6.25) {
                        m_qwtMarker->setXValue(6);
                    } else if(scalePosX>6.25 && scalePosX<=6.75) {
                        m_qwtMarker->setXValue(6.5);
                    } else if(scalePosX>6.75 && scalePosX<=7.25) {
                        m_qwtMarker->setXValue(7);
                    } else if(scalePosX>7.25 && scalePosX<=7.75) {
                        m_qwtMarker->setXValue(7.5);
                    } else if(scalePosX>7.75 && scalePosX<=8.25) {
                        m_qwtMarker->setXValue(8);
                    } else if(scalePosX>8.25 && scalePosX<=8.75) {
                        m_qwtMarker->setXValue(8.5);
                    } else if(scalePosX>8.75 && scalePosX<=9.25) {
                        m_qwtMarker->setXValue(9);
                    } else if(scalePosX>9.25 && scalePosX<=9.75) {
                        m_qwtMarker->setXValue(9.5);
                    } else if(scalePosX>9.75 && scalePosX<=10.25) {
                        m_qwtMarker->setXValue(10);
                    } else if(scalePosX>10.25 && scalePosX<=10.75) {
                        m_qwtMarker->setXValue(10.5);
                    } else if(scalePosX>10.75 && scalePosX<=11) {
                        m_qwtMarker->setXValue(11);
                    }

                    double val=m_qwtMarker->value().x();
                    setTime((QTime::currentTime().addSecs(-m_iChartLength + static_cast<int>(val))).toString("hh:mm:ss"));

                    if(m_iChartLength == 4) {
                        setLAeqValue(QString::number(m_afLAeqBuffer4[static_cast<int>(val*125)],'f',1));
                    } else if (m_iChartLength == 11) {
                        setLAeqValue(QString::number(m_afLAeqBuffer11[static_cast<int>(val*125)],'f',1));
                    }

                    m_qwtMarker->attach(m_qwtPlot);

                    replotAndUpdateBA();
                  }
                    break;
                }
            }
        }
        break;
  }
  default: break;
  }
}
