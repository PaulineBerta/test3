/**
*******************************************************************************
** @file    BarQuick2.cpp
** @version
** @author  Wevioo SE Team
** @brief   This class provides the implementation of the frequency-bar chart
**         in the QML based upon QQuickPaintedItem and QWT.
**
*******************************************************************************
**/
#include "BarQuick2.h"
#include <qwt_plot.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_renderer.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_multi_barchart.h>
#include <qwt_column_symbol.h>
#include <qwt_plot_layout.h>
#include <qwt_legend.h>
#include <qwt_scale_draw.h>
#include <qwt_scale_engine.h>
#include <QGestureEvent>
#include <QGesture>

/**
*******************************************************************************
** @fn  BarQuick2::BarQuick2(QQuickItem* parent) : QQuickPaintedItem(parent)
** @param[in] QQuickItem *parent: holds an instance of the QQuickItem which
**            defines all the attributes that defines a visual item
**            (position, height, width, ..)
** @param[out]
** @brief BarQuick2's class constructor, derives from QQuickPaintedItem
**        enabling the widget to be painted in QML.
**
** @return
**
*******************************************************************************
**/
BarQuick2::BarQuick2(QQuickItem* parent) : QQuickPaintedItem(parent)
, m_qwtPlot(nullptr), m_barItem(nullptr), m_iOctaveConfig(36)
, m_uiWidth(0) ,m_cFrequency(" "), m_cLeqTValue(" "), m_cLeqGValue(" ")
{
  setFlag(QQuickItem::ItemHasContents, true);

  setAcceptTouchEvents(true);
  setAcceptedMouseButtons(Qt::MouseButton::AllButtons);

  connect(this, &QQuickPaintedItem::widthChanged, this, &BarQuick2::updatePlotSize);
  connect(this, &QQuickPaintedItem::heightChanged, this, &BarQuick2::updatePlotSize);
}

/**
*******************************************************************************
** @fn  BarQuick2::~BarQuick2()
** @param[in]
** @param[out]
** @brief destructor of BarQuick2 class: deletes the pointer to the bar-plot item
**         and the overall plot
**
** @return
**
*******************************************************************************
**/
BarQuick2::~BarQuick2()
{
  delete m_barItem;
  m_barItem=nullptr;

  delete m_qwtPlot;
  m_qwtPlot = nullptr;
}

/**
*******************************************************************************
** @fn  void BarQuick2::replotAndUpdate()
** @param[in] void
** @param[out] void
** @brief refresches the plot
**
** @return void
**
*******************************************************************************
**/
void BarQuick2::replotAndUpdate()
{
  m_qwtPlot->replot();
  update();
}

/**
*******************************************************************************
** @fn  void BarQuick2::initQwtPlot()
** @param[in] void
** @param[out] void
** @brief initializes and sets different parameters of the bar plot:
**        - Style of the plot interface
**        - layout of different Axes and grid
**        - initializes tha bar chart items and attaches to the 2D-plot
**
** @return void
**
*******************************************************************************
**/
void BarQuick2::initBarPlot()
{
  m_qwtPlot = new QwtPlot();
  m_qwtPlot->setAttribute(Qt::WA_AcceptTouchEvents, true);
  m_qwtPlot->setAttribute(Qt::WA_StaticContents, false);
  m_qwtPlot->grabGesture(Qt::SwipeGesture);

  m_qwtPlot->setFocusPolicy(Qt::StrongFocus);
  m_qwtPlot->grabMouse();
  setAcceptTouchEvents(true);

  m_qwtPlot->setAutoReplot(false);
  m_qwtPlot->setStyleSheet("background-color:#353535; color:white; border-radius: 0px; font: 10pt; QwtPlot { padding: 0px }");
  m_qwtPlot->plotLayout()->setCanvasMargin(0);
  m_qwtPlot->plotLayout()->setAlignCanvasToScales(true);
  m_qwtPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  QFont font;
  font.setPixelSize(7);
  font.setFamily("Arial-BoldMT");
  font.setBold(true);
  font.setWeight(QFont::ExtraBold);
  font.setHintingPreference(QFont::HintingPreference::PreferNoHinting);
  m_qwtPlot->setFont(font);
  m_qwtPlot->setAxisFont(QwtPlot::xBottom, font);
  m_qwtPlot->setAxisFont(QwtPlot::yLeft, font);

  updatePlotSize();

  QwtPlotGrid *grid = new QwtPlotGrid();
  grid->enableX(true);
  grid->enableY(true);

  QPen pen;
  pen.setStyle(Qt::SolidLine);
  pen.setColor(Qt::white);
  pen.setWidthF(0.5);
  grid->setPen(pen);
  grid->attach(m_qwtPlot);


  /*!< Set X axis */
  m_qwtPlot->setAxisAutoScale(QwtPlot::xBottom,false);
  m_qwtPlot->setAxisScaleDraw(QwtPlot::xBottom,new FrequencyScaleDraw(octave()));

  if(octave() == 36){
      m_qwtPlot->setAxisScale(QwtPlot::xBottom, 0,octave()+3,3);
    }
  else {
      m_qwtPlot->setAxisMaxMinor(QwtPlot::xBottom, 0);
      m_qwtPlot->setAxisScale(QwtPlot::xBottom, 0,octave()+1,1);
    }


  /*!< set Y axis */
  QwtScaleDraw* qwtScale= new QwtScaleDraw;
  qwtScale->enableComponent(QwtAbstractScaleDraw::Ticks, false);
  m_qwtPlot->setAxisScaleDraw(QwtPlot::yLeft,qwtScale);
  m_qwtPlot->setAxisMaxMinor(QwtPlot::yLeft, 0);
  m_qwtPlot->setAxisScale(QwtPlot::yLeft, +20, +140);

  m_barItem = new QwtPlotMultiBarChart();
  m_barItem->setLayoutPolicy( QwtPlotMultiBarChart::AutoAdjustSamples);

  m_barItem->setMargin(3);
  m_barItem->setSpacing(2);
  m_barItem->attach( m_qwtPlot);

  /*!< set the line marker */
  m_qwtMarker= new QwtPlotMarker;
  m_qwtMarker->setLineStyle(QwtPlotMarker::VLine);
  m_qwtMarker->setLabelAlignment(Qt::AlignRight | Qt::AlignTop);
  m_qwtMarker->setLinePen(QColor(Qt::green),2,Qt::SolidLine);

  if(octave() == 36){
      m_qwtMarker->setXValue(24);
    }
  else {
      m_qwtMarker->setXValue(8);
    }
  m_qwtMarker->attach(m_qwtPlot);

  replotAndUpdate();
}

void BarQuick2::step(int iStep){
  double val=m_qwtMarker->value().x()+(iStep*1);
  if(octave()==36){
      if(val>=2 && val<= octave()+1)
        {
          m_qwtMarker->setXValue(static_cast<int>(val));
        }
      else if(val > octave()+1){
          m_qwtMarker->setXValue(2);
        }
      else {
          m_qwtMarker->setXValue(37);
        }
    }
  else {
      if(val>=1 && val<= octave())
        {
          m_qwtMarker->setXValue(static_cast<int>(val));
        }
      else if(val>octave()) {
          m_qwtMarker->setXValue(1);
        }
      else{
          m_qwtMarker->setXValue(12);
        }
    }
  m_qwtMarker->attach(m_qwtPlot);
  replotAndUpdate();
}

/**
*******************************************************************************
** @fn  int BarQuick2::octave()
** @param[in] void
** @param[out] int
** @brief returns the current octave parameter
**
** @return the current-octave configuration
**
*******************************************************************************
**/
int BarQuick2::octave()
{
  return m_iOctaveConfig;
}

/**
*******************************************************************************
** @fn  BarQuick2::setOctave(int iOctave)
** @param[in] int
** @param[out] void
** @brief sets the current octave parameter
**
** @return void
**
*******************************************************************************
**/
void BarQuick2::setOctave(int iOctave)
{
  if(iOctave==12 || iOctave==36){
      m_iOctaveConfig=iOctave;
      emit octaveChanged();
    }
}

/**
*******************************************************************************
** @fn  void BarQuick2::populate(TrAcousticRes rAcousticRes)
** @param[in] TrAcousticRes rAcousticRes
** @param[out] void
** @brief sets the frequential-bar graph's data with data from TrAcousticRes
**
** @return void
**
*******************************************************************************
**/
void BarQuick2::populate(AcousticRes rAcousticRes)
{
  static const char *colors[] = { "#adcff0", "#0092ff"};
  const int numSamples = octave();
  const int numBars = 2;

  for ( int i = 0; i < numBars; i++ )
    {
      QwtColumnSymbol *symbol = new QwtColumnSymbol( QwtColumnSymbol::Box );
      symbol->setLineWidth(0);
      symbol->setFrameStyle( QwtColumnSymbol::Plain);
      symbol->setPalette( QPalette( colors[i] ) );
      m_barItem->setSymbol( i, symbol );
    }

  QVector< QVector<double> > series;

  //First Space
  QVector<double> valuesInit;
  valuesInit += 0;
  valuesInit += 0;
  series += valuesInit;

  if(numSamples == 36){
      //Extra Space
      QVector<double> valuesExtra;
      valuesExtra += 0;
      valuesExtra += 0;
      series += valuesExtra;
    }

  for (int index = 0; index < numSamples; index++) // freqCount 12 or 36
    {
      // Add Y component in Bar Chart
      QVector<double> values;
      values +=  qAbs(rAcousticRes.adLeq().at(index));
      values +=  qAbs(rAcousticRes.adLeqG().at(index));

      series += values;
    }

  m_barItem->setSamples( series );
  if(static_cast<bool>(m_qwtMarker->value().x())){

      m_qwtMarker->attach(m_qwtPlot);

      QString cFrequency;
      if(m_barItem->isVisible()){
      if(octave()==36){
          switch(int(m_qwtMarker->xValue()))
            {
            case 0:
              cFrequency="";break;
            case 1:
              cFrequency="";break;
            case 2:
              cFrequency="6.3 Hz";break;
            case 3:
              cFrequency="8 Hz";break;
            case 4:
              cFrequency="10 Hz";break;
            case 5:
              cFrequency="12.5 Hz";break;
            case 6:
              cFrequency="16 Hz";break;
            case 7:
              cFrequency="20 Hz";break;
            case 8:
              cFrequency="25 Hz";break;
            case 9:
              cFrequency="31.5 Hz";break;
            case 10:
              cFrequency="40 Hz";break;
            case 11:
              cFrequency="50 Hz";break;
            case 12:
              cFrequency="63 Hz";break;
            case 13:
              cFrequency="80 Hz";break;
            case 14:
              cFrequency="100 Hz";break;
            case 15:
              cFrequency="125 Hz";break;
            case 16:
              cFrequency="160 Hz";break;
            case 17:
              cFrequency="200 Hz";break;
            case 18:
              cFrequency="250 Hz";break;
            case 19:
              cFrequency="315 Hz";break;
            case 20:
              cFrequency="400 Hz";break;
            case 21:
              cFrequency="500 Hz";break;
            case 22:
              cFrequency="630 Hz";break;
            case 23:
              cFrequency="800 Hz";break;
            case 24:
              cFrequency="1 KHz";break;
            case 25:
              cFrequency="1.25 KHz";break;
            case 26:
              cFrequency="1.6 KHz";break;
            case 27:
              cFrequency="2 KHz";break;
            case 28:
              cFrequency="2.5 KHz";break;
            case 29:
              cFrequency="3.15 KHz";break;
            case 30:
              cFrequency="4 KHz";break;
            case 31:
              cFrequency="5 KHz";break;
            case 32:
              cFrequency="6.3 KHz";break;
            case 33:
              cFrequency="8 KHz";break;
            case 34:
              cFrequency="10 KHz";break;
            case 35:
              cFrequency="12.5 KHz";break;
            case 36:
              cFrequency="16 KHz";break;
            case 37:
              cFrequency="20 KHz"; break;
            case 38:
              cFrequency="";break;
            case 39:
              cFrequency="";break;

            default: cFrequency="";break;
            }
        }
      else {
          switch(int(m_qwtMarker->xValue()))
            {
            case 0:
              cFrequency="";break;
            case 1:
              cFrequency="8 Hz";break;
            case 2:
              cFrequency="16 Hz";break;
            case 3:
              cFrequency="31.5 Hz";break;
            case 4:
              cFrequency="63 Hz";break;
            case 5:
              cFrequency="125 Hz";break;
            case 6:
              cFrequency="250 Hz";break;
            case 7:
              cFrequency="500 Hz";break;
            case 8:
              cFrequency="1 KHz";break;
            case 9:
              cFrequency="2 KHz";break;
            case 10:
              cFrequency="4 KHz";break;
            case 11:
              cFrequency="8 KHz";break;
            case 12:
              cFrequency="16 KHz";break;
            case 13:
              cFrequency="";break;

            default: cFrequency="";break;
            }
        }

      setFrequency(cFrequency);
      setLeqTValue(QString::number(m_barItem->data()->sample(m_qwtMarker->xValue()).set.first(),'f',1));
      setLeqGValue(QString::number(m_barItem->data()->sample(m_qwtMarker->xValue()).set.last(),'f',1));
    }
    }
  else {
      setFrequency("");
      setLeqTValue("NaN");
      setLeqGValue("NaN");
    }
}

/**
*******************************************************************************
** @fn  void BarQuick2::paint(QPainter* painter)
** @param[in] QPainter* painter
** @param[out] void
** @brief this function is called by the QML Scene Graph which paints the
**        Qwt widgets to screen.
**
** @return void
**
*******************************************************************************
**/
void BarQuick2::paint(QPainter* painter)
{
  if (m_qwtPlot)
    {
      painter->save();
      QwtPlotRenderer renderer;
      renderer.setLayoutFlag(QwtPlotRenderer::FrameWithScales);
      QFont font;
      font.setPixelSize(7);
      font.setFamily("Arial-BoldMT");
      font.setBold(true);
      font.setWeight(QFont::ExtraBold);
      font.setHintingPreference(QFont::HintingPreference::PreferNoHinting);
      painter->setFont(font);
      renderer.render(m_qwtPlot, painter, m_qwtPlot->rect());
      painter->restore();
    }
}

/**
*******************************************************************************
** @fn  void BarQuick2::passData(TrAcousticRes rAcousticRes)
** @param[in] TrAcousticRes rAcousticRes
** @param[out] void
** @brief slot called every 200ms to append data from the TrAcousticRes
**        structure and updates the frequential-bar graph
**
** @return void
**
*******************************************************************************
**/
void BarQuick2::passData(AcousticRes rAcousticRes)
{
  populate(rAcousticRes);
  replotAndUpdate();
}

/**
*******************************************************************************
** @fn  void BarQuick2::updatePlotSize()
** @param[in] void
** @param[out] void
** @brief slot that updates the graphic size, called everytime the width or
**        height has been changed in the QQuickPaintedItem
**
** @return void
**
*******************************************************************************
**/
void BarQuick2::updatePlotSize()
{
  if (m_qwtPlot) {
      m_qwtPlot->setGeometry(0, 0, static_cast<int>(width()), static_cast<int>(height()));
    }
}

/**
*******************************************************************************
** @fn  void BarQuick2::event(QEvent *event)
** @param[in] QEvent *event: the event corresponding to screen touching
**              on the bar chart
** @param[out] void
** @brief virtual function called everytime a touch has been sensed on the chart
**        and displays a marker at the touched point
**
** @return void
**
*******************************************************************************
**/
bool BarQuick2::event(QEvent* event){
  qDebug()<< " event!!";
  switch(event->type()){
    case QEvent::TouchBegin:
    case QEvent::TouchUpdate:
    case QEvent::TouchEnd:
      {
        if (m_qwtPlot) {
            QList<QTouchEvent::TouchPoint> touchPoints = static_cast<QTouchEvent *>(event)->touchPoints();
            foreach (const QTouchEvent::TouchPoint &touchPoint, touchPoints) {
                switch (touchPoint.state()) {
                  case Qt::TouchPointStationary:
                  case Qt::TouchPointMoved:
                  case Qt::TouchPointPressed:continue;
                  case Qt::TouchPointReleased:
                    {
                      const QTouchEvent::TouchPoint &touchPoint = touchPoints.first( );
                      QPointF currentTouchPointPos=touchPoint.pos();
                      if (currentTouchPointPos.isNull()) {
                          qDebug() << "touch is NULL" ;
                          break;
                        }

                      if(octave()==36){
                        double scalePosX=currentTouchPointPos.x()*(octave()+1)/m_uiWidth;
                        if(scalePosX>=2 && scalePosX<= octave()+1)
                          {
                            m_qwtMarker->setXValue(static_cast<int>(scalePosX));
                          }
                        else {
                            m_qwtMarker->setXValue(0);
                          }
                      }
                      else {
                          double scalePosX=currentTouchPointPos.x()*(octave())/m_uiWidth;
                          if(scalePosX>=1 && scalePosX<= octave())
                            {
                            m_qwtMarker->setXValue(static_cast<int>(scalePosX));

                          }
                          else{
                              m_qwtMarker->setXValue(0);
                            }
                        }
                      m_qwtMarker->attach(m_qwtPlot);
                      replotAndUpdate();
                    }
                  }
              }
          }
        break;
      }
    default:
    break;
    }
  return 1;
}
