/**
*******************************************************************************
** @file    ChartQuick2.cpp
** @version
** @author  Wevioo SE Team
** @brief   This class provides the implementation of the temporal charts
**         presented in QML based QQuickPaintedItem and QWT.
**
*******************************************************************************
**/
#include "ChartQuick2.h"

#include <qwt_plot.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_renderer.h>
#include <qwt_scale_div.h>
#include <qwt_scale_engine.h>
#include <QDebug>
/**
*******************************************************************************
** @fn  ChartQuick2::ChartQuick2(QQuickItem* parent) : QQuickPaintedItem(parent)
** @param[in] QQuickItem *parent: holds an instance of the QQuickItem which
**            defines all the attributes that defines a visual item
**            (position, height, width, ..)
** @param[out]
** @brief ChartQuick2's class constructor, derives from QQuickPaintedItem
**        enabling the widget to be painted in QML.
**
** @return
**
*******************************************************************************
**/
ChartQuick2::ChartQuick2(QQuickItem* parent) : QQuickPaintedItem(parent)
, m_qwtPlot(nullptr) {

  setFlag(QQuickItem::ItemHasContents, true);
  setAcceptTouchEvents(true);
  setAcceptedMouseButtons(Qt::MouseButton::AllButtons);

  connect(this, &QQuickPaintedItem::widthChanged, this, &ChartQuick2::updatePlotSize);
  connect(this, &QQuickPaintedItem::heightChanged, this, &ChartQuick2::updatePlotSize);

  for (int i = 0; i < BUFFER_SIZE; i++) {
      m_xDataBuffer[i] = i/5.0;
      m_afLAeqBuffer[i] = 0;
      m_afLCeqBuffer[i] = 0;
      m_afLCpeakBuffer[i] = 0;
  }
}

/**
*******************************************************************************
** @fn  ChartQuick2::~ChartQuick2()
** @param[in]
** @param[out]
** @brief destructor of ChartQuick2 class: delets the pointer to the plot
**
** @return
**
*******************************************************************************
**/
ChartQuick2::~ChartQuick2() {
  delete m_qwtPlot;
  m_qwtPlot = nullptr;
}

/**
*******************************************************************************
** @fn  void ChartQuick2::Initialize()
** @param[in] void
** @param[oinitializes the data buffer at start and at reset clicked
**
** @return
**
*******************************************************************************
**/
void ChartQuick2::Initialize() {
  for (int i = 0; i < BUFFER_SIZE; i++) {
      m_xDataBuffer[i] = i/5.0;
      m_afLAeqBuffer[i] = 0;
      m_afLCeqBuffer[i] = 0;
      m_afLCpeakBuffer[i] = 0;
  }

  m_bAtStart=true;
  replotAndUpdate();
}

/**
*******************************************************************************
** @fn  void ChartQuick2::replotAndUpdate()
** @param[in] void
** @param[out] void
** @brief refresches the plot
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2::replotAndUpdate() {
  m_qwtPlot->updateAxes();
  m_qwtPlot->replot();
  update();
}

/**
*******************************************************************************
** @fn  void ChartQuick2::initQwtPlot()
** @param[in] void
** @param[out] void
** @brief initializes and sets different parameters of the plot:
**        - Style of the plot interface
**        - layout of different Axes and grid
**        - initializes the curves to be painted in the graph
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2::initQwtPlot() {
  m_qwtPlot = new QwtPlot();
  m_qwtPlot->setAttribute(Qt::WA_AcceptTouchEvents, true);
  m_qwtPlot->setAttribute(Qt::WA_StaticContents, true);

  // after replot() we need to call update() - so disable auto replot
  m_qwtPlot->setAutoReplot(false);
  m_qwtPlot->setStyleSheet("background-color:#353535; color:white; border-radius: 0px; font: 9pt; QwtPlot { padding: 0px }");
  m_qwtPlot->plotLayout()->setCanvasMargin(0);
  m_qwtPlot->plotLayout()->setAlignCanvasToScales(true);
  m_qwtPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  QFont font;
  font.setPixelSize(7);
  font.setFamily("Arial-BoldMT");
  font.setBold(true);
  font.setWeight(QFont::ExtraBold);
  font.setHintingPreference(QFont::HintingPreference::PreferNoHinting);
  m_qwtPlot->setFont(font);
  m_qwtPlot->setAxisFont(QwtPlot::xBottom, font);
  m_qwtPlot->setAxisFont(QwtPlot::yLeft, font);

  updatePlotSize();

  /*!< Set Grid */
  QwtPlotGrid *grid = new QwtPlotGrid();
  //enable the x and y axis lines
  grid->enableY(true);
  grid->enableXMin (true);

  QPen pen;
  pen.setStyle(Qt::SolidLine);
  pen.setColor(Qt::white);
  pen.setWidthF(0.5);
  grid->setPen(pen);
  grid->attach(m_qwtPlot);

  /*!< Set X axis */
  m_qwtPlot->setAxisMaxMinor(QwtPlot::xBottom, 1);
  m_qwtPlot->setAxisMaxMajor(QwtPlot::xBottom, 3);

  m_qwtPlot->setAxisAutoScale(QwtPlot::xBottom,true);
  timeScale=new TimeScaleDraw(QTime::currentTime().addSecs(-120));
  m_qwtPlot->setAxisScaleDraw(QwtPlot::xBottom,timeScale);
  timeScale->setLength(120);
  m_qwtPlot->setAxisScale(QwtPlot::xBottom, 0, timeScale->length(), 60.0);
  m_qwtPlot->axisScaleEngine( QwtPlot::xBottom )->setAttribute( QwtScaleEngine::Inverted, true);
  m_qwtPlot->axisScaleEngine( QwtPlot::xBottom )->setMargins(0,0);

  /*!< set Y axis */
  QwtScaleDraw* qwtScale= new QwtScaleDraw;
  qwtScale->setTickLength(QwtScaleDiv::MajorTick,0);
  qwtScale->setTickLength(QwtScaleDiv::MinorTick,0);
  m_qwtPlot->setAxisScaleDraw(QwtPlot::yLeft,qwtScale);
  m_qwtPlot->setAxisMaxMinor(QwtPlot::yLeft, 0);
  m_qwtPlot->setAxisScale(QwtPlot::yLeft, +20, +140);

  /*!< LAeq temporal Curve */
  m_curveLAeq = new QwtPlotCurve();
  m_curveLAeq->setRenderHint(QwtPlotItem::RenderAntialiased,true);
  m_curveLAeq->setPen(QPen(QColor("#fa3500"))); //red curve
  m_curveLAeq->setStyle(QwtPlotCurve::Lines);
  m_curveLAeq->setRenderHint(QwtPlotItem::RenderAntialiased);
  m_curveLAeq->attach(m_qwtPlot);
  /*!< LCeq temporal Curve */
  m_curveLCeq = new QwtPlotCurve();
  m_curveLCeq->setRenderHint(QwtPlotItem::RenderAntialiased,true);
  m_curveLCeq->setPen(QPen(QColor("#ff7a00" ))); //orange curve
  m_curveLCeq->setStyle(QwtPlotCurve::Lines);
  m_curveLCeq->setRenderHint(QwtPlotItem::RenderAntialiased);
  m_curveLCeq->attach(m_qwtPlot);
  /*!< LCpeak temporal Curve */
  m_curveLCpeak = new QwtPlotCurve();
  m_curveLCpeak->setRenderHint(QwtPlotItem::RenderAntialiased,true);
  m_curveLCpeak->setPen(QPen(QColor("#fffc00"))); //yellow curve
  m_curveLCpeak->setStyle(QwtPlotCurve::Lines);
  m_curveLCpeak->setRenderHint(QwtPlotItem::RenderAntialiased);
  m_curveLCpeak->attach(m_qwtPlot);

  /*!< set the line marker */
  m_qwtMarker= new QwtPlotMarker;
  m_qwtMarker->setLineStyle(QwtPlotMarker::VLine);
  m_qwtMarker->setLabelAlignment(Qt::AlignRight | Qt::AlignTop);
  m_qwtMarker->setLinePen(QColor(Qt::green),2,Qt::SolidLine);

  replotAndUpdate();
}

/**
*******************************************************************************
** @fn  void ChartQuick2::paint(QPainter* painter)
** @param[in] QPainter* painter
** @param[out] void
** @brief this function is called by the QML Scene Graph which paints the
**        Qwt widgets to screen.
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2::paint(QPainter* painter) {
    if (m_qwtPlot) {
      painter->save();
      QwtPlotRenderer renderer;
      renderer.setLayoutFlag(QwtPlotRenderer::FrameWithScales);
      QFont font;
      font.setPixelSize(7);
      font.setFamily("Arial-BoldMT");
      font.setBold(true);
      font.setWeight(QFont::ExtraBold);
      font.setHintingPreference(QFont::HintingPreference::PreferNoHinting);
      painter->setFont(font);
      renderer.render(m_qwtPlot, painter, m_qwtPlot->rect());

      painter->restore();
    }
}

/**
*******************************************************************************
** @fn  void ChartQuick2::updatePlotSize()
** @param[in] void
** @param[out] void
** @brief slot that updates the graphic size, called everytime the width or
**        height has been changed in the QQuickPaintedItem
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2::updatePlotSize() {
  if(m_qwtPlot) {
      m_qwtPlot->setGeometry(0, 0, static_cast<int>(width()), static_cast<int>(height()));
  }
}

/**
*******************************************************************************
** @fn  void ChartQuick2::populate(QVector<double> adAcousticRes)
** @param[in] QVector<double> adAcousticRes
** @param[out] void
** @brief adds points to the different curves and updates the bottom axis
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2::populate(QVector<double> adAcousticRes) {
  for (int i = 0; i < BUFFER_SIZE - 1; i++) {
      m_afLAeqBuffer[i] = m_afLAeqBuffer[i+1];
      m_afLCeqBuffer[i] = m_afLCeqBuffer[i+1];
      m_afLCpeakBuffer[i] = m_afLCpeakBuffer[i+1];
  }

  m_afLAeqBuffer[BUFFER_SIZE - 1] = qAbs(adAcousticRes.at(0));
  m_afLCeqBuffer[BUFFER_SIZE - 1] = qAbs(adAcousticRes.at(1));
  m_afLCpeakBuffer[BUFFER_SIZE - 1] = qAbs(adAcousticRes.at(2));

  m_curveLAeq->setRawSamples(m_xDataBuffer, m_afLAeqBuffer, BUFFER_SIZE);
  m_curveLCeq->setRawSamples(m_xDataBuffer, m_afLCeqBuffer, BUFFER_SIZE);
  m_curveLCpeak->setRawSamples(m_xDataBuffer, m_afLCpeakBuffer, BUFFER_SIZE);

  m_qwtPlot->setAxisScaleDraw(QwtPlot::xBottom,new TimeScaleDraw(QTime::currentTime().addSecs(-120)));

  if(m_bAtStart) {
      setMarker();
      m_bAtStart=false;
  }

  if(m_qwtMarker->value().x() >=0 &&  m_qwtMarker->value().x() <=120) {
      m_qwtMarker->setXValue(m_qwtMarker->value().x()-0.2);
      setLAeqValue(QString::number(m_afLAeqBuffer[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
      setLCeqValue(QString::number(m_afLCeqBuffer[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
      setLCpeakValue(QString::number(m_afLCpeakBuffer[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
      m_qwtMarker->attach(m_qwtPlot);

  } else if(m_qwtMarker->value().x()>120) {
      m_qwtMarker->setXValue(0);
      setTime((QTime::currentTime().addSecs(-120 + static_cast<int>(m_qwtMarker->value().x()))).toString("hh:mm:ss"));
      setLAeqValue(QString::number(m_afLAeqBuffer[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
      setLCeqValue(QString::number(m_afLCeqBuffer[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
      setLCpeakValue(QString::number(m_afLCpeakBuffer[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));

  } else if(m_qwtMarker->value().x()<0) {
      m_qwtMarker->setXValue(120);
      setTime((QTime::currentTime().addSecs(-120 + static_cast<int>(m_qwtMarker->value().x()))).toString("hh:mm:ss"));
      setLAeqValue(QString::number(m_afLAeqBuffer[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
      setLCeqValue(QString::number(m_afLCeqBuffer[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
      setLCpeakValue(QString::number(m_afLCpeakBuffer[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
  }
}

/**
*******************************************************************************
** @fn  void ChartQuick2::passDataFIT(QVector<double> adAcousticRes)
** @param[in] QVector<double> adAcousticRes
** @param[out] void
** @brief slot called every one second to append data from the TrAcousticRes
**        structure to the painted curves and updates the temporal graph
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2::passDataFIT(QVector<double> adAcousticRes) {
  populate(adAcousticRes);
  replotAndUpdate();
}

/**
*******************************************************************************
** @fn  void ChartQuick2::touchEvent(QTouchEvent *event)
** @param[in] QTouchEvent *event: the event corresponding to screen touching
**              on the bar chart
** @param[out] void
** @brief virtual function called everytime a touch has been sensed on the chart
**        and displays a marker at the touched point
**
** @return void
**
*******************************************************************************
**/
void ChartQuick2::touchEvent(QTouchEvent *event) {
  qDebug() << " *** touch event temporal chart *** " ;

  switch(event->type()) {
    case QEvent::TouchBegin:
    case QEvent::TouchUpdate:
    case QEvent::TouchEnd:{

      if(m_qwtPlot) {
          QList<QTouchEvent::TouchPoint> touchPoints = static_cast<QTouchEvent *>(event)->touchPoints();
          foreach (const QTouchEvent::TouchPoint &touchPoint, touchPoints) {

              switch (touchPoint.state()) {
                  case Qt::TouchPointStationary:
                  case Qt::TouchPointMoved:
                  case Qt::TouchPointPressed: continue;
                  case Qt::TouchPointReleased:{
                      const QTouchEvent::TouchPoint &touchPoint = touchPoints.first();
                      QPointF currentTouchPointPos=touchPoint.pos();

                      if (currentTouchPointPos.isNull()) {
                          qDebug() << "touch is NULL" ;
                          break;
                      }

                      double scalePosX=currentTouchPointPos.x()*120/m_uiWidth;
                      if(scalePosX>=0 && scalePosX < 120) {
                          m_qwtMarker->setXValue(scalePosX);

                      } else {
                          m_qwtMarker->setXValue(0);
                      }

                      double val=m_qwtMarker->value().x();
                      setTime((QTime::currentTime().addSecs(-120 + static_cast<int>(val))).toString("hh:mm:ss"));
                      setLAeqValue(QString::number(m_afLAeqBuffer[static_cast<int>(val*5)],'f',1));
                      setLCeqValue(QString::number(m_afLCeqBuffer[static_cast<int>(val*5)],'f',1));
                      setLCpeakValue(QString::number(m_afLCpeakBuffer[static_cast<int>(val*5)],'f',1));
                      m_qwtMarker->attach(m_qwtPlot);
                      replotAndUpdate();
                    }

                    break;
                  }
              }
          }
      break;
  }
  default: break;
  }
}
