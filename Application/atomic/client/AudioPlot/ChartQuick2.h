/**
*******************************************************************************
** @file    ChartQuick2.h
** @version
** @author  Wevioo SE Team
** @brief   This class provides the implementation of the temporal charts
**         presented in QML based upon QQuickPaintedItem and QWT.
**
*******************************************************************************
**/
#ifndef QMLPLOT_H
#define QMLPLOT_H

#include <QtQuick>
#include "qwt_plot.h"
#include "qwt_plot_curve.h"
#include "qwt_scale_draw.h"
#include <qwt_plot_marker.h>
#include "rep_atomic_replica.h"

#define BUFFER_SIZE 600                             /*!< 2*60*5=600 points/serie */
#define FIT_VALUE_ENV   200                             /*!< 200ms FIT */

/**
*******************************************************************************
** @class  class TimeScaleDraw : public QwtScaleDraw
** @brief  provide a customized implementation of the abscisse scale
**         based on the current time formatted in "hh:mm:ss" for the
**         temporal graph.
**
*******************************************************************************
**/
class TimeScaleDraw : public QwtScaleDraw
{
public:
  /**
  *******************************************************************************
  ** @fn  TimeScaleDraw(const QTime &base)
  ** @param[in] QTime &base : pass the current time
  ** @param[out] void
  ** @brief TimeScaleDraw's constructor
  **
  ** @return void
  **
  *******************************************************************************
  **/
  TimeScaleDraw(const QTime &base) :
    baseTime(base)
  {
    /*!< set's scale tick lengthes to zero */
    enableComponent(QwtAbstractScaleDraw::Ticks, false);
  }


  /**
  *******************************************************************************
  ** @fn  virtual QwtText label(double v) const
  ** @param[in] double v: holds the current index
  ** @param[out] QwtText
  ** @brief returns the v labels
  **
  ** @return QwtText: returns the label at the index v
  **
  *******************************************************************************
  **/
  virtual QwtText label(double v) const
  {
    QTime upTime = baseTime.addSecs(static_cast<int> (v));
    return upTime.toString("hh:mm:ss");
  }

private:
  QTime baseTime;   /*!< holds the time for the abscisse scale */
};

/*!<
 * The QQuickPaintedItem makes it possible to use the QPainter API
 * with the QML Scene Graph. It sets up a textured rectangle in the
 * Scene Graph and uses a QPainter to paint onto the texture. When
 * the render target is a QImage,QPainter first renders into the image
 * then the content is uploaded to the texture.
 * Call update() to trigger a repaint.
 * */

/**
*******************************************************************************
** @class  class ChartQuick2 : public QQuickPaintedItem
** @brief  sets the temporal graphs using the Qwt library
**          and uplodes it into QML
**
*******************************************************************************
**/
class ChartQuick2 : public QQuickPaintedItem
{
  Q_OBJECT
  Q_PROPERTY(int chartWidth READ chartWidth WRITE setChartWidth NOTIFY chartWidthChanged)

  /*!< Data to be set in the panel under the plot */
  Q_PROPERTY(QString time READ time WRITE setTime NOTIFY timeChanged)
  Q_PROPERTY(QString lAeqValue READ lAeqValue WRITE setLAeqValue NOTIFY lAeqValueChanged)
  Q_PROPERTY(QString lCeqValue READ lCeqValue WRITE setLCeqValue NOTIFY lCeqValueChanged)
  Q_PROPERTY(QString lCpeakValue READ lCpeakValue WRITE setLCpeakValue NOTIFY lCpeakValueChanged)


public:
  /**
  *******************************************************************************
  ** @fn  ChartQuick2::ChartQuick2(QQuickItem* parent) : QQuickPaintedItem(parent)
  ** @param[in] QQuickItem *parent: holds an instance of the QQuickItem which
  **            defines all the attributes that defines a visual item
  **            (position, height, width, ..)
  ** @param[out]
  ** @brief ChartQuick2's class constructor, derives from QQuickPaintedItem
  **        enabling the widget to be painted in QML.
  **
  ** @return
  **
  *******************************************************************************
  **/
  ChartQuick2(QQuickItem* parent = nullptr);

  /**
  *******************************************************************************
  ** @fn  ChartQuick2::~ChartQuick2()
  ** @param[in]
  ** @param[out]
  ** @brief destructor of ChartQuick2 class: deletes the pointer to the plot
  **
  ** @return
  **
  *******************************************************************************
  **/
  virtual ~ChartQuick2();

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2::paint(QPainter* painter)
  ** @param[in] QPainter* painter
  ** @param[out] void
  ** @brief this function is called by the QML Scene Graph which paints the
  **        Qwt widgets to screen.
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void paint(QPainter* painter);

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2::initQwtPlot()
  ** @param[in] void
  ** @param[out] void
  ** @brief initializes and sets different parameters of the plot:
  **        - Style of the plot interface
  **        - layout of different Axes and grid
  **        - initializes the curves to be painted in the graph
  **
  ** @return void
  **
  *******************************************************************************
  **/
  Q_INVOKABLE void initQwtPlot();

  /**
  *******************************************************************************
  ** @fn  void step(int iStep)
  ** @param[in] int iStep: could be 1: moves ahead
  **                                -1: moves back
  ** @param[out] void
  ** @brief moves the marker in the graph by one or multiple steps ahead or back,
  **        depending on which direction is clicked on.
  **
  ** @return void
  **
  *******************************************************************************
  **/
  Q_INVOKABLE void step(int iStep){
    qDebug()<<"Step :" << iStep << "new Value :" << m_qwtMarker->value().x()+iStep*1;

    double val=m_qwtMarker->value().x()+iStep*1;
    if( val >=0 &&  val <=120){
      m_qwtMarker->setXValue(m_qwtMarker->value().x()+iStep*1);
      setTime((QTime::currentTime().addSecs(-120 + static_cast<int>(val))).toString("hh:mm:ss"));
      setLAeqValue(QString::number(m_afLAeqBuffer[static_cast<int>(val*5)],'f',1));
      setLCeqValue(QString::number(m_afLCeqBuffer[static_cast<int>(val*5)],'f',1));
      setLCpeakValue(QString::number(m_afLCpeakBuffer[static_cast<int>(val*5)],'f',1));
      m_qwtMarker->attach(m_qwtPlot);
    }
    replotAndUpdate();
  }

public slots:
  /**
  *******************************************************************************
  ** @fn  void ChartQuick2::passDataFIT(QVector<double>)
  ** @param[in] QVector<double>
  ** @param[out] void
  ** @brief slot called every one second to append data from the TrAcousticRes
  **        structure to the painted curves and updates the temporal graph
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void passDataFIT(QVector<double>);

  /**
  *******************************************************************************
  ** @fn  int chartWidth()
  ** @param[in] void
  ** @param[out] int: width
  ** @brief gets the width of the device's screen
  **
  ** @return void
  **
  *******************************************************************************
  **/
  int chartWidth(){return m_uiWidth;}

  /**
  *******************************************************************************
  ** @fn  void setChartWidth(const int width)
  ** @param[in] int
  ** @param[out] void
  ** @brief sets the width attributes
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setChartWidth(const int width){
    if(width){m_uiWidth=width;
      emit chartWidthChanged();}}

  /**
  *******************************************************************************
  ** @fn  void setMarker()
  ** @param[in] void
  ** @param[out] void
  ** @brief every 200ms the Marker is updated and moved with the chart
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setMarker(){
        m_qwtMarker->setXValue(120);
        m_qwtMarker->attach(m_qwtPlot);
        setTime(QTime::currentTime().toString("hh:mm:ss"));
        setLAeqValue(QString::number(m_afLAeqBuffer[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
        setLCeqValue(QString::number(m_afLCeqBuffer[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
        setLCpeakValue(QString::number(m_afLCpeakBuffer[static_cast<int>(m_qwtMarker->value().x()*5)],'f',1));
  }

  /**
  *******************************************************************************
  ** @fn  void Initialize()
  ** @param[in] void
  ** @param[oinitializes the data buffer at start and at reset clicked
  **
  ** @return
  **
  *******************************************************************************
  **/
  void Initialize();
  /**
  *******************************************************************************
  ** @fn  QString time()
  ** @param[in] void
  ** @param[out] QString
  ** @brief returns the x coordinate of the marker
  ** @return the x coordinate of the marker
  **
  *******************************************************************************
  **/
  QString time(){return m_cTime;}

  /**
  *******************************************************************************
  ** @fn  void setTime(const QString cTime)
  ** @param[in] const QString  current x-coordinate of marker
  **             to pass to the QML part
  ** @param[out] void
  ** @brief sets the x-corrdinate of the marker
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setTime(const QString cTime){
    m_cTime=cTime;
        emit timeChanged();}

  /**
  *******************************************************************************
  ** @fn  QString lAeqValue()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the lAeq Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  QString lAeqValue(){return m_cLAeqValue;}

  /**
  *******************************************************************************
  ** @fn  void setLAeqValue(const QString cLAeq)
  ** @param[in] const QString cLAeq
  ** @param[out] void
  ** @brief sets the lAeq Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setLAeqValue(const QString cLAeq){
    m_cLAeqValue=cLAeq;
        emit lAeqValueChanged();}

  /**
  *******************************************************************************
  ** @fn  QString lCeqValue()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the lCeq Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  QString lCeqValue(){return m_cLCeqValue;}

  /**
  *******************************************************************************
  ** @fn  void setLCeqValue(const QString cLCeq)
  ** @param[in] const QString cLCeq
  ** @param[out] void
  ** @brief sets the lCeq Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setLCeqValue(const QString cLAeq){
    m_cLCeqValue=cLAeq;
        emit lCeqValueChanged();}

  /**
  *******************************************************************************
  ** @fn  QString lCpeakValue()
  ** @param[in] void
  ** @param[out] QString
  ** @brief gets the lCpeak Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  QString lCpeakValue(){return m_cLCpeakValue;}

  /**
  *******************************************************************************
  ** @fn  void setLCpeakValue(const QString cLCpeak)
  ** @param[in] const QString cLCpeak
  ** @param[out] void
  ** @brief sets the LCpeak Value
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void setLCpeakValue(const QString cLCpeak){
    m_cLCpeakValue=cLCpeak;
        emit lCpeakValueChanged();}

signals:
  void chartWidthChanged();               /*!< emitted when the screen's width has changed */
  void timeChanged();                     /*!< called every 200ms or upon moving the marker to update its x coordinate */
  void lAeqValueChanged();                /*!< called everytime the marker is moved to update the LAeq Value*/
  void lCeqValueChanged();                /*!< called everytime the marker is moved to update the LCeq Value*/
  void lCpeakValueChanged();              /*!< called everytime the marker is moved to update the LCpeak Value*/

protected:
  /**
  *******************************************************************************
  ** @fn  void ChartQuick2::populate(QVector<double> )
  ** @param[in] QVector<double>
  ** @param[out] void
  ** @brief adds points to the different curves and updates the bottom axis
  **
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void populate(QVector<double>);

protected:
  /**
  *******************************************************************************
  ** @fn  virtual void touchEvent(QTouchEvent *event)
  ** @param[in] QTouchEvent *event: the event corresponding to screen touching
  **              on the bar chart
  ** @param[out] void
  ** @brief virtual function called everytime a touch has been sensed on the chart
  **
  ** @return void
  **
  *******************************************************************************
  **/
  virtual void touchEvent(QTouchEvent *event);


private:
  QwtPlot*         m_qwtPlot;                         /*!< holds a reference to the two-dimentional graph */
  QwtPlotMarker*   m_qwtMarker;                       /*!< a marker that is displayed due to user's touch at the touched point */

  QwtPlotCurve*    m_curveLAeq;                       /*!< holds a reference to the LAeq-plot item that represents a series of points */
  double m_afLAeqBuffer[BUFFER_SIZE];                 /*!< stores the data of LAeq to be painted in the LAeqCurve, maximum size: BUFFER_SIZE*/

  QwtPlotCurve*    m_curveLCeq;                       /*!< holds a reference to the LCeq-plot item that represents a series of points */
  double m_afLCeqBuffer[BUFFER_SIZE];                 /*!< stores the data of LCeq to be painted in the LCeqCurve, maximum size: BUFFER_SIZE*/

  QwtPlotCurve*    m_curveLCpeak;                     /*!< holds a reference to the LCpeak-plot item that represents a series of points */
  double m_afLCpeakBuffer[BUFFER_SIZE];               /*!< stores the data of LCpeak to be painted in the LCpeakCurve, maximum size: BUFFER_SIZE*/

  double m_xDataBuffer[BUFFER_SIZE];                  /*!< stores the data to be painted, maximum size: BUFFER_SIZE*/
  TimeScaleDraw* timeScale;                           /*!< corresponds to the time scale of the temporal chart */

  int   m_uiWidth;                                    /*!< corresponds to the width of the device's screen */

  QString   m_cTime;                                  /*!< corresponds to the x-coordinate of the marker */
  QString   m_cLAeqValue;                             /*!< corresponds to the LAeq coordinate at the marker position */
  QString   m_cLCeqValue;                             /*!< corresponds to the LCeq coordinate at the marker position */
  QString   m_cLCpeakValue;                           /*!< corresponds to the LCpeak coordinate at the marker position */
  bool      m_bAtStart=true;                          /*!< set the marker position at launching the application */

  /**
  *******************************************************************************
  ** @fn  void ChartQuick2::replotAndUpdate()
  ** @param[in] void
  ** @param[out] void
  ** @brief refresches the plot
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void replotAndUpdate();

private slots:
  /**
  *******************************************************************************
  ** @fn  void ChartQuick2::updatePlotSize()
  ** @param[in] void
  ** @param[out] void
  ** @brief updates the graphic size
  **
  ** @return void
  **
  *******************************************************************************
  **/
  void updatePlotSize();
};

#endif // QMLPLOT_H
